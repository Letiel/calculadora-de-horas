<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();
		$data["horario_expediente"] = "00:00:00";
		$data["horario_final_dia"] = "";

		for ($i=1; $i <= 5 ; $i++) {
			// Horários Padrões
			$data["padrao"]["horario_entrada_$i"] = ($post["horario_entrada_$i"]!= "") ? $post["horario_entrada_$i"].":00" : "";
			$data["padrao"]["horario_saida_$i"] = ($post["horario_saida_$i"]!= "") ? $post["horario_saida_$i"].":00" : "";

			// if(strtotime($data["padrao"]["horario_saida_$i"]) < strtotime($data["padrao"]["horario_entrada_$i"])) {
			// 	$data["padrao"]["horario_saida_$i"] = $this->somarHoras($data["padrao"]["horario_saida_$i"], "24:00:00");
			// }
			// Horários Informados

			$data["horarios"]["entrada$i"] = ($post["entrada$i"]!= "") ? $post["entrada$i"].":00" : "";
			$data["horarios"]["saida$i"] = ($post["saida$i"]!= "") ? $post["saida$i"].":00" : "";

			// if(strtotime($data["horarios"]["saida$i"]) < strtotime($data["horarios"]["entrada$i"])) {
			// 	$data["horarios"]["saida$i"] = $this->somarHoras($data["horarios"]["saida$i"], "24:00:00");
			// }

			// ----------------- calculando horário final do dia
			if($data["padrao"]["horario_saida_$i"] != ":00" && $data["padrao"]["horario_saida_$i"] != ""){
				$data["horario_final_dia"] = $data["padrao"]["horario_saida_$i"];
			}

		}

		for ($i=1; $i <= 5 ; $i++) {

			// ----------------- calculando turnos
			if ($data["padrao"]["horario_saida_$i"] != "" && $data["padrao"]["horario_entrada_$i"] != "") {
				if (strtotime(date("H:i:s", strtotime($data["padrao"]["horario_saida_$i"]))) < strtotime(date("H:i:s", strtotime($data["padrao"]["horario_entrada_$i"]))))
					$saida = $this->somarHoras($data["padrao"]["horario_saida_$i"], "24:00:00");
				else
					$saida = $data["padrao"]["horario_saida_$i"];

				$data["turnos"]["turno_$i"] = $this->calcularIntervaloHoras2($saida, $data["padrao"]["horario_entrada_$i"]);
			}
			
			// ----------------- calculando intervalos
			$proximo = $i+1;

			if ($i >=1 && $i <=4) {
				if ($data["padrao"]["horario_saida_$i"] != "" && $data["padrao"]["horario_entrada_$proximo"] != "") {

					if (strtotime(date("H:i:s", strtotime($data["padrao"]["horario_entrada_$proximo"]))) < strtotime(date("H:i:s", strtotime($data["padrao"]["horario_saida_$i"]))))
						$proxima_entrada = $this->somarHoras($data["padrao"]["horario_entrada_$proximo"], "24:00:00");
					else
						$proxima_entrada = $data["padrao"]["horario_entrada_$proximo"];
					$data["intervalos"]["intervalo_$i"] =  $this->calcularIntervaloHoras2($proxima_entrada, $data["padrao"]["horario_saida_$i"]);
				}else{
					$data["intervalos"]["intervalo_$i"] = "00:00:00";
				}

			}

			// ----------------- calculando horário do expediente diário
			if (isset($data['turnos']["turno_$i"]))
				$data["horario_expediente"] = $this->somarHoras($data["horario_expediente"],$data['turnos']["turno_$i"]);
		}

		for ($i=1; $i <= 5 ; $i++) {
			if(strtotime(date("H:i:s", strtotime($data["horarios"]["entrada$i"]))) > strtotime(date("H:i:s", strtotime($data['padrao']["horario_entrada_$i"])))){}
		}






		echo "<pre>";
		// echo $this->calcularIntervaloHoras2($data["padrao"]["horario_entrada_1"], $data["horarios"]["entrada1"]);
		// echo "<br />";

		// if(strtotime($data["horarios"]["saida1"]) >= strtotime($data["padrao"]["horario_entrada_2"]) && $data["padrao"]["horario_entrada_2"] != ""){
		// 	if(isset($data["intervalos"]["intervalo_1"])){
		// 		echo $data["intervalos"]["intervalo_1"];
		// 	}else{
		// 		if(strtotime($data["horarios"]["saida1"]) > strtotime($data["padrao"]["horario_entrada_1"])){
		// 			echo $this->calcularIntervaloHoras2($data["padrao"]["horario_saida_1"], $data["horarios"]["saida1"]);
		// 		}else{
		// 			echo $this->calcularIntervaloHoras2($data["horarios"]["saida1"], $data["padrao"]["horario_saida_1"]);
		// 		}
		// 	}
		// }else{
		// 	if(strtotime($data["horarios"]["saida1"]) <= strtotime($data["horarios"]["entrada1"])){
		// 		if($data["padrao"]["horario_entrada_5"] != "" && $data["padrao"]["horario_saida_5"] != "" && $data["padrao"]["horario_saida_5"] != ""){
		// 			$ate_00 = $this->somarHoras($this->calcularIntervaloHoras2("23:59:59", $data["padrao"]["horario_saida_5"]), "00:00:01");
		// 			$ate_00 = $this->somarHoras($ate_00, $data["intervalos"]["intervalo_4"]);
		// 			echo "<p>5</p>";
		// 		}else if($data["padrao"]["horario_entrada_4"] != "" && $data["padrao"]["horario_saida_4"] != "" && $data["padrao"]["horario_saida_4"] != ""){
		// 			$ate_00 = $this->somarHoras($this->calcularIntervaloHoras2("23:59:59", $data["padrao"]["horario_saida_4"]), "00:00:01");
		// 			$ate_00 = $this->somarHoras($ate_00, $data["intervalos"]["intervalo_3"]);
		// 			echo "<p>4</p>";
		// 		}else if($data["padrao"]["horario_entrada_3"] != "" && $data["padrao"]["horario_saida_3"] != "" && $data["padrao"]["horario_saida_3"] != ""){
		// 			$ate_00 = $this->somarHoras($this->calcularIntervaloHoras2("23:59:59", $data["padrao"]["horario_saida_3"]), "00:00:01");
		// 			$ate_00 = $this->somarHoras($ate_00, $data["intervalos"]["intervalo_2"]);
		// 			echo "<p>3</p>";
		// 		}else if($data["padrao"]["horario_entrada_2"] != "" && $data["padrao"]["horario_saida_2"] != "" && $data["padrao"]["horario_saida_2"] != ""){
		// 			$ate_00 = $this->somarHoras($this->calcularIntervaloHoras2("23:59:59", $data["padrao"]["horario_saida_2"]), "00:00:01");
		// 			$ate_00 = $this->somarHoras($ate_00, $data["intervalos"]["intervalo_1"]);
		// 			echo "<p>2</p>";
		// 		}else{
		// 			$ate_00 = $this->somarHoras($this->calcularIntervaloHoras2("23:59:59", $data["padrao"]["horario_saida_1"]), "00:00:01");
		// 		}
		// 		echo "<b>".$this->somarHoras($data["horarios"]["saida1"], $ate_00)."</b>1<br />";
		// 		// echo $this->calcularIntervaloHoras($post["dia_calculo"]." ".$data["horarios"]["saida1"], date("Y-m-d H:i:s", strtotime($post["dia_calculo"]." ".$data["padrao"]["horario_saida_1"])));
		// 	}
		// 	else{
		// 		echo $this->calcularIntervaloHoras2($data["horarios"]["saida1"], $data["padrao"]["horario_saida_1"]);
		// 	}
		// }

		// echo "<hr />";//horários 2

		// if(strtotime($data["horarios"]["entrada2"]) <= strtotime($data["padrao"]["horario_entrada_2"])){
		// 	if(strtotime($data["horarios"]["entrada2"]) <= strtotime($data["padrao"]["horario_saida_1"])){
		// 		echo $data["intervalos"]["intervalo_1"]."<br />";
		// 		if(strtotime($data["horarios"]["entrada2"]) < strtotime($data["padrao"]["horario_entrada_1"])){
		// 			echo $this->calcularIntervaloHoras2($data["padrao"]["horario_entrada_1"], $data["horarios"]["entrada2"])."<br />";
		// 		}
		// 	}else{
		// 		echo $this->calcularIntervaloHoras2($data["padrao"]["horario_entrada_2"], $data["horarios"]["entrada2"]);
		// 	}
		// }

		// if(strtotime($data["horarios"]["saida2"]) > strtotime($data["padrao"]["horario_saida_2"])){

		// }
		
		echo "<br />";
		$this->calcularExtras($data, 1);
		print_r($data);
		echo "</pre>";
	}

	function calcularExtras($data, $indice){
		for ($i=1; $i <= 5; $i++) {
			for ($j=1; $j <= 5; $j++) { 
				if($i == $j || $i > $j){ //Pra não pegar os intervalos anteriores... Creio que ta certo...
					continue;
				}
				$anterior = $j-1;
				$a = (($j - 1) == 0) ? 5 : $j - 1;
				if($data["padrao"]["horario_entrada_$i"] != "" && $data["padrao"]["horario_saida_$i"] != "" &&
					$data["horarios"]["entrada$i"] != "" && $data["horarios"]["saida$i"] != ""){
					echo "<p><b>i = $i <> j = $j</b></p>";
					if(strtotime($data["horarios"]["saida$i"]) > strtotime($data["horarios"]["entrada$i"])){
						if(strtotime($data["horarios"]["saida$i"]) >= strtotime($data["padrao"]["horario_entrada_$j"]) && isset($data["intervalos"]["intervalo_".$anterior])) {
							echo $data["intervalos"]["intervalo_".$anterior];
						}else{
							if(strtotime($data["horarios"]["saida$i"]) > strtotime($data["padrao"]["horario_saida_$a"])) {
								//if(strtotime($data["horarios"]["saida$i"]) < strtotime($data["padrao"]["horario_entrada_$j"])) {
									echo $this->calcularIntervaloHoras2($data["horarios"]["saida$i"], $data["padrao"]["horario_saida_$a"]);
								// }else{
								// 	echo $data["intervalos"]["intervalo_".$a]." |";
								// }
							}
						}	
					}else{
						if(strtotime($data["horarios"]["entrada$i"]) <= strtotime($data["padrao"]["horario_entrada_$a"])){
							echo $data["intervalos"]["intervalo_".$anterior];
						}
						if ($j == 5) {
							echo "<p>Até meia noite: ".$this->calcularIntervaloHoras2("24:00:00", $data["horario_final_dia"])."</p>";
							if(strtotime($data["horarios"]["saida$i"]) <= strtotime($data["padrao"]["horario_entrada_1"])){
								echo $data["horarios"]["saida$i"];
							}else{
								$data["padrao"]["horario_entrada_1"];
							}
						}
					}
				}


				echo "<br />";
			}
		}
	}


	function somarHoras($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador2 + $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}


	function calcularIntervaloHoras2($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador1 - $acumulador2;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function calcularIntervaloHoras($entrada, $saida){//
		
		$date_time  = new DateTime($entrada);
		$diff       = $date_time->diff(new DateTime($saida));
		$dias = $diff->d;
		$horas = ($dias*24) + $diff->h;
		return ($diff->invert == 1 ? "-" : "").str_pad($horas, 2, '0', STR_PAD_LEFT).":".str_pad($diff->i, 2, '0', STR_PAD_LEFT).":".str_pad($diff->s, 2, '0', STR_PAD_LEFT);
	}
}