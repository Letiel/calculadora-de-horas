<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculadora extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("UsuariosModel", "usuarios");
		$this->load->model("FuncionariosModel", "funcionarios");
		$this->load->model("CalculadoraModel", "calculadora");
	}
	public function index(){
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(2);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/calculadora";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 2;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->funcionarios->getFuncionarios()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$funcionarios = $this->funcionarios->getFuncionarios($inicio, $maximo)->result();
		$this->load->view("calculadora-selecionar", array("funcionarios"=>$funcionarios, "paginacao"=>$this->pagination->create_links()));
	}

	public function calcular(){
		if($this->session->userdata("logado")){
			$funcionario = $this->calculadora->getFuncionario((int)$this->uri->segment(3));
			if($funcionario->num_rows() > 0){
				$this->session->set_flashdata("retorno", "toastr.info('Tecle <b>TAB</b> para alternar os campos.', 'Dica!');");
				$funcionario = $funcionario->first_row();
				$feriados = $this->calculadora->getFeriados($funcionario->id_empresa)->result();
				$extras = $this->calculadora->getFaixasExtras($funcionario->id)->result();
				$dia["domingo"] = $this->calculadora->getHorariosDia($funcionario->id, "domingo")->first_row();
				$dia["segunda"] = $this->calculadora->getHorariosDia($funcionario->id, "segunda")->first_row();
				$dia["terca"] = $this->calculadora->getHorariosDia($funcionario->id, "terca")->first_row();
				$dia["quarta"] = $this->calculadora->getHorariosDia($funcionario->id, "quarta")->first_row();
				$dia["quinta"] = $this->calculadora->getHorariosDia($funcionario->id, "quinta")->first_row();
				$dia["sexta"] = $this->calculadora->getHorariosDia($funcionario->id, "sexta")->first_row();
				$dia["sabado"] = $this->calculadora->getHorariosDia($funcionario->id, "sabado")->first_row();
				$this->load->view("calculadora", array("funcionario"=>$funcionario, "feriados"=>$feriados, "extras"=>$extras, "dia"=>$dia));
				// $this->load->view("calculo");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Funcionário não encontrado', 'Ops');");
				redirect("/calculadora");
			}
		}else{
			redirect("/");
		}
	}

	public function efetuar_calculo(){
		if(!$this->session->userdata("logado")){
			exit();
		}
		

		//------------------ VALIDAÇÕES ------------------------------
		// $this->form_validation->set_rules("horario_entrada_1", "Horário padrão de entrada 1", "required");
		// $this->form_validation->set_rules("horario_saida_1", "Horário padrão de saída 1", "required");

		$this->form_validation->set_rules("entrada1", "Horário de entrada 1", "");
		$this->form_validation->set_rules("saida1", "Horário de saída 1", "");
		$this->form_validation->set_rules("dia_calculo", "Dia do cálculo", "required");

		// MAIS ALGUMAS VALIDAÇÕES

		// for ($i=1; $i <= 5; $i++) { 
			// if($data["padrao"]["entrada"]["_$i"]->format("Y") != "-0001")
			// 	$this->form_validation->set_rules("horario_saida_$i", "Horário padrão de saída $i", "required");
			// if($data["padrao"]["saida"]["_$i"]->format("Y") != "-0001")
			// 	$this->form_validation->set_rules("horario_entrada_$i", "Horário padrão de entrada $i", "required");

			// if($data["horario"]["entrada"]["_$i"]->format("Y") != "-0001"){
			// 	$this->form_validation->set_rules("saida$i", "Horário de saída $i", "required");
			// }
			// if($data["horario"]["saida"]["_$i"]->format("Y") != "-0001"){
			// 	$this->form_validation->set_rules("entrada$i", "Horário de entrada $i", "required");
			// }

			// if($data["horario"]["saida"]["_$i"] > $data["ultimo_horario"]){
			// 	$data["erro"] = "1";
			// 	$data["mensagem"] = "Horário de saída $i deve ser menor do que ".$data["ultimo_horario"]->format("H:i:s");
			// 	echo json_encode($data);
			// 	exit();
			// }

			// if($i < 5){
			// 	if($data["horario"]["entrada"]["_".($i+1)]->format("Y") != "-0001"){
			// 		if($data["horario"]["saida"]["_$i"]->format("His") > $data["horario"]["entrada"]["_".($i+1)]->format("His")){
			// 			$data["erro"] = "1";
			// 			$data["mensagem"] = "Horário de entrada ".($i+1)." não é válido.";
			// 			echo json_encode($data);
			// 			exit();
			// 		}
			// 	}
			// }
		// }

		// if(!$this->form_validation->run()){
		// 	$data["erro"] = "1";
		// 	$data["mensagem"] = validation_errors();
		// 	echo json_encode($data);
		// 	exit();
		// }

		//------------------ FIM VALIDAÇÕES ------------------------------
		$this->calculadora->calcularHorasFuncionario();

	}

	function calcular_periodo(){
		$this->form_validation->set_rules("data_inicio", "Data de Início", "required");
		$this->form_validation->set_rules("data_fim", "Data de Fim", "required");
		$this->form_validation->set_rules("id_funcionario", "Id do Funcionário", "required|is_numeric");

		if($this->form_validation->run()){
			$this->calculadora->calcular_periodo();
		}
	}

	function getAfastamentos(){
		if($this->session->userdata("logado")){
			$this->calculadora->getAfastamentos();
		}
	}

	function getPreenchidos(){
		$this->form_validation->set_rules("id_funcionario", "Funcionário", "required|is_numeric");
		$this->form_validation->set_rules("data_inicio", "Data de início", "required");
		$this->form_validation->set_rules("data_fim", "Data de Fim", "required");

		if($this->form_validation->run()){
			$this->calculadora->getPreenchidos();
		}
	}
}