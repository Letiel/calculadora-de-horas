<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$data = array();
		// $data['excedido']['entrada']["total"] = "00:00:00";
		// $data['excedido']['saida']["total"] = "00:00:00";

		$data['calculo']['entrada']['extra']['total'] = "00:00:00";
		$data['calculo']['entrada']['atraso']['total'] = "00:00:00";
		$data['calculo']['entrada']['extra']['tolerado'] = "00:00:00";
		$data['calculo']['entrada']['atraso']['tolerado'] = "00:00:00";
		$data['calculo']['saida']['extra']['total'] = "00:00:00";
		$data['calculo']['saida']['atraso']['total'] = "00:00:00";
		$data['calculo']['saida']['extra']['tolerado'] = "00:00:00";
		$data['calculo']['saida']['atraso']['tolerado'] = "00:00:00";
		$data['calculo']['soma']['total'] = "00:00:00";
		$data['horas_diarias'] = "00:00:00";
		$data['excedido']['entrada']['extra']['soma'] = "00:00:00";
		$data['excedido']['saida']['extra']['soma'] = "00:00:00";
		$data['excedido']['entrada']['atraso']['soma'] = "00:00:00";
		$data['excedido']['saida']['atraso']['soma'] = "00:00:00";
		$data['excedido']['entrada']['extra']['soma_real'] = "00:00:00";
		$data['excedido']['saida']['extra']['soma_real'] = "00:00:00";
		$data['excedido']['entrada']['atraso']['soma_real'] = "00:00:00";
		$data['excedido']['saida']['atraso']['soma_real'] = "00:00:00";


		$post = $this->input->post();
		$post['dia_calculo'] = date("Y-d-m", strtotime($post['dia_calculo']));
		$utilizar_clt   = $post['utilizar_clt'] == "false" ? false : true;
		$post['tol_diaria'] = $utilizar_clt ? "00:10:00" : $post['tol_diaria'].":00";
		$compensacao_diaria   = $post['compensacao_diaria'] == "false" ? false : true;

		for ($i=1; $i <= 5; $i++) {
			$data['calculo']["soma"]["_$i"] = "00:00:00";
			$post['tolerancia_entrada_'.$i.'_antes'] = $utilizar_clt ? "00:05" : $post['tolerancia_entrada_'.$i.'_antes'];
			$post["outrodia$i"] = $post["outrodia$i"] == "false" ? false : true;
			$data['entrada']["_$i"] = $post['dia_calculo']." ".$post["entrada$i"].":00";
			$data['saida']["_$i"] = $post['dia_calculo']." ".$post["saida$i"].":00";
			$data['saida']["_$i"] = !$post["outrodia$i"] ? $data['saida']["_$i"] : date("Y-m-d H:i:s", strtotime("+1 days", strtotime($data['saida']["_$i"])));
			//até aqui só pegando valores de POST e declarando valores iniciais
		}
		for ($i=1; $i <= 5; $i++) {
			//---------------CALCULANDO A CARGA DIÁRIA----------------------
			$intervalo = $this->calcularIntervaloHoras($post['horario_entrada_'.$i], $post['horario_saida_'.$i]);
			$data['horas_diarias'] = $this->somarHoras($intervalo, $data['horas_diarias']);
			if($post["entrada$i"] != "" && $post["saida$i"] != ""){
				//---------------CALCULANDO TOLERÂNCIAS DE CADA CAMPO---------------------
				if($post["entrada$i"] != "")
					$data['calculo']['entrada']["_$i"] = $this->calcularTolerancia($post["entrada$i"], $post["horario_entrada_$i"].":00", $post["tolerancia_entrada_".$i."_antes"].":00", $post["tolerancia_entrada_".$i."_depois"].":00", "entrada");
				if($post["saida$i"] != ""){
					$post['saida$i'] = !$post["outrodia$i"] ? date("H:i:s", strtotime($data['saida']["_$i"])) : $this->somarHoras(date("H:i:s", strtotime($post["saida$i"])), "24:00:00");
					$data['calculo']['saida']["_$i"] = $this->calcularTolerancia($post["saida$i"], $post["horario_saida_$i"].":00", $post["tolerancia_saida_".$i."_antes"].":00", $post["tolerancia_saida_".$i."_depois"].":00", "saida");
				}
				//---------------CALCULANDO SOMA DE EXTRAS E ATRASOS----------------------
				$data['calculo']['entrada']['extra']['total'] = $this->somarHoras($data['calculo']['entrada']['extra']['total'], $data['calculo']['entrada']["_$i"]["extra_real"]);
				$data['calculo']['entrada']['atraso']['total'] = $this->somarHoras($data['calculo']['entrada']['atraso']['total'], $data['calculo']['entrada']["_$i"]["atraso_real"]);

				$data['calculo']['entrada']['extra']['tolerado'] = $this->somarHoras($data['calculo']['entrada']['extra']['tolerado'], $data['calculo']['entrada']["_$i"]["extra"]);
				$data['calculo']['entrada']['atraso']['tolerado'] = $this->somarHoras($data['calculo']['entrada']['atraso']['tolerado'], $data['calculo']['entrada']["_$i"]["atraso"]);

				$data['calculo']['saida']['extra']['total'] = $this->somarHoras($data['calculo']['saida']['extra']['total'], $data['calculo']['saida']["_$i"]["extra_real"]);
				$data['calculo']['saida']['atraso']['total'] = $this->somarHoras($data['calculo']['saida']['atraso']['total'], $data['calculo']['saida']["_$i"]["atraso_real"]);

				$data['calculo']['saida']['extra']['tolerado'] = $this->somarHoras($data['calculo']['saida']['extra']['tolerado'], $data['calculo']['saida']["_$i"]["extra"]);
				$data['calculo']['saida']['atraso']['tolerado'] = $this->somarHoras($data['calculo']['saida']['atraso']['tolerado'], $data['calculo']['saida']["_$i"]["atraso"]);
				//---------------CALCULANDO SOMA DE HORAS TRABALHADAS----------------------
				$data['calculo']["soma"]["_$i"] = $this->calcularIntervaloHoras($data['entrada']["_$i"], $data['saida']["_$i"], $post["outrodia$i"]);
				$data['calculo']['soma']['total'] = $this->somarHoras($data['calculo']['soma']['total'], $data['calculo']['soma']["_$i"]);

				//---------------CALCULANDO INTERVALOS TOLERADOS----------------------
				if($post["entrada$i"] != "")
					$data['excedido']['entrada']["_$i"] = $this->calcularTolerancia($post["entrada$i"], $post["horario_entrada_$i"].":00", $post["tolerancia_entrada_".$i."_antes"].":00", $post["tolerancia_entrada_".$i."_depois"].":00", "entrada");
				if($post["saida$i"] != "")
					$data['excedido']['saida']["_$i"] = $this->calcularTolerancia($post["saida$i"], $post["horario_saida_$i"].":00", $post["tolerancia_saida_".$i."_antes"].":00", $post["tolerancia_saida_".$i."_depois"].":00", "saida");

				if(isset($data['excedido']['entrada']["_$i"]['extra'])){
					$data['excedido']['entrada']['extra']['soma'] = $this->somarHoras($data['excedido']['entrada']['extra']['soma'], $data['excedido']['entrada']["_$i"]["extra"]);
				}

				if(isset($data['excedido']['saida']["_$i"]['extra'])){
					$data['excedido']['saida']['extra']['soma'] = $this->somarHoras($data['excedido']['saida']['extra']['soma'], $data['excedido']['saida']["_$i"]["extra"]);
				}

				if(isset($data['excedido']['entrada']["_$i"]['atraso'])){
					$data['excedido']['entrada']['atraso']['soma'] = $this->somarHoras($data['excedido']['entrada']['atraso']['soma'], $data['excedido']['entrada']["_$i"]["atraso"]);
				}

				if(isset($data['excedido']['saida']["_$i"]['atraso'])){
					$data['excedido']['saida']['atraso']['soma'] = $this->somarHoras($data['excedido']['saida']['atraso']['soma'], $data['excedido']['saida']["_$i"]["atraso"]);
				}
				//---
				if(isset($data['excedido']['entrada']["_$i"]['extra_real'])){
					$data['excedido']['entrada']['extra']['soma_real'] = $this->somarHoras($data['excedido']['entrada']['extra']['soma_real'], $data['excedido']['entrada']["_$i"]["extra_real"]);
				}

				if(isset($data['excedido']['saida']["_$i"]['extra_real'])){
					$data['excedido']['saida']['extra']['soma_real'] = $this->somarHoras($data['excedido']['saida']['extra']['soma_real'], $data['excedido']['saida']["_$i"]["extra_real"]);
				}

				if(isset($data['excedido']['entrada']["_$i"]['atraso_real'])){
					$data['excedido']['entrada']['atraso']['soma_real'] = $this->somarHoras($data['excedido']['entrada']['atraso']['soma_real'], $data['excedido']['entrada']["_$i"]["atraso_real"]);
				}

				if(isset($data['excedido']['saida']["_$i"]['atraso_real'])){
					$data['excedido']['saida']['atraso']['soma_real'] = $this->somarHoras($data['excedido']['saida']['atraso']['soma_real'], $data['excedido']['saida']["_$i"]["atraso_real"]);
				}
			}

			$data['excedido']['atraso']['soma_real'] = $this->somarHoras($data['excedido']['entrada']['atraso']['soma_real'], $data['excedido']['saida']['atraso']['soma_real']);

			$data['excedido']['extra']['soma_real'] = $this->somarHoras($data['excedido']['entrada']['extra']['soma_real'], $data['excedido']['saida']['extra']['soma_real']);

			//-------------CALCULANDO TOLERÂNCIA DIÁRIA--------------------------------------
			if(strtotime($data['excedido']['extra']['soma_real']) > strtotime($post['tol_diaria'])){
				$data['excedido']['extra']['passou_tol_diaria'] = "1";
			}else{
				$data['excedido']['extra']['passou_tol_diaria'] = "0";
			}

			if(strtotime($data['excedido']['atraso']['soma_real']) > strtotime($post['tol_diaria'])){
				$data['excedido']['atraso']['passou_tol_diaria'] = "1";
			}else{
				$data['excedido']['atraso']['passou_tol_diaria'] = "0";
			}

			$data['excedido']['extra']['soma'] = $this->somarHoras($data['excedido']['saida']['extra']['soma'], $data['excedido']['entrada']['extra']['soma']);

			$data['excedido']['atraso']['soma'] = $this->somarHoras($data['excedido']['saida']['atraso']['soma'], $data['excedido']['entrada']['atraso']['soma']);

			$data['calculo']['extra']['soma'] = $this->somarHoras($data['calculo']['entrada']['extra']['total'], $data['calculo']['saida']['extra']['total']);
			$data['calculo']['atraso']['soma'] = $this->somarHoras($data['calculo']['entrada']['atraso']['total'], $data['calculo']['saida']['atraso']['total']);

		}

		if($data['excedido']['atraso']['passou_tol_diaria'])
			$data['excedido']['atraso_total'] = $this->calcularIntervaloHoras($data['calculo']['soma']['total'], $data['horas_diarias']);
		else
			$data['excedido']['atraso_total'] = $this->calcularIntervaloHoras(date("H:i:s", strtotime($data['calculo']['soma']['total'])), $data['horas_diarias']);

		if($data['excedido']['extra']['passou_tol_diaria'])
			$data['excedido']['extra_total'] = $this->calcularIntervaloHoras($data['horas_diarias'], $data['calculo']['soma']['total']);
		else
			$data['excedido']['extra_total'] = $this->calcularIntervaloHoras($data['horas_diarias'], date("H:i:s", strtotime($data['calculo']['soma']['total'])));


		// if(strtotime($data['calculo']['soma']['total']) - strtotime($data['horas_diarias']) > strtotime($post['tol_diaria'])){
		// 	$data['passou_tol_diaria'] = "1";
		// }else{
		// 	$data['passou_tol_diaria'] = "0";
		// }

		// echo date("H:i:s", strtotime($data['calculo']['soma']['total']) - strtotime($data['horas_diarias']));
		// echo " < ";
		// echo date("H:i:s", strtotime($post['tol_diaria']));
		// // echo date("H:i:s");
		// exit();

		if(substr($data['excedido']['atraso_total'], 0, 1) == "-")
			$data['excedido']['atraso_total'] = "00:00:00";
		if(substr($data['excedido']['extra_total'], 0, 1) == "-")
			$data['excedido']['extra_total'] = "00:00:00";

		// $data['excedido']['calculo_real'] = $this->calcularTolerancia($data['calculo']['soma']['total'], $data['horas_diarias'], $post['tol_diaria'], $post['tol_diaria'], "saida");
		//imprimindo o resultado em formato JSON
		echo json_encode($data);
	}

	public function calcularCompensacao($tolerancias, $soma_total){
		if(strtotime($soma_total) >= $tolerancias['abaixo'] && strtotime($soma_total) <= $tolerancias['acima'])
			return "1";
		else
			return "0";
	}

	public function somarHoras($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador2 + $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function forcarPositivo($data){
		$hora1 = explode(":",$data);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];

		$resultado = -$acumulador1;
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		return str_pad(abs($hora_ponto), 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
	}

	function forcarNegativo($data){
		$hora1 = explode(":",$data);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];

		$resultado = $acumulador1;
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		return str_pad("-".$hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
	}

	function calcularToleranciaCompensacao($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima){
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);
		
		return $horario_tolerado;
	}

	function calcularTolerancia($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima, $tipo){
		$nome = "";
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);

		$array = array(
			"status"=>"",
			"extra_real"=>"00:00:00",
			"atraso_real"=>"00:00:00",
			"atraso"=>"00:00:00",
			"extra"=>"00:00:00",
		);
		if($entrada > strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "atraso";
			else
				$nome = "extra";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", strtotime($horario_padrao)), date("H:i:s", $entrada));
		}

		if($entrada < strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "extra";
			else
				$nome = "atraso";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", strtotime($horario_padrao)));
		}
		if($entrada > $horario_tolerado['acima']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $horario_tolerado['acima']), date("H:i:s", $entrada));
		}

		if($entrada < $horario_tolerado['abaixo']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", $horario_tolerado['abaixo']));
		}
		$array['status'] = $nome;
		return $array;
	}

	function calcularHorasToleradas($entrada, $saida, $outro_dia = false, $somar = false){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		if($somar){
			$resultado = $acumulador2 + $acumulador1;
		}else{
			$resultado = $acumulador2 - $acumulador1;
		}
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function calcularIntervaloHoras($entrada, $saida, $outro_dia = false, $somar = false){//
		
		$date_time  = new DateTime($entrada);
		$diff       = $date_time->diff(new DateTime($saida));
		// print_r($diff);
		// echo $diff->format('%y ano(s), %m mês(s), %d dia(s), %H hora(s), %i minuto(s) e %s segundo(s)')."<br />";
		$dias = $diff->d;
		$horas = ($dias*24) + $diff->h;
		return ($diff->invert == 1 ? "-" : "").str_pad($horas, 2, '0', STR_PAD_LEFT).":".str_pad($diff->i, 2, '0', STR_PAD_LEFT).":".str_pad($diff->s, 2, '0', STR_PAD_LEFT);
	}
}
