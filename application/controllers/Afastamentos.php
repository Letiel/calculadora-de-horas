<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Afastamentos extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("AfastamentosModel", "afastamentos");
			$this->load->model("FuncionariosModel", "funcionarios");
			if(!$this->session->userdata("logado")){
				redirect("/");
			}
		}

		function index(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/afastamentos";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->afastamentos->getTiposAfastamentos()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$afastamentos = $this->afastamentos->getTiposAfastamentos($inicio, $maximo)->result();
			$this->load->view("afastamentos", array("afastamentos"=>$afastamentos, "paginacao"=>$this->pagination->create_links(), "link"=>"/afastamentos/selecionar-funcionario/"));
		}

		function cadastrar_tipo(){
			$this->form_validation->set_rules("descricao", "Nome", "required");
			$this->form_validation->set_rules("id_empresa", "Empresa", "required|is_numeric");

			if($this->form_validation->run()){
				$this->afastamentos->cadastrar_tipo();
			}

			$this->load->view("afastamentos-cadastrar-tipo");
		}

		function selecionar_funcionario(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(4);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/afastamentos/selecionar-funcionario/".$this->uri->segment(3);
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 4;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->funcionarios->getFuncionarios()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$funcionarios = $this->funcionarios->getFuncionarios($inicio, $maximo)->result();
			$this->load->view("afastamentos-selecionar-funcionario", array("funcionarios"=>$funcionarios, "paginacao"=>$this->pagination->create_links(), "afastamento"=>$this->uri->segment(3)));
		}

		function atribuir(){
			$afastamento = $this->uri->segment(3);
			$funcionario = $this->uri->segment(4);
			if(empty($afastamento)){
				redirect("/afastamentos");
				exit();
			}
			$this->form_validation->set_rules("id_funcionario", "Funcionário", "required|is_numeric");
			$this->form_validation->set_rules("id_tipo", "Afastamento", "required|is_numeric");
			$this->form_validation->set_rules("data_inicio", "Data Inicial", "required");
			$this->form_validation->set_rules("data_fim", "Data Final", "required");
			$this->form_validation->set_error_delimiters("", "");
			$post = $this->input->post();
			if(!empty($post["hora_fim"]))
				$this->form_validation->set_rules("hora_inicio", "Hora Inicial", "required");
			if(!empty($post["hora_inicio"]))
				$this->form_validation->set_rules("hora_fim", "Hora Final", "required");

			if($this->form_validation->run()){
				$this->afastamentos->atribuir();
			}

			$this->load->view("afastamentos-atribuir", array("afastamento"=>$afastamento, "funcionario"=>$funcionario));
		}

		function excluir(){
			$this->form_validation->set_rules("id", "ID", "required");
			if($this->form_validation->run()){
				$this->afastamentos->excluir();
			}else{
				echo validation_errors();
			}
		}

		function editable(){
			$this->form_validation->set_rules("pk", "ID", "required|is_numeric");
			$this->form_validation->set_rules("name", "Name", "required");
			$this->form_validation->set_rules("value", "Value", "required");

			if($this->form_validation->run()){
				echo json_encode($this->afastamentos->editable());
			}else{
				echo json_encode(array('success'=>false, 'msg'=>validation_errors()));
			}
		}

		function editar(){
			$id_afastamento = (int) $this->uri->segment(3);
			if(empty($id_afastamento)){
				redirect("/funcionarios");
				exit();
			}
			$this->form_validation->set_rules("data_inicio", "Data Inicial", "required");
			$this->form_validation->set_rules("data_fim", "Data Final", "required");
			$this->form_validation->set_error_delimiters("", "");


			$post = $this->input->post();
			if(!empty($post["hora_fim"]))
				$this->form_validation->set_rules("hora_inicio", "Hora Inicial", "required");
			if(!empty($post["hora_inicio"]))
				$this->form_validation->set_rules("hora_fim", "Hora Final", "required");

			if($this->form_validation->run()){
				$this->afastamentos->editar($id_afastamento);
			}
			$afastamento = $this->afastamentos->getAfastamento($id_afastamento)->first_row();
			$this->load->view("funcionario-afastamento-editar", array("afastamento"=>$afastamento));
		}

		function excluir_afastamento_funcionario(){
			$this->afastamentos->excluir_afastamento_funcionario();
		}
	}