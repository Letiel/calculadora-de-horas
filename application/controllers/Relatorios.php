<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata("logado")){
			redirect("/");
		}
	}

	function index(){
		$this->load->view("relatorios-selecionar");
	}

	function funcionarios(){
		$this->load->model("FuncionariosModel", "funcionarios");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(4);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/selecionar-funcionario/".$this->uri->segment(3);
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 4;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->funcionarios->getFuncionarios()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$funcionarios = $this->funcionarios->getFuncionarios($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-funcionario", array("funcionarios"=>$funcionarios, "paginacao"=>$this->pagination->create_links(), "link"=>"funcionario"));
	}

	function funcionario(){
		$funcionario = $this->uri->segment(3);
			// $this->session->set_flashdata("retorno", "toastr.error('Funcionário não encontrado', 'Ops');");
		if(!is_numeric($funcionario) || empty($funcionario)){
			$this->session->set_flashdata("retorno", "toastr.error('Selecione um funcionário', 'Ops');");
			redirect("/relatorios");
		}

		$this->load->model("CalculadoraModel", "calculadora");
		$funcionario = $this->calculadora->getFuncionario((int)$this->uri->segment(3));

		if($funcionario->num_rows() > 0){
			$funcionario = $funcionario->first_row();
			$feriados = $this->calculadora->getFeriados($funcionario->id_empresa)->result();
			$extras = $this->calculadora->getFaixasExtras($funcionario->id)->result();
			$dia["domingo"] = $this->calculadora->getHorariosDia($funcionario->id, "domingo")->first_row();
			$dia["segunda"] = $this->calculadora->getHorariosDia($funcionario->id, "segunda")->first_row();
			$dia["terca"] = $this->calculadora->getHorariosDia($funcionario->id, "terca")->first_row();
			$dia["quarta"] = $this->calculadora->getHorariosDia($funcionario->id, "quarta")->first_row();
			$dia["quinta"] = $this->calculadora->getHorariosDia($funcionario->id, "quinta")->first_row();
			$dia["sexta"] = $this->calculadora->getHorariosDia($funcionario->id, "sexta")->first_row();
			$dia["sabado"] = $this->calculadora->getHorariosDia($funcionario->id, "sabado")->first_row();
			$this->load->view("relatorio-funcionario", array("funcionario"=>$funcionario, "feriados"=>$feriados, "extras"=>$extras, "dia"=>$dia));
		}

	}

	function empresas(){
		$this->load->model("EmpresasModel", "empresas");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/selecionar-empresa/";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->empresas->getEmpresasConta()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$empresas = $this->empresas->getEmpresasConta($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-empresa", array("empresas"=>$empresas, "paginacao"=>$this->pagination->create_links(), "link"=>"/relatorios/empresa"));
	}

	function empresa(){
		$empresa = $this->input->post("id_empresa");
		$de = $this->input->post("de");
		$ate = $this->input->post("ate");
		if(empty($empresa) || !is_numeric($empresa)){
			$this->session->set_flashdata("retorno", "toastr.error('Nenhuma empresa encontrada.', 'Ops');");
			redirect("/relatorios/empresas");
		}

		$this->load->model("CalculadoraModel", "calculadora");
		$this->load->model("EmpresasModel", "empresas");
		$valores = $this->calculadora->relatorioEmpresa($empresa, $de, $ate);
		$empresa = $this->empresas->getEmpresa($empresa)->first_row();
		$this->load->view("relatorio-empresa-departamento", array("valores"=>$valores, "tipo"=>"Empresa", "empresa"=>$empresa));
	}

	function departamentos(){
		$this->load->model("DepartamentosModel", "departamentos");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/selecionar-departamento/";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->departamentos->getDepartamentos()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$departamentos = $this->departamentos->getDepartamentos($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-departamento", array("departamentos"=>$departamentos, "paginacao"=>$this->pagination->create_links(), "link"=>"/relatorios/departamento"));
	}

	function departamento(){
		$departamento = $this->input->post("id_departamento");
		$de = $this->input->post("de");
		$ate = $this->input->post("ate");
		if(empty($departamento) || !is_numeric($departamento)){
			$this->session->set_flashdata("retorno", "toastr.error('Nenhuma departamento encontrado.', 'Ops');");
			redirect("/relatorios/departamentos");
		}

		$this->load->model("CalculadoraModel", "calculadora");
		$this->load->model("DepartamentosModel", "departamentos");
		$valores = $this->calculadora->relatorioDepartamento($departamento, $de, $ate);
		$departamento = $this->departamentos->getDepartamento($departamento)->first_row();
		$this->load->view("relatorio-empresa-departamento", array("valores"=>$valores, "tipo"=>"Departamento", "departamento"=>$departamento));
	}

	function afastamentos(){
		$this->load->view("relatorios-afastamentos-selecionar-tipo");
	}

	function afastamentos_funcionarios(){
		$this->load->model("FuncionariosModel", "funcionarios");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/afastamentos-funcionarios";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->funcionarios->getFuncionarios()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$funcionarios = $this->funcionarios->getFuncionarios($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-funcionario-afastamentos", array("funcionarios"=>$funcionarios, "paginacao"=>$this->pagination->create_links(), "link"=>"funcionario-afastamentos"));
	}

	function funcionario_afastamentos(){
		$this->load->model("CalculadoraModel", "calculadora");
		// print_r($this->input->post());
		$post = $this->input->post();
		// echo "<pre>";
		$resultado = (object)$this->calculadora->relatorioAfastamentosFuncionario();
		$funcionario = $this->calculadora->getFuncionario($post["id_funcionario"])->first_row();
		// print_r($resultado);
		// echo "</pre>";
		$this->load->view("relatorio-afastamentos-funcionario", array("resultado"=>$resultado, "funcionario"=>$funcionario, "post"=>$post));
	}

	function afastamentos_empresas(){
		$this->load->model("EmpresasModel", "empresas");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/afastamentos-empresas/";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->empresas->getEmpresasConta()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$empresas = $this->empresas->getEmpresasConta($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-empresa", array("empresas"=>$empresas, "paginacao"=>$this->pagination->create_links(), "link"=>"/relatorios/empresa-afastamentos"));
	}

	function empresa_afastamentos(){
		$post = $this->input->post();
		$this->load->model("CalculadoraModel", "calculadora");
		$this->load->model("EmpresasModel", "empresas");
		$resultado = $this->calculadora->relatorioAfastamentosEmpresa();
		$empresa = $this->empresas->getEmpresa($post["id_empresa"])->first_row();
		$this->load->view("relatorio-afastamentos-empresa", array("resultado"=>$resultado, "empresa"=>$empresa, "post"=>$post));
	}

	function afastamentos_departamentos(){
		$this->load->model("DepartamentosModel", "departamentos");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/afastamentos-departamentos/";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->departamentos->getDepartamentos()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$departamentos = $this->departamentos->getDepartamentos($inicio, $maximo)->result();
		$this->load->view("relatorios-selecionar-departamento", array("departamentos"=>$departamentos, "paginacao"=>$this->pagination->create_links(), "link"=>"/relatorios/departamento-afastamentos"));
	}

	function departamento_afastamentos(){
		$post = $this->input->post();
		$this->load->model("CalculadoraModel", "calculadora");
		$this->load->model("DepartamentosModel", "departamentos");
		$resultado = $this->calculadora->relatorioAfastamentosDepartamento();
		$departamento = $this->departamentos->getDepartamento($post["id_departamento"])->first_row();
		$this->load->view("relatorio-afastamentos-departamento", array("resultado"=>$resultado, "departamento"=>$departamento, "post"=>$post));
	}

	function afastamentos_tipos(){
		$this->load->model("AfastamentosModel", "afastamentos");
		// -----------------------PAGINAÇÃO------------------------------
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(3);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/relatorios/afastamentos_tipos";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->afastamentos->getTiposAfastamentos()->num_rows();
		$this->pagination->initialize($config);
		// -----------------------PAGINAÇÃO------------------------------
		$afastamentos = $this->afastamentos->getTiposAfastamentos($inicio, $maximo)->result();
		$this->load->view("afastamentos-lista", array("afastamentos"=>$afastamentos, "paginacao"=>$this->pagination->create_links(), "link"=>"/relatorios/tipo_afastamentos/"));
	}

	function tipo_afastamentos(){
		$post = $this->input->post();
		$this->load->model("CalculadoraModel", "calculadora");
		$this->load->model("AfastamentosModel", "afastamentos");
		$resultado = $this->calculadora->relatorioAfastamentosTipo();
		$tipo = $this->afastamentos->getTipoAfastamento($post["id_tipo"])->first_row();
		$this->load->view("relatorio-afastamentos-tipo", array("resultado"=>$resultado, "tipo"=>$tipo, "post"=>$post));
	}
}