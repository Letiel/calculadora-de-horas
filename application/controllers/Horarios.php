<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Horarios extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("HorariosModel", "horarios");
			if(!$this->session->userdata("logado"))
				redirect("/");
		}

		function index(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/horarios";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->horarios->getGrupos()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$horarios = $this->horarios->getGrupos($inicio, $maximo)->result();
			$this->load->view("horarios", array("horarios"=>$horarios, "paginacao"=>$this->pagination->create_links()));
		}

		function cadastrar_grupo(){
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("id_empresa", "Empresa", "required|is_numeric");
			if($this->form_validation->run()){
				$this->horarios->cadastrar_grupo();
			}
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresasConta()->result();
			$this->load->view("horarios-cadastrar-grupo", array("empresas"=>$empresas));
		}

		function editar(){
			$id = (int) $this->uri->segment("3");
			$grupo_horario = $this->horarios->getGrupo($id);
			if($grupo_horario->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.warning('Horário não encontrado');");
				redirect("/horarios");
			}
			$grupo_horario = $grupo_horario->first_row();
			$domingo = $this->horarios->getHorariosGrupo($id, "domingo")->first_row();
			$segunda = $this->horarios->getHorariosGrupo($id, "segunda")->first_row();
			$terca = $this->horarios->getHorariosGrupo($id, "terca")->first_row();
			$quarta = $this->horarios->getHorariosGrupo($id, "quarta")->first_row();
			$quinta = $this->horarios->getHorariosGrupo($id, "quinta")->first_row();
			$sexta = $this->horarios->getHorariosGrupo($id, "sexta")->first_row();
			$sabado = $this->horarios->getHorariosGrupo($id, "sabado")->first_row();

			$this->load->model("HorasExtrasModel", "horas");
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresasConta()->result();
			$extras = $this->horas->getHorasExtras($id)->result();
			$this->load->view("horarios-editar", array(
				"grupo_horario"=>$grupo_horario,
				"domingo"=>$domingo,
				"segunda"=>$segunda,
				"terca"=>$terca,
				"quarta"=>$quarta,
				"quinta"=>$quinta,
				"sexta"=>$sexta,
				"sabado"=>$sabado,
				"extras"=>$extras,
				"empresas"=>$empresas
				));
		}

		function editable(){
			$post = $this->input->post();

			$name = $_POST['name'];

			switch ($name) {
				case 'padrao_entrada_1':
					$this->form_validation->set_rules('value', 'Horário de Entrada 1', 'required|addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_saida_1':
					$this->form_validation->set_rules('value', 'Horário de Saída 1', 'required|addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_entrada_2':
					$this->form_validation->set_rules('value', 'Horário de Entrada 2', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_saida_2':
					$this->form_validation->set_rules('value', 'Horário de Saída 2', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_entrada_3':
					$this->form_validation->set_rules('value', 'Horário de Entrada 3', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_saida_3':
					$this->form_validation->set_rules('value', 'Horário de Saída 3', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_entrada_4':
					$this->form_validation->set_rules('value', 'Horário de Entrada 4', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_saida_4':
					$this->form_validation->set_rules('value', 'Horário de Saída 4', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_entrada_5':
					$this->form_validation->set_rules('value', 'Horário de Entrada 5', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'padrao_saida_5':
					$this->form_validation->set_rules('value', 'Horário de Saída 5', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'carga_horaria':
					//terá que ser calculado à parte
					break;
				case 'regras_tolerancias':
					$this->form_validation->set_rules("value", "Regras de Tolerâncias", "required");
					break;
				case 'utiliza_compensacao':
					$this->form_validation->set_rules("value", "Compensação", "required");
					break;
				case 'tolerancia_diaria':
					//talvez tenha que testar baseado nas regras... visto que se for utilizado CLT, esses valores não são alteráveis [daqui para baixo]
					$this->form_validation->set_rules('value', 'Tolerância Diária', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_1_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 1 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_1_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 1 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_1_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 1 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_1_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 1 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_2_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 2 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_2_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 2 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_2_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 2 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_2_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 2 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_3_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 3 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_3_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 3 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_3_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 3 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_3_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 3 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_4_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 4 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_4_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 4 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_4_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 4 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_4_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 4 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_5_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 5 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_entrada_5_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Entrada 5 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_5_antes':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 5 - Antes', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'tol_saida_5_depois':
					$this->form_validation->set_rules('value', 'Tolerância de Saída 5 - Depois', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'comp_tol_falta':
					$this->form_validation->set_rules('value', 'Tolerância da Compensação - Falta', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'comp_tol_extra':
					$this->form_validation->set_rules('value', 'Tolerância da Compensação - Extra', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				case 'fechamento':
					$this->form_validation->set_rules('value', 'Horário de Fechamento', 'addslashes|max_length[8]|min_length[5]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 8 caracteres.', "min_length"=>"Mínimo de 5 caracteres."));
					break;
				default:
					$this->form_validation->set_rules('value', 'Horário de Fechamento', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
			}

			if($this->form_validation->run()){
				$result = $this->horarios->editable();
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors(), 'carga_horaria'=>"");
			}
			echo json_encode($result);
		}

		function copiar_horario(){
			$this->horarios->copiar_horario();
		}

		function avancado(){
			$this->horarios->avancado();
		}

		function preencher_avancado(){
			$this->horarios->preencher_avancado();
		}
	}