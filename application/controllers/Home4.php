<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home4 extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();
		$data["soma_intervalos_trabalhados"] = "00:00:00";
		$data["calculo"]["extra"]["soma"] = "00:00:00";

		for ($i=1; $i <= 5 ; $i++) {
			$post["outrodia$i"] = $post["outrodia$i"] == "false" ? false : true;
			$data["horario_entrada"]["_$i"] = $post["horario_entrada_$i"].":00";
			$data["horario_saida"]["_$i"] = $post["horario_saida_$i"].":00";
			//-----------------CALCULANDO TOTAL DE HORAS TRABALHADAS E CARGA DO DIA--------------
			if($post["entrada$i"] != "" && $post["saida$i"] != ""){
				$data["entrada"]["_$i"] = $post["entrada$i"].":00";
				$data["saida"]["_$i"] = $post["saida$i"].":00";
				$data['saida']["_$i"] = !$post["outrodia$i"] ? $data['saida']["_$i"] : date("Y-m-d H:i:s", strtotime("+1 days", strtotime($data['saida']["_$i"])));
				$data["tolerancia"]["entrada"]["_$i"]["antes"] = $post["tolerancia_entrada_".$i."_antes"].":00";
				$data["tolerancia"]["entrada"]["_$i"]["depois"] = $post["tolerancia_entrada_".$i."_depois"].":00";
				$data["tolerancia"]["saida"]["_$i"]["antes"] = $post["tolerancia_saida_".$i."_antes"].":00";
				$data["tolerancia"]["saida"]["_$i"]["depois"] = $post["tolerancia_saida_".$i."_depois"].":00";

				$data['intervalo_trabalhado']["_$i"] = $this->calcularIntervaloHoras($data["entrada"]["_$i"], $data["saida"]["_$i"]);
				$data["soma_intervalos_trabalhados"] = $this->somarHoras($data["soma_intervalos_trabalhados"], $data["intervalo_trabalhado"]["_$i"]);

				//--------------CALCULANDO TOLERÂNCIAS------------------
				$data["horarios_tolerados"]["entrada"]["_$i"] = $this->calcularTolerancia($data["entrada"]["_$i"], $data["horario_entrada"]["_$i"], $data["tolerancia"]["entrada"]["_$i"]["antes"], $data["tolerancia"]["entrada"]["_$i"]["depois"], "entrada");
				$data["horarios_tolerados"]["saida"]["_$i"] = $this->calcularTolerancia($data["saida"]["_$i"], $data["horario_saida"]["_$i"], $data["tolerancia"]["saida"]["_$i"]["antes"], $data["tolerancia"]["saida"]["_$i"]["depois"], "saida");

				$data["soma_faltas"]["_$i"] = $this->somarHoras($data["horarios_tolerados"]["entrada"]["_$i"]["falta_real"], $data["horarios_tolerados"]["saida"]["_$i"]["falta_real"]);
				$data["soma_extras"]["_$i"] = $this->somarHoras($data["horarios_tolerados"]["entrada"]["_$i"]["extra_real"], $data["horarios_tolerados"]["saida"]["_$i"]["extra_real"]);

				$data["calculo"]["extra"]["soma"] = $this->somarHoras($data["calculo"]["extra"]["soma"], $data["soma_extras"]["_$i"]);


			}
			if($data["horario_saida"]["_$i"] != ":00"){
				// ----------------- calculando horário final do dia
				$data["horario_final_dia"] = $data["horario_saida"]["_$i"];
			}

		}
		// ------------------------------- INTERVALOS ENTRE TURNOS ------------------------------- //
		if($data["horario_saida"]["_1"] != ":00" && $data["horario_entrada"]["_2"] != ":00"){
			if(strtotime($data["horario_entrada"]["_2"]) < strtotime($data["horario_saida"]["_1"])){
				$horario_entrada = $this->somarHoras($data["horario_entrada"]["_2"], "24:00:00");//somando um dia (segundos)
			}else
				$horario_entrada = $data["horario_entrada"]["_2"];
			$data["entre_horario"]["1_2"] = $this->calcularIntervaloHoras2($horario_entrada, $data["horario_saida"]["_1"]);
		}
		if($data["horario_saida"]["_2"] != ":00" && $data["horario_entrada"]["_3"] != ":00"){
			if(strtotime($data["horario_entrada"]["_3"]) < strtotime($data["horario_saida"]["_2"])){
				$horario_entrada = $this->somarHoras($data["horario_entrada"]["_3"], "24:00:00");//somando um dia (segundos)
			}else
				$horario_entrada = $data["horario_entrada"]["_3"];
			$data["entre_horario"]["2_3"] = $this->calcularIntervaloHoras2($horario_entrada, $data["horario_saida"]["_2"]);
		}
		if($data["horario_saida"]["_3"] != ":00" && $data["horario_entrada"]["_4"] != ":00"){
			if(strtotime($data["horario_entrada"]["_4"]) < strtotime($data["horario_saida"]["_3"])){
				$horario_entrada = $this->somarHoras($data["horario_entrada"]["_4"], "24:00:00");//somando um dia (segundos)
			}else
				$horario_entrada = $data["horario_entrada"]["_4"];
			$data["entre_horario"]["3_4"] = $this->calcularIntervaloHoras2($horario_entrada, $data["horario_saida"]["_3"]);
		}
		if($data["horario_saida"]["_4"] != ":00" && $data["horario_entrada"]["_5"] != ":00"){
			if(strtotime($data["horario_entrada"]["_5"]) < strtotime($data["horario_saida"]["_4"])){
				$horario_entrada = $this->somarHoras($data["horario_entrada"]["_5"], "24:00:00");//somando um dia (segundos)
			}else
				$horario_entrada = $data["horario_entrada"]["_5"];
			$data["entre_horario"]["4_5"] = $this->calcularIntervaloHoras2($horario_entrada, $data["horario_saida"]["_4"]);
		}

		// ------------------------------- CALCULAR EXTRAS ------------------------------- //
		for ($i=1; $i <= 5 ; $i++) { 
			$extra = "00:00:00";
			if ($post["outrodia$i"]) {
				if ($data["entrada"]["_$i"] <= $data["horario_saida"]["_1"]) {
					if (isset($data["entre_horario"]["1_2"]))
						$extra = $this->somarHoras($data["entre_horario"]["1_2"], $extra);
				}
				if ($data["entrada"]["_$i"] <= $data["horario_saida"]["_2"]) {
					if (isset($data["entre_horario"]["2_3"]))
						$extra = $this->somarHoras($data["entre_horario"]["2_3"], $extra);
				}

				if ($data["entrada"]["_$i"] <= $data["horario_saida"]["_3"]) {
					if (isset($data["entre_horario"]["3_4"]))
						$extra = $this->somarHoras($data["entre_horario"]["3_4"], $extra);
				}

				if ($data["entrada"]["_$i"] <= $data["horario_saida"]["_4"]) {
					if (isset($data["entre_horario"]["4_5"]))
						$extra = $this->somarHoras($data["entre_horario"]["4_5"], $extra);
				}

				// Calcula a diferença do horário de final de turno até a meia noite
				$calculo = $this->calcularIntervaloHoras2("23:59:59", $data['horario_final_dia']);
				$calculo = $this->somarHoras($calculo, "00:00:01");

				if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) <= strtotime(date("H:i:s", strtotime($data["horario_entrada"]["_1"])))) {
						echo "<h1>blá</h1>";
						$calculo = $this->somarHoras($calculo, date("H:i:s", strtotime($data["saida"]["_$i"])));
						$extra = $this->somarHoras($calculo, $extra);
				}else{
					$calculo = $this->somarHoras($calculo, date("H:i:s", strtotime($data["horario_entrada"]["_1"])));
					$extra = $this->somarHoras($calculo, $extra);

					// Testa se está dentro do primeiro intervalo
					if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) > strtotime(date("H:i:s", strtotime($data["horario_saida"]["_1"])))) {
						if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) < strtotime(date("H:i:s", strtotime($data["horario_entrada"]["_2"])))) {
							$extra_primeiro_intervalo = $this->calcularIntervaloHoras2(date("H:i:s", strtotime($data["saida"]["_$i"])), date("H:i:s", strtotime($data["horario_saida"]["_1"])));
							$extra = $this->somarHoras($extra_primeiro_intervalo, $extra);
						} else{
							if (isset($data["entre_horario"]["1_2"]))
								$extra = $this->somarHoras($data["entre_horario"]["1_2"], $extra);
						}
					}

					// Testa se está dentro do segundo intervalo
					if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) > strtotime(date("H:i:s", strtotime($data["horario_saida"]["_2"])))) {
						if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) < strtotime(date("H:i:s", strtotime($data["horario_entrada"]["_3"])))) {
							$extra_primeiro_intervalo = $this->calcularIntervaloHoras2(date("H:i:s", strtotime($data["saida"]["_$i"])), date("H:i:s", strtotime($data["horario_saida"]["_2"])));
							$extra = $this->somarHoras($extra_primeiro_intervalo, $extra);
						} else{
							if (isset($data["entre_horario"]["2_3"]))
								$extra = $this->somarHoras($data["entre_horario"]["2_3"], $extra);
						}
					}

					// Testa se está dentro do terceiro intervalo
					if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) > strtotime(date("H:i:s", strtotime($data["horario_saida"]["_3"])))) {
						if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) < strtotime(date("H:i:s", strtotime($data["horario_entrada"]["_4"])))) {
							$extra_primeiro_intervalo = $this->calcularIntervaloHoras2(date("H:i:s", strtotime($data["saida"]["_$i"])), date("H:i:s", strtotime($data["horario_saida"]["_3"])));
							$extra = $this->somarHoras($extra_primeiro_intervalo, $extra);
						} else{
							if (isset($data["entre_horario"]["3_4"]))
								$extra = $this->somarHoras($data["entre_horario"]["3_4"], $extra);
						}
					}

					// Testa se está dentro do terceiro intervalo
					if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) > strtotime(date("H:i:s", strtotime($data["horario_saida"]["_4"])))) {
							if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) < strtotime(date("H:i:s", strtotime($data["horario_entrada"]["_5"])))) {
								$extra_primeiro_intervalo = $this->calcularIntervaloHoras2(date("H:i:s", strtotime($data["saida"]["_$i"])), date("H:i:s", strtotime($data["horario_saida"]["_4"])));
								$extra = $this->somarHoras($extra_primeiro_intervalo, $extra);
							} else{
								if (isset($data["entre_horario"]["4_5"]))
									$extra = $this->somarHoras($data["entre_horario"]["4_5"], $extra);
							}
					}

					// Testa se passou do ultimo horario do dia
					if (strtotime(date("H:i:s", strtotime($data["saida"]["_$i"]))) > strtotime(date("H:i:s", strtotime($data["horario_final_dia"])))) {
						$apos_expediente = $this->calcularIntervaloHoras2(date("H:i:s", strtotime($data["saida"]["_$i"])), date("H:i:s", strtotime($data["horario_final_dia"])));
						$extra = $this->somarHoras($apos_expediente, $extra);
					}
				}


			}else{
				if (isset($data["entrada"]["_$i"])) {
					$diferenca_entrada = $this->subtrairHoras($data["saida"]["_1"], $data["horario_entrada"]["_2"]);
					$extra = $this->subtrairHoras($data["calculo"]["extra"]["soma"], $diferenca_entrada);

					if(strtotime($data["horario_saida"]["_2"]) < strtotime($data["saida"]["_1"])){
						$extra = $this->somarHoras($this->subtrairHoras($data["saida"]["_1"], $data["horario_saida"]["_2"]), $extra);
					}

				}
			}
					echo $extra." || ";

		}

		// echo json_encode($data);
	}

	function subtrairHoras($hora1, $hora2){//função provavelmente redundante, mas serve para descrição do que está sendo feito
		$hora1 = strtotime($hora1);
		$hora2 = strtotime($hora2);
		return date("H:i:s", $hora1 - $hora2);
	}

	function calcularTolerancia($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima, $tipo){
		$nome = "";
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);

		$array = array(
			"status"=>"",
			"extra_real"=>"00:00:00",
			"falta_real"=>"00:00:00",
			"falta"=>"00:00:00",
			"extra"=>"00:00:00",
		);
		if($entrada > strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "falta";
			else
				$nome = "extra";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", strtotime($horario_padrao)), date("H:i:s", $entrada));
		}

		if($entrada < strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "extra";
			else
				$nome = "falta";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", strtotime($horario_padrao)));
		}
		if($entrada > $horario_tolerado['acima']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $horario_tolerado['acima']), date("H:i:s", $entrada));
		}

		if($entrada < $horario_tolerado['abaixo']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", $horario_tolerado['abaixo']));
		}
		$array['status'] = $nome;
		return $array;
	}

	function calcularHorasToleradas($entrada, $saida, $outro_dia = false, $somar = false){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		if($somar){
			$resultado = $acumulador2 + $acumulador1;
		}else{
			$resultado = $acumulador2 - $acumulador1;
		}
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function somarHoras($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador2 + $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function calcularIntervaloHoras($entrada, $saida, $outro_dia = false, $somar = false){
		$date_time  = new DateTime($entrada);
		$diff       = $date_time->diff(new DateTime($saida));
		// print_r($diff);
		// echo $diff->format('%y ano(s), %m mês(s), %d dia(s), %H hora(s), %i minuto(s) e %s segundo(s)')."<br />";
		$dias = $diff->d;
		$horas = ($dias*24) + $diff->h;
		return ($diff->invert == 1 ? "-" : "").str_pad($horas, 2, '0', STR_PAD_LEFT).":".str_pad($diff->i, 2, '0', STR_PAD_LEFT).":".str_pad($diff->s, 2, '0', STR_PAD_LEFT);
	}

	function calcularIntervaloHoras2($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador1 - $acumulador2;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

}