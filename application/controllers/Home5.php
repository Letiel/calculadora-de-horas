<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();
		$data["horario_expediente"] = "00:00:00";
		$data["horario_final_dia"] = "";

		for ($i=1; $i <= 5 ; $i++) {
			// Horários Padrões
			$data["padrao"]["horario_entrada_$i"] = ($post["horario_entrada_$i"]!= "") ? $post["horario_entrada_$i"].":00" : "";
			$data["padrao"]["horario_saida_$i"] = ($post["horario_saida_$i"]!= "") ? $post["horario_saida_$i"].":00" : "";


			// Horários Informados

			$data["horarios"]["entrada$i"] = ($post["entrada$i"]!= "") ? $post["entrada$i"].":00" : "";
			$data["horarios"]["saida$i"] = ($post["saida$i"]!= "") ? $post["saida$i"].":00" : "";

			// ----------------- calculando horário final do dia
			if($data["padrao"]["horario_saida_$i"] != ":00" && $data["padrao"]["horario_saida_$i"] != ""){
				$data["horario_final_dia"] = $data["padrao"]["horario_saida_$i"];
			}

		}

		for ($i=1; $i <= 5 ; $i++) {

			// ----------------- calculando turnos
			if ($data["padrao"]["horario_saida_$i"] != "" && $data["padrao"]["horario_entrada_$i"] != "") {
				if (strtotime(date("H:i:s", strtotime($data["padrao"]["horario_saida_$i"]))) < strtotime(date("H:i:s", strtotime($data["padrao"]["horario_entrada_$i"]))))
					$saida = $this->somarHoras($data["padrao"]["horario_saida_$i"], "24:00:00");
				else
					$saida = $data["padrao"]["horario_saida_$i"];

				$data["turnos"]["turno_$i"] = $this->calcularIntervaloHoras2($saida, $data["padrao"]["horario_entrada_$i"]);
			}
			
			// ----------------- calculando intervalos
			$proximo = $i+1;

			if ($i >=1 && $i <=4) {
				if ($data["padrao"]["horario_saida_$i"] != "" && $data["padrao"]["horario_entrada_$proximo"] != "") {

					if (strtotime(date("H:i:s", strtotime($data["padrao"]["horario_entrada_$proximo"]))) < strtotime(date("H:i:s", strtotime($data["padrao"]["horario_saida_$i"]))))
						$proxima_entrada = $this->somarHoras($data["padrao"]["horario_entrada_$proximo"], "24:00:00");
					else
						$proxima_entrada = $data["padrao"]["horario_entrada_$proximo"];
					$data["intervalos"]["intervalo_$i"] =  $this->calcularIntervaloHoras2($proxima_entrada, $data["padrao"]["horario_saida_$i"]);
				}else{
					$data["intarvalos"]["intervalo_$i"] = "";
				}

			}

			// ----------------- calculando horário do expedente diário
			if (isset($data['turnos']["turno_$i"]))
				$data["horario_expediente"] = $this->somarHoras($data["horario_expediente"],$data['turnos']["turno_$i"]);
		}

		for ($i=1; $i <= 5 ; $i++) {
			if(strtotime(date("H:i:s", strtotime($data["horarios"]["entrada$i"]))) > strtotime(date("H:i:s", strtotime($data['padrao']["horario_entrada_$i"])))){}
		}






		echo "<pre>";
		echo $this->calcularIntervaloHoras2($data["padrao"]["horario_entrada_1"], $data["horarios"]["entrada1"]);
		echo "<br />";

		if(strtotime($data["padrao"]["horario_saida_1"]) <= strtotime($data["padrao"]["horario_entrada_1"]))
			$outro_dia = true;
		else
			$outro_dia = false;
		if(strtotime($data["horarios"]["saida1"]) >= strtotime($data["padrao"]["horario_entrada_2"]) && isset($data["intervalos"]["saida1"])){
			echo $data["intervalos"]["intervalo_1"];
		}else if(!isset($data["intervalos"]["intervalo_1"])){
			if($outro_dia && strtotime($data["horarios"]["saida1"]) >= $data["horarios"]["entrada1"]){
				echo $this->calcularIntervaloHoras2($this->somarHoras($data["horarios"]["saida1"], "00:00:00"), $this->somarHoras($data["padrao"]["horario_saida_1"], "00:00:00"))."--";
			}else{
				echo $this->calcularIntervaloHoras2($data["horarios"]["saida1"], $data["padrao"]["horario_saida_1"])."|";
			}
		}else{
			echo $this->calcularIntervaloHoras2($data["horarios"]["saida1"], $data["padrao"]["horario_saida_1"]);
		}
		echo "<br />";
		// print_r($data);
		echo "</pre>";

	}


	function somarHoras($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador2 + $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}


	function calcularIntervaloHoras2($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador1 - $acumulador2;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

}