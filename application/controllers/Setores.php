<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Setores extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("SetoresModel", "setores");
			if(!$this->session->userdata("logado"))
				redirect("/");
		}

		function index(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/setores";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->setores->getSetores()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$setores = $this->setores->getSetores($inicio, $maximo)->result();
			$this->load->view("setores", array("setores"=>$setores, 'paginacao'=>$this->pagination->create_links()));
		}

		function cadastrar(){
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("id_departamento", "Departamento", "required");
			if($this->form_validation->run()){
				$this->setores->cadastrar();
			}
			$this->load->model("DepartamentosModel", "departamentos");
			$departamentos = $this->departamentos->getDepartamentos()->result();
			$this->load->view("setores-cadastrar", array("departamentos"=>$departamentos));
		}

		function editar(){
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("id_departamento", "Departamento", "required");
			$id = $this->uri->segment(3);
			if($this->form_validation->run()){
				$this->setores->editar($id);
			}
			$this->load->model("DepartamentosModel", "departamentos");
			$departamentos = $this->departamentos->getDepartamentos();
			$setor = $this->setores->getSetor($id);
			if($departamentos->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum departamento cadastrado!', 'Ops');");
				redirect("/funcionarios");
			}else if($setor->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Setor não encontrado', 'Ops');");
				redirect("/setores");
			}
			$this->load->view("setores-editar", array("setor"=>$setor->first_row(), "departamentos"=>$departamentos->result()));
		}

		function excluir(){
			$this->setores->excluir();
		}

		function select2_departamentos(){
			$this->setores->select2_departamentos();
		}
	}