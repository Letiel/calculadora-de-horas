<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Operadores extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("UsuariosModel", "usuarios");
			$this->usuarios->atualizar_sessao();
			if(!$this->session->userdata("logado") || !$this->session->userdata("adm")){
				$this->session->set_flashdata("retorno", "toastr.error('Você não possui permissão.', 'Bloqueado!');");
				redirect("/");
			}
			$this->load->model("OperadoresModel", "operadores");
		}

		function index(){
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/setores";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->operadores->getOperadores()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$operadores = $this->operadores->getOperadores($inicio, $maximo)->result();
			$this->load->view("operadores", array("operadores"=>$operadores, "paginacao"=>$this->pagination->create_links()));
		}

		function cadastrar(){
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("login", "Login", "required|is_unique[usuarios.login]", array("is_unique"=>"Login já existente."));
			$this->form_validation->set_error_delimiters("", "");

			if($this->form_validation->run()){
				$this->operadores->cadastrar();
				$this->load->view("operadores-pos-cadastro");
				exit();
			}
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresasConta()->result();
			$this->load->view("operadores-cadastrar", array("empresas"=>$empresas));
		}

		function pos_cadastro(){
			$senha = $this->session->flashdata("senha");
			if(empty($senha)){
				redirect("/");
				exit();
			}
			$this->load->view("operadores-pos-cadastro", array('senha'=>$senha));
		}

		function editar(){
			$id = $this->uri->segment(3);
			if(empty($id)){
				$this->session->set_flashdata("retorno", "toastr.error('Ops!', 'Operador não encontrado.');");
				redirect("/operadores");
			}
			$operador = $this->operadores->getOperador($id);
			if($operador->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Ops!', 'Operador não encontrado.');");
				redirect("/operadores");
			}else{
				$total_operador = $operador->num_rows();
				$operador = $operador->first_row();
			}

			$this->form_validation->set_rules("nome", "Nome", "required");
			$post = $this->input->post();
			if(isset($post["login"])){
				if($post["login"] == $operador->login)
					$this->form_validation->set_rules("login", "Login", "required");
				else
					$this->form_validation->set_rules("login", "Login", "required|is_unique[usuarios.login]", array("is_unique"=>"Login já existente."));

				$this->form_validation->set_error_delimiters("", "");

				if($this->form_validation->run()){
					$this->operadores->editar($id);
				}else{
					$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Verifique os campos.');");
				}
			}
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresas();
			if($empresas->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhuma empresa cadastrada!', 'Ops');");
				redirect("/empresas");
			}else if($total_operador == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Operador não encontrado!', 'Ops');");
				redirect("/operadores");
			}
			$usuario_empresa = $this->operadores->getEmpresasOperador($id);
			$this->load->view("operadores-editar", array("operador"=>$operador, "empresas"=>$empresas->result(), "usuario_empresa"=>$usuario_empresa));
		}

		function gerar_nova_senha(){
			$this->form_validation->set_rules("id", "ID do Operador", "required|is_numeric");
			$this->form_validation->set_error_delimiters("", "");
			if($this->form_validation->run()){
				$this->operadores->gerar_nova_senha();
			}else{
				echo json_encode(array("sucesso"=>"0", "conteudo"=>validation_errors()));
			}
		}

		function excluir(){
			$this->form_validation->set_rules("id", "ID", "is_numeric");
			if($this->form_validation->run()){
				$this->operadores->excluir();
			}
		}
	}