<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Funcionarios extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("FuncionariosModel", "funcionarios");
			$this->load->model("AfastamentosModel", "afastamentos");
			if(!$this->session->userdata("logado"))
				redirect("/");
		}

		function index(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/funcionarios";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->funcionarios->getFuncionarios()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------
			$funcionarios = $this->funcionarios->getFuncionarios($inicio, $maximo)->result();
			$this->load->view("funcionarios", array("funcionarios"=>$funcionarios, "paginacao"=>$this->pagination->create_links()));
		}

		function cadastrar(){
			$this->form_validation->set_rules("id_empresa", "Empresa", "required");
			$this->form_validation->set_rules("id_cidade", "Cidade", "required");
			$this->form_validation->set_rules("nome", "Nome Completo", "required");
			$this->form_validation->set_rules("numero_folha", "Número da Folha", "");
			$this->form_validation->set_rules("pis", "PIS", "");
			$this->form_validation->set_rules("numero_identificador", "Número Identificador", "");
			$this->form_validation->set_rules("ctps", "CTPS", "");
			$this->form_validation->set_rules("id_horario", "Horário", "required");
			$this->form_validation->set_rules("id_setor", "Setor", "");
			$this->form_validation->set_rules("id_departamento", "Departamento", "");
			$this->form_validation->set_rules("admissao", "Data de Admissão", "");
			$this->form_validation->set_rules("demissao", "Data de Demissão", "");
			$this->form_validation->set_rules("observacoes", "Observações", "nl2br");
			$this->form_validation->set_error_delimiters("", "");
			if($this->form_validation->run()){
				$this->funcionarios->cadastrar();
			}
			$this->load->model("EmpresasModel", "empresas");
			$this->load->model("HorariosModel", "horarios");
			$this->load->model("DepartamentosModel", "departamentos");
			$this->load->model("SetoresModel", "setores");
			$empresas = $this->empresas->getEmpresasConta();
			$horarios = $this->horarios->getGrupos();
			$departamentos = $this->departamentos->getDepartamentos();
			$setores = $this->setores->getSetores();

			if($empresas->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhuma empresa cadastrada!', 'Ops');");
				redirect("/empresas");
			}else if($horarios->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum horário cadastrado!', 'Ops');");
				redirect("/horarios");
			}else{
				$this->load->view("funcionarios-cadastrar", array(
					"empresas"=>$empresas->result(),
					"horarios"=>$horarios->result(),
					"departamentos"=>$departamentos->result(),
					"setores"=>$setores->result()
				));
			}

		}

		function editar(){
			$this->form_validation->set_rules("id_empresa", "Empresa", "required");
			$this->form_validation->set_rules("id_cidade", "Cidade", "required");
			$this->form_validation->set_rules("nome", "Nome Completo", "required");
			$this->form_validation->set_rules("numero_folha", "Número da Folha", "");
			$this->form_validation->set_rules("pis", "PIS", "");
			$this->form_validation->set_rules("numero_identificador", "Número Identificador", "required");
			$this->form_validation->set_rules("ctps", "CTPS", "");
			$this->form_validation->set_rules("id_horario", "Horário", "required");
			$this->form_validation->set_rules("id_setor", "Setor", "");
			$this->form_validation->set_rules("id_departamento", "Departamento", "");
			$this->form_validation->set_rules("admissao", "Data de Admissão", "");
			$this->form_validation->set_rules("demissao", "Data de Demissão", "");
			$this->form_validation->set_rules("observacoes", "Observações", "nl2br");
			$this->form_validation->set_error_delimiters("", "");
			if($this->form_validation->run()){
				$this->funcionarios->editar();
			}
			$funcionario = $this->funcionarios->getFuncionario($this->uri->segment(3));
			$afastamentos = $this->afastamentos->getTiposAfastamentos()->result();
			
			$this->load->model("EmpresasModel", "empresas");
			$this->load->model("HorariosModel", "horarios");
			$this->load->model("DepartamentosModel", "departamentos");
			$this->load->model("SetoresModel", "setores");
			$empresas = $this->empresas->getEmpresasConta();
			$horarios = $this->horarios->getGrupos();
			$departamentos = $this->departamentos->getDepartamentos();
			$setores = $this->setores->getSetores();

			if($empresas->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhuma empresa cadastrada!', 'Ops');");
				redirect("/empresas");
			}else if($horarios->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum horário cadastrado!', 'Ops');");
				redirect("/horarios");
			}else if($funcionario->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Funcionário não encontrado!', 'Ops');");
				redirect("/funcionarios");
			}else{
				$this->load->view("funcionarios-editar", array(
					"funcionario"=>$funcionario->first_row(), 
					"afastamentos"=>$afastamentos,
					"empresas"=>$empresas->result(),
					"horarios"=>$horarios->result(),
					"departamentos"=>$departamentos->result(),
					"setores"=>$setores->result()
				));
			}
		}

		function select2_empresas(){
			$this->funcionarios->select2_empresas();
		}

		function select2_horarios(){
			$this->funcionarios->select2_horarios();
		}

		function select2_departamentos(){
			$this->funcionarios->select2_departamentos();
		}

		function select2_setores(){
			$this->funcionarios->select2_setores();
		}

		function atribuir_afastamento(){
			$this->form_validation->set_rules("id_funcionario", "Funcionário", "required|is_numeric");
			$this->form_validation->set_rules("id_tipo", "Afastamento", "required|is_numeric");
			$this->form_validation->set_rules("data_inicio", "Data Inicial", "required");
			$this->form_validation->set_rules("data_fim", "Data Final", "required");

			$funcionario = $this->input->post("id_funcionario");
			if($this->form_validation->run()){
				$this->afastamentos->atribuir("/funcionarios/afastamentos/".$funcionario, $funcionario);
			}else{
				redirect("/funcionarios/editar/".$funcionario);
			}
		}

		function excluir(){
			$this->form_validation->set_rules("id", "ID", "required|is_numeric");
			$this->form_validation->set_error_delimiters("", "");
			if($this->form_validation->run()){
				$this->funcionarios->excluir();
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', '".validation_errors()."');");
			}
		}

		function afastamentos(){
			$afastamentos = $this->afastamentos->getAfastamentosFuncionario()->result();
			$tipos_afastamentos = $this->afastamentos->getTiposAfastamentos()->result();
			$funcionario = $this->funcionarios->getFuncionario($this->uri->segment(3))->first_row();
			$this->load->view("funcionario-afastamentos", array("afastamentos"=>$afastamentos, "tipos_afastamentos"=>$tipos_afastamentos, "funcionario"=>$funcionario, "paginacao"=>""));
		}
	}