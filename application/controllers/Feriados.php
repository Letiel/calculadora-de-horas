<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Feriados extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("FeriadosModel", "feriados");
		}

		function index(){
			// -----------------------PAGINAÇÃO------------------------------
			$this->load->library("pagination");
			$pag = (int) $this->uri->segment(2);
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/feriados";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
			$config['full_tag_close'] 	= '</ul></nav></div>';
			$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['num_tag_close'] 	= '</span></li>';
			$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close'] 	= '</span></li>';
			$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close'] 	= '</span></li>';
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = FALSE;
			$config['uri_segment'] = 2;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->feriados->getFeriados()->num_rows();
			$this->pagination->initialize($config);
			// -----------------------PAGINAÇÃO------------------------------

			$feriados = $this->feriados->getFeriados($inicio, $maximo)->result();
			$this->load->view("feriados", array("feriados"=>$feriados, "paginacao"=>$this->pagination->create_links()));
		}

		function cadastrar(){
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("data", "Data", "required");
			if($this->form_validation->run()){
				$this->feriados->cadastrar();
			}
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresasConta()->result();
			$this->load->view("feriados-cadastrar", array("empresas"=>$empresas));
		}

		function editar(){
			$id = $this->uri->segment(3);
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("data", "Data", "required");
			if($this->form_validation->run()){
				$this->feriados->editar($id);
			}
			$this->load->model("EmpresasModel", "empresas");
			$empresas = $this->empresas->getEmpresasConta();

			$feriado = $this->feriados->getFeriado($id)->first_row();
			if($empresas->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhuma empresa cadastrada!', 'Ops');");
				redirect("/empresas");
			}else if($feriado->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum feriado encontrado!', 'Ops');");
				redirect("/feriados");
			}


			$this->load->view("feriados-editar", array("feriado"=>$feriado->first_row(), "empresas"=>$empresas->result()));
		}

		function excluir(){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$this->feriados->excluir();
			}
		}

		// function cadastrar_feriados(){
		// 	$this->feriados->inserirFeriados(2017);
		// }
	}