<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home2 extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$data = array();
		$data['calculo']['soma'] = "0000-00-00 00:00:00";
		$data['excedido']['entrada']["total"] = "00:00:00";
		$data['excedido']['saida']["total"] = "00:00:00";
		$post = $this->input->post();
		$post['dia_calculo'] = date("Y-d-m", strtotime($post['dia_calculo']));
		$post['horas_diarias'] = "00:00:00";
		$utilizar_clt   = $post['utilizar_clt'] == "false" ? false : true;
		$compensacao_diaria   = $post['compensacao_diaria'] == "false" ? false : true;
		$post['outrodia1'] = $post['outrodia1'] == "false" ? false : true;
		$post['outrodia2'] = $post['outrodia2'] == "false" ? false : true;
		$post['outrodia3'] = $post['outrodia3'] == "false" ? false : true;
		$post['outrodia4'] = $post['outrodia4'] == "false" ? false : true;
		$post['outrodia5'] = $post['outrodia5'] == "false" ? false : true;

		for ($i=1; $i <= 5; $i++) {
			$intervalo = $this->calcularIntervaloHoras($post['horario_entrada_'.$i], $post['horario_saida_'.$i]).":00";
			$post['horas_diarias'] = $this->somarHoras($intervalo, $post['horas_diarias']);
			if($post["entrada$i"] != "" && $post["saida$i"] != ""){
				$post['tolerancia_entrada_'.$i.'_antes'] = $utilizar_clt ? "00:05" : $post['tolerancia_entrada_'.$i.'_antes'];
				$data['entrada']["_$i"] = $post['dia_calculo']." ".$post["entrada$i"].":00";
				$data['saida']["_$i"] = $post['dia_calculo']." ".$post["saida$i"].":00";
				$data['saida']["_$i"] = !$post["outrodia$i"] ? $data['saida']["_$i"] : date("Y-m-d H:i:s", strtotime("+1 days", strtotime($data['saida']["_$i"])));


				if($post["entrada$i"] != "")
					$data['excedido']['entrada']["_$i"] = $this->calcularTolerancia($post["entrada$i"], $post["horario_entrada_$i"].":00", $post["tolerancia_entrada_".$i."_antes"].":00", $post["tolerancia_entrada_".$i."_depois"].":00");
				if($post["saida$i"] != "")
					$data['excedido']['saida']["_$i"] = $this->calcularTolerancia($post["saida$i"], $post["horario_saida_$i"].":00", $post["tolerancia_saida_".$i."_antes"].":00", $post["tolerancia_saida_".$i."_depois"].":00");

				// $data['excedido']['entrada']['dia'] = $data['excedido']['entrada']["$i"]['valor_real'];
				// echo $data['excedido']['saida']["$i"]['valor_real'];
				if(isset($data['excedido']['entrada']["_$i"]['valor_real'])){
					// if($data['excedido']['entrada']["$i"]['sinal'] == "+")
						$data['excedido']['entrada']["total"] = $this->somarHoras($data['excedido']['entrada']["_$i"]['valor_real'], $data['excedido']['entrada']["total"]);
				}

				if(isset($data['excedido']['saida']["_$i"]['valor_real'])){
					// if($data['excedido']['saida']["$i"]['sinal'] == "-")
						$data['excedido']['saida']["total"] = $this->somarHoras($data['excedido']['saida']["total"], $data['excedido']['saida']["_$i"]['sinal'].$data['excedido']['saida']["_$i"]['valor_real'], true);

				}

				$data['calculo']["_$i"] = $this->calcularIntervaloHoras($data["entrada"]["_$i"], $data["saida"]["_$i"], $post["outrodia$i"]);
				$data['calculo']['soma'] = $this->somarHoras($data['calculo']["_$i"].":00", $data['calculo']['soma']);
				$data["excedido"]['total'] = $this->somarHoras($data['excedido']['entrada']['total'], $data['excedido']['saida']['total']);
			}
		}
		$data['calculo']["carga_diaria"] = $post['horas_diarias'];
		$data['calculo']["final"] = $this->calcularIntervaloHoras($post['horas_diarias'], $data['calculo']['soma']).":00";

		// echo "<pre>";
		$tolerancias_compensacao = $this->calcularToleranciaCompensacao($this->forcarPositivo($data['calculo']['final']), $post['horas_diarias'], $post['tolerancia_falta_compensacao'].":00", $post['tolerancia_extra_compensacao'].":00");
		$data['compensacao']['dentro_tolerancia'] = $this->calcularCompensacao($tolerancias_compensacao, $data['calculo']['soma']);
		echo json_encode($data);
		// echo "</pre>";
	}

	public function calcularCompensacao($tolerancias, $soma_total){
		if(strtotime($soma_total) >= $tolerancias['abaixo'] && strtotime($soma_total) <= $tolerancias['acima'])
			return "1";
		else
			return "0";
	}

	public function somarHoras($entrada, $saida){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		$resultado = $acumulador2 + $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function forcarPositivo($data){
		$hora1 = explode(":",$data);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];

		$resultado = -$acumulador1;
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		return str_pad(abs($hora_ponto), 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
	}

	function forcarNegativo($data){
		$hora1 = explode(":",$data);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];

		$resultado = $acumulador1;
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		return str_pad("-".$hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
	}

	function calcularToleranciaCompensacao($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima){
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);
		
		return $horario_tolerado;
	}

	function calcularTolerancia($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima){
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);

		$array = array(
			"sinal"=>"",
			"valor"=>"",
		);
		if($entrada > strtotime($horario_padrao)){
			$array['sinal'] = "+";
			$array['valor_real'] = $this->calcularHorasToleradas(date("H:i:s", strtotime($horario_padrao)), date("H:i:s", $entrada));
		}

		if($entrada < strtotime($horario_padrao)){
			$array['sinal'] = "-";
			$array['valor_real'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", strtotime($horario_padrao)));
		}
		if($entrada > $horario_tolerado['acima']){
			$array['valor'] = $this->calcularHorasToleradas(date("H:i:s", $horario_tolerado['acima']), date("H:i:s", $entrada));
			$diferenca = $entrada - $horario_tolerado['acima'];
		}

		if($entrada < $horario_tolerado['abaixo']){
			$diferenca = $entrada - $horario_tolerado['abaixo'];
			$array['valor'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", $horario_tolerado['abaixo']));
		}
		return $array;
	}

	function calcularHorasToleradas($entrada, $saida, $outro_dia = false, $somar = false){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		if($somar){
			$resultado = $acumulador2 + $acumulador1;
		}else{
			$resultado = $acumulador2 - $acumulador1;
		}
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function calcularIntervaloHoras($entrada, $saida, $outro_dia = false, $somar = false){
		// $entrada = $entrada.":00";
		// $saida = $saida.":00";
		// if($outro_dia){
		// 	$saida = date("Y-m-d H:i:s", strtotime("+1 days", strtotime($saida)));
		// }
		$date_time  = new DateTime($entrada);
		$diff       = $date_time->diff( new DateTime($saida));
		// print_r($diff);
		// echo $diff->format('%y ano(s), %m mês(s), %d dia(s), %H hora(s), %i minuto(s) e %s segundo(s)')."<br />";
		$dias = $diff->d;
		$horas = ($dias*24) + $diff->h;
		return ($diff->invert == 1 ? "-" : "").str_pad($horas, 2, '0', STR_PAD_LEFT).":".str_pad($diff->i, 2, '0', STR_PAD_LEFT);
	}
}
