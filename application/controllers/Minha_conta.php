<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Minha_conta extends CI_Controller{
		function __construct(){
			parent::__construct();
			if(!$this->session->userdata("logado")){
				redirect("/");
			}
			$this->load->model("UsuariosModel", "usuarios");
			$this->usuarios->atualizar_sessao();
		}

		function index(){
			$post = $this->input->post();
			if(isset($post["login"])){
				
				$this->form_validation->set_rules("nome", "Nome", "required");
				if($post["login"] == $this->session->userdata("login"))
					$this->form_validation->set_rules("login", "Login", "required");
				else
					$this->form_validation->set_rules("login", "Login", "required|is_unique[usuarios.login]", array("is_unique"=>"Login já existente."));
				if(isset($post["senha_antiga"]) && isset($post["nova_senha"]))
					if(!empty($post["senha_antiga"] || !empty($post["nova_senha"]))){
						$this->form_validation->set_rules("senha_antiga", "Senha Antiga", "required");
						$this->form_validation->set_rules("nova_senha", "Nova Senha", "required");
					}

				$this->form_validation->set_error_delimiters("", "");
				if($this->form_validation->run()){
					$this->usuarios->atualizar_conta();
				}
			}
			$this->load->model("OperadoresModel", "operadores");
			$operador = $this->operadores->getOperador($this->session->userdata("id"))->first_row();
			$this->load->view("minha-conta", array("operador"=>$operador));
		}
	}