<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("ContasModel", "contas");
		$this->load->model("UsuariosModel", "usuarios");
	}
	public function index(){
		$this->form_validation->set_rules("login", "Login", "required");
		$this->form_validation->set_rules("senha", "Senha", "required");
		$this->form_validation->set_error_delimiters("", "");

		$retorno = "";
		if($this->form_validation->run()){
			if(!$this->usuarios->login())
				$retorno = "Usuário e senha não combinam!";
		}

		if($this->session->userdata("logado")){
			$this->usuarios->consultar_totais();
			$this->usuarios->atualizar_sessao();
			$conta = $this->contas->getValidadeConta()->first_row();
			$this->load->view("home", array("conta"=>$conta));
		}else{
			$this->load->view("login", array("retorno"=>$retorno));
		}
	}

	public function logout(){
		$this->session->unset_userdata(
			array(
				"id", "id_conta", "data_validade", "login", "logado"
			)
		);
		redirect("/");
	}


	public function calculo(){
		if($this->session->userdata("logado")){
			$this->load->view("calculo");
		}else{
			redirect("/");
		}
		print_r($_SESSION);
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();
		$data["erro"] = "0";
		$extra = array();
		$falta = array();

		$post["dia_calculo"] = explode("/", $post["dia_calculo"]);
		$post["dia_calculo"] = $post["dia_calculo"][2]."-".$post["dia_calculo"][1]."-".$post["dia_calculo"][0];
		$utilizar_clt   = $post['utilizar_clt'] == "false" ? false : true;
		$post['tol_diaria'] = $utilizar_clt ? "00:10:00" : $post['tol_diaria'].":00";
		$compensacao_diaria   = $post['compensacao_diaria'] == "false" ? false : true;

		for ($i=1; $i <= 5 ; $i++) {
			// horários padrões
			$data["padrao"]["entrada"]["_$i"] = $post["horario_entrada_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_entrada_$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["padrao"]["saida"]["_$i"] = $post["horario_saida_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_saida_$i"]) : new DateTime("0000-00-00 00:00:00");

			//horários informados
			$data["horario"]["entrada"]["_$i"] = $post["entrada$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["entrada$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["horario"]["saida"]["_$i"] = $post["saida$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["saida$i"]) : new DateTime("0000-00-00 00:00:00");
			$post['tolerancia_entrada_'.$i.'_antes'] = $utilizar_clt ? "00:05" : $post['tolerancia_entrada_'.$i.'_antes'];
			$post['tolerancia_entrada_'.$i.'_depois'] = $utilizar_clt ? "00:05" : $post['tolerancia_entrada_'.$i.'_depois'];
			$post['tolerancia_saida_'.$i.'_antes'] = $utilizar_clt ? "00:05" : $post['tolerancia_saida_'.$i.'_antes'];
			$post['tolerancia_saida_'.$i.'_depois'] = $utilizar_clt ? "00:05" : $post['tolerancia_saida_'.$i.'_depois'];

			$tolerancia["entrada"]["antes"]["_$i"] = $post["tolerancia_entrada_".$i."_antes"];
			$tolerancia["entrada"]["depois"]["_$i"] = $post["tolerancia_entrada_".$i."_depois"];
			$tolerancia["saida"]["antes"]["_$i"] = $post["tolerancia_saida_".$i."_antes"];
			$tolerancia["saida"]["depois"]["_$i"] = $post["tolerancia_saida_".$i."_depois"];
		}
		$tolerancia_diaria = $post["tol_diaria"];
		$tolerancia_comp_falta = $post["tolerancia_falta_compensacao"].":00";
		$tolerancia_comp_extra = $post["tolerancia_extra_compensacao"].":00";

		if($data["padrao"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
			$data["padrao"]["saida"]["_1"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_2"] < $data["padrao"]["saida"]["_1"]){
			$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_2"] < $data["padrao"]["entrada"]["_2"]){
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_3"] < $data["padrao"]["saida"]["_2"]){
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_3"] < $data["padrao"]["entrada"]["_3"]){
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_4"] < $data["padrao"]["saida"]["_3"]){
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_4"] < $data["padrao"]["entrada"]["_4"]){
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_5"] < $data["padrao"]["saida"]["_4"]){
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_5"] < $data["padrao"]["entrada"]["_5"]){
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			//
		}

		if($data["horario"]["saida"]["_1"] < $data["horario"]["entrada"]["_1"]){
			$data["horario"]["saida"]["_1"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_2"] < $data["horario"]["saida"]["_1"]){
			$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_2"] < $data["horario"]["entrada"]["_2"]){
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_3"] < $data["horario"]["saida"]["_2"]){
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_3"] < $data["horario"]["entrada"]["_3"]){
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_4"] < $data["horario"]["saida"]["_3"]){
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_4"] < $data["horario"]["entrada"]["_4"]){
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_5"] < $data["horario"]["saida"]["_4"]){
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_5"] < $data["horario"]["entrada"]["_5"]){
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		$meia_noite = new DateTime($post["dia_calculo"]." 00:00:00");
		$meia_noite->add(new DateInterval("P1D"));
		if($data["padrao"]["saida"]["_5"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_5"] > $meia_noite){
			$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_5"];
		}else if($data["padrao"]["saida"]["_4"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_4"] > $meia_noite){
			$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_4"];
		}else if($data["padrao"]["saida"]["_3"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_3"] > $meia_noite){
			$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_3"];
		}else if($data["padrao"]["saida"]["_2"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_2"] > $meia_noite){
			$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_2"];
		}else if($data["padrao"]["saida"]["_1"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_1"] > $meia_noite){
			$data["ultimo_horario"] = clone $data["padrao"]["horario"]["_1"];
		}else{
			$data["ultimo_horario"] = $meia_noite;
		}

		// --------------

		for ($i=1; $i <= 5 ; $i++) { 
			// Comparações para horários informados
			$anterior["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$anterior["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			$posterior["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$posterior["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			
			$anterior2["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$anterior2["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			$posterior2["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$posterior2["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			
			$anterior3["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$anterior3["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			$posterior3["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$posterior3["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			
			$anterior4["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$anterior4["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			$posterior4["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$posterior4["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

			$anterior5["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$anterior5["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
			$posterior5["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
			$posterior5["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

			// Adicionando e subtraindo dias
			$anterior["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$posterior["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior["saida"]["_$i"]->add(new DateInterval("P1D"));
			
			$anterior2["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior2["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior2["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior2["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$posterior2["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior2["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior2["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior2["saida"]["_$i"]->add(new DateInterval("P1D"));
			
			$anterior3["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior3["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior3["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior3["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior3["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior3["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$posterior3["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior3["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior3["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior3["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior3["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior3["saida"]["_$i"]->add(new DateInterval("P1D"));
			
			$anterior4["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior4["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$posterior4["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior4["saida"]["_$i"]->add(new DateInterval("P1D"));

			$anterior5["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["entrada"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$anterior5["saida"]["_$i"]->sub(new DateInterval("P1D"));
			$posterior5["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["entrada"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["saida"]["_$i"]->add(new DateInterval("P1D"));
			$posterior5["saida"]["_$i"]->add(new DateInterval("P1D"));
		}

		// -----------------------------------------------------------
		for ($i=1; $i <= 5 ; $i++){
			//---------------DEFININDO LOCALIZAÇÕES DOS HORÁRIOS----------------------
			$localizacao["entrada"]["$i"] = "5-1";
			$localizacao["saida"]["$i"] = "5-1";

			if( ($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_1"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_1"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_1"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_1"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_1"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_1"])){
					$localizacao["entrada"]["$i"] = "1";
			}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_2"]) || 
				($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_2"])){
					$localizacao["entrada"]["$i"] = "1-2";
			}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_2"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_2"])) && $data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
					$localizacao["entrada"]["$i"] = "2";
			}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_3"]) || 
				($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_3"])){
					$localizacao["entrada"]["$i"] = "2-3";
			}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_3"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_3"])) && $data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
					$localizacao["entrada"]["$i"] = "3";
			}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_4"]) || 
				($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_4"])){
					$localizacao["entrada"]["$i"] = "3-4";
			}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_4"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_4"])) && $data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
					$localizacao["entrada"]["$i"] = "4";
			}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_5"]) || 
				($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_5"])){
					$localizacao["entrada"]["$i"] = "4-5";
			}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_5"]) || 
				($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_5"])) && $data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
					$localizacao["entrada"]["$i"] = "5";
			}

			// ------------------------------

			if( ($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_1"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_1"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_1"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_1"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_1"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_1"])){
					$localizacao["saida"]["$i"] = "1";
			}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_2"]) || 
				($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_2"])){
					$localizacao["saida"]["$i"] = "1-2";
			}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_2"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_2"])) && $data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
					$localizacao["saida"]["$i"] = "2";
			}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_3"]) || 
				($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_3"])){
					$localizacao["saida"]["$i"] = "2-3";
			}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_3"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_3"])) && $data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
					$localizacao["saida"]["$i"] = "3";
			}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_4"]) || 
				($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_4"])){
					$localizacao["saida"]["$i"] = "3-4";
			}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_4"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_4"])) && $data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
					$localizacao["saida"]["$i"] = "4";
			}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_5"]) || 
				($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_5"])){
					$localizacao["saida"]["$i"] = "4-5";
			}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_5"]) || 
				($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_5"])) && $data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
					$localizacao["saida"]["$i"] = "5";
			}
		}

		//------------------ VALIDAÇÕES ------------------------------
		// $_POST = $data;

		$this->form_validation->set_rules("horario_entrada_1", "Horário padrão de entrada 1", "required");
		$this->form_validation->set_rules("horario_saida_1", "Horário padrão de saída 1", "required");

		$this->form_validation->set_rules("entrada1", "Horário de entrada 1", "required");
		$this->form_validation->set_rules("saida1", "Horário de saída 1", "required");

		// if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_saida_2", "Horário de saída 2", "required");
		// if($data["padrao"]["saida"]["_2"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_entrada_2", "Horário de entrada 2", "required");
		// if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_saida_3", "Horário de saída 1", "required");
		// if($data["padrao"]["saida"]["_3"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_entrada_3", "Horário de entrada 3", "required");
		// if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_saida_4", "Horário de saída 1", "required");
		// if($data["padrao"]["saida"]["_4"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_entrada_4", "Horário de entrada 4", "required");
		// if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_saida_5", "Horário de saída 1", "required");
		// if($data["padrao"]["saida"]["_5"]->format("Y") != "-0001")
		// 	$this->form_validation->set_rules("horario_entrada_5", "Horário de entrada 5", "required");

		for ($i=1; $i <= 5; $i++) { 
			if($data["padrao"]["entrada"]["_$i"]->format("Y") != "-0001")
				$this->form_validation->set_rules("horario_saida_$i", "Horário padrão de saída $i", "required");
			if($data["padrao"]["saida"]["_$i"]->format("Y") != "-0001")
				$this->form_validation->set_rules("horario_entrada_$i", "Horário padrão de entrada $i", "required");

			if($data["horario"]["entrada"]["_$i"]->format("Y") != "-0001"){
				$this->form_validation->set_rules("saida$i", "Horário de saída $i", "required");
			}
			if($data["horario"]["saida"]["_$i"]->format("Y") != "-0001"){
				$this->form_validation->set_rules("entrada$i", "Horário de entrada $i", "required");
			}

			if($data["horario"]["saida"]["_$i"] > $data["ultimo_horario"]){
				$data["erro"] = "1";
				$data["mensagem"] = "Horário de saída $i deve ser menor do que ".$data["ultimo_horario"]->format("H:i:s");
				echo json_encode($data);
				exit();
			}

			if($i < 5){
				if($data["horario"]["entrada"]["_".($i+1)]->format("Y") != "-0001"){
					if($data["horario"]["saida"]["_$i"]->format("His") > $data["horario"]["entrada"]["_".($i+1)]->format("His")){
						$data["erro"] = "1";
						$data["mensagem"] = "Horário de entrada ".($i+1)." não é válido.";
						echo json_encode($data);
						exit();
					}
				}
			}
		}

		if(!$this->form_validation->run()){
			$data["erro"] = "1";
			$data["mensagem"] = validation_errors();
			echo json_encode($data);
			exit();
		}

		
		//------------------ FIM VALIDAÇÕES ------------------------------

		//---------------CALCULANDO CARGA, HORAS EXTRAS E FALTAS-----------------
		$data["carga_horaria"] = "00:00:00";
		$turno1 = $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S");
		$turno2 = $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S");
		$turno3 = $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S");
		$turno4 = $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S");
		$turno5 = $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S");
		$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno1);
		$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno2);
		$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno3);
		$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno4);
		$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno5);

		$data["total_trabalhado"] = "00:00:00";
		$per1 = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"])->format("%H:%I:%S");
		$per2 = $data["horario"]["entrada"]["_2"]->diff($data["horario"]["saida"]["_2"])->format("%H:%I:%S");
		$per3 = $data["horario"]["entrada"]["_3"]->diff($data["horario"]["saida"]["_3"])->format("%H:%I:%S");
		$per4 = $data["horario"]["entrada"]["_4"]->diff($data["horario"]["saida"]["_4"])->format("%H:%I:%S");
		$per5 = $data["horario"]["entrada"]["_5"]->diff($data["horario"]["saida"]["_5"])->format("%H:%I:%S");
		$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per1);
		$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per2);
		$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per3);
		$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per4);
		$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per5);
		for ($i=1; $i <= 5 ; $i++){
			// echo " <br />----------------| $i |----------------- <br />";
			switch ($localizacao["entrada"]["$i"]) {//horas extras
				case "5-1"://entrada está anterior ao padrão de entrada 1
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$calc = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							break;
						case "1-2":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "2":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							break;
						case "2-3":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "3":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							break;
						case "3-4":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
							if(!$calc->invert)
								$extra[$i][] = $calc->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
							}else{
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
					}//fim switch
					break;
				case '1'://entrada está dentro do padrão 1
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($temp)->format("%H:%I:%S");
								}
							}
							break;
						case "1-2":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "2":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							break;
						case "2-3":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "3":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							break;
						case "3-4":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "1-2":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){								
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){								
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							break;
						case "2-3":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "3":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							break;
						case "3-4":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "2":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
							}
							break;
						case "2-3":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "3":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							break;
						case "3-4":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "2-3":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
							}
							break;
						case "2-3":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}else{
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							break;
						case "3-4":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "3":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}
							break;
						case "2-3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}
							}
							break;
						case "3-4":
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "4":
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "3-4":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}
							break;
						case "2-3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}
							break;
						case "3-4":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}else{
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "4":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							break;
						case "4-5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "4":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}
							break;
						case "2-3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}
							break;
						case "3-4":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "4":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								}
							}
							break;
						case "4-5":
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
						case "5":
							$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "4-5":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}
							break;
						case "2-3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}
							break;
						case "3-4":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "4":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
							}
							break;
						case "4-5":
							if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}else{
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "5":
							$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
							break;
						case "5-1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}else{
								if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
							}
							break;
					}//fim switch
					break;
				case "5":
					switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
						case "1":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
							}
							break;
						case "1-2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "2":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
							}
							break;
						case "2-3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "3":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
							}
							break;
						case "3-4":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");

							}
							break;
						case "4":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");

							}
							break;
						case "4-5":
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							}
							break;
						case "5":
							if($data["horario"]["saida"]["_$i"]->format("YmdHis") < $data["horario"]["entrada"]["_$i"]->format("YmdHis")){
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								}
							}
							break;
						case "5-1":
							$temp = clone $data["padrao"]["saida"]["_5"];
							if($data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_1"])
								$temp->sub(new DateInterval("P1D"));
							$extra[$i][] = $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
							break;
					}//fim switch
					break;
			}
			if($data["horario"]["entrada"]["_$i"]->format("Y") == "-0001")
				continue;

			/*switch($localizacao["entrada"]["$i"]){//horas faltas
				case "1":
					switch($localizacao["saida"]["$i"]){
						case "1":
							$cumpriu["1"] = true;
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."<br />";
							$temp = clone $data["horario"]["saida"]["_$i"];
							$temp->setDate($data["padrao"]["saida"]["_1"]->format("Y"), $data["padrao"]["saida"]["_1"]->format("m"), $data["padrao"]["saida"]["_1"]->format("d"));
							echo $temp->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<-<br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							}
							break;
						case "1-2":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."<br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							}
							break;
						case "2":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")."<-<br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							}
							break;
						case "2-3":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							}
							break;
						case "3":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")."<-<br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}
							break;
						case "3-4":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							}
							break;
						case "4":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")."<-<br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}
							break;
						case "4-5":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
								echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
							}
							break;
						case "5":
							echo $data["padrao"]["entrada"]["_1"]->diff($data["horario"]["entrada"]["_1"])->format("%H:%I:%S")."-><br />";
							echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")."<-<br />";
							break;
					}
					break;
				case "1-2":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
						$temp = clone $data["horario"]["saida"]["_$i"];
						$temp->setDate($data["padrao"]["saida"]["_1"]->format("Y"), $data["padrao"]["saida"]["_1"]->format("m"), $data["padrao"]["saida"]["_1"]->format("d"));
						echo $temp->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<-<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
						}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
						}
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
						}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2 <br />";
						}
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")."<-<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
						}
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3 <br />";
						}
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")."<-<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4 <br />";
						}
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")."<-<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5 <br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["horario"]["saida"]["_$i"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")."<-<br />";
						break;
					}
					break;
				case "2":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data['padrao']["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
					}
					break;
				case "2-3":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["entrada"]["_2"];
						$temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
												// echo $data['padrao']["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						}
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")."<br />";
												// $temp = clone $data["padrao"]["entrada"]["_2"];
												// $temp->setDate($data["horario"]["entrada"]["_$i"]->format("Y"), $data["horario"]["entrada"]["_$i"]->format("m"), $data["horario"]["entrada"]["_$i"]->format("d"));
												// echo $temp->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						echo $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
					}
					break;
				case "3":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_4"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_5"];
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
					}
					break;
				case "3-4":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						}
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						$temp = clone $data["padrao"]["saida"]["_4"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						$temp = clone $data["padrao"]["saida"]["_5"];
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						break;
					}
					break;
				case "4":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						echo $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
							echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						}
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						echo $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
					}
					break;
				case "4-5":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<br />";
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						break;
						case "4-5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						break;
					}
					break;
				case "5":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_4"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						$temp = clone $data["padrao"]["saida"]["_5"];
												// $temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["horario"]["entrada"]["_$i"])->format("%H:%I:%S")."-><br />";
						break;
					}
					break;
				case "5-1":
					switch($localizacao["saida"]["$i"]){
						case "1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						$temp = clone $data["padrao"]["saida"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "1-2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
						case "2":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						$temp = clone $data["padrao"]["saida"]["_2"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "2-3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
						case "3":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						$temp = clone $data["padrao"]["saida"]["_3"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "3-4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						$temp = clone $data["padrao"]["saida"]["_4"];
						$temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "4":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
						case "5":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						$temp = clone $data["padrao"]["saida"]["_5"];
												// $temp->setDate($data["horario"]["saida"]["_$i"]->format("Y"), $data["horario"]["saida"]["_$i"]->format("m"), $data["horario"]["saida"]["_$i"]->format("d"));
						echo $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S")."<-<br />";
						break;
						case "5-1":
						echo $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S")." |1<br />";
						echo $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S")." |2<br />";
						echo $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S")." |3<br />";
						echo $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S")." |4<br />";
						echo $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S")." |5<br />";
						break;
					}
					break;
			}*/
			// echo " <br />----------------| $i |----------------- <br />";
		} // Fim do For

		$data["extra"] = "00:00:00";
		$data["falta_tol"]["total"]["soma_real"] = "00:00:00";
		$data["falta_tol"]["total"]["soma"] = "00:00:00";
		$data["extra_tol"]["total"]["soma_real"] = "00:00:00";
		$data["extra_tol"]["total"]["soma"] = "00:00:00";
		for ($i=1; $i <= 5; $i++) {
			if(isset($extra[$i]))
				for ($j=0; $j < sizeof($extra[$i]); $j++) { 
					$data["extra"] = $this->somarHoras($data["extra"], $extra[$i][$j]);
				}

			if($data["horario"]["entrada"]["_$i"]->format("Y") != "-0001"){
				$data["tolerancia"]["entrada"]["_$i"] = $this->calcularTolerancia($data["horario"]["entrada"]["_$i"]->format("H:i:s"), $data["padrao"]["entrada"]["_$i"]->format("H:i:s"), $tolerancia["entrada"]["antes"]["_$i"].":00", $tolerancia["entrada"]["depois"]["_$i"].":00", "entrada");
				$data["tolerancia"]["saida"]["_$i"] = $this->calcularTolerancia($data["horario"]["saida"]["_$i"]->format("H:i:s"), $data["padrao"]["saida"]["_$i"]->format("H:i:s"), $tolerancia["saida"]["antes"]["_$i"].":00", $tolerancia["saida"]["depois"]["_$i"].":00", "saida");

				//valores reais, sem tolerancias
				$data["falta_tol"]["_$i"]["soma_real"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["falta_real"], $data["tolerancia"]["saida"]["_$i"]["falta_real"]);
				$data["falta_tol"]["total"]["soma_real"] = $this->somarHoras($data["falta_tol"]["_$i"]["soma_real"], $data["falta_tol"]["total"]["soma_real"]);

				$data["extra_tol"]["_$i"]["soma_real"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["extra_real"], $data["tolerancia"]["saida"]["_$i"]["extra_real"]);
				$data["extra_tol"]["total"]["soma_real"] = $this->somarHoras($data["extra_tol"]["_$i"]["soma_real"], $data["extra_tol"]["total"]["soma_real"]);
				
				//subtraídos das tolerancias
				$data["falta_tol"]["_$i"]["soma"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["falta"], $data["tolerancia"]["saida"]["_$i"]["falta"]);
				$data["falta_tol"]["total"]["soma"] = $this->somarHoras($data["falta_tol"]["_$i"]["soma"], $data["falta_tol"]["total"]["soma"]);

				$data["extra_tol"]["_$i"]["soma"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["extra"], $data["tolerancia"]["saida"]["_$i"]["extra"]);
				$data["extra_tol"]["total"]["soma"] = $this->somarHoras($data["extra_tol"]["_$i"]["soma"], $data["extra_tol"]["total"]["soma"]);
			}


			unset($data["horario"]["entrada"]["_$i"]);
			unset($data["horario"]["saida"]["_$i"]);
			unset($data["padrao"]["entrada"]["_$i"]);
			unset($data["padrao"]["saida"]["_$i"]);
			unset($data["ultimo_horario"]);
		}

		// $data["falta"] = "00:00:00";

		$data["total_dentro"] = $this->somarHoras($data["extra"], $data["total_trabalhado"], true);
		$data["falta"] = $this->somarHoras($data["total_dentro"], $data["carga_horaria"], true);
		if(strtotime($data["extra_tol"]["total"]["soma_real"]) > strtotime($tolerancia_diaria)){
			$data['passou_tol_diaria']['extra'] = "1";
		}else{
			$data['passou_tol_diaria']['extra'] = "0";
			$data["extra"] = $this->somarHoras($data["extra"], $data["extra_tol"]["total"]["soma_real"], true);
			$data["extra"] = $this->somarHoras($data["extra"], $data["extra_tol"]["total"]["soma"]);
		}

		if(strtotime($data["falta_tol"]["total"]["soma_real"]) > strtotime($tolerancia_diaria)){
			$data['passou_tol_diaria']['atraso'] = "1";
		}else{
			$data['passou_tol_diaria']['atraso'] = "0";
			$data["falta"] = $this->somarHoras($data["falta"], $data["falta_tol"]["total"]["soma_real"], true);
			// $data["falta"] = $this->somarHoras($data["falta"], $data["falta_tol"]["total"]["soma"]);
		}
		if($compensacao_diaria){
			if(strtotime($data["total_trabalhado"]) >= strtotime($data["carga_horaria"])){//ta fazendo extra
				$data["compensacao"] = date("H:i:s", strtotime($data["extra"]))." | ".date("H:i:s", strtotime($tolerancia_comp_extra));
				if(strtotime($data["extra"]) <= strtotime($tolerancia_comp_extra)){
					$data["extra"] = "00:00:00";
				}else{
					// $data["extra"] = $this->somarHoras($data["extra"], $data["falta"], true);
					$data["extra"] = $this->somarHoras($data["carga_horaria"], $data["total_trabalhado"], true);
				}
				$data["falta"] = "--:--:--";
			}

			if(strtotime($data["total_trabalhado"]) <= strtotime($data["carga_horaria"])){//falta
				if(strtotime($data["falta"]) <= strtotime($tolerancia_comp_falta)){
					$data["falta"] = "00:00:00";
				}else{
					// $data["falta"] = $this->somarHoras($data["falta"], $data["extra"], true);
					$data["falta"] = $this->somarHoras($data["total_trabalhado"], $data["carga_horaria"], true);
				}
				$data["extra"] = "--:--:--";
			}
		}//else{
			
		// }

		// echo "<pre>";
		// echo "<br />";
		// echo "<h2 class='text-info'>".$data["extra"]." - extra</h2>";
		// echo "<h2 class='text-success'>".$data["carga_horaria"]." - Carga-Horária</h2>";
		// echo "<h2 class='text-warning'>".$data["total_dentro"]." - Total Dentro do Horário</h2>";
		// echo "<h2 class='text-danger'>".$data["falta"]." - Falta</h2>";
		// echo "<hr />";
		// print_r($extra);
		// echo "<hr />";
		// print_r($localizacao);
		// echo "<hr />";
		// print_r($data);
		$data["localizacao"] = $localizacao;
		echo json_encode($data);
		// echo "</pre>";
	}

	public function somarHoras($entrada, $saida, $subtrair = false){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		
		if(!$subtrair)
			$resultado = $acumulador2 + $acumulador1;
		else
			$resultado = $acumulador2 - $acumulador1;

		if($resultado <= 0){
			$hora_ponto = ceil($resultado / 3600);
		}
		else{
			$hora_ponto = floor($resultado / 3600);
		}
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return str_replace("-", "", $tempo);
	}

	function calcularTolerancia($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima, $tipo){
		$nome = "";
		$d_entrada = new DateTime($entrada);
		$d_horario_padrao = new DateTime($horario_padrao);
		$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
		$entrada = strtotime($entrada);

		$diff = $d_entrada->diff($d_horario_padrao);
		$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo
		
		//calculando horário tolerado acima do horário nominal
		$segundos = 0;
		list($h, $m, $s) = explode(":", $tolerancia_acima);
		$segundos += $h * 3600;
		$segundos += $m * 60;
		$segundos += $s;
		$add = strtotime($horario_padrao) + $segundos;

		$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
		$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);

		$array = array(
			"status"=>"",
			"extra_real"=>"00:00:00",
			"falta_real"=>"00:00:00",
			"falta"=>"00:00:00",
			"extra"=>"00:00:00",
		);
		if($entrada > strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "falta";
			else
				$nome = "extra";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", strtotime($horario_padrao)), date("H:i:s", $entrada));
		}

		if($entrada < strtotime($horario_padrao)){
			if($tipo == "entrada")
				$nome = "extra";
			else
				$nome = "falta";
			$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", strtotime($horario_padrao)));
		}
		if($entrada > $horario_tolerado['acima']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $horario_tolerado['acima']), date("H:i:s", $entrada));
		}

		if($entrada < $horario_tolerado['abaixo']){
			$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", $horario_tolerado['abaixo']));
		}
		$array['status'] = $nome;
		return $array;
	}

	function calcularHorasToleradas($entrada, $saida, $outro_dia = false, $somar = false){
		$hora1 = explode(":",$entrada);
		$hora2 = explode(":",$saida);
		$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
		if($somar){
			$resultado = $acumulador2 + $acumulador1;
		}else{
			$resultado = $acumulador2 - $acumulador1;
		}
		$hora_ponto = floor($resultado / 3600);
		$resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = floor($resultado / 60);
		$resultado = $resultado - ($min_ponto * 60);
		$secs_ponto = $resultado;
		$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		return $tempo;
	}

	function calcularPorcentagemHoras($entrada, $porcentagem){
		// $entrada = "01:30:00";
	    $hora1 = explode(":",$entrada);
	    $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
	    $acumulador1 = $acumulador1 * ($porcentagem/100);
	    $resultado = $acumulador1;
	    $hora_ponto = floor($resultado / 3600);
	    $resultado = $resultado - ($hora_ponto * 3600);
		$min_ponto = abs(floor($resultado / 60));
		$resultado = abs($resultado - ($min_ponto * 60));
		$secs_ponto = $resultado;
		echo str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
	}
}