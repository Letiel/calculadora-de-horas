<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Horas_extras extends CI_Controller{
		function __construct(){
			parent::__construct();
			if(!$this->session->userdata("logado"))
				redirect("/");
			$this->load->model("HorasExtrasModel", "horas");
		}

		// function index(){
		// 	// -----------------------PAGINAÇÃO------------------------------
		// 	// $this->load->library("pagination");
		// 	// $pag = (int) $this->uri->segment(2);
		// 	// $maximo = 10;
		// 	// $inicio = ($pag == null) ? 0 : $pag;
		// 	// if($inicio > 0)
		// 	// 	$inicio = $maximo * ($inicio -1);

		// 	// $config['base_url'] = "/horas-extras";
		// 	// $config['per_page'] = $maximo;
		// 	// $config['first_link'] = '<<';
		// 	// $config['last_link'] = '>>';
		// 	// $config['next_link'] = '>';
		// 	// $config['prev_link'] = '<';   
		// 	// $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
		// 	// $config['full_tag_close'] 	= '</ul></nav></div>';
		// 	// $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		// 	// $config['num_tag_close'] 	= '</span></li>';
		// 	// $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		// 	// $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		// 	// $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		// 	// $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		// 	// $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		// 	// $config['prev_tagl_close'] 	= '</span></li>';
		// 	// $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		// 	// $config['first_tagl_close'] = '</span></li>';
		// 	// $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		// 	// $config['last_tagl_close'] 	= '</span></li>';
		// 	// $config['use_page_numbers'] = TRUE;
		// 	// $config['enable_query_strings'] = TRUE;
		// 	// $config['page_query_string'] = FALSE;
		// 	// $config['uri_segment'] = 2;
		// 	// $config['num_links'] = 3;

		// 	// $config['total_rows'] = $this->horas->getHorasExtras()->num_rows();
		// 	// $this->pagination->initialize($config);
		// 	// -----------------------PAGINAÇÃO------------------------------
		// 	// $horas = $this->horas->getHorasExtras($inicio, $maximo)->result();
		// 	// $this->load->view("horas-extras", array("horas"=>$horas, "paginacao"=>$this->pagination->create_links()));
		// }

		function cadastrar(){
			if(empty($this->uri->segment(3)) || !is_numeric($this->uri->segment(3)) || !$this->horas->existe($this->uri->segment(3))){
				$this->session->set_flashdata("retorno", "toastr.warning('Horário inválido');");
				redirect("/horarios");
			}
			$_POST["id_horario"] = $this->uri->segment(3);
			$this->form_validation->set_rules("id_horario", "Horário", "required|is_numeric");
			if(!empty($_POST["de"]) && !empty($_POST["ate"]) && !empty($_POST["coluna"])){
				$this->horas->cadastrarFaixas();
			}
			$this->load->model("HorariosModel", "horarios");
			$horarios = $this->horarios->getGrupos()->result();
			$this->load->view("horas-extras-cadastrar", array("horarios"=>$horarios));
		}

		function edit(){//aqui retorna as infos para carregar o modal de edição
			$this->form_validation->set_rules("id", "ID", "required|is_numeric");
			if($this->form_validation->run()){
				$this->horas->edit();
			}
		}

		function editar(){ //aqui efetiva a edição
			$this->form_validation->set_rules("id_horario", "<b>Horário</b>", "required|is_numeric");
			$this->form_validation->set_rules("de", "<b>De</b>", "required");
			$this->form_validation->set_rules("ate", "<b>Até</b>", "");
			$this->form_validation->set_rules("coluna", "<b>Coluna</b>", "required");
			if($this->form_validation->run()){
				$this->horas->editar();
			}else{
				$this->form_validation->set_error_delimiters("<p class='text-danger mx-auto'>", "</p>");
				echo validation_errors();
			}
		}

		function excluir(){
			$this->form_validation->set_rules("id", "ID", "required|is_numeric");
			if($this->form_validation->run()){
				$this->horas->excluir();
			}
		}
	}