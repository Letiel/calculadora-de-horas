<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Empresas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("EmpresasModel", "empresas");
		}

		function index(){
			if($this->session->userdata("logado")){
				// -----------------------PAGINAÇÃO------------------------------
				$this->load->library("pagination");
				$pag = (int) $this->uri->segment(2);
				$maximo = 10;
				$inicio = ($pag == null) ? 0 : $pag;
				if($inicio > 0)
					$inicio = $maximo * ($inicio -1);

				$config['base_url'] = "/empresas";
				$config['per_page'] = $maximo;
				$config['first_link'] = '<<';
				$config['last_link'] = '>>';
				$config['next_link'] = '>';
				$config['prev_link'] = '<';   
				$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination justify-content-end">';
				$config['full_tag_close'] 	= '</ul></nav></div>';
				$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
				$config['num_tag_close'] 	= '</span></li>';
				$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
				$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
				$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
				$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
				$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
				$config['prev_tagl_close'] 	= '</span></li>';
				$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
				$config['first_tagl_close'] = '</span></li>';
				$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
				$config['last_tagl_close'] 	= '</span></li>';
				$config['use_page_numbers'] = TRUE;
				$config['enable_query_strings'] = TRUE;
				$config['page_query_string'] = FALSE;
				$config['uri_segment'] = 2;
				$config['num_links'] = 3;

				$config['total_rows'] = $this->empresas->getEmpresasConta()->num_rows();
				$this->pagination->initialize($config);
				// -----------------------PAGINAÇÃO------------------------------
				$empresas = $this->empresas->getEmpresasConta($inicio, $maximo);
				$this->load->view("empresas", array(
					"empresas"=>$empresas->result(),
					"paginacao"=>$this->pagination->create_links()
				));
			}
			else
				redirect("/");
		}

		function cadastrar(){
			if($this->session->userdata("logado")){
				$this->form_validation->set_rules("razao_social", "Razão Social", "required");
				$this->form_validation->set_rules("nome_fantasia", "Nome Fantasia", "required");
				$this->form_validation->set_rules("cnpj", "CNPJ", "required");
				$this->form_validation->set_rules("id_cidade", "Cidade", "required|is_numeric");
				$this->form_validation->set_error_delimiters("", "");
				if($this->form_validation->run()){
					$this->empresas->cadastrar();
				}
				$this->load->view("empresas-cadastrar");
			}else
				redirect("/");
		}

		function editar(){
			if($this->session->userdata("logado")){
				$this->form_validation->set_rules("razao_social", "Razão Social", "required");
				$this->form_validation->set_rules("nome_fantasia", "Nome Fantasia", "required");
				$this->form_validation->set_rules("cnpj", "CNPJ", "required");
				$this->form_validation->set_rules("id_cidade", "Cidade", "required|is_numeric");
				$this->form_validation->set_error_delimiters("", "");
				$empresa = (int) $this->uri->segment(3);
				if($this->form_validation->run()){
					$this->empresas->editar();
				}
				$empresa = $this->empresas->getEmpresa($empresa);
				if($empresa->num_rows() == 0){
					$this->session->set_flashdata("retorno", "toastr.error('Empresa não encontrada!', 'Ops');");
					redirect("/empresas");
				}else{
					$this->load->view("empresas-editar", array("empresa"=>$empresa->first_row()));
				}
			}else
				redirect("/");
		}

		function excluir(){
			if($this->session->userdata("logado")){
				$this->empresas->excluir();
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
				echo '<meta http-equiv="refresh" content="0">';
			}
		}

		function select2_cidades(){
			if($this->session->userdata("logado")){
				$this->empresas->select2_cidades();
			}
		}
	}