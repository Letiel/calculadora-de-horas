<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();

		$post["dia_calculo"] = explode("/", $post["dia_calculo"]);
		$post["dia_calculo"] = $post["dia_calculo"][2]."-".$post["dia_calculo"][1]."-".$post["dia_calculo"][0];

		for ($i=1; $i <= 5 ; $i++) {
			// horários padrões
			$data["padrao"]["entrada"]["_$i"] = $post["horario_entrada_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_entrada_$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["padrao"]["saida"]["_$i"] = $post["horario_saida_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_saida_$i"]) : new DateTime("0000-00-00 00:00:00");

			//horários informados
			$data["horario"]["entrada"]["_$i"] = $post["entrada$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["entrada$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["horario"]["saida"]["_$i"] = $post["saida$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["saida$i"]) : new DateTime("0000-00-00 00:00:00");
		}

		if($data["padrao"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
			$data["padrao"]["saida"]["_1"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_2"] < $data["padrao"]["saida"]["_1"]){
			$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_2"] < $data["padrao"]["entrada"]["_2"]){
			$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_3"] < $data["padrao"]["saida"]["_2"]){
			$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_3"] < $data["padrao"]["entrada"]["_3"]){
			$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_4"] < $data["padrao"]["saida"]["_3"]){
			$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_4"] < $data["padrao"]["entrada"]["_4"]){
			$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["entrada"]["_5"] < $data["padrao"]["saida"]["_4"]){
			$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["padrao"]["saida"]["_5"] < $data["padrao"]["entrada"]["_5"]){
			$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			//
		}

		if($data["horario"]["saida"]["_1"] < $data["horario"]["entrada"]["_1"]){
			$data["horario"]["saida"]["_1"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_2"] < $data["horario"]["saida"]["_1"]){
			$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_2"] < $data["horario"]["entrada"]["_2"]){
			$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_3"] < $data["horario"]["saida"]["_2"]){
			$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_3"] < $data["horario"]["entrada"]["_3"]){
			$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_4"] < $data["horario"]["saida"]["_3"]){
			$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_4"] < $data["horario"]["entrada"]["_4"]){
			$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["entrada"]["_5"] < $data["horario"]["saida"]["_4"]){
			$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["saida"]["_5"] < $data["horario"]["entrada"]["_5"]){
			$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			//
		}

		//-----------------------CALCULANDO HORAS EXTRAS-------------------------
		if($data["padrao"]["saida"]["_5"]->format("Y") != "-0001")
			$ultimo_horario = clone $data["padrao"]["saida"]["_5"];
		else if($data["padrao"]["saida"]["_4"]->format("Y") != "-0001")
			$ultimo_horario = clone $data["padrao"]["saida"]["_4"];
		else if($data["padrao"]["saida"]["_3"]->format("Y") != "-0001")
			$ultimo_horario = clone $data["padrao"]["saida"]["_3"];
		else if($data["padrao"]["saida"]["_2"]->format("Y") != "-0001")
			$ultimo_horario = clone $data["padrao"]["saida"]["_2"];
		else if($data["padrao"]["saida"]["_1"]->format("Y") != "-0001")
			$ultimo_horario = clone $data["padrao"]["saida"]["_1"];

		if($data["horario"]["entrada"]["_1"] < $data["padrao"]["entrada"]["_1"]){//é menor do que padrão de entrada 1
				
			$ultimo_horario->setDate($data["horario"]["entrada"]["_1"]->format("Y"), $data["horario"]["entrada"]["_1"]->format("m"), $data["horario"]["entrada"]["_1"]->format("d")-1);
			if($data["horario"]["entrada"]["_1"] > $ultimo_horario){//é maior do que padrão de saída 5... (estão no intervalo dos dias)
				if($data["horario"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
					$res = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"]);
					if(!$res->invert)
						echo $res->format("%H:%I:%S")."<br />";
				}else{
					$res = $data["horario"]["entrada"]["_1"]->diff($data["padrao"]["entrada"]["_1"]);
					if(!$res->invert)
						echo $res->format("%H:%I:%S")."<br />";
					if($data["horario"]["saida"]["_1"] > $data["padrao"]["saida"]["_1"]){//se ele saiu depois do padrão de saída 1
						if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_2"] || $data["padrao"]["entrada"]["_2"]->format("Y") == "-0001"){
							$res = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_1"]);
							if(!$res->invert)
								echo $res->format("%H:%I:%S")."<br />";
						}else{
							$res = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"]);
							if(!$res->invert)
								echo $res->format("%H:%I:%S")."<br />";
							if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_3"] || $data["padrao"]["entrada"]["_3"]->format("Y") == "-0001"){
								$res = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_1"]);
								if(!$res->invert)
									echo $res->format("%H:%I:%S")."<br />";
							}else{
								$res = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"]);
								if(!$res->invert)
									echo $res->format("%H:%I:%S")."<br />";
								if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_4"] || $data["padrao"]["entrada"]["_4"]->format("Y") == "-0001"){
									$res = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_1"]);
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."<br />";
								}else{
									$res = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"]);
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."<br />";
									if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_5"] || $data["padrao"]["entrada"]["_5"]->format("Y") == "-0001"){
										$res = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_1"]);
										if(!$res->invert)
											echo $res->format("%H:%I:%S")."<br />";
									}else{
										$res = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"]);
										if(!$res->invert)
											echo $res->format("%H:%I:%S")."<br />";
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
										if($data["horario"]["saida"]["_1"] <= $temp || $data["padrao"]["saida"]["_5"]->format("Y") == "-0001"){
											$res = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_1"]);
											if(!$res->invert)
												echo $res->format("%H:%I:%S")."<br />";
										}else{
											$res = $data["padrao"]["saida"]["_5"]->diff($temp);
											if(!$res->invert)
												echo $res->format("%H:%I:%S")."<br />";
										}
									}
								}
							}
						}
					}
				}
			}else{//é menor do que padrão de saída 5 OU padrão de saída 5 não existe
				if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_1"]){//saída menor do que padrão de entrada 1
					echo $ultimo_horario->diff($data["horario"]["saida"]["_1"])->format("%H:%I:%S")."<br />";
				}else{//se horário de saída 1 é maior do que padrão de entrada 1 imprime o intervalo dos horários padrões
					echo $ultimo_horario->diff($data["padrao"]["entrada"]["_1"])->format("%H:%I:%S")."<br />";
				}
			}
		}else{//horário de entrada 1 é maior do que padrão de entrada 1
			if($data["horario"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
				$res = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"]);
				if(!$res->invert)
					echo $res->format("%H:%I:%S")."a<br />";
			}else{
				$res = $data["horario"]["entrada"]["_1"]->diff($data["padrao"]["entrada"]["_1"]);
				if(!$res->invert)
					echo $res->format("%H:%I:%S")."b<br />";
				if($data["horario"]["saida"]["_1"] > $data["padrao"]["saida"]["_1"]){//se ele saiu depois do padrão de saída 1
					if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_2"] || $data["padrao"]["entrada"]["_2"]->format("Y") == "-0001"){
						$temp = $data["padrao"]["entrada"]["_1"];
						$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
						if($data["horario"]["saida"]["_1"] > $temp){
							$res = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_1"]);
						}else{
							$res = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_1"]);
						}
						if(!$res->invert)
							echo $res->format("%H:%I:%S")."c<br />";
					}else{
						if($data["horario"]["entrada"]["_1"] < $data["padrao"]["saida"]["_1"]){
							$res = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"]);
							if(!$res->invert)
								echo $res->format("%H:%I:%S")."d<br />";
						}else if($data["horario"]["entrada"]["_1"] < $data["padrao"]["entrada"]["_2"]){
							$temp = clone $data["padrao"]["entrada"]["_2"];
							$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
							$res = $data["horario"]["entrada"]["_1"]->diff($temp);
							if(!$res->invert)
								echo $res->format("%H:%I:%S")."e<br />";
						}
						if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_3"] || $data["padrao"]["entrada"]["_3"]->format("Y") == "-0001"){
							$temp = clone $data["padrao"]["entrada"]["_1"];
							$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
							if($data['horario']["saida"]["_1"]->format("Ymd") == $data['horario']["entrada"]["_1"]->format("Ymd")){
								$temp->add(new DateInterval("P1D"));
							}
							if($data["horario"]["saida"]["_1"] > $temp){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->add(new DateInterval("P1D"));
								$res = $data["padrao"]["saida"]["_2"]->diff($temp);
							}else{
								if($data["horario"]["entrada"]["_1"] > $data["padrao"]["saida"]["_2"]){
									$res = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"]);
								}else{
									$res = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_1"]);
								}
							}
							if(!$res->invert)
								echo $res->format("%H:%I:%S")."f<br />";
						}else{
							if($data["horario"]["entrada"]["_1"] < $data["padrao"]["saida"]["_2"]){
								$res = $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_2"]);
								if(!$res->invert)
									echo $res->format("%H:%I:%S")."ff<br />";
							}else if($data["horario"]["entrada"]["_1"] < $data["padrao"]["entrada"]["_3"]){
								$temp = clone $data["padrao"]["entrada"]["_3"];
								$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
								$res = $data["horario"]["entrada"]["_1"]->diff($temp);
								if(!$res->invert)
									echo $res->format("%H:%I:%S")."g<br />";
							}
							// $res = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"]);
							// if(!$res->invert)
							// 	echo $res->format("%H:%I:%S")."<br />";
							if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_4"] || $data["padrao"]["entrada"]["_4"]->format("Y") == "-0001"){
								$temp = clone $data["padrao"]["entrada"]["_1"];
								$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
								if($data["horario"]["saida"]["_1"] > $temp){
									$res = $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_3"]);
								}else{
									if($data["horario"]["entrada"]["_1"] > $data["padrao"]["saida"]["_3"]){
										$res = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"]);
									}else{
										$res = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_1"]);
									}
								}
								if(!$res->invert)
									echo $res->format("%H:%I:%S")."h<br />";
							}else{
								if($data["horario"]["entrada"]["_1"] < $data["padrao"]["saida"]["_3"]){
									$res = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"]);
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."i<br />";
								}else{
									$temp = clone $data["padrao"]["entrada"]["_4"];
									$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
									$res = $data["horario"]["entrada"]["_1"]->diff($temp);
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."j<br />";
								}
								// $res = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"]);
								// if(!$res->invert)
								// 	echo $res->format("%H:%I:%S")."<br />";
								if($data["horario"]["saida"]["_1"] <= $data["padrao"]["entrada"]["_5"] || $data["padrao"]["entrada"]["_5"]->format("Y") == "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
									if($data['horario']["saida"]["_1"]->format("Ymd") == $data['horario']["entrada"]["_1"]->format("Ymd")){
										$temp->add(new DateInterval("P1D"));
									}//aqui
									if($data["horario"]["saida"]["_1"] > $temp){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$res = $data["padrao"]["saida"]["_4"]->diff($temp);
										echo "piu";
									}else{
										if($data["horario"]["entrada"]["_1"] > $data["padrao"]["saida"]["_4"]){
											$res = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"]);
											echo "roin roin";
										}else{
											$res = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_1"]);
											echo "pruu";
										}
									}
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."k<br />";
								}else{
									$res = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"]);
									if(!$res->invert)
										echo $res->format("%H:%I:%S")."<br />";
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->setDate($data["horario"]["saida"]["_1"]->format("Y"), $data["horario"]["saida"]["_1"]->format("m"), $data["horario"]["saida"]["_1"]->format("d"));
									if($data["horario"]["saida"]["_1"] <= $temp || $data["padrao"]["saida"]["_5"]->format("Y") == "-0001"){
										$res = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_1"]);
										if(!$res->invert)
											echo $res->format("%H:%I:%S")."l<br />";
									}else{
										$res = $data["padrao"]["saida"]["_5"]->diff($temp);
										if(!$res->invert)
											echo $res->format("%H:%I:%S")."m<br />";
									}
								}
							}
						}
					}
				}
			}
		}


		echo "<pre>";
		echo "<br />";
		print_r($data);
		echo "</pre>";
	}
}