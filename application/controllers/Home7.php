<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index(){
		$this->load->view('home');
	}

	public function calcular(){
		$post = $this->input->post();
		$data = array();
		$data["horario_expediente"] = "00:00:00";
		$data["horario_final_dia"] = "";

		$post["dia_calculo"] = explode("/", $post["dia_calculo"]);
		$post["dia_calculo"] = $post["dia_calculo"][2]."-".$post["dia_calculo"][1]."-".$post["dia_calculo"][0];

		for ($i=1; $i <= 5 ; $i++) {
			// horários padrões
			$data["padrao"]["_$i"]["entrada"] = $post["horario_entrada_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_entrada_$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["padrao"]["_$i"]["saida"] = $post["horario_saida_$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["horario_saida_$i"]) : new DateTime("0000-00-00 00:00:00");

			//horários informados
			$data["horario"]["_$i"]["entrada"] = $post["entrada$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["entrada$i"]) : new DateTime("0000-00-00 00:00:00");
			$data["horario"]["_$i"]["saida"] = $post["saida$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["saida$i"]) : new DateTime("0000-00-00 00:00:00");
		}
		
		//intervalos
		if($data["padrao"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_2"]["entrada"]->format("Y") != "-0001"){
			if($data["padrao"]["_2"]["entrada"] < $data["padrao"]["_1"]["saida"]){
				$data["padrao"]["_2"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_3"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_4"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_2"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_3"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_4"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["saida"]->add(new DateInterval('P1D'));
			}
			$data["intervalo"]["1_2"] = new DateTime($data["padrao"]["_2"]["entrada"]->diff($data["padrao"]["_1"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
		}else{
			$data["intervalo"]["1_2"] = new DateTime("0000-00-00 00:00:00");
		}
		if($data["padrao"]["_2"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_3"]["entrada"]->format("Y") != "-0001"){
			if($data["padrao"]["_3"]["entrada"] < $data["padrao"]["_2"]["saida"]){
				$data["padrao"]["_3"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_4"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_3"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_4"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["saida"]->add(new DateInterval('P1D'));
			}
			$data["intervalo"]["2_3"] = new DateTime($data["padrao"]["_3"]["entrada"]->diff($data["padrao"]["_2"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
		}else{
			$data["intervalo"]["2_3"] = new DateTime("0000-00-00 00:00:00");
		}
		if($data["padrao"]["_3"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_4"]["entrada"]->format("Y") != "-0001"){
			if($data["padrao"]["_4"]["entrada"] < $data["padrao"]["_3"]["saida"]){
				$data["padrao"]["_4"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_4"]["saida"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["saida"]->add(new DateInterval('P1D'));
			}
			$data["intervalo"]["3_4"] = new DateTime($data["padrao"]["_4"]["entrada"]->diff($data["padrao"]["_3"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
		}else{
			$data["intervalo"]["3_4"] = new DateTime("0000-00-00 00:00:00");
		}
		if($data["padrao"]["_4"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_5"]["entrada"]->format("Y") != "-0001"){
			if($data["padrao"]["_5"]["entrada"] < $data["padrao"]["_4"]["saida"]){
				$data["padrao"]["_5"]["entrada"]->add(new DateInterval('P1D'));
				$data["padrao"]["_5"]["saida"]->add(new DateInterval('P1D'));
			}
			$data["intervalo"]["4_5"] = new DateTime($data["padrao"]["_5"]["entrada"]->diff($data["padrao"]["_4"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
		}else{
			$data["intervalo"]["4_5"] = new DateTime("0000-00-00 00:00:00");
		}

		if($data["padrao"]["_5"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["entrada"]->format("Y") != "-0001"){
			$temp = clone $data["padrao"]["_1"]["entrada"];
			if($temp < $data["padrao"]["_5"]["saida"]){
				$temp->add(new DateInterval('P1D'));
			}
			$data["intervalo"]["final_inicio"] = new DateTime($temp->diff($data["padrao"]["_5"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
		}else{
			if($data["padrao"]["_4"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["entrada"]->format("Y") != "-0001"){
				$temp = clone $data["padrao"]["_1"]["entrada"];
				if($temp < $data["padrao"]["_4"]["saida"]){
					$temp->add(new DateInterval('P1D'));
				}
				$data["intervalo"]["final_inicio"] = new DateTime($temp->diff($data["padrao"]["_4"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
			}else{
				if($data["padrao"]["_3"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["entrada"]->format("Y") != "-0001"){
					$temp = clone $data["padrao"]["_1"]["entrada"];
					if($temp < $data["padrao"]["_3"]["saida"]){
						$temp->add(new DateInterval('P1D'));
					}
					$data["intervalo"]["final_inicio"] = new DateTime($temp->diff($data["padrao"]["_3"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
				}else{
					if($data["padrao"]["_2"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["entrada"]->format("Y") != "-0001"){
						$temp = clone $data["padrao"]["_1"]["entrada"];
						if($temp < $data["padrao"]["_2"]["saida"]){
							$temp->add(new DateInterval('P1D'));
						}
						$data["intervalo"]["final_inicio"] = new DateTime($temp->diff($data["padrao"]["_2"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
					}else{
						if($data["padrao"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["entrada"]->format("Y") != "-0001"){
							$temp = clone $data["padrao"]["_1"]["entrada"];
							if($temp < $data["padrao"]["_1"]["saida"]){
								$temp->add(new DateInterval('P1D'));
							}
							$data["intervalo"]["final_inicio"] = new DateTime($temp->diff($data["padrao"]["_1"]["saida"])->format("%Y-%m-%d %H:%i:%s"));
						}else{
							$data["intervalo"]["final_inicio"] = new DateTime("0000-00-00 00:00:00");
						}
					}
				}
			}
		}

		//-----------------------CALCULANDO HORAS EXTRAS-------------------------
		if($data["horario"]["_1"]["entrada"] >= $data["horario"]["_1"]["saida"]){
			$data["horario"]["_1"]["saida"]->add(new DateInterval('P1D'));
		}

		if($data["horario"]["_1"]["entrada"] < $data["padrao"]["_1"]["entrada"]){
			if($data["horario"]["_1"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_5"]["saida"]->format("Y") != "-0001"){
					$temp = clone $data["padrao"]["_5"]["saida"];
					$temp->setDate($data["horario"]["_1"]["entrada"]->format("Y"), $data["horario"]["_1"]["entrada"]->format("m"), $data["horario"]["_1"]["entrada"]->format("d"));
					if($data["horario"]["_1"]["entrada"] < $temp){
						if($data["horario"]["_1"]["entrada"] < $data["padrao"]["_5"]["saida"]){
							if($data["horario"]["_1"]["saida"] >= $data["padrao"]["_1"]["entrada"]){
								echo $data["intervalo"]["final_inicio"]->format("H:i:s")."|<";
							}else{
								$temp = clone $data["padrao"]["_5"]["saida"];
								$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
								echo $data["horario"]["_1"]["saida"]->diff($temp)->format("%H:%I:%S");
							}
						}else{
							echo $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_5"]["saida"])->format("%H:%I:%S");
						}
					}else{
						if($data["horario"]["_1"]["saida"] < $data["padrao"]["_1"]["entrada"]){
							$temp = $data["horario"]["_1"]["entrada"]->diff($data["horario"]["_1"]["saida"]);
							if(!$temp->invert)
								echo $temp->format("%H:%I:%S").">>>";
						}else{
							$temp = $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_1"]["entrada"]);
							if(!$temp->invert)
								echo $temp->format("%H:%I:%S");
						}
					}
				echo "<br />";
			}else{
				if($data["horario"]["_1"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_4"]["saida"]->format("Y") != "-0001"){
						$temp = clone $data["padrao"]["_4"]["saida"];
						$temp->setDate($data["horario"]["_1"]["entrada"]->format("Y"), $data["horario"]["_1"]["entrada"]->format("m"), $data["horario"]["_1"]["entrada"]->format("d"));
						if($data["horario"]["_1"]["entrada"] > $temp){
							echo $data["intervalo"]["final_inicio"]->format("H:i:s")."|<";
						}else{
							$temp = $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_1"]["entrada"]);
							if(!$temp->invert)
								echo $temp->format("%H:%I:%S");
						}
					echo "<br />";
				}else{
					if($data["horario"]["_1"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_3"]["saida"]->format("Y") != "-0001"){
							$temp = clone $data["padrao"]["_3"]["saida"];
							$temp->setDate($data["horario"]["_1"]["entrada"]->format("Y"), $data["horario"]["_1"]["entrada"]->format("m"), $data["horario"]["_1"]["entrada"]->format("d"));
							if($data["horario"]["_1"]["entrada"] > $temp){
								echo $data["intervalo"]["final_inicio"]->format("H:i:s")."|<";
							}else{
								$temp = $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_1"]["entrada"]);
								if(!$temp->invert)
									echo $temp->format("%H:%I:%S");
							}
						echo "<br />";
					}else{
						if($data["horario"]["_1"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_2"]["saida"]->format("Y") != "-0001"){
								$temp = clone $data["padrao"]["_2"]["saida"];
								$temp->setDate($data["horario"]["_1"]["entrada"]->format("Y"), $data["horario"]["_1"]["entrada"]->format("m"), $data["horario"]["_1"]["entrada"]->format("d"));
								if($data["horario"]["_1"]["entrada"] > $temp){
									echo $data["intervalo"]["final_inicio"]->format("H:i:s")."|<";
								}else{
									$temp = $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_1"]["entrada"]);
									if(!$temp->invert)
										echo $temp->format("%H:%I:%S");
								}
							echo "<br />";
						}else{
							if($data["horario"]["_1"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_1"]["saida"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["_1"]["saida"];
									$temp->setDate($data["horario"]["_1"]["entrada"]->format("Y"), $data["horario"]["_1"]["entrada"]->format("m"), $data["horario"]["_1"]["entrada"]->format("d"));
									if($data["horario"]["_1"]["entrada"] > $temp){
										echo $data["intervalo"]["final_inicio"]->format("H:i:s")."|<";
									}else{
										$temp = $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_1"]["entrada"]);
										if(!$temp->invert)
											echo $temp->format("%H:%I:%S");
									}
								echo "<br />";
							}
						}
					}
				}
			}
		}
		echo "<hr />";//até aqui calculando horas extras antes do horário padrão 1

		$temp = clone $data["padrao"]["_2"]["entrada"];
		// $temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));

		if($temp->format("Y-m-d") == $data["padrao"]["_2"]["entrada"]->format("Y-m-d") && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_2"]["entrada"]){
			if($data["horario"]["_1"]["saida"] >= $temp){
				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_1"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_1"]["saida"])
					echo $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_2"]["entrada"])->format("%H:%I:%S");
				else
					echo $data["intervalo"]["1_2"]->format("H:i:s");
			}else{
				if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["saida"]->format("Y") != "-0001"){
					if($data["horario"]["_1"]["saida"] < $data["padrao"]["_2"]["entrada"] && $data["horario"]["_1"]["saida"] > $data["padrao"]["_1"]["saida"])
						echo $data["horario"]["_1"]["saida"]->diff($data["padrao"]["_1"]["saida"])->format("%H:%I:%S")."<-";
					else{
						$diferenca = $data["padrao"]["_1"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."|||";
						}
					}
				}
			}
		}else{
			if($data["padrao"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_2"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_2"]["entrada"]->format("Y") == "-0001" && $data["padrao"]["_3"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_3"]["entrada"]->format("Y") == "-0001" && $data["padrao"]["_4"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_4"]["entrada"]->format("Y") == "-0001" && $data["padrao"]["_5"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_5"]["entrada"]->format("Y") == "-0001"){
				$temp = clone $data["padrao"]["_1"]["entrada"];
				$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
				if($data["horario"]["_1"]["saida"] >= $temp){
					if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_1"]["saida"]){
						$diferenca = $data["horario"]["_1"]["entrada"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."{}";
						}
					}else{
						$diferenca = $data["padrao"]["_1"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."()";
						}
					}
				}else{
					$temp = clone $data["padrao"]["_1"]["entrada"];
					$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
					$diferenca = $data["padrao"]["_1"]["saida"]->diff($temp);
					// if(!$diferenca->invert){
						echo $diferenca->format("%H:%I:%S")."[]";
					// }
				}
			}
		}
		echo "<br />";

		$temp = clone $data["padrao"]["_3"]["entrada"];
		// $temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		if($temp->format("Y-m-d") == $data["padrao"]["_3"]["entrada"]->format("Y-m-d") && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_3"]["entrada"]){
			if($data["horario"]["_1"]["saida"] >= $temp && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_3"]["entrada"]){
				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_2"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_2"]["saida"])
					echo $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_3"]["entrada"])->format("%H:%I:%S")."|";
				else
					echo $data["intervalo"]["2_3"]->format("H:i:s")." --";
			}else{
				if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_2"]["saida"]->format("Y") != "-0001"){
					if($data["horario"]["_1"]["saida"] < $data["padrao"]["_3"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_2"]["saida"] && $data["horario"]["_1"]["saida"] > $data["padrao"]["_1"]["saida"])
						echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S")."<-";
					else{
						$diferenca = $data["padrao"]["_2"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."===";
						}
					}
				}
			}
		}else{
			// if($data["padrao"]["_2"]["saida"]->format("Y") != "-0001"){
			// 	$diferenca = $data["padrao"]["_2"]["saida"]->diff($data["horario"]["_1"]["saida"]);
			// 	if(!$diferenca->invert){
			// 		echo $diferenca->format("%H:%I:%S");
			// 	}
			// }

			if($data["padrao"]["_2"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_2"]["entrada"]->format("Y") != "-0001" && $data["padrao"]["_3"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_3"]["entrada"]->format("Y") == "-0001" && $data["padrao"]["_4"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_4"]["entrada"]->format("Y") == "-0001" && $data["padrao"]["_5"]["saida"]->format("Y") == "-0001" && $data["padrao"]["_5"]["entrada"]->format("Y") == "-0001"){
				$temp = clone $data["padrao"]["_2"]["entrada"];
				$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
				if($data["horario"]["_1"]["saida"] >= $temp){
					if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_2"]["saida"]){
						$diferenca = $data["horario"]["_1"]["entrada"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."{}";
						}
					}else{
						$diferenca = $data["padrao"]["_2"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."()";
						}
					}
				}else{
					$temp = clone $data["padrao"]["_2"]["entrada"];
					$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
					$diferenca = $data["padrao"]["_2"]["saida"]->diff($temp);
					// if(!$diferenca->invert){
						echo $diferenca->format("%H:%I:%S")."[]{{";
					// }
				}
			}
		}
		echo "<br />";

		$temp = clone $data["padrao"]["_4"]["entrada"];
		// $temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		if($temp->format("Y-m-d") == $data["padrao"]["_4"]["entrada"]->format("Y-m-d") && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_4"]["entrada"]){
			if($data["horario"]["_1"]["saida"] >= $temp && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_4"]["entrada"]){
				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_3"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_3"]["saida"])
					echo $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_4"]["entrada"])->format("%H:%I:%S");
				else
					echo $data["intervalo"]["3_4"]->format("H:i:s");
			}else{
				if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_3"]["saida"]->format("Y") != "-0001"){
					if($data["horario"]["_1"]["saida"] < $data["padrao"]["_4"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_3"]["saida"] && $data["horario"]["_1"]["saida"] > $data["padrao"]["_1"]["saida"])
						echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S")."<--";
					else{
						$diferenca = $data["padrao"]["_3"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."??";
						}
					}
				}
			}
		}else{
			if($data["padrao"]["_3"]["saida"]->format("Y") != "-0001"){
				$diferenca = $data["padrao"]["_3"]["saida"]->diff($data["horario"]["_1"]["saida"]);
				if(!$diferenca->invert){
					echo $diferenca->format("%H:%I:%S");
				}
			}

			
		}
		echo "<br />";

		$temp = clone $data["padrao"]["_5"]["entrada"];
		// $temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		if($temp->format("Y-m-d") == $data["padrao"]["_5"]["entrada"]->format("Y-m-d") && $data["horario"]["_1"]["entrada"] < $data["padrao"]["_5"]["entrada"]){
			if($data["horario"]["_1"]["saida"] > $data["padrao"]["_5"]["entrada"]){
				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_4"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_4"]["saida"])
					echo $data["horario"]["_1"]["entrada"]->diff($data["padrao"]["_5"]["entrada"])->format("%H:%I:%S");
				else
					echo $data["intervalo"]["4_5"]->format("H:i:s");
			}else{
				if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_4"]["saida"]->format("Y") != "-0001"){
					if($data["horario"]["_1"]["saida"] < $data["padrao"]["_5"]["entrada"] && $data["horario"]["_1"]["entrada"] > $data["padrao"]["_4"]["saida"] && $data["horario"]["_1"]["saida"] > $data["padrao"]["_1"]["saida"])
						echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S")."<--";
					else{
						$diferenca = $data["padrao"]["_4"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$diferenca->invert){
							echo $diferenca->format("%H:%I:%S")."?";
						}
					}
				}
			}
		}else{
			if($data["padrao"]["_4"]["saida"]->format("Y") != "-0001"){
				$diferenca = $data["padrao"]["_4"]["saida"]->diff($data["horario"]["_1"]["saida"]);
				if(!$diferenca->invert){
					echo $diferenca->format("%H:%I:%S");
				}
			}
		}
		echo "<br />";

		if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_5"]["saida"]->format("Y") != "-0001"){
			$temp = clone $data["horario"]["_1"]["saida"];
			if($data["horario"]["_1"]["saida"]->format("Ymd") > $data["horario"]["_1"]["entrada"]->format("Ymd"))
				$temp->sub(new DateInterval('P1D'));//diminuindo para fazer o teste
			if($temp <= $data["horario"]["_1"]["entrada"]){
				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_5"]["saida"]){
					echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S");
				}else{
					$temp = clone $data["padrao"]["_1"]["entrada"];
					$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
					if($data["horario"]["_1"]["saida"] > $temp){
						echo $data["intervalo"]["final_inicio"]->format("H:i:s");
					}else{
						$passou_final = $data["padrao"]["_5"]["saida"]->diff($data["horario"]["_1"]["saida"]);
						if(!$passou_final->invert)
							echo $passou_final->format("%H:%I:%S")." passados da saída 5";					
					}
				}
				echo "<br />";
			}
		}//else{
		// 	if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_4"]["saida"]->format("Y") != "-0001"){
		// 		$temp = clone $data["horario"]["_1"]["saida"];
		// 		if($data["horario"]["_1"]["saida"]->format("Ymd") > $data["horario"]["_1"]["entrada"]->format("Ymd"))
		// 			$temp->sub(new DateInterval('P1D'));//diminuindo para fazer o teste
		// 		if($temp <= $data["horario"]["_1"]["entrada"]){
		// 			if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_4"]["saida"]){
		// 				echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S");
		// 			}else{
		// 				$temp = clone $data["padrao"]["_1"]["entrada"];
		// 				$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		// 				if($data["horario"]["_1"]["saida"] > $temp){
		// 					echo $data["intervalo"]["final_inicio"]->format("H:i:s");
		// 				}else{
		// 					$passou_final = $data["padrao"]["_4"]["saida"]->diff($data["horario"]["_1"]["saida"]);
		// 					if(!$passou_final->invert)
		// 						echo $passou_final->format("%H:%I:%S")." passados da saída 4";					
		// 				}
		// 			}
		// 			echo "<br />";
		// 		}
		// 	}else{
		// 		if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_3"]["saida"]->format("Y") != "-0001"){
		// 			$temp = clone $data["horario"]["_1"]["saida"];
		// 			if($data["horario"]["_1"]["saida"]->format("Ymd") > $data["horario"]["_1"]["entrada"]->format("Ymd"))
		// 				$temp->sub(new DateInterval('P1D'));//diminuindo para fazer o teste
		// 			if($temp <= $data["horario"]["_1"]["entrada"]){
		// 				if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_3"]["saida"]){
		// 					echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S");
		// 				}else{
		// 					$temp = clone $data["padrao"]["_1"]["entrada"];
		// 					$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		// 					if($data["horario"]["_1"]["saida"] > $temp){
		// 						echo $data["intervalo"]["final_inicio"]->format("H:i:s");
		// 					}else{
		// 						$passou_final = $data["padrao"]["_3"]["saida"]->diff($data["horario"]["_1"]["saida"]);
		// 						if(!$passou_final->invert)
		// 							echo $passou_final->format("%H:%I:%S")." passados da saída 3";					
		// 					}
		// 				}
		// 				echo "<br />";
		// 			}
		// 		}else{
		// 			if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_2"]["saida"]->format("Y") != "-0001"){
		// 				$temp = clone $data["horario"]["_1"]["saida"];
		// 				if($data["horario"]["_1"]["saida"]->format("Ymd") > $data["horario"]["_1"]["entrada"]->format("Ymd"))
		// 					$temp->sub(new DateInterval('P1D'));//diminuindo para fazer o teste
		// 				if($temp <= $data["horario"]["_1"]["entrada"]){
		// 					if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_2"]["saida"]){
		// 						echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S");
		// 					}else{
		// 						$temp = clone $data["padrao"]["_1"]["entrada"];
		// 						$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		// 						if($data["horario"]["_1"]["saida"] > $temp){
		// 							echo $data["intervalo"]["final_inicio"]->format("H:i:s");
		// 						}else{
		// 							$passou_final = $data["padrao"]["_2"]["saida"]->diff($data["horario"]["_1"]["saida"]);
		// 							if(!$passou_final->invert)
		// 								echo $passou_final->format("%H:%I:%S")." passados da saída 2";					
		// 						}
		// 					}
		// 					echo "<br />";
		// 				}
		// 			}else{
		// 				if($data["horario"]["_1"]["saida"]->format("Y") != "-0001" && $data["padrao"]["_1"]["saida"]->format("Y") != "-0001"){
		// 					$temp = clone $data["horario"]["_1"]["saida"];
		// 					if($data["horario"]["_1"]["saida"]->format("Ymd") > $data["horario"]["_1"]["entrada"]->format("Ymd"))
		// 						$temp->sub(new DateInterval('P1D'));//diminuindo para fazer o teste
		// 					if($temp <= $data["horario"]["_1"]["entrada"]){
		// 						if($data["horario"]["_1"]["entrada"] > $data["padrao"]["_1"]["saida"]){
		// 							echo $data["horario"]["_1"]["saida"]->diff($data["horario"]["_1"]["entrada"])->format("%H:%I:%S");
		// 						}else{
		// 							$temp = clone $data["padrao"]["_1"]["entrada"];
		// 							$temp->setDate($data["horario"]["_1"]["saida"]->format("Y"), $data["horario"]["_1"]["saida"]->format("m"), $data["horario"]["_1"]["saida"]->format("d"));
		// 							if($data["horario"]["_1"]["saida"] > $temp){
		// 								echo $data["intervalo"]["final_inicio"]->format("H:i:s");
		// 							}else{
		// 								$passou_final = $data["padrao"]["_1"]["saida"]->diff($data["horario"]["_1"]["saida"]);
		// 								if(!$passou_final->invert)
		// 									echo $passou_final->format("%H:%I:%S")." passados da saída 1";					
		// 							}
		// 						}
		// 						echo "<br />";
		// 					}
		// 				}else{

		// 				}
		// 			}
		// 		}
		// 	}
		// }

		echo "<pre>";
		echo "<br />";
		print_r($data);
		echo "</pre>";
	}
}