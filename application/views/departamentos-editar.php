<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Alterar Departamento</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Alterar Departamento
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome</label>
													<input class="form-control" type="text" name="nome" placeholder="Razão Social" value="<?= $departamento->nome ?>" />
													<p class="text-danger"><?= form_error("nome") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Empresa</label>
													<select class="form-control" name="id_empresa" id="empresas">
														<?php foreach ($empresas as $empresa): ?>
															<option value="<?= $empresa->id ?>" <?= $empresa->id == $departamento->id_empresa ? "selected" : "" ?>><?= $empresa->nome_fantasia ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_empresa") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#empresas").select2({
				theme: "bootstrap",
				ajax: {
				    url: '/funcionarios/select2_empresas',
				    dataType: 'json',
				    delay: 500,
				    data: function (params) {
				      return {
				        q: params.term,
				      };
				    },
				    processResults: function (data, params) {
				      return {
				        results: data.items,
				      };
				    },
				    cache: true
				  },
				  placeholder: 'Empresa',
				  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				  minimumInputLength: 1,
				  'locale': 'pt-br'
				});
			});
	</script>
</body>
</html>
