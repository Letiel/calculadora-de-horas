<!DOCTYPE html>
<html>
<head>
	<title>Calculo de horas</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style type="text/css">
		.col-xs-1{
			padding: 0px 1px;
		}

		.form-control{
			padding: 0px 3px;
		}
	</style>
</head>
<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="tudo">
				<div class="col-xs-12">
					<div class="col-xs-2">
						<div class="form-group">
							<label>Horas diárias</label>
							<input class="form-control hora" id="horas_diarias" value="08:00" />
						</div>
						<div class="form-group">
							<label>Utilizar regras da CLT para tolerâncias</label>
							<input type="checkbox" id="utilizar_clt" checked="true" />
						</div>
					</div>
					<div class="col-xs-10">
						<div class="clt" style="display: none;">
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 1 antes</label>
									<input class="form-control hora" id="tolerancia_entrada_1_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 1 depois</label>
									<input class="form-control hora" id="tolerancia_entrada_1_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 2 antes</label>
									<input class="form-control hora" id="tolerancia_entrada_2_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 2 depois</label>
									<input class="form-control hora" id="tolerancia_entrada_2_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 3 antes</label>
									<input class="form-control hora" id="tolerancia_entrada_3_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 3 depois</label>
									<input class="form-control hora" id="tolerancia_entrada_3_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 4 antes</label>
									<input class="form-control hora" id="tolerancia_entrada_4_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de entrada 4 depois</label>
									<input class="form-control hora" id="tolerancia_entrada_4_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 1 antes</label>
									<input class="form-control hora" id="tolerancia_saida_1_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 1 depois</label>
									<input class="form-control hora" id="tolerancia_saida_1_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 2 antes</label>
									<input class="form-control hora" id="tolerancia_saida_2_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 2 depois</label>
									<input class="form-control hora" id="tolerancia_saida_2_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 3 antes</label>
									<input class="form-control hora" id="tolerancia_saida_3_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 3 depois</label>
									<input class="form-control hora" id="tolerancia_saida_3_depois" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 4 antes</label>
									<input class="form-control hora" id="tolerancia_saida_4_antes" value="00:05" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Tolerância de saída 4 depois</label>
									<input class="form-control hora" id="tolerancia_saida_4_depois" value="00:05" />
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de entrada 1</label>
									<input class="form-control hora" id="horario_entrada_1" value="08:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de saída 1</label>
									<input class="form-control hora" id="horario_saida_1" value="12:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de entrada 2</label>
									<input class="form-control hora" id="horario_entrada_2" value="13:15" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de saída 2</label>
									<input class="form-control hora" id="horario_saida_2" value="18:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de entrada 3</label>
									<input class="form-control hora" id="horario_entrada_3" value="18:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de saída 3</label>
									<input class="form-control hora" id="horario_saida_3" value="19:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de entrada 4</label>
									<input class="form-control hora" id="horario_entrada_4" value="19:00" />
								</div>
							</div>
							<div class="col-xs-2">
								<div class="form-group">
									<label>Horário de saída 4</label>
									<input class="form-control hora" id="horario_saida_4" value="22:00" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-10">
					<?php
						$mes = 2;
						$ano = 2017;
						$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
						for ($i=1; $i <= $numero; $i++) {
							?>
								<div class="clearfix"></div>
								<div class="conjunto-<?= $i ?>">
									<h3><?= str_pad($i, 2, '0', STR_PAD_LEFT)."/".str_pad($mes, 2, '0', STR_PAD_LEFT) ?></h3>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Entrada 1</label>
											<input class="form-control hora entrada-1" placeholder="__:__" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Saída 1</label>
											<input class="form-control hora saida-1 saida" placeholder="__:__" />
											<label><input type="checkbox" class="outro-dia-1" />Outro dia</label>
											
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Entrada 2</label>
											<input class="form-control hora entrada-2" placeholder="__:__" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Saída 2</label>
											<input class="form-control hora saida-2 saida" placeholder="__:__" />
											<label>Outro dia</label>
											<input type="checkbox" class="outro-dia-2" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Entrada 3</label>
											<input class="form-control hora entrada-3" placeholder="__:__" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Saída 3</label>
											<input class="form-control hora saida-3 saida" placeholder="__:__" />
											<label>Outro dia</label>
											<input type="checkbox" class="outro-dia-3" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Entrada 4</label>
											<input class="form-control hora entrada-4" placeholder="__:__" />
										</div>
									</div>
									<div class="col-xs-1">
										<div class="form-group">
											<label>Saída 4</label>
											<input class="form-control hora saida-4 saida" placeholder="__:__" />
											<label>Outro dia</label>
											<input type="checkbox" class="outro-dia-4" />
										</div>
									</div>
									<button class="btn btn-primary pull-right calcular" id="dia_calculo" value="<?= date('d/m/Y') ?>">Calcular</button>
									<div class="clearfix"></div>
									<div class="resultado"></div>
									<hr />
								</div>
							<?php
						}
					?>
				</div>
			</div>
			<!-- <button class="btn btn-success adicionar">Adicionar novo dia</button> -->
		</div>
	</div>

	<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			conjuntos = 1;
			$('.hora').mask('00:00');
			$('#dia_calculo').mask('00/00/0000');

			$("#utilizar_clt").change(function(){
				if(!$("#utilizar_clt").prop("checked")){
					$(".clt").removeAttr("style");
				}else
					$(".clt").hide();
			});

			// $(".saida").blur(function(){
			// 	elemento = $(this);
			// 	$.post("calcular.php", {
			// 			horas_diarias: $("#horas_diarias").val(),
			// 			utilizar_clt: $("#utilizar_clt").prop("checked"),
			// 			entrada1: elemento.parent().parent().parent().find(".entrada-1").val(),
			// 			saida1: elemento.parent().parent().parent().find(".saida-1").val(),
			// 			outrodia1: elemento.parent().parent().parent().find(".outro-dia-1").prop('checked'),
			// 			tolerancia_entrada_1: elemento.parent().parent().parent().find("#tolerancia_entrada_1").val(),
			// 			tolerancia_saida_1: elemento.parent().parent().parent().find("#tolerancia_saida_1").val(),
			// 			entrada2: elemento.parent().parent().parent().find(".entrada-2").val(),
			// 			saida2: elemento.parent().parent().parent().find(".saida-2").val(),
			// 			outrodia2: elemento.parent().parent().parent().find(".outro-dia-2").prop('checked'),
			// 			tolerancia_entrada_2: elemento.parent().parent().parent().find("#tolerancia_entrada_2").val(),
			// 			tolerancia_saida_2: elemento.parent().parent().parent().find("#tolerancia_saida_2").val(),
			// 			entrada3: elemento.parent().parent().parent().find(".entrada-3").val(),
			// 			saida3: elemento.parent().parent().parent().find(".saida-3").val(),
			// 			outrodia3: elemento.parent().parent().parent().find(".outro-dia-3").prop('checked'),
			// 			tolerancia_entrada_3: elemento.parent().parent().parent().find("#tolerancia_entrada_3").val(),
			// 			tolerancia_saida_3: elemento.parent().parent().parent().find("#tolerancia_saida_3").val(),
			// 			entrada4: elemento.parent().parent().parent().find(".entrada-4").val(),
			// 			saida4: elemento.parent().parent().parent().find(".saida-4").val(),
			// 			outrodia4: elemento.parent().parent().parent().find(".outro-dia-4").prop('checked'),
			// 			tolerancia_entrada_4: elemento.parent().parent().parent().find("#tolerancia_entrada_4").val(),
			// 			tolerancia_saida_4: elemento.parent().parent().parent().find("#tolerancia_saida_4").val(),
			// 		}, function(result){
			// 			elemento.parent().parent().parent().find(".resultado").html(result);
			// 		}
			// 	);
			// });

			$(document).on('click', ".calcular", function(event){
			// $(".conjunto-"+conjuntos+" .calcular").click(function(){
				$.post("/index.php/home/calcular", {
					dia_calculo: $("#dia_calculo").val(),
					horas_diarias: $("#horas_diarias").val(),
					horario_entrada_1: $("#horario_entrada_1").val(),
					horario_entrada_2: $("#horario_entrada_2").val(),
					horario_entrada_3: $("#horario_entrada_3").val(),
					horario_entrada_4: $("#horario_entrada_4").val(),
					horario_saida_1: $("#horario_saida_1").val(),
					horario_saida_2: $("#horario_saida_2").val(),
					horario_saida_3: $("#horario_saida_3").val(),
					horario_saida_4: $("#horario_saida_4").val(),
					utilizar_clt: $("#utilizar_clt").prop("checked"),

					entrada1: $(event.target).parent().find(".entrada-1").val(),
					saida1: $(event.target).parent().find(".saida-1").val(),
					outrodia1: $(event.target).parent().find(".outro-dia-1").prop('checked'),

					tolerancia_entrada_1_antes: $("#tolerancia_entrada_1_antes").val(),
					tolerancia_entrada_1_depois: $("#tolerancia_entrada_1_depois").val(),
					tolerancia_saida_1_antes: $("#tolerancia_saida_1_antes").val(),
					tolerancia_saida_1_depois: $("#tolerancia_saida_1_depois").val(),

					entrada2: $(event.target).parent().find(".entrada-2").val(),
					saida2: $(event.target).parent().find(".saida-2").val(),
					outrodia2: $(event.target).parent().find(".outro-dia-2").prop('checked'),

					tolerancia_entrada_2_antes: $("#tolerancia_entrada_2_antes").val(),
					tolerancia_entrada_2_depois: $("#tolerancia_entrada_2_depois").val(),
					tolerancia_saida_2_antes: $("#tolerancia_saida_2_antes").val(),
					tolerancia_saida_2_depois: $("#tolerancia_saida_2_depois").val(),

					entrada3: $(event.target).parent().find(".entrada-3").val(),
					saida3: $(event.target).parent().find(".saida-3").val(),
					outrodia3: $(event.target).parent().find(".outro-dia-3").prop('checked'),

					tolerancia_entrada_3_antes: $("#tolerancia_entrada_3_antes").val(),
					tolerancia_entrada_3_depois: $("#tolerancia_entrada_3_depois").val(),
					tolerancia_saida_3_antes: $("#tolerancia_saida_3_antes").val(),
					tolerancia_saida_3_depois: $("#tolerancia_saida_3_depois").val(),

					entrada4: $(event.target).parent().find(".entrada-4").val(),
					saida4: $(event.target).parent().find(".saida-4").val(),
					outrodia4: $(event.target).parent().find(".outro-dia-4").prop('checked'),

					tolerancia_entrada_4_antes: $("#tolerancia_entrada_4_antes").val(),
					tolerancia_entrada_4_depois: $("#tolerancia_entrada_4_depois").val(),
					tolerancia_saida_4_antes: $("#tolerancia_saida_4_antes").val(),
					tolerancia_saida_4_depois: $("#tolerancia_saida_4_depois").val(),
				}, function(result){
					$(event.target).parent().find(".resultado").html(result);
				});
			});

			$(".adicionar").click(function(){
				conjuntos+=1;
				var div = $("<div class='conjunto-"+conjuntos+"'></div>");
				$(".tudo").append(div);
				// $("#base").clone().appendTo(".conjunto-"+conjuntos);
				var base = $('<div id="base"><div class="col-xs-1"><div class="form-group"><label>Entrada</label><input class="form-control hora entrada-1" placeholder="__:__" /></div></div><div class="col-xs-1"><div class="form-group"><label>Saída</label><input class="form-control hora saida-1" placeholder="__:__" /><label>Outro dia</label><input type="checkbox" class="outro-dia-1" /></div></div><div class="col-xs-1"><div class="form-group"><label>Entrada</label><input class="form-control hora entrada-2" placeholder="__:__" /></div></div><div class="col-xs-1"><div class="form-group"><label>Saída</label><input class="form-control hora saida-2" placeholder="__:__" /><label>Outro dia</label><input type="checkbox" class="outro-dia-2" /></div></div><div class="col-xs-1"><div class="form-group"><label>Entrada</label><input class="form-control hora entrada-3" placeholder="__:__" /></div></div><div class="col-xs-1"><div class="form-group"><label>Saída</label><input class="form-control hora saida-3" placeholder="__:__" /><label>Outro dia</label><input type="checkbox" class="outro-dia-3" /></div></div><button class="btn btn-primary pull-right calcular">Calcular</button><div class="clearfix"></div><div class="resultado"></div><hr /></div>');
				$(".conjunto-"+conjuntos).append(base);
				$(".conjunto-"+conjuntos+" input").val("");
				$(".conjunto-"+conjuntos+" .resultado").html("");
				$('.form-control hora').mask('00:00');
			});
		});
	</script>
	
</body>
</html>