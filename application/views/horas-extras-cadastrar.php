<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Cadastrar Horas Extras</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Cadastrar Horas Extras
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-2">
												<div class="form-group">
													<label>Faixas</label>
													<input type="number" max="10" min="1" class="form-control faixas" placeholder="--" value='1' />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<fieldset>
													<legend>Faixas</legend>
													<div class="campos"></div>
												</fieldset>
											</div>
										</div>
										<div class="row">
											<p class="text-info"><b>* Deixe o último horário em branco para tempo indefinido.</b></p>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		calcularCampos();
		function calcularCampos(){
			faixas = $(".faixas").val();
			div = $(".campos");
			var elementos = $(".campo").length;
			if(faixas != null && faixas != "" && elementos != faixas){
				if(elementos < faixas){
					for (var i = elementos+1; i <= faixas; i++) {
						$('<div>', {
						    id: i,
						    class: 'row campo',
						}).appendTo(div);

						$('<div>', {
						    class: 'form-group col-4 group-de',
						}).appendTo(document.getElementById(i));

						$('<label>', {
						    html: 'De',
						}).appendTo($(".campos").find("#"+i).find(".group-de"));

						if(i == 1){
							value = "00:00";
						}else{
							value = "";
						}
						$('<input>', {
						    placeholder: '--:--',
						    name: 'de[]',
						    class: 'form-control hora de',
						    value: value,
						    required: true,
						}).appendTo($(".campos").find("#"+i).find(".group-de"));

						$('<div>', {
						    class: 'form-group col-4 group-ate',
						}).appendTo(document.getElementById(i));

						$('<label>', {
						    html: 'Até',
						}).appendTo($(".campos").find("#"+i).find(".group-ate"));

						$('<input>', {
						    placeholder: '--:--',
						    name: 'ate[]',
						    class: 'form-control hora ate',
						    onChange: "preencherCampos(this)"
						}).appendTo($(".campos").find("#"+i).find(".group-ate"));

						$('<div>', {
						    class: 'form-group col-4 group-coluna',
						}).appendTo(document.getElementById(i));

						$('<label>', {
						    html: 'Coluna',
						}).appendTo($(".campos").find("#"+i).find(".group-coluna"));

						$('<input>', {
						    placeholder: 'Coluna',
						    name: 'coluna[]',
						    class: 'form-control',
						    required: true,
						}).appendTo($(".campos").find("#"+i).find(".group-coluna"));
					}
				}else{
					for (var i = elementos; i > faixas; i--) {
						$(".campos").find("#"+i).remove();
					}
				}

				var maskBehavior = function (val) {
			        val = val.split(":");
			        return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
			    }

			    spOptions = {
			        onKeyPress: function(val, e, field, options) {
			            field.mask(maskBehavior.apply({}, arguments), options);
			        },
			        translation: {
			            'H': { pattern: /[0-2]/, optional: false },
			            'Z': { pattern: /[0-3]/, optional: false },
			            'M': { pattern: /[0-5]/, optional: false}
			        }
			    };

			    $('.hora').mask(maskBehavior, spOptions);
				campos = faixas;
			}
		}

		function preencherCampos(e){
			elementos = $(".campo").length;
			// atual = $(this).parent().parent();
			atual = e.parentElement.parentElement.id;
			if(elementos > atual){
				++atual;
				$("#"+(atual)).find(".de").val($(e).val());
				$("#"+(atual)).find(".de").removeClass("disabled");
				$("#"+(atual)).find(".de").addClass("disabled");
				$("#"+(atual)).find(".de").attr("disabled", true);
			}
		}

		$(document).ready(function(){
			$(".faixas").on("input", function(){
				calcularCampos();
			});
			$(".faixas").on("change", function(){
				calcularCampos();
			});

			$("form").submit(function(){
				$(".hora").attr("disabled", false);
			});

			$("#horarios").select2({
				theme: "bootstrap",
				placeholder: 'Horário',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});
		});
	</script>
</body>
</html>
