<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Quadro Inicial</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12">
							<?php if (strtotime($conta->data_validade) <= strtotime(date("Y-m-d H:i:s", strtotime("+7 days")))): ?>
								<h4 class="text-danger float-right">Sua conta expira em <b><?= date("d/m/Y", strtotime($conta->data_validade)) ?></b></h4>
							<?php else: ?>
								<h6 class="float-right">Sua conta expira em <b><?= date("d/m/Y", strtotime($conta->data_validade)) ?></b></h6>
							<?php endif ?>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/calculadora/">
								<div class="card card-tile card-xs bg-primary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-alarm-clock batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Calcular</div>
											<div class="tile-description">Novo Cálculo</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/relatorios">
								<div class="card card-tile card-xs bg-warning bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-clipboard-alt batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Relatórios</div>
											<div class="tile-description">Já efetuados</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-12"><hr /></div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/funcionarios">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-users batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Funcionários</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/departamentos">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-grid batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Departamentos</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/setores">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-grid batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Setores</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="empresas">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-store batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Empresas</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/horarios">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-clock batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Horários</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/feriados">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-asterisk batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Feriados</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/afastamentos">
								<div class="card card-tile card-xs bg-secondary bg-gradient text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-spam batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Afastamentos</div>
											<div class="tile-description">Tipos</div>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-12">
							<hr />
						</div>
						<?php if ($this->session->userdata("adm")): ?>
							<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
								<a href="/operadores">
									<div class="card card-tile card-xs bg-success text-center">
										<div class="card-body p-4">
											<div class="tile-left">
												<i class="batch-icon batch-icon-users batch-icon-xxl"></i>
											</div>
											<div class="tile-right">
												<div class="tile-number">Operadores</div>
												<div class="tile-description">Gerenciar</div>
											</div>
										</div>
									</div>
								</a>
							</div>
						<?php endif ?>
						<div class="col-md-6 col-lg-6 col-xl-3 mb-5">
							<a href="/minha-conta">
								<div class="card card-tile card-xs bg-secondary text-center">
									<div class="card-body p-4">
										<div class="tile-left">
											<i class="batch-icon batch-icon-user-alt-2 batch-icon-xxl"></i>
										</div>
										<div class="tile-right">
											<div class="tile-number">Minha Conta</div>
											<div class="tile-description">Gerenciar</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
</body>
</html>
