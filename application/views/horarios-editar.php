<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Alterar Grupo de Horário</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/x-editable/css/bootstrap-editable.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Grupo de Horário
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome</label>
													<input class="form-control" type="text" name="nome" placeholder="Nome" value="<?= $grupo_horario->nome ?>" />
													<p class="text-danger"><?= form_error("nome") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Empresa</label>
													<select class="form-control" name="id_empresa" id="empresas">
														<?php foreach ($empresas as $empresa): ?>
															<option value="<?= $empresa->id ?>" <?= $empresa->id == $grupo_horario->id_empresa ? "selected" : "" ?>><?= $empresa->nome_fantasia ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_empresa") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Horários
									<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target=".bd-example-modal-sm">Copiar horário</button>
								</div>
								<div class="card-body">
									<p class="text-info"><b>Dica!</b> Clique sobre os itens que deseja alterar.</p>
									<form method="post">
										<div class="row">
											<table class="table table-datatable table-striped table-hover table-responsive">
												<thead>
													<tr>
														<th class="all">Dia</th>
														<th class="all">Ent 1</th>
														<th class="all">Saí 1</th>
														<th class="all">Ent 2</th>
														<th class="all">Saí 2</th>
														<th class="all">Ent 3</th>
														<th class="all">Saí 3</th>
														<th class="all">Ent 4</th>
														<th class="all">Saí 4</th>
														<th class="all">Ent 5</th>
														<th class="all">Saí 5</th>
														<th class="all">Carga Horária</th>
														<th class="none">Regras de Tolerância</th>
														<th class="none">Compensação</th>
														<th class="none">Tolerância Diária</th>

														<th class="none">Tolerância Ent 1 Antes</th>
														<th class="none">Tolerância Ent 1 Depois</th>
														<th class="none">Tolerância Saí 1 Antes</th>
														<th class="none">Tolerância Saí 1 Depois</th>
														
														<th class="none">Tolerância Ent 2 Antes</th>
														<th class="none">Tolerância Ent 2 Depois</th>
														<th class="none">Tolerância Saí 2 Antes</th>
														<th class="none">Tolerância Saí 2 Depois</th>
														
														<th class="none">Tolerância Ent 3 Antes</th>
														<th class="none">Tolerância Ent 3 Depois</th>
														<th class="none">Tolerância Saí 3 Antes</th>
														<th class="none">Tolerância Saí 3 Depois</th>
														
														<th class="none">Tolerância Ent 4 Antes</th>
														<th class="none">Tolerância Ent 4 Depois</th>
														<th class="none">Tolerância Saí 4 Antes</th>
														<th class="none">Tolerância Saí 4 Depois</th>

														<th class="none">Tolerância Ent 5 Antes</th>
														<th class="none">Tolerância Ent 5 Depois</th>
														<th class="none">Tolerância Saí 5 Antes</th>
														<th class="none">Tolerância Saí 5 Depois</th>

														<th class="none">Tolerância Compensação Falta</th>
														<th class="none">Tolerância Compensação Extra</th>
														<th class="none">Fechamento</th>
													</tr>
												</thead>
												<tbody>	
													<tr>
														<td><b>Segunda Feira</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $segunda->padrao_entrada_1 == "" ? "--:--" : $segunda->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $segunda->padrao_saida_1 == "" ? "--:--" :  $segunda->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $segunda->padrao_entrada_2 == "" ? "--:--" : $segunda->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $segunda->padrao_saida_2 == "" ? "--:--" :  $segunda->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $segunda->padrao_entrada_3 == "" ? "--:--" : $segunda->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $segunda->padrao_saida_3 == "" ? "--:--" :  $segunda->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $segunda->padrao_entrada_4 == "" ? "--:--" : $segunda->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $segunda->padrao_saida_4 == "" ? "--:--" :  $segunda->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $segunda->padrao_entrada_5 == "" ? "--:--" : $segunda->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $segunda->padrao_saida_5 == "" ? "--:--" :  $segunda->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $segunda->carga_horaria == "" ? "--:--" :  $segunda->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-segunda" data-type="select" data-pk="<?= $segunda->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $segunda->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-segunda" data-type="select" data-pk="<?= $segunda->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $segunda->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $segunda->tolerancia_diaria == "" ? "--:--" :  $segunda->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $segunda->tol_entrada_1_antes == "" ? "--:--" :  $segunda->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $segunda->tol_entrada_1_depois == "" ? "--:--" :  $segunda->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $segunda->tol_saida_1_antes == "" ? "--:--" :  $segunda->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $segunda->tol_saida_1_depois == "" ? "--:--" :  $segunda->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $segunda->tol_entrada_2_antes == "" ? "--:--" :  $segunda->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $segunda->tol_entrada_2_depois == "" ? "--:--" :  $segunda->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $segunda->tol_saida_2_antes == "" ? "--:--" :  $segunda->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $segunda->tol_saida_2_depois == "" ? "--:--" :  $segunda->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $segunda->tol_entrada_3_antes == "" ? "--:--" :  $segunda->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $segunda->tol_entrada_3_depois == "" ? "--:--" :  $segunda->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $segunda->tol_saida_3_antes == "" ? "--:--" :  $segunda->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $segunda->tol_saida_3_depois == "" ? "--:--" :  $segunda->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $segunda->tol_entrada_4_antes == "" ? "--:--" :  $segunda->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $segunda->tol_entrada_4_depois == "" ? "--:--" :  $segunda->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $segunda->tol_saida_4_antes == "" ? "--:--" :  $segunda->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $segunda->tol_saida_4_depois == "" ? "--:--" :  $segunda->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $segunda->tol_entrada_5_antes == "" ? "--:--" :  $segunda->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $segunda->tol_entrada_5_depois == "" ? "--:--" :  $segunda->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $segunda->tol_saida_5_antes == "" ? "--:--" :  $segunda->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $segunda->tol_saida_5_depois == "" ? "--:--" :  $segunda->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $segunda->comp_tol_falta == "" ? "--:--" :  $segunda->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $segunda->comp_tol_extra == "" ? "--:--" :  $segunda->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $segunda->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $segunda->fechamento == "" ? "--:--" :  $segunda->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Terça Feira</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $terca->padrao_entrada_1 == "" ? "--:--" : $terca->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $terca->padrao_saida_1 == "" ? "--:--" :  $terca->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $terca->padrao_entrada_2 == "" ? "--:--" : $terca->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $terca->padrao_saida_2 == "" ? "--:--" :  $terca->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $terca->padrao_entrada_3 == "" ? "--:--" : $terca->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $terca->padrao_saida_3 == "" ? "--:--" :  $terca->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $terca->padrao_entrada_4 == "" ? "--:--" : $terca->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $terca->padrao_saida_4 == "" ? "--:--" :  $terca->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $terca->padrao_entrada_5 == "" ? "--:--" : $terca->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $terca->padrao_saida_5 == "" ? "--:--" :  $terca->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $terca->carga_horaria == "" ? "--:--" :  $terca->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-terca" data-type="select" data-pk="<?= $terca->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $terca->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-terca" data-type="select" data-pk="<?= $terca->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $terca->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $terca->tolerancia_diaria == "" ? "--:--" :  $terca->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $terca->tol_entrada_1_antes == "" ? "--:--" :  $terca->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $terca->tol_entrada_1_depois == "" ? "--:--" :  $terca->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $terca->tol_saida_1_antes == "" ? "--:--" :  $terca->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $terca->tol_saida_1_depois == "" ? "--:--" :  $terca->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $terca->tol_entrada_2_antes == "" ? "--:--" :  $terca->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $terca->tol_entrada_2_depois == "" ? "--:--" :  $terca->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $terca->tol_saida_2_antes == "" ? "--:--" :  $terca->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $terca->tol_saida_2_depois == "" ? "--:--" :  $terca->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $terca->tol_entrada_3_antes == "" ? "--:--" :  $terca->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $terca->tol_entrada_3_depois == "" ? "--:--" :  $terca->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $terca->tol_saida_3_antes == "" ? "--:--" :  $terca->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $terca->tol_saida_3_depois == "" ? "--:--" :  $terca->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $terca->tol_entrada_4_antes == "" ? "--:--" :  $terca->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $terca->tol_entrada_4_depois == "" ? "--:--" :  $terca->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $terca->tol_saida_4_antes == "" ? "--:--" :  $terca->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $terca->tol_saida_4_depois == "" ? "--:--" :  $terca->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $terca->tol_entrada_5_antes == "" ? "--:--" :  $terca->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $terca->tol_entrada_5_depois == "" ? "--:--" :  $terca->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $terca->tol_saida_5_antes == "" ? "--:--" :  $terca->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $terca->tol_saida_5_depois == "" ? "--:--" :  $terca->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $terca->comp_tol_falta == "" ? "--:--" :  $terca->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $terca->comp_tol_extra == "" ? "--:--" :  $terca->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $terca->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $terca->fechamento == "" ? "--:--" :  $terca->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Quarta Feira</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $quarta->padrao_entrada_1 == "" ? "--:--" : $quarta->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $quarta->padrao_saida_1 == "" ? "--:--" :  $quarta->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $quarta->padrao_entrada_2 == "" ? "--:--" : $quarta->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $quarta->padrao_saida_2 == "" ? "--:--" :  $quarta->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $quarta->padrao_entrada_3 == "" ? "--:--" : $quarta->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $quarta->padrao_saida_3 == "" ? "--:--" :  $quarta->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $quarta->padrao_entrada_4 == "" ? "--:--" : $quarta->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $quarta->padrao_saida_4 == "" ? "--:--" :  $quarta->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $quarta->padrao_entrada_5 == "" ? "--:--" : $quarta->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $quarta->padrao_saida_5 == "" ? "--:--" :  $quarta->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $quarta->carga_horaria == "" ? "--:--" :  $quarta->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-quarta" data-type="select" data-pk="<?= $quarta->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $quarta->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-quarta" data-type="select" data-pk="<?= $quarta->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $quarta->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $quarta->tolerancia_diaria == "" ? "--:--" :  $quarta->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $quarta->tol_entrada_1_antes == "" ? "--:--" :  $quarta->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $quarta->tol_entrada_1_depois == "" ? "--:--" :  $quarta->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $quarta->tol_saida_1_antes == "" ? "--:--" :  $quarta->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $quarta->tol_saida_1_depois == "" ? "--:--" :  $quarta->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $quarta->tol_entrada_2_antes == "" ? "--:--" :  $quarta->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $quarta->tol_entrada_2_depois == "" ? "--:--" :  $quarta->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $quarta->tol_saida_2_antes == "" ? "--:--" :  $quarta->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $quarta->tol_saida_2_depois == "" ? "--:--" :  $quarta->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $quarta->tol_entrada_3_antes == "" ? "--:--" :  $quarta->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $quarta->tol_entrada_3_depois == "" ? "--:--" :  $quarta->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $quarta->tol_saida_3_antes == "" ? "--:--" :  $quarta->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $quarta->tol_saida_3_depois == "" ? "--:--" :  $quarta->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $quarta->tol_entrada_4_antes == "" ? "--:--" :  $quarta->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $quarta->tol_entrada_4_depois == "" ? "--:--" :  $quarta->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $quarta->tol_saida_4_antes == "" ? "--:--" :  $quarta->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $quarta->tol_saida_4_depois == "" ? "--:--" :  $quarta->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $quarta->tol_entrada_5_antes == "" ? "--:--" :  $quarta->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $quarta->tol_entrada_5_depois == "" ? "--:--" :  $quarta->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $quarta->tol_saida_5_antes == "" ? "--:--" :  $quarta->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $quarta->tol_saida_5_depois == "" ? "--:--" :  $quarta->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $quarta->comp_tol_falta == "" ? "--:--" :  $quarta->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $quarta->comp_tol_extra == "" ? "--:--" :  $quarta->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quarta->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $quarta->fechamento == "" ? "--:--" :  $quarta->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Quinta Feira</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $quinta->padrao_entrada_1 == "" ? "--:--" : $quinta->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $quinta->padrao_saida_1 == "" ? "--:--" :  $quinta->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $quinta->padrao_entrada_2 == "" ? "--:--" : $quinta->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $quinta->padrao_saida_2 == "" ? "--:--" :  $quinta->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $quinta->padrao_entrada_3 == "" ? "--:--" : $quinta->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $quinta->padrao_saida_3 == "" ? "--:--" :  $quinta->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $quinta->padrao_entrada_4 == "" ? "--:--" : $quinta->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $quinta->padrao_saida_4 == "" ? "--:--" :  $quinta->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $quinta->padrao_entrada_5 == "" ? "--:--" : $quinta->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $quinta->padrao_saida_5 == "" ? "--:--" :  $quinta->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $quinta->carga_horaria == "" ? "--:--" :  $quinta->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-quinta" data-type="select" data-pk="<?= $quinta->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $quinta->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-quinta" data-type="select" data-pk="<?= $quinta->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $quinta->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $quinta->tolerancia_diaria == "" ? "--:--" :  $quinta->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $quinta->tol_entrada_1_antes == "" ? "--:--" :  $quinta->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $quinta->tol_entrada_1_depois == "" ? "--:--" :  $quinta->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $quinta->tol_saida_1_antes == "" ? "--:--" :  $quinta->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $quinta->tol_saida_1_depois == "" ? "--:--" :  $quinta->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $quinta->tol_entrada_2_antes == "" ? "--:--" :  $quinta->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $quinta->tol_entrada_2_depois == "" ? "--:--" :  $quinta->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $quinta->tol_saida_2_antes == "" ? "--:--" :  $quinta->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $quinta->tol_saida_2_depois == "" ? "--:--" :  $quinta->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $quinta->tol_entrada_3_antes == "" ? "--:--" :  $quinta->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $quinta->tol_entrada_3_depois == "" ? "--:--" :  $quinta->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $quinta->tol_saida_3_antes == "" ? "--:--" :  $quinta->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $quinta->tol_saida_3_depois == "" ? "--:--" :  $quinta->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $quinta->tol_entrada_4_antes == "" ? "--:--" :  $quinta->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $quinta->tol_entrada_4_depois == "" ? "--:--" :  $quinta->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $quinta->tol_saida_4_antes == "" ? "--:--" :  $quinta->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $quinta->tol_saida_4_depois == "" ? "--:--" :  $quinta->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $quinta->tol_entrada_5_antes == "" ? "--:--" :  $quinta->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $quinta->tol_entrada_5_depois == "" ? "--:--" :  $quinta->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $quinta->tol_saida_5_antes == "" ? "--:--" :  $quinta->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $quinta->tol_saida_5_depois == "" ? "--:--" :  $quinta->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $quinta->comp_tol_falta == "" ? "--:--" :  $quinta->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $quinta->comp_tol_extra == "" ? "--:--" :  $quinta->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $quinta->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $quinta->fechamento == "" ? "--:--" :  $quinta->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Sexta Feira</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $sexta->padrao_entrada_1 == "" ? "--:--" : $sexta->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $sexta->padrao_saida_1 == "" ? "--:--" :  $sexta->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $sexta->padrao_entrada_2 == "" ? "--:--" : $sexta->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $sexta->padrao_saida_2 == "" ? "--:--" :  $sexta->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $sexta->padrao_entrada_3 == "" ? "--:--" : $sexta->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $sexta->padrao_saida_3 == "" ? "--:--" :  $sexta->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $sexta->padrao_entrada_4 == "" ? "--:--" : $sexta->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $sexta->padrao_saida_4 == "" ? "--:--" :  $sexta->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $sexta->padrao_entrada_5 == "" ? "--:--" : $sexta->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $sexta->padrao_saida_5 == "" ? "--:--" :  $sexta->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $sexta->carga_horaria == "" ? "--:--" :  $sexta->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-sexta" data-type="select" data-pk="<?= $sexta->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $sexta->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-sexta" data-type="select" data-pk="<?= $sexta->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $sexta->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $sexta->tolerancia_diaria == "" ? "--:--" :  $sexta->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $sexta->tol_entrada_1_antes == "" ? "--:--" :  $sexta->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $sexta->tol_entrada_1_depois == "" ? "--:--" :  $sexta->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $sexta->tol_saida_1_antes == "" ? "--:--" :  $sexta->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $sexta->tol_saida_1_depois == "" ? "--:--" :  $sexta->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $sexta->tol_entrada_2_antes == "" ? "--:--" :  $sexta->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $sexta->tol_entrada_2_depois == "" ? "--:--" :  $sexta->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $sexta->tol_saida_2_antes == "" ? "--:--" :  $sexta->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $sexta->tol_saida_2_depois == "" ? "--:--" :  $sexta->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $sexta->tol_entrada_3_antes == "" ? "--:--" :  $sexta->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $sexta->tol_entrada_3_depois == "" ? "--:--" :  $sexta->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $sexta->tol_saida_3_antes == "" ? "--:--" :  $sexta->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $sexta->tol_saida_3_depois == "" ? "--:--" :  $sexta->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $sexta->tol_entrada_4_antes == "" ? "--:--" :  $sexta->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $sexta->tol_entrada_4_depois == "" ? "--:--" :  $sexta->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $sexta->tol_saida_4_antes == "" ? "--:--" :  $sexta->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $sexta->tol_saida_4_depois == "" ? "--:--" :  $sexta->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $sexta->tol_entrada_5_antes == "" ? "--:--" :  $sexta->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $sexta->tol_entrada_5_depois == "" ? "--:--" :  $sexta->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $sexta->tol_saida_5_antes == "" ? "--:--" :  $sexta->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $sexta->tol_saida_5_depois == "" ? "--:--" :  $sexta->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $sexta->comp_tol_falta == "" ? "--:--" :  $sexta->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $sexta->comp_tol_extra == "" ? "--:--" :  $sexta->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sexta->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $sexta->fechamento == "" ? "--:--" :  $sexta->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Sábado</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $sabado->padrao_entrada_1 == "" ? "--:--" : $sabado->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $sabado->padrao_saida_1 == "" ? "--:--" :  $sabado->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $sabado->padrao_entrada_2 == "" ? "--:--" : $sabado->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $sabado->padrao_saida_2 == "" ? "--:--" :  $sabado->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $sabado->padrao_entrada_3 == "" ? "--:--" : $sabado->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $sabado->padrao_saida_3 == "" ? "--:--" :  $sabado->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $sabado->padrao_entrada_4 == "" ? "--:--" : $sabado->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $sabado->padrao_saida_4 == "" ? "--:--" :  $sabado->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $sabado->padrao_entrada_5 == "" ? "--:--" : $sabado->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $sabado->padrao_saida_5 == "" ? "--:--" :  $sabado->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $sabado->carga_horaria == "" ? "--:--" :  $sabado->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-sabado" data-type="select" data-pk="<?= $sabado->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $sabado->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-sabado" data-type="select" data-pk="<?= $sabado->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $sabado->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $sabado->tolerancia_diaria == "" ? "--:--" :  $sabado->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $sabado->tol_entrada_1_antes == "" ? "--:--" :  $sabado->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $sabado->tol_entrada_1_depois == "" ? "--:--" :  $sabado->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $sabado->tol_saida_1_antes == "" ? "--:--" :  $sabado->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $sabado->tol_saida_1_depois == "" ? "--:--" :  $sabado->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $sabado->tol_entrada_2_antes == "" ? "--:--" :  $sabado->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $sabado->tol_entrada_2_depois == "" ? "--:--" :  $sabado->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $sabado->tol_saida_2_antes == "" ? "--:--" :  $sabado->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $sabado->tol_saida_2_depois == "" ? "--:--" :  $sabado->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $sabado->tol_entrada_3_antes == "" ? "--:--" :  $sabado->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $sabado->tol_entrada_3_depois == "" ? "--:--" :  $sabado->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $sabado->tol_saida_3_antes == "" ? "--:--" :  $sabado->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $sabado->tol_saida_3_depois == "" ? "--:--" :  $sabado->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $sabado->tol_entrada_4_antes == "" ? "--:--" :  $sabado->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $sabado->tol_entrada_4_depois == "" ? "--:--" :  $sabado->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $sabado->tol_saida_4_antes == "" ? "--:--" :  $sabado->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $sabado->tol_saida_4_depois == "" ? "--:--" :  $sabado->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $sabado->tol_entrada_5_antes == "" ? "--:--" :  $sabado->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $sabado->tol_entrada_5_depois == "" ? "--:--" :  $sabado->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $sabado->tol_saida_5_antes == "" ? "--:--" :  $sabado->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $sabado->tol_saida_5_depois == "" ? "--:--" :  $sabado->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $sabado->comp_tol_falta == "" ? "--:--" :  $sabado->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $sabado->comp_tol_extra == "" ? "--:--" :  $sabado->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $sabado->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $sabado->fechamento == "" ? "--:--" :  $sabado->fechamento?></a>
														</td>
													</tr>
													<tr>
														<td><b>Domingo</b></td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_entrada_1" data-url="/horarios/editable" data-title="Padrão de Entrada 1"><?= $domingo->padrao_entrada_1 == "" ? "--:--" : $domingo->padrao_entrada_1 ?>
															</a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_saida_1" data-url="/horarios/editable" data-title="Padrão de Saída 1"><?= $domingo->padrao_saida_1 == "" ? "--:--" :  $domingo->padrao_saida_1?></a>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_entrada_2" data-url="/horarios/editable" data-title="Padrão de Entrada 2"><?= $domingo->padrao_entrada_2 == "" ? "--:--" : $domingo->padrao_entrada_2 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_saida_2" data-url="/horarios/editable" data-title="Padrão de Saída 2"><?= $domingo->padrao_saida_2 == "" ? "--:--" :  $domingo->padrao_saida_2?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_entrada_3" data-url="/horarios/editable" data-title="Padrão de Entrada 3"><?= $domingo->padrao_entrada_3 == "" ? "--:--" : $domingo->padrao_entrada_3 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_saida_3" data-url="/horarios/editable" data-title="Padrão de Saída 3"><?= $domingo->padrao_saida_3 == "" ? "--:--" :  $domingo->padrao_saida_3?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_entrada_4" data-url="/horarios/editable" data-title="Padrão de Entrada 4"><?= $domingo->padrao_entrada_4 == "" ? "--:--" : $domingo->padrao_entrada_4 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_saida_4" data-url="/horarios/editable" data-title="Padrão de Saída 4"><?= $domingo->padrao_saida_4 == "" ? "--:--" :  $domingo->padrao_saida_4?></a>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_entrada_5" data-url="/horarios/editable" data-title="Padrão de Entrada 5"><?= $domingo->padrao_entrada_5 == "" ? "--:--" : $domingo->padrao_entrada_5 ?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="padrao_saida_5" data-url="/horarios/editable" data-title="Padrão de Saída 5"><?= $domingo->padrao_saida_5 == "" ? "--:--" :  $domingo->padrao_saida_5?></a>
														</td>

														<td>
															<span class="carga_horaria"><?= $domingo->carga_horaria == "" ? "--:--" :  $domingo->carga_horaria ?></span>
														</td>
														
														
														<td>
															<a href="#" class="editavel-regras-domingo" data-type="select" data-pk="<?= $domingo->id ?>" data-name="regras_tolerancias" data-url="/horarios/editable" data-title="Regras de tolerância"><?= $domingo->regras_tolerancias ?></a>
															</td>
														<td>
															<a href="#" class="editavel-compensacao-domingo" data-type="select" data-pk="<?= $domingo->id ?>" data-name="utiliza_compensacao" data-url="/horarios/editable" data-title="Utiliza Compensação"><?= $domingo->utiliza_compensacao ? "Sim" : "Não" ?></a>
															</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tolerancia_diaria" data-url="/horarios/editable" data-title="Tolerância Diária"><?= $domingo->tolerancia_diaria == "" ? "--:--" :  $domingo->tolerancia_diaria?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_1_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Antes"><?= $domingo->tol_entrada_1_antes == "" ? "--:--" :  $domingo->tol_entrada_1_antes?></a>
																
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_1_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 1 - Depois"><?= $domingo->tol_entrada_1_depois == "" ? "--:--" :  $domingo->tol_entrada_1_depois?></a>
															
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_1_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Antes"><?= $domingo->tol_saida_1_antes == "" ? "--:--" :  $domingo->tol_saida_1_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_1_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 1 - Depois"><?= $domingo->tol_saida_1_depois == "" ? "--:--" :  $domingo->tol_saida_1_depois?></a>
															</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_2_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Antes"><?= $domingo->tol_entrada_2_antes == "" ? "--:--" :  $domingo->tol_entrada_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_2_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 2 - Depois"><?= $domingo->tol_entrada_2_depois == "" ? "--:--" :  $domingo->tol_entrada_2_depois?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_2_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Antes"><?= $domingo->tol_saida_2_antes == "" ? "--:--" :  $domingo->tol_saida_2_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_2_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 2 - Depois"><?= $domingo->tol_saida_2_depois == "" ? "--:--" :  $domingo->tol_saida_2_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_3_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Antes"><?= $domingo->tol_entrada_3_antes == "" ? "--:--" :  $domingo->tol_entrada_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_3_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 3 - Depois"><?= $domingo->tol_entrada_3_depois == "" ? "--:--" :  $domingo->tol_entrada_3_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_3_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Antes"><?= $domingo->tol_saida_3_antes == "" ? "--:--" :  $domingo->tol_saida_3_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_3_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 3 - Depois"><?= $domingo->tol_saida_3_depois == "" ? "--:--" :  $domingo->tol_saida_3_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_4_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Antes"><?= $domingo->tol_entrada_4_antes == "" ? "--:--" :  $domingo->tol_entrada_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_4_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 4 - Depois"><?= $domingo->tol_entrada_4_depois == "" ? "--:--" :  $domingo->tol_entrada_4_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_4_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Antes"><?= $domingo->tol_saida_4_antes == "" ? "--:--" :  $domingo->tol_saida_4_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_4_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 4 - Depois"><?= $domingo->tol_saida_4_depois == "" ? "--:--" :  $domingo->tol_saida_4_depois?>
														</td>

														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_5_antes" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Antes"><?= $domingo->tol_entrada_5_antes == "" ? "--:--" :  $domingo->tol_entrada_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_entrada_5_depois" data-url="/horarios/editable" data-title="Tolerância de Entrada 5 - Depois"><?= $domingo->tol_entrada_5_depois == "" ? "--:--" :  $domingo->tol_entrada_5_depois?>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_5_antes" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Antes"><?= $domingo->tol_saida_5_antes == "" ? "--:--" :  $domingo->tol_saida_5_antes?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="tol_saida_5_depois" data-url="/horarios/editable" data-title="Tolerância de Saída 5 - Depois"><?= $domingo->tol_saida_5_depois == "" ? "--:--" :  $domingo->tol_saida_5_depois?>
														</td>
														
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="comp_tol_falta" data-url="/horarios/editable" data-title="Tolerância de Falta - Compensação"><?= $domingo->comp_tol_falta == "" ? "--:--" :  $domingo->comp_tol_falta?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="comp_tol_extra" data-url="/horarios/editable" data-title="Tolerância de Extra - Compensação"><?= $domingo->comp_tol_extra == "" ? "--:--" :  $domingo->comp_tol_extra?></a>
														</td>
														<td>
															<a href="#" class="editavel" data-type="text" data-pk="<?= $domingo->id ?>" data-name="fechamento" data-url="/horarios/editable" data-title="Fechamento"><?= $domingo->fechamento == "" ? "--:--" :  $domingo->fechamento?></a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Extras
									<a href="/horas-extras/cadastrar/<?= $grupo_horario->id ?>" class="btn btn-primary float-right"><span class="batch-icon batch-icon-marquee-plus mr-3"></span>Novo</a>
								</div>
								<div class="card-body">
									<div class="row">
										<table class="table table-datatable table-striped table-hover table-responsive">
											<thead>
												<tr>
													<th class="all">De</th>
													<th class="all">Até</th>
													<th class="all">Coluna</th>
													<th class="all">Alterar</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($extras as $extra): ?>
													<tr>
														<td><?= $extra->de ?></td>
														<td><?= $extra->ate == "" ? "--:--" : $extra->ate ?></td>
														<td><b><?= $extra->coluna ?></b></td>
														<td>
															<button type="button" value="<?= $extra->id ?>" class="btn btn-info btn-md editar"><span class="batch-icon batch-icon-pencil"></span></button>
															<button type="button" class="btn btn-danger btn-md btn-excluir" value="<?= $extra->id ?>"><span class="batch-icon batch-icon-bin-alt-2"></span></button>
															<button type="button" class="btn btn-link btn-avancado" data-coluna="<?= $extra->coluna ?>" value="<?= $extra->id ?>"><span class="batch-icon batch-icon-settings-alt-2 mr-3"></span></button>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal_avancado">
	  <div class="modal-dialog">
	    <div class="modal-content">

	      <div class="modal-header">
	        <h4 class="modal-title">Avançado</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <form class="avancado" onsubmit="return false">
		      <div class="modal-body">
		      	<div class="erros"></div>
		        <div class="form-group">
		        	<h4>Coluna: <b class='nome_col'></b></h4>
		        	<p class="text-info">Selecione os dias que devem ser direcionados à coluna</p>
		        	<label>Dias</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_feriados" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Feriados</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_segunda" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Segunda-Feira</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_terca" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Terça-Feira</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_quarta" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Quarta-Feira</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_quinta" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Quinta-Feira</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_sexta" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Sexta-Feira</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_sabado" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Sábado</span>
		        	</label>
		        	<br />
		        	<label class="custom-control custom-checkbox">
		        		<input type="checkbox" id="avancado_domingo" class="custom-control-input">
		        		<span class="custom-control-indicator"></span>
		        		<span class="custom-control-description">Domingo</span>
		        	</label>
		        </div>
		      </div>

		      <div class="modal-footer">
		        <button class="btn btn-primary">Salvar</button>
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	<div class="modal fade" id="modalFaixa">
	  <div class="modal-dialog">
	    <div class="modal-content">

	      <div class="modal-header">
	        <h4 class="modal-title">Alterar Faixa</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <form class="alterar" onsubmit="return false;">
		      <div class="modal-body">
		      	<div class="erros"></div>
		        <div class="form-group">
		        	<label>De</label>
		        	<input type="text" name="de" class='form-control de hora' />
		        </div>
		        <div class="form-group">
		        	<label>Até</label>
		        	<input type="text" name="ate" class='form-control ate hora' />
		        </div>
		        <div class="form-group">
		        	<label>Coluna</label>
		        	<input type="text" name="coluna" class='form-control coluna' />
		        	<input type="hidden" name="id" class='form-control id disabled' style="display: none;" />
		        	<input type="hidden" name="id_horario" class='form-control id_horario disabled' style="display: none;" />
		        </div>
		      </div>

		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary">Alterar</button>
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Copiar Horário</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Copiar de</label>
						<select id="de" class="form-control">
							<option value="segunda">Segunda-Feira</option>
							<option value="terca">Terça-Feira</option>
							<option value="quarta">Quarta-Feira</option>
							<option value="quinta">Quinta-Feira</option>
							<option value="sexta">Sexta-Feira</option>
							<option value="sabado">Sábado</option>
							<option value="domingo">Domingo</option>
						</select>
					</div>
					<div class="form-group">
						<label>Para</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="segunda" class="custom-control-input" checked>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Segunda</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="terca" class="custom-control-input" checked>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Terça</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="quarta" class="custom-control-input" checked>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Quarta</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="quinta" class="custom-control-input" checked>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Quinta</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="sexta" class="custom-control-input" checked>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Sexta</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="sabado" class="custom-control-input">
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Sábado</span>
						</label>
						<br />
						<label class="custom-control custom-checkbox">
							<input type="checkbox" id="domingo" class="custom-control-input">
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Domingo</span>
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-primary copiar">Copiar</button>
				</div>
			</div>
		</div>
	</div>

	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/x-editable/js/bootstrap-editable.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		function criar_editable(){
			$(".editavel").editable({
				emptytext: '--',
				tpl: "<input type='text' id='horario' placeholder='--:--' />",
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
					$(this).parent().parent().find(".carga_horaria").html(response.carga_horaria);
				}
			}).on('shown',function(){
				$("input#horario").mask("00:00");
		  	});

		  	$(".editavel-regras-domingo").editable({
				emptytext: '--',
				value: "<?= $domingo->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-segunda").editable({
				emptytext: '--',
				value: "<?= $segunda->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-terca").editable({
				emptytext: '--',
				value: "<?= $terca->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-quarta").editable({
				emptytext: '--',
				value: "<?= $quarta->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-quinta").editable({
				emptytext: '--',
				value: "<?= $quinta->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-sexta").editable({
				emptytext: '--',
				value: "<?= $sexta->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-regras-sabado").editable({
				emptytext: '--',
				value: "<?= $sabado->regras_tolerancias ?>",
				source: [
				{value: "CLT", text: 'CLT'},
				{value: "Personalizadas", text: 'Personalizadas'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});
		  	//compensacao
		  	$(".editavel-compensacao-domingo").editable({
				emptytext: '--',
				value: "<?= $domingo->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-segunda").editable({
				emptytext: '--',
				value: "<?= $segunda->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-terca").editable({
				emptytext: '--',
				value: "<?= $terca->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-quarta").editable({
				emptytext: '--',
				value: "<?= $quarta->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-quinta").editable({
				emptytext: '--',
				value: "<?= $quinta->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-sexta").editable({
				emptytext: '--',
				value: "<?= $sexta->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});

		  	$(".editavel-compensacao-sabado").editable({
				emptytext: '--',
				value: "<?= $sabado->utiliza_compensacao ?>",
				source: [
				{value: "1", text: 'Sim'},
				{value: "0", text: 'Não'},
				],
				ajaxOptions: {
					dataType: 'json'
				},
				success: function(response, newValue) {
					if(!response) {
						return "Erro desconhecido";
					}

					if(response.success === false) {
						return response.msg;
					}
				}
			});
		}
		var col_avancado = 0;
		$(document).ready(function(){

			$(".btn-avancado").click(function(){
				col_avancado = $(this).val();
				col_avancado_nome = $(this).attr("data-coluna");
				$(".nome_col").html(col_avancado_nome);

				$.post("/horarios/preencher_avancado", {
					id_coluna: col_avancado
				}, function(result){
					result = JSON.parse(result);
					if(result.segunda == true){
						$("#avancado_segunda").prop("checked", true);
					}else{
						$("#avancado_segunda").prop("checked", false);
					}
					if(result.terca == true){
						$("#avancado_terca").prop("checked", true);
					}else{
						$("#avancado_terca").prop("checked", false);
					}
					if(result.quarta == true){
						$("#avancado_quarta").prop("checked", true);
					}else{
						$("#avancado_quarta").prop("checked", false);
					}
					if(result.quinta == true){
						$("#avancado_quinta").prop("checked", true);
					}else{
						$("#avancado_quinta").prop("checked", false);
					}
					if(result.sexta == true){
						$("#avancado_sexta").prop("checked", true);
					}else{
						$("#avancado_sexta").prop("checked", false);
					}
					if(result.sabado == true){
						$("#avancado_sabado").prop("checked", true);
					}else{
						$("#avancado_sabado").prop("checked", false);
					}
					if(result.domingo == true){
						$("#avancado_domingo").prop("checked", true);
					}else{
						$("#avancado_domingo").prop("checked", false);
					}
					if(result.feriados == true){
						$("#avancado_feriados").prop("checked", true);
					}else{
						$("#avancado_feriados").prop("checked", false);
					}
				});

				$("#modal_avancado").modal("show");
			});

			$(".avancado").submit(function(){
				$.post("/horarios/avancado", {
					id_horario: <?= $grupo_horario->id ?>,
					id_coluna: col_avancado,
					feriados: $("#avancado_feriados").prop("checked"),
					segunda: $("#avancado_segunda").prop("checked"),
					terca: $("#avancado_terca").prop("checked"),
					quarta: $("#avancado_quarta").prop("checked"),
					quinta: $("#avancado_quinta").prop("checked"),
					sexta: $("#avancado_sexta").prop("checked"),
					sabado: $("#avancado_sabado").prop("checked"),
					domingo: $("#avancado_domingo").prop("checked"),
				}, function(result){
					location.reload();
				});
			});

			$(".copiar").click(function(){
				$.post("/horarios/copiar_horario", {
					grupo: <?= $grupo_horario->id ?>,
					de: $("#de").val(),
					segunda: $("#segunda").prop("checked"),
					terca: $("#terca").prop("checked"),
					quarta: $("#quarta").prop("checked"),
					quinta: $("#quinta").prop("checked"),
					sexta: $("#sexta").prop("checked"),
					sabado: $("#sabado").prop("checked"),
					domingo: $("#domingo").prop("checked"),
				}, function(result){
					location.reload();
				});
			});

			$('#example tbody').on('click', 'td.details-control', function () {
		        criar_editable();
		    });
		    criar_editable();
			table = $(".table-datatable").DataTable({
				paging: false,
				searching: false,
			    ordering:  false,
			    responsive: true,
			    info: false,
			    language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    }
				}
			});

			table.on( 'responsive-display', function () {
			    criar_editable();
			} );
			$("#empresas").select2({
				theme: "bootstrap",
				placeholder: 'Empresa',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});

			$("input.hora").mask("00:00");

			$(".editar").click(function(){
				$.post("/horas-extras/edit/", {
					id: $(this).val()
				}, function(result){
					if(result == ""){
						alert("Ocorreu um erro.");
					}else{
						result = jQuery.parseJSON(result);
						$(".de").val(result.de);
						$(".ate").val(result.ate);
						$(".coluna").val(result.coluna);
						$(".id").val(result.id);
						$(".id_horario").val(result.id_horario);
						$("#modalFaixa").modal("show");
					}
				});
			});

			$(".alterar").submit(function(){
				$.post("/horas-extras/editar", {
					de: $(".de").val(),
					ate: $(".ate").val(),
					coluna: $(".coluna").val(),
					id_horario: $(".id_horario").val(),
					id: $(".id").val(),
				}, function(result){
					if(result == ""){
						location.reload();
					}else{
						$(".erros").html(result);
					}
				});
			});

			$(".btn-excluir").click(function(){
				if(confirm("Tem certeza?")){
					$.post("/horas-extras/excluir", {
						id: $(this).val(),
					}, function(result){
						if(result != ""){
							alert(result);
						}else{
							location.reload();
						}
					});
				}
			});
		});
	</script>
</body>
</html>
