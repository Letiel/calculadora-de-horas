<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Afastamentos</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Afastamentos de <b><?= $funcionario->nome ?></b>
									<button data-toggle="modal" data-target="#AfastamentosModal" class="btn btn-outline-primary float-right"><span class="batch-icon batch-icon-marquee-plus mr-3"></span>Afastamento</button>
								</div>
								<div class="card-body">
									<table class="table table-datatable table-striped table-hover table-responsive">
										<thead>
											<tr>
												<th>Tipo de Afastamento</th>
												<th>De</th>
												<th>Até</th>
												<?php if ($this->session->userdata("adm")): ?>
													<th>Alterar</th>
												<?php endif ?>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($afastamentos as $afastamento): ?>
												<tr>
													<td><?= $afastamento->descricao ?></td>
													<td><?= $afastamento->data_inicio ?></td>
													<td><?= $afastamento->data_fim ?></td>
													<?php if ($this->session->userdata("adm")): ?>
														<td>
															<a title="Editar" href="/afastamentos/editar/<?= $afastamento->id ?>" class="btn btn-info btn-md"><span class="batch-icon batch-icon-pencil"></span></a>
															<button class="btn btn-danger btn-md btn-excluir" value="<?= $afastamento->id ?>" title="Excluir"><span class="batch-icon batch-icon-bin-alt-2"></span></button>
														</td>
													<?php endif ?>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
									<?= $paginacao ?>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="AfastamentosModal" tabindex="-1" role="dialog" aria-labelledby="afastamentosModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="afastamentosModalLabel">Atribuir Afastamento</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <form action="/funcionarios/atribuir-afastamento" method="post">
		      <div class="modal-body">
		      	<input type="hidden" style="display: none" name="id_funcionario" value="<?= $funcionario->id ?>">
		        <div class="form-group">
		        	<label>Tipo de Afastamento</label>
		        	<select class='form-control' name='id_tipo' required>
		        		<option value="">Selecione</option>
		        		<?php foreach ($tipos_afastamentos as $afastamento): ?>
		        			<option value="<?= $afastamento->id ?>"><?= $afastamento->descricao ?></option>
		        		<?php endforeach ?>
		        	</select>
		        </div>
		        <div class="row">
		        	<div class="col-6">
		        		<div class="form-group">
		        			<label>Data Inicial</label>
		        			<input class='form-control' type="datetime-local" name="data_inicio" required />
		        		</div>
		        	</div>
		        	<div class="col-6">
		        		<div class="form-group">
		        			<label>Data Final</label>
		        			<input class='form-control' type="datetime-local" name="data_fim" required />
		        		</div>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="submit" class="btn btn-primary">Salvar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".btn-excluir").click(function(){
				if(confirm("Tem certeza? Esta ação não pode ser desfeita.")){
					$.post("/afastamentos/excluir_afastamento_funcionario", {
						id: $(this).val()
					}, function(result){
						location.reload();
					});
				}
			});
		});
	</script>
</body>
</html>
