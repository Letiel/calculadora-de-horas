<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Cadastrar Funcionário</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Cadastrar Funcionário
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label>Empresa</label>
													<select class="form-control" name="id_empresa" id="empresas">
														<option value="">Selecione</option>
														<?php foreach ($empresas as $empresa): ?>
															<option value="<?= $empresa->id ?>"><?= $empresa->nome_fantasia ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_empresa") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Cidade</label>
													<select class="form-control" name="id_cidade" id="cidades">
														<option value="">Selecione</option>
													</select>
													<p class="text-danger"><?= form_error("id_cidade") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Horário</label>
													<select class="form-control" name="id_horario" id="horarios">
														<option value="">Selecione</option>
														<?php foreach ($horarios as $horario): ?>
															<option value="<?= $horario->id ?>"><?= $horario->nome ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_horario") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Nome Completo</label>
													<input class="form-control" type="text" name="nome" placeholder="Nome Completo" value="<?= set_value('nome') ?>" />
													<p class="text-danger"><?= form_error("nome") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Nº Folha</label>
													<input class="form-control" type="text" name="numero_folha" placeholder="Nº Folha" value="<?= set_value('numero_folha') ?>" />
													<p class="text-danger"><?= form_error("numero_folha") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>PIS</label>
													<input class="form-control pis" type="text" name="pis" placeholder="PIS" value="<?= set_value('pis') ?>" />
													<p class="text-danger"><?= form_error("pis") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Nº Identificador</label>
													<input class="form-control" type="text" name="numero_identificador" placeholder="Nº Identificador" value="<?= set_value('numero_identificador') ?>" />
													<p class="text-danger"><?= form_error("numero_identificador") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>CTPS</label>
													<input class="form-control" type="text" name="ctps" placeholder="CTPS" value="<?= set_value('ctps') ?>" />
													<p class="text-danger"><?= form_error("ctps") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Departamento</label>
													<select class="form-control" name="id_departamento" id="departamentos">
														<option value="">Selecione</option>
														<?php foreach ($departamentos as $departamento): ?>
															<option value="<?= $departamento->id ?>"><?= $departamento->nome ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_departamento") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Setor</label>
													<select class="form-control" name="id_setor" id="setores">
														<option value="">Selecione</option>
														<?php foreach ($setores as $setor): ?>
															<option value="<?= $setor->id ?>"><?= $setor->nome ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("id_setor") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Admissão</label>
													<input class="form-control data" type="text" name="admissao" placeholder="Admissão" value="<?= set_value('admissao') ?>" />
													<p class="text-danger"><?= form_error("admissao") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Demissão</label>
													<input class="form-control data" type="text" name="demissao" placeholder="Demissão" value="<?= set_value('demissao') ?>" />
													<p class="text-danger"><?= form_error("demissao") ?></p>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>Observações</label>
													<textarea class="form-control" name="observacoes"><?= set_value('observacoes') ?></textarea>
													<p class="text-danger"><?= form_error("observacoes") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".data").mask("00/00/0000");
			$(".pis").mask("000.00000.00-0");
			$("#cidades").select2({
				theme: "bootstrap",
				ajax: {
				    url: '/empresas/select2_cidades',
				    dataType: 'json',
				    delay: 500,
				    data: function (params) {
				      return {
				        q: params.term,
				      };
				    },
				    processResults: function (data, params) {
				      return {
				        results: data.items,
				      };
				    },
				    cache: true
				  },
				placeholder: 'Cidade',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				minimumInputLength: 1,
			});

			$("#empresas").select2({
				theme: "bootstrap",
				placeholder: 'Empresa',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});

			$("#horarios").select2({
				theme: "bootstrap",
				placeholder: 'Horário',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});

			$("#departamentos").select2({
				theme: "bootstrap",
				placeholder: 'Departamento',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});

			$("#setores").select2({
				theme: "bootstrap",
				placeholder: 'Setor',
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			});
		});
	</script>
</body>
</html>
