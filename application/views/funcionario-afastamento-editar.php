<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Alterar Afastamento</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Alterar Afastamento
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label>Data Inicial</label>
													<input type="hidden" name="id_funcionario" value="<?= $afastamento->id_funcionario ?>" style="display: none;" />
													<input class="form-control" type="date" name="data_inicio" placeholder="Data Inicial" value="<?= date("Y-m-d", strtotime($afastamento->data_inicio)) ?>" />
													<p class="text-danger"><?= form_error("data_inicio") ?></p>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group">
													<label>Hora Inicial</label>
													<input class="form-control" type="time" name="hora_inicio" placeholder="Hora Inicial" value="<?= date("H:i:s", strtotime($afastamento->data_inicio)) ?>" />
													<p class="text-danger"><?= form_error("hora_inicio") ?></p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Data Final</label>
													<input class="form-control" type="date" name="data_fim" placeholder="Data Final" value="<?= date("Y-m-d", strtotime($afastamento->data_fim)) ?>" />
													<p class="text-danger"><?= form_error("data_fim") ?></p>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group">
													<label>Hora Final</label>
													<input class="form-control" type="time" name="hora_fim" placeholder="Hora Final" value="<?= date("H:i:s", strtotime($afastamento->data_fim)) ?>" />
													<p class="text-danger"><?= form_error("hora_fim") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			toastr.info('Os campos de <b>hora</b> são opcionais. Caso <b>não</b> sejam informados, os <b>dias inteiros</b> serão considerados.');
		});
	</script>
</body>
</html>
