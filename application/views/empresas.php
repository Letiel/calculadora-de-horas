<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Empresas</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Empresas
									<a href="/empresas/cadastrar" class="btn btn-primary float-right"><span class="batch-icon batch-icon-marquee-plus mr-3"></span>Novo</a>
								</div>
								<div class="card-body">
									<table class="table table-datatable table-striped table-hover table-responsive">
										<thead>
											<tr>
												<th>Nome Fantasia</th>
												<th>CNPJ</th>
												<th>Cidade</th>
												<?php if ($this->session->userdata("adm")): ?>
													<th>Alterar</th>
												<?php endif ?>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($empresas as $empresa): ?>
												<tr>
													<td><?= $empresa->nome_fantasia ?></td>
													<td><?= $empresa->cnpj ?></td>
													<td><?= $empresa->nome_cidade ?></td>
													<?php if ($this->session->userdata("adm")): ?>
														<td>
															<a href="/empresas/editar/<?= $empresa->id ?>" class="btn btn-info btn-md"><span class="batch-icon batch-icon-pencil"></span></a>
															<button class="btn btn-danger btn-md btn-excluir" value="<?= $empresa->id ?>"><span class="batch-icon batch-icon-bin-alt-2"></span></button>
														</td>
													<?php endif ?>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
									<?= $paginacao ?>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".table-datatable").DataTable({
				paging: false,
				searching: false,
			    ordering:  false,
			    responsive: true,
			    info: false,
			    language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    }
				}
			});

			$(".btn-excluir").click(function(){
				if(confirm("Tem certeza?")){
					$.post("/empresas/excluir", {
						id: $(this).val()
					}, function(result){
						$("body").append(result);
						if(result == ""){
							location.reload();
						}
					});
				}
			});
		});
	</script>
</body>
</html>
