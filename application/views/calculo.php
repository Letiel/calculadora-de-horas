<!DOCTYPE html>
<html>
<head>
	<title>Calculo de horas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet"> -->
	<style type="text/css">
		body{
			background-color: #fdfdfd;
			padding-top:32px; 
			font-family: 'Raleway', sans-serif;
		}

		.input-hora{
			width: 9%;
			margin: 0 1% 15px 0;
			float: left;
		}

		@media(max-width: 1178px){
			.input-hora{
				width: 19%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}

		@media(max-width: 988px){
			.input-hora{
				width: 49%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}
		.table tr td{
			padding: 0 !important;
		}
		.falta, .extra, .carga_horaria, .horas_trabalhadas{
			padding: 12px 5px !important;
			display: block;
		}
		.table tr td input{
			width: 100%;
			border: none;
			background: transparent;
			padding: 8px;
		}
		.table tr td input.hora.saida-1,
		.table tr td input.hora.saida-2,
		.table tr td input.hora.saida-3,
		.table tr td input.hora.saida-4,
		.table tr td input.hora.saida-5{
			width: calc(100% - 20px);
		}
		.table tr td input.checkDia{
			width: 20px;
			float: right;
			margin: 16px 0;
			display: none;
		}
		.table tr td input:focus{
			outline: none;
			background: #fff;
		}

		.calcular:focus,
		.calcular:hover,
		.calcular:active{
			outline: none;
			text-decoration: none;
		}
	</style>
	<style type="text/css" media="print">
		.periodo-horario, .compensacao, .clt, .horarios, .btn_calcular, .calcular{
			display: none;
		}
		.hora{
			width: 50px;
		}
	</style>
	<script defer type="text/javascript" src="/fontawesome/js/fontawesome-all.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="periodo-horario">
				<div class="col-md-12">
					<h3>Período</h3>
				</div>	
				<div class="col-md-6">
					<div class="col-md-6 col-sm-6" style="padding: 0 30px 0 0;">
						<p>De:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							<input type="text" name="periodo_inicio" id="periodo_inicio" class="form-control datepicker">
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<p>Até:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							<input type="text" name="periodo_fim" id="periodo_fim" class="form-control datepicker">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<!-- <div class="checkbox" style="margin-top: 30px;">
						<label>
							<input type="checkbox" id="utilizar_clt" checked="true" /> Utilizar regras da CLT para tolerâncias
						</label>
					</div> -->
					<div class="form-group">
						<p>Regras de tolerâncias</p>
						<select class="form-control" id="utilizar_clt" style="margin-top: 5px">
							<option value="true" selected>CLT - Artigo 58</option>
							<option value="false">Tolerâncias Personalizadas</option>
						</select>
					</div>
					<div class="form-group compensacao_diaria" style="display: none;">
						<label>
							<input type="checkbox" id="compensacao_diaria" /> Utilizar compensação diária
						</label>
					</div>
				</div>
			</div>

			<div class="tolerancias clt" style="display: none;">
				<div class="col-md-12">
					<hr>
					<div class="col-md-4">
						<h3>Horários de Tolerância</h3>
					</div>
				</div>
				<div class="col-md-12">
					<div class="input-hora">
						<p>Tol. Dia</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tol_diaria" value="00:10" />
						</div>
					</div>
					<div class="clearfix"></div>
					<!-- ENTRADA 1 -->
					<div class="input-hora">
						<p>Ent. 1 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_1_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 1 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_1_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 1 -->

					<!-- ENTRADA 2 -->

					<div class="input-hora">
						<p>Ent. 2 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_2_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 2 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_2_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 2 -->

					<!-- ENTRADA 3 -->

					<div class="input-hora">
						<p>Ent. 3 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_3_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 3 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_3_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 3 -->

					<!-- ENTRADA 4 -->

					<div class="input-hora">
						<p>Ent. 4 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_4_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 4 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_4_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 4 -->

					<!-- ENTRADA 5 -->

					<div class="input-hora">
						<p>Ent. 5 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_5_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 5 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_5_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 5 -->

					<!-- SAIDA 1 -->
					<div class="input-hora">
						<p>Saída 1 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_1_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 1 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_1_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 1 -->

					<!-- SAIDA 2 -->

					<div class="input-hora">
						<p>Saída 2 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_2_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 2 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_2_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 2 -->

					<!-- SAIDA 3 -->

					<div class="input-hora">
						<p>Saída 3 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_3_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 3 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_3_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 3 -->

					<!-- SAIDA 4 -->

					<div class="input-hora">
						<p>Saída 4 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_4_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 4 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_4_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 4 -->

					<!-- SAIDA 5 -->

					<div class="input-hora">
						<p>Saída 5 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_5_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 5 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_5_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 5 -->

				</div>
			</div>

			<div class="compensacao" style="display: none;">
				<div class="col-md-12">
					<hr>
					<h3>Horários de Tolerância da compensação</h3>
				</div>
				<div class="col-md-12">
					<!-- ENTRADA 1 -->
					<div class="input-hora">
						<p>Tol. de Falta</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_falta_compensacao" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Tol. de Extra</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_extra_compensacao" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 1 -->
				</div>
			</div>

			<div class="horarios">
				<div class="col-md-12">
					<hr>
					<h3>Horários Padrão</h3>
					<div class="input-hora">
						<p>Entrada 1:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_1" value="08:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 1:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_1" value="12:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 2:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_2" value="14:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 2:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_2" value="18:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 3:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_3" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 3:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_3" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 4:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_4" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 4:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_4" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 5:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_5" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 5:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_5" value="" />
						</div>
					</div>
					<div class="clearfix"></div>
					<hr>

				</div>
			</div>


			<div class="col-md-12">
				<div class="calculos" style="display: none; margin-bottom: 100px;">
					<h3>Calcular Horários</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Data</th>
									<th>Entrada 1</th>
									<th>Saída 1</th>
									<th>Entrada 2</th>
									<th>Saída 2</th>
									<th>Entrada 3</th>
									<th>Saída 3</th>
									<th>Entrada 4</th>
									<th>Saída 4</th>
									<th>Entrada 5</th>
									<th>Saída 5</th>
									<th>Cg Hor.</th>
									<th>H.Trab</th>
									<th>Falta</th>
									<th>Extra</th>
									<th class="btn_calcular">Calcular</th>
								</tr>
							</thead>
							<tbody id="calcularTable">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal_carregando" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	        <h1 class="text-center"><i class="fas fa-spinner fa-spin"></i></h1>
	        <h3 class='text-center'>Calculando</h3>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
			<div class="modal-body">
				<h1 class="text-center text-danger"><i class="fas fa-exclamation"></i></h1>
				<!-- <h3 class='text-center'></h3> -->
				<h4 class="text-center text-danger mensagem"></h4>
				<div class="text-center">
					<button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
				</div>
			</div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="/assets/js/datepicker-pt-br.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>


	<!-- Moment -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment-with-locales.min.js"></script>

	<script type="text/javascript">
		function calculaDias(date1, date2){
			//formato do brasil 'pt-br'
			moment.locale('pt-br');
			//setando data1
			var data1 = moment(date1,'DD/MM/YYYY');
			//setando data2
			var data2 = moment(date2,'DD/MM/YYYY');
			//tirando a diferenca da data2 - data1 em dias
			var diff  = data2.diff(data1, 'days');

			return diff;
		}

		function toDate(texto) {
			let partes = texto.split('/');
			return new Date(partes[2], partes[1]-1, partes[0]);
		}

		function toString(date) {
			return ('0' + date.getDate()).slice(-2) + '/' +
			('0' + (date.getMonth() + 1)).slice(-2) + '/' +
			date.getFullYear();
		}

	</script>
	<!-- Moment -->

	<script type="text/javascript">
		function carregando(){
			$("#modal_carregando").modal("show");
		}

		function carregou(){
			$("#modal_carregando").modal("hide");			
		}

		function mensagem(mensagem){
			$(".mensagem").html(mensagem);
			$("#modal_mensagem").modal("show");
		}

		$(function(){
			$('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
				language: 'pt-BR',
			});

			$("#utilizar_clt").change(function(){
				if($("#utilizar_clt").val() == "false"){
					$(".clt").removeAttr("style");
					$(".compensacao_diaria").show();
					if($("#compensacao_diaria").prop("checked")){
						$(".clt").hide();
						$(".compensacao").show();
					}
				}else{
					$(".clt").hide();
					$(".compensacao_diaria").hide();
					if($("#compensacao_diaria").prop("checked")){
						$(".compensacao").hide();
					}
				}
			});

			$("#compensacao_diaria").change(function(){
				if($("#compensacao_diaria").prop("checked")){
					$(".clt").hide();
					$(".compensacao").show();
				}else{
					$(".clt").show();
					$(".compensacao").hide();
				}
			});

			$(".datepicker").change(function(){
				if ($("#periodo_inicio").val() != "" && $("#periodo_fim").val() != "") {
					totalDias = calculaDias($("#periodo_inicio").val(), $("#periodo_fim").val());	

					let d1 = toDate($("#periodo_inicio").val()),
					d2 = toDate($("#periodo_fim").val()),
					datas = [];

					datas.push( toString(d1) );

					while ( d1 < d2 ) {
						d1.setDate( d1.getDate() + 1 );
						datas.push( toString(d1) );
					}

					if (totalDias >= 0) {
						result = "";
						$(".calculos").show();
						for (var i = 0; i < datas.length; i++) {
							
							result += '<tr class="conjunto-'+i+'"> <th scope="row">'+datas[i]+'</th> <td> <input type="text" class="hora entrada-1" placeholder="__:__"> <small class="resultado-entrada-1"></small></td> <td> <input type="text" class="hora saida-1" placeholder="__:__"> <input type="checkbox" class="checkDia outro-dia-1" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> <small class="resultado-saida-1"></small></td> <td> <input type="text" class="hora entrada-2" placeholder="__:__"> <small class="resultado-entrada-2"></small></td> <td> <input type="text" class="hora saida-2" placeholder="__:__"> <input type="checkbox" class="checkDia outro-dia-2" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> <small class="resultado-saida-2"></small></td> <td> <input type="text" class="hora entrada-3" placeholder="__:__"> <small class="resultado-entrada-3"></small></td> <td> <input type="text" class="hora saida-3" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-3" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> <small class="resultado-saida-3"></small></td> <td> <input type="text" class="hora entrada-4" placeholder="__:__"> <small class="resultado-entrada-4"></small></td> <td> <input type="text" class="hora saida-4" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-4" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> <small class="resultado-saida-4"></small></td> <td> <input type="text" class="hora entrada-5" placeholder="__:__"> <small class="resultado-entrada-5"></small></td> <td> <input type="text" class="hora saida-5" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-5" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> <small class="resultado-saida-5"></small></td><td><span class="carga_horaria"></td><td><span class="horas_trabalhadas"></span></span></td> <td><span class="falta"></span></td> <td><span class="extra"></span></td> <td><button class="btn btn-link calcular" value="'+datas[i]+'">Calcular</button></td></tr> <tr><td colspan="15" class="resultado"></td></tr>';
						}
						$("#calcularTable").html(result);
						$('.hora').mask('00:00');
						$("[data-toggle=popover]").popover();
						$(	".table").show();
					}

				}
			});

			$(document).on('click', ".calcular", function(event){
				carregando();
				$.post("/index.php/home/calcular", {
					dia_calculo: $(this).val(),
					tol_diaria: $("#tol_diaria").val(),
					horas_diarias: $("#horas_diarias").val(),
					compensacao_diaria: $("#compensacao_diaria").prop("checked"),
					horario_entrada_1: $("#horario_entrada_1").val(),
					horario_entrada_2: $("#horario_entrada_2").val(),
					horario_entrada_3: $("#horario_entrada_3").val(),
					horario_entrada_4: $("#horario_entrada_4").val(),
					horario_entrada_5: $("#horario_entrada_5").val(),
					horario_saida_1: $("#horario_saida_1").val(),
					horario_saida_2: $("#horario_saida_2").val(),
					horario_saida_3: $("#horario_saida_3").val(),
					horario_saida_4: $("#horario_saida_4").val(),
					horario_saida_5: $("#horario_saida_5").val(),
					utilizar_clt: $("#utilizar_clt").val(),
					tolerancia_falta_compensacao: $("#tolerancia_falta_compensacao").val(),
					tolerancia_extra_compensacao: $("#tolerancia_extra_compensacao").val(),

					entrada1: $(event.target).parent().parent().find(".entrada-1").val(),
					saida1: $(event.target).parent().parent().find(".saida-1").val(),
					outrodia1: $(event.target).parent().parent().find($(".outro-dia-1")).prop('checked'),

					tolerancia_entrada_1_antes: $("#tolerancia_entrada_1_antes").val(),
					tolerancia_entrada_1_depois: $("#tolerancia_entrada_1_depois").val(),
					tolerancia_saida_1_antes: $("#tolerancia_saida_1_antes").val(),
					tolerancia_saida_1_depois: $("#tolerancia_saida_1_depois").val(),

					entrada2: $(event.target).parent().parent().find(".entrada-2").val(),
					saida2: $(event.target).parent().parent().find(".saida-2").val(),
					outrodia2: $(event.target).parent().parent().find($(".outro-dia-2")).prop('checked'),

					tolerancia_entrada_2_antes: $("#tolerancia_entrada_2_antes").val(),
					tolerancia_entrada_2_depois: $("#tolerancia_entrada_2_depois").val(),
					tolerancia_saida_2_antes: $("#tolerancia_saida_2_antes").val(),
					tolerancia_saida_2_depois: $("#tolerancia_saida_2_depois").val(),

					entrada3: $(event.target).parent().parent().find(".entrada-3").val(),
					saida3: $(event.target).parent().parent().find(".saida-3").val(),
					outrodia3: $(event.target).parent().parent().find($(".outro-dia-3")).prop('checked'),

					tolerancia_entrada_3_antes: $("#tolerancia_entrada_3_antes").val(),
					tolerancia_entrada_3_depois: $("#tolerancia_entrada_3_depois").val(),
					tolerancia_saida_3_antes: $("#tolerancia_saida_3_antes").val(),
					tolerancia_saida_3_depois: $("#tolerancia_saida_3_depois").val(),

					entrada4: $(event.target).parent().parent().find(".entrada-4").val(),
					saida4: $(event.target).parent().parent().find(".saida-4").val(),
					outrodia4: $(event.target).parent().parent().find($(".outro-dia-4")).prop('checked'),

					tolerancia_entrada_4_antes: $("#tolerancia_entrada_4_antes").val(),
					tolerancia_entrada_4_depois: $("#tolerancia_entrada_4_depois").val(),
					tolerancia_saida_4_antes: $("#tolerancia_saida_4_antes").val(),
					tolerancia_saida_4_depois: $("#tolerancia_saida_4_depois").val(),

					entrada5: $(event.target).parent().parent().find(".entrada-5").val(),
					saida5: $(event.target).parent().parent().find(".saida-5").val(),
					outrodia5: $(event.target).parent().parent().find($(".outro-dia-5")).prop('checked'),

					tolerancia_entrada_5_antes: $("#tolerancia_entrada_5_antes").val(),
					tolerancia_entrada_5_depois: $("#tolerancia_entrada_5_depois").val(),
					tolerancia_saida_5_antes: $("#tolerancia_saida_5_antes").val(),
					tolerancia_saida_5_depois: $("#tolerancia_saida_5_depois").val(),
				}, function(result){
					// $(".resultado").html('<td colspan="17">'+result+'</td>');
					// $(".resultado").show();

					// var classe = $(event.target).parent().parent().attr("class");
					resultado = jQuery.parseJSON(result);
					// $(event.target).parent().parent().find($(".resultado")).html(result);
					// $(event.target).parent().parent().find(".carga_horaria").html(resultado.calculo.soma.total);
					carregou();

					if(resultado.erro == "0"){
						$(event.target).parent().parent().find($(".falta")).html(resultado.falta);
						$(event.target).parent().parent().find($(".extra")).html(resultado.extra);
						$(event.target).parent().parent().find($(".carga_horaria")).html(resultado.carga_horaria);
						$(event.target).parent().parent().find($(".horas_trabalhadas")).html(resultado.total_trabalhado);
					}else{
						mensagem(resultado.mensagem);
					}
					// if(resultado.excedido.atraso.passou_tol_diaria == "1"){
					// 	if(resultado.excedido.atraso.soma != "00:00:00" && resultado.dia.atraso_real == "00:00:00")
					// 		$(event.target).parent().parent().find($(".falta")).html(resultado.excedido.atraso.soma);
					// 	else
					// 		$(event.target).parent().parent().find($(".falta")).html(resultado.dia.atraso_real);
					// }else{
					// 	if(resultado.excedido.atraso.soma != "00:00:00" && resultado.dia.atraso_real == "00:00:00")
					// 		$(event.target).parent().parent().find($(".falta")).html(resultado.dia.atraso_real);
					// 	else
					// 		$(event.target).parent().parent().find($(".falta")).html(resultado.excedido.atraso.soma);
					// }
					// if(resultado.excedido.extra.passou_tol_diaria == "1"){
					// 	if(resultado.excedido.extra.soma != "00:00:00" && resultado.dia.extra_real == "00:00:00")
					// 		$(event.target).parent().parent().find($(".extra")).html(resultado.excedido.extra.soma);
					// 	else
					// 		$(event.target).parent().parent().find($(".extra")).html(resultado.dia.extra_real);
					// }else{
					// 	if(resultado.excedido.extra.soma != "00:00:00" && resultado.dia.extra_real == "00:00:00")
					// 		$(event.target).parent().parent().find($(".extra")).html(resultado.dia.extra_real);
					// 	else
					// 		$(event.target).parent().parent().find($(".extra")).html(resultado.excedido.extra.soma);
							
					// }
				});
			});
		})
	</script>




</body>
</html>