<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Relatórios</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Tipo de Relatório
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-6 col-lg-4 col-xl-3 mb-5">
											<a href="/relatorios/funcionarios">
												<div class="card card-tile card-xs bg-primary text-center">
													<div class="card-body p-4">
														<div class="tile-left">
															<i class="batch-icon batch-icon-users batch-icon-xxl"></i>
														</div>
														<div class="tile-right">
															<div class="tile-number">Funcionários</div>
															<div class="tile-description">Ver</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-md-6 col-lg-4 col-xl-3 mb-5">
											<a href="/relatorios/empresas">
												<div class="card card-tile card-xs bg-primary text-center">
													<div class="card-body p-4">
														<div class="tile-left">
															<i class="batch-icon batch-icon-store batch-icon-xxl"></i>
														</div>
														<div class="tile-right">
															<div class="tile-number">Empresas</div>
															<div class="tile-description">Ver</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-md-6 col-lg-4 col-xl-3 mb-5">
											<a href="/relatorios/departamentos">
												<div class="card card-tile card-xs bg-primary text-center">
													<div class="card-body p-4">
														<div class="tile-left">
															<i class="batch-icon batch-icon-grid batch-icon-xxl"></i>
														</div>
														<div class="tile-right">
															<div class="tile-number" style="font-size: 1.5rem;">Departamentos</div>
															<div class="tile-description">Ver</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-md-6 col-lg-4 col-xl-3 mb-5">
											<a href="/relatorios/afastamentos">
												<div class="card card-tile card-xs bg-primary text-center">
													<div class="card-body p-4">
														<div class="tile-left">
															<i class="batch-icon batch-icon-power batch-icon-xxl"></i>
														</div>
														<div class="tile-right">
															<div class="tile-number" style="font-size: 1.5rem;">Afastamentos</div>
															<div class="tile-description">Ver</div>
														</div>
													</div>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			
		});
	</script>
</body>
</html>
