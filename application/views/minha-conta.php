<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Minha Conta</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Minha Conta
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome</label>
													<input class="form-control" type="text" name="nome" placeholder="Nome" value="<?= $operador->nome ?>" />
													<p class="text-danger"><?= form_error("nome") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Senha Antiga</label>
													<input type="password" name="senha_antiga" class="form-control" placeholder="******" />
													<small>* Deixe em branco caso não queira trocar.</small>
													<p class="text-danger"><?= form_error("senha_antiga") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Login</label>
													<input class="form-control" type="text" name="login" placeholder="Login" value="<?= $operador->login ?>" />
													<p class="text-danger"><?= form_error("login") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Nova Senha</label>
													<input type="password" name="nova_senha" class="form-control" placeholder="******" />
													<small>* Deixe em branco caso não queira trocar.</small>
													<p class="text-danger"><?= form_error("nova_senha") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#empresas").select2({
				theme: "bootstrap",
				  placeholder: 'Empresas',
				  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				});
			});
	</script>
</body>
</html>
