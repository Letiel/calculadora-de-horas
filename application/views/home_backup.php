<!DOCTYPE html>
<html>
<head>
	<title>Calculo de horas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet">
	<style type="text/css">
		body{
			background-color: #fdfdfd;
			padding-top:32px; 
			font-family: 'Raleway', sans-serif;
		}

		.input-hora{
			width: 9%;
			margin: 0 1% 15px 0;
			float: left;
		}

		@media(max-width: 1178px){
			.input-hora{
				width: 19%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}

		@media(max-width: 988px){
			.input-hora{
				width: 49%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}
		.table tr td{
			padding: 0 !important;
		}
		.table tr td input{
			width: 100%;
			border: none;
			background: transparent;
			padding: 8px;
		}
		.table tr td input.hora.saida-1,
		.table tr td input.hora.saida-2,
		.table tr td input.hora.saida-3,
		.table tr td input.hora.saida-4,
		.table tr td input.hora.saida-5{
			width: calc(100% - 20px);
		}
		.table tr td input.checkDia{
			width: 20px;
			float: right;
			margin: 16px 0;
		}
		.table tr td input:focus{
			outline: none;
			background: #fff;
		}

		.calcular:focus,
		.calcular:hover,
		.calcular:active{
			outline: none;
			text-decoration: none;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="periodo-horario">
				<div class="col-md-12">
					<h3>Período</h3>
				</div>	
				<div class="col-md-6">
					<div class="col-md-6 col-sm-6" style="padding: 0 30px 0 0;">
						<p>De:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							<input type="text" name="periodo_inicio" id="periodo_inicio" class="form-control datepicker">
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<p>Até:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							<input type="text" name="periodo_fim" id="periodo_fim" class="form-control datepicker">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="checkbox" style="margin-top: 30px;">
						<label>
							<input type="checkbox" id="utilizar_clt" checked="true" /> Utilizar regras da CLT para tolerâncias
						</label>
					</div>
				</div>
			</div>

			<div class="tolerancias clt" style="display: none;">
				<div class="col-md-12">
					<hr>
					<h3>Horários de Tolerância</h3>
				</div>
				<div class="col-md-12">
					<!-- ENTRADA 1 -->
					<div class="input-hora">
						<p>Ent. 1 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_1_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 1 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_1_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 1 -->

					<!-- ENTRADA 2 -->

					<div class="input-hora">
						<p>Ent. 2 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_2_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 2 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_2_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 2 -->

					<!-- ENTRADA 3 -->

					<div class="input-hora">
						<p>Ent. 3 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_3_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 3 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_3_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 3 -->

					<!-- ENTRADA 4 -->

					<div class="input-hora">
						<p>Ent. 4 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_4_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 4 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_4_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 4 -->

					<!-- ENTRADA 5 -->

					<div class="input-hora">
						<p>Ent. 5 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_5_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Ent. 5 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_entrada_5_depois" value="00:05" />
						</div>
					</div>
					<!-- ENTRADA 5 -->

					<!-- SAIDA 1 -->
					<div class="input-hora">
						<p>Saída 1 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_1_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 1 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_1_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 1 -->

					<!-- SAIDA 2 -->

					<div class="input-hora">
						<p>Saída 2 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_2_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 2 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_2_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 2 -->

					<!-- SAIDA 3 -->

					<div class="input-hora">
						<p>Saída 3 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_3_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 3 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_3_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 3 -->

					<!-- SAIDA 4 -->

					<div class="input-hora">
						<p>Saída 4 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_4_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 4 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_4_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 4 -->

					<!-- SAIDA 5 -->

					<div class="input-hora">
						<p>Saída 5 Antes:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_5_antes" value="00:05" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 5 Depois:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="tolerancia_saida_5_depois" value="00:05" />
						</div>
					</div>
					<!-- SAIDA 5 -->

				</div>
			</div>

			<div class="horarios">
				<div class="col-md-12">
					<hr>
					<h3>Horários Padrão</h3>
					<div class="input-hora">
						<p>Entrada 1:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_1" value="08:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 1:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_1" value="12:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 2:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_2" value="13:15" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 2:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_2" value="18:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 3:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_3" value="18:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 3:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_3" value="19:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 4:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_4" value="19:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 4:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_4" value="22:00" />
						</div>
					</div>

					<div class="input-hora">
						<p>Entrada 5:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_entrada_5" value="" />
						</div>
					</div>

					<div class="input-hora">
						<p>Saída 5:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							<input class="form-control hora" id="horario_saida_5" value="" />
						</div>
					</div>
					<div class="clearfix"></div>
					<hr>

				</div>
			</div>


			<div class="calculos" style="display: none;">
				<div class="col-md-12">
					<h3>Calcular Horários</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Data</th>
									<th>Entrada 1</th>
									<th>Saída 1</th>
									<th>Entrada 2</th>
									<th>Saída 2</th>
									<th>Entrada 3</th>
									<th>Saída 3</th>
									<th>Entrada 4</th>
									<th>Saída 4</th>
									<th>Entrada 5</th>
									<th>Saída 5</th>
									<th>Calcular</th>
								</tr>
							</thead>
							<tbody id="calcularTable">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>


	<!-- Moment -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment-with-locales.min.js"></script>

	<script type="text/javascript">
		function calculaDias(date1, date2){
			//formato do brasil 'pt-br'
			moment.locale('pt-br');
			//setando data1
			var data1 = moment(date1,'DD/MM/YYYY');
			//setando data2
			var data2 = moment(date2,'DD/MM/YYYY');
			//tirando a diferenca da data2 - data1 em dias
			var diff  = data2.diff(data1, 'days');

			return diff;
		}

		function toDate(texto) {
			let partes = texto.split('/');
			return new Date(partes[2], partes[1]-1, partes[0]);
		}

		function toString(date) {
			return ('0' + date.getDate()).slice(-2) + '/' +
			('0' + (date.getMonth() + 1)).slice(-2) + '/' +
			date.getFullYear();
		}

	</script>
	<!-- Moment -->

	<script type="text/javascript">
		$(function(){
			$('.datepicker').datepicker({
				format: 'dd/mm/yyyy'
			});

			$("#utilizar_clt").change(function(){
				if(!$("#utilizar_clt").prop("checked")){
					$(".clt").removeAttr("style");
				}else
				$(".clt").hide();
			});

			$(".datepicker").change(function(){
				if ($("#periodo_inicio").val() != "" && $("#periodo_fim").val() != "") {
					totalDias = calculaDias($("#periodo_inicio").val(), $("#periodo_fim").val());	

					let d1 = toDate($("#periodo_inicio").val()),
					d2 = toDate($("#periodo_fim").val()),
					datas = [];

					datas.push( toString(d1) );

					while ( d1 < d2 ) {
						d1.setDate( d1.getDate() + 1 );
						datas.push( toString(d1) );
					}

					if (totalDias >= 0) {
						result = "";
						$(".calculos").show();
						for (var i = 0; i < datas.length; i++) {
							checkbox1 = "<label><input type='checkbox' onClick='Change(this)' class='checkboxDia outro-dia-1'> Outro Dia</label>";
							checkbox2 = "<label><input type='checkbox' onClick='Change(this)' class='checkboxDia outro-dia-2'> Outro Dia</label>";
							checkbox3 = "<label><input type='checkbox' onClick='Change(this)' class='checkboxDia outro-dia-3'> Outro Dia</label>";
							checkbox4 = "<label><input type='checkbox' onClick='Change(this)' class='checkboxDia outro-dia-4'> Outro Dia</label>";
							checkbox5 = "<label><input type='checkbox' onClick='Change(this)' class='checkboxDia outro-dia-5'> Outro Dia</label>";
							result += '<tr class="conjunto-'+i+'"> <th scope="row">'+datas[i]+'</th> <td> <input type="text" class="hora entrada-1" placeholder="__:__"> </td> <td> <input type="text" class="hora saida-1" placeholder="__:__"> <input type="checkbox" class="checkDia outro-dia-1" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> </td> <td> <input type="text" class="hora entrada-2" placeholder="__:__"> </td> <td> <input type="text" class="hora saida-2" placeholder="__:__"> <input type="checkbox" class="checkDia outro-dia-2" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> </td> <td> <input type="text" class="hora entrada-3" placeholder="__:__"> </td> <td> <input type="text" class="hora saida-3" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-3" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> </td> <td> <input type="text" class="hora entrada-4" placeholder="__:__"> </td> <td> <input type="text" class="hora saida-4" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-4" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> </td> <td> <input type="text" class="hora entrada-5" placeholder="__:__"> </td> <td> <input type="text" class="hora saida-5" placeholder="__:__" > <input type="checkbox" class="checkDia outro-dia-5" data-trigger="hover" data-toggle="popover" data-placement="top" data-html="true" data-content="Outro Dia"> </td> <th><button class="btn btn-link calcular" value="'+datas[i]+'">Calcular</button></th> </tr> <tr class="resultado-conjunto-'+i+'" style="display: none;"></tr>';
						}
						$("#calcularTable").html(result);
						$('.hora').mask('00:00');
						$("[data-toggle=popover]").popover();
						$(	".table").show();
					}

				}
			});

			$(document).on('click', ".calcular", function(event){
				$.post("/index.php/home/calcular", {
					dia_calculo: $(this).val(),
					horas_diarias: $("#horas_diarias").val(),
					horario_entrada_1: $("#horario_entrada_1").val(),
					horario_entrada_2: $("#horario_entrada_2").val(),
					horario_entrada_3: $("#horario_entrada_3").val(),
					horario_entrada_4: $("#horario_entrada_4").val(),
					horario_entrada_5: $("#horario_entrada_5").val(),
					horario_saida_1: $("#horario_saida_1").val(),
					horario_saida_2: $("#horario_saida_2").val(),
					horario_saida_3: $("#horario_saida_3").val(),
					horario_saida_4: $("#horario_saida_4").val(),
					horario_saida_5: $("#horario_saida_5").val(),
					utilizar_clt: $("#utilizar_clt").prop("checked"),

					entrada1: $(event.target).parent().parent().find(".entrada-1").val(),
					saida1: $(event.target).parent().parent().find(".saida-1").val(),
					outrodia1: $(event.target).parent().parent().find($(".outro-dia-1")).prop('checked'),

					tolerancia_entrada_1_antes: $("#tolerancia_entrada_1_antes").val(),
					tolerancia_entrada_1_depois: $("#tolerancia_entrada_1_depois").val(),
					tolerancia_saida_1_antes: $("#tolerancia_saida_1_antes").val(),
					tolerancia_saida_1_depois: $("#tolerancia_saida_1_depois").val(),

					entrada2: $(event.target).parent().parent().find(".entrada-2").val(),
					saida2: $(event.target).parent().parent().find(".saida-2").val(),
					outrodia2: $(event.target).parent().parent().find($(".outro-dia-2")).prop('checked'),

					tolerancia_entrada_2_antes: $("#tolerancia_entrada_2_antes").val(),
					tolerancia_entrada_2_depois: $("#tolerancia_entrada_2_depois").val(),
					tolerancia_saida_2_antes: $("#tolerancia_saida_2_antes").val(),
					tolerancia_saida_2_depois: $("#tolerancia_saida_2_depois").val(),

					entrada3: $(event.target).parent().parent().find(".entrada-3").val(),
					saida3: $(event.target).parent().parent().find(".saida-3").val(),
					outrodia3: $(event.target).parent().parent().find($(".outro-dia-3")).prop('checked'),

					tolerancia_entrada_3_antes: $("#tolerancia_entrada_3_antes").val(),
					tolerancia_entrada_3_depois: $("#tolerancia_entrada_3_depois").val(),
					tolerancia_saida_3_antes: $("#tolerancia_saida_3_antes").val(),
					tolerancia_saida_3_depois: $("#tolerancia_saida_3_depois").val(),

					entrada4: $(event.target).parent().parent().find(".entrada-4").val(),
					saida4: $(event.target).parent().parent().find(".saida-4").val(),
					outrodia4: $(event.target).parent().parent().find($(".outro-dia-4")).prop('checked'),

					tolerancia_entrada_4_antes: $("#tolerancia_entrada_4_antes").val(),
					tolerancia_entrada_4_depois: $("#tolerancia_entrada_4_depois").val(),
					tolerancia_saida_4_antes: $("#tolerancia_saida_4_antes").val(),
					tolerancia_saida_4_depois: $("#tolerancia_saida_4_depois").val(),

					entrada5: $(event.target).parent().parent().find(".entrada-5").val(),
					saida5: $(event.target).parent().parent().find(".saida-5").val(),
					outrodia5: $(event.target).parent().parent().find($(".outro-dia-5")).prop('checked'),

					tolerancia_entrada_5_antes: $("#tolerancia_entrada_5_antes").val(),
					tolerancia_entrada_5_depois: $("#tolerancia_entrada_5_depois").val(),
					tolerancia_saida_5_antes: $("#tolerancia_saida_5_antes").val(),
					tolerancia_saida_5_depois: $("#tolerancia_saida_5_depois").val(),
				}, function(result){
					var classe = $(event.target).parent().parent().attr("class");
					$(".resultado-" + classe).html('<td colspan="12">'+result+'</td>');
					$(".resultado-" + classe).show();
				});
			});
		})
	</script>




</body>
</html>