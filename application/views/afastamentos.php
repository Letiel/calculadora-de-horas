<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Afastamentos</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/x-editable/css/bootstrap-editable.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Tipos de Afastamento
									<a href="/afastamentos/cadastrar-tipo" class="btn btn-primary float-right"><span class="batch-icon batch-icon-marquee-plus mr-3"></span>Novo</a>
								</div>
								<div class="card-body">
									<p class='text-info'><b>Dica!</b> Clique sobre o nome para alterá-lo.</p>
									<table class="table table-datatable table-striped table-hover table-responsive">
										<thead>
											<tr>
												<th>Tipo</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($afastamentos as $afastamento): ?>
												<tr>
													<td>
														<a href="#" class="editavel" data-type="text" data-pk="<?= $afastamento->id ?>" data-name="fechamento" data-url="/afastamentos/editable" data-title="Tipo"><?= $afastamento->descricao ?></a>
													</td>
													<td>
														<a href='<?= $link ?><?= $afastamento->id ?>' class="btn btn-outline-success"><i class='text-success batch-icon batch-icon-user-alt'></i> Atribuir</a>
														<button value="<?= $afastamento->id ?>" class="btn btn-danger btn-excluir"><i class='batch-icon batch-icon-bin'></i></button>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
									<?= $paginacao ?>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/x-editable/js/bootstrap-editable.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){

				$(".editavel").editable({
					emptytext: 'Não Informado',
					ajaxOptions: {
						dataType: 'json'
					},
					success: function(response, newValue) {
						if(!response) {
							return "Erro desconhecido";
						}

						if(response.success === false) {
							return response.msg;
						}
					}
				});

			$(".table-datatable").DataTable({
				paging: false,
				searching: false,
			    ordering:  false,
			    responsive: true,
			    info: false,
			    language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    }
				}
			});

			$(".btn-excluir").click(function(){
				if(confirm("Todos os afastamentos de funcionários deste tipo serão descartados. Tem certeza?")){
					$.post("/afastamentos/excluir", {
						id: $(this).val()
					}, function(result){
						console.log(result);
						if(result == ""){
							location.reload();
						}else{
							alert("Ocorreu um problema.");
						}
					});
				}
			});
		});
	</script>
</body>
</html>
