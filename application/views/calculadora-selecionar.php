<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Selecionar Funcionário</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Selecione um Funcionário para o Cálculo
								</div>
								<div class="card-body">
									<table class="table table-datatable table-striped table-hover table-responsive">
										<thead>
											<tr>
												<th>Nome</th>
												<th>Empresa</th>
												<th>Horário</th>
												<th>Departamento</th>
												<th>Setor</th>
												<th>Alterar</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($funcionarios as $funcionario): ?>
												<tr>
													<td><?= $funcionario->nome ?></td>
													<td><?= $funcionario->nome_fantasia ?></td>
													<td><?= $funcionario->nome_horario ?></td>
													<td><?= $funcionario->nome_departamento ?></td>
													<td><?= $funcionario->nome_setor ?></td>
													<td>
														<a href="/calculadora/calcular/<?= $funcionario->id ?>" class="btn btn-success">Selecionar</a>
														<!-- <button class="btn btn-danger btn-md btn-excluir" value="<?= $funcionario->id ?>"><span class="batch-icon batch-icon-bin-alt-2"></span></button> -->
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
									<?= $paginacao ?>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			toastr.info('Selecione o Funcionário para Calcular.');
		});
	</script>
</body>
</html>
