<!DOCTYPE html>
<html>
<head>
	<title>Relatório de Afastamentos</title>
	<?php include "inc/headBasico.php"; ?>
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet"> -->
	<style type="text/css">
		body{
			background-color: #fdfdfd;
			font-family: 'Raleway', sans-serif;
		}

		.table tr td, .table tr th{
			padding: 0 !important;
			text-align: center;
		}
	</style>
	
	<script defer type="text/javascript" src="/fontawesome/js/fontawesome-all.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<div class="pt-2 mt-3 pl-5 pb-3 float-left infos">
					<h2>Informações:</h2>
					<h5>Funcionario: <?= $funcionario->nome ?></h5>
					<h5>Empresa: <?= $funcionario->nome_empresa ?></h5>
					<h5>Departamento: <?= $funcionario->nome_departamento ?></h5>
					<h5>Período selecionado: <?= date("d/m/Y", strtotime($post["de"]))." até ".date("d/m/Y", strtotime($post["ate"]))  ?></h5>
				</div>
				<div class="col-12 float-left">
					<hr />
				</div>
			</div>
			<div class="col-12">
				<div class="calculos" style="margin-bottom: 150px;">
					<h3 class='mt-3 pl-4 h3_calcular'>Total de Afastamentos do Funcionário no Período</h3>
					<p class="text-info">* Esta calculadora ainda está em fase de testes. Por favor, revise os resultados dos cálculos.</p>
					<div class="table-responsive">
						<table class="table table-datatable table-bordered">
							<thead>
								<tr>
									<th>Dia</th>
									<th>Horas Dentro dos Turnos</th>
									<th>Total de Horas</th>
								</tr>
							</thead>
							<tbody id="calcularTable">
								<?php foreach ($resultado->data["dias"] as $dia): ?>
									<tr>
										<td><?= date("d/m/Y", strtotime($dia["data"])) ?></td>
										<td><?= $dia["dentro"] ?></td>
										<td><?= $dia["total"] ?></td>
									</tr>
								<?php endforeach ?>
								<tr>
									<td><b>Total</b></td>
									<td><b><?= $resultado->total_dentro ?></b></td>
									<td><b><?= $resultado->total ?></b></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include "inc/js.php" ?>

	<!-- DATATABLES ADICIONAIS -->
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

	<script type="text/javascript">
		var buttonCommon = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                	if($(node).find("input").val() != undefined && $(node).find("input").val() != "undefined"){
	                		return "<center>"+$(node).find("input").val()+"</center>";
	                	}else if($(node).find("small").html() != undefined && $(node).find("small").html() != "undefined"){
	                		return "<center>"+$(node).find("small").html()+"</center>";
	                    }else if($(node).find("span").html() != undefined && $(node).find("span").html() != "undefined"){
	                		return "<center>"+$(node).find("span").html()+"</center>";
	                    }else if($(node).find("b").html() != undefined && $(node).find("b").html() != "undefined"){
	                		return "<center><b>"+$(node).find("b").html()+"</b></center>";
	                    }else if($(node).find("td").html() != undefined && $(node).find("td").html() != "undefined"){
	                		return "<center>"+$(node).find("td").html()+"</center>";
	                    }else{
	                    	return "";
	                    }
	                }
	            }
	        }
	    };

	    var button2 = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                	if($(node).find("input").val() != undefined && $(node).find("input").val() != "undefined"){
	                		return $(node).find("input").val();
	                	}else if($(node).find("small").html() != undefined && $(node).find("small").html() != "undefined"){
	                		return $(node).find("small").html();
	                    }else if($(node).find("span").html() != undefined && $(node).find("span").html() != "undefined"){
	                		return $(node).find("span").html();
	                    }else if($(node).find("b").html() != undefined && $(node).find("b").html() != "undefined"){
	                		return $(node).find("b").html();
	                    }else if($(node).find("td").html() != undefined && $(node).find("td").html() != "undefined"){
	                		return $(node).find("td").html();
	                    }else{
	                    	return "";
	                    }
	                }
	            }
	        }
	    };
		var table;
		function iniciarDataTables(){
			table = $(".table-datatable").DataTable({
				paging: false,
				searching: false,
			    ordering:  false,
			    responsive: true,
			    info: false,
			    dom: 'Bfrtip',
			    buttons: [
	                $.extend( true, {}, button2, {
	                    extend: 'copyHtml5',
	                    orientation: 'landscape',
	                    text: 'Copiar',
	                    className: "btn btn-info",
	                } ),
	                $.extend( true, {}, button2, {
	                    extend: 'excelHtml5',
	                    text: "Excel",
	                    className: "btn btn-success",
	                } ),
	                <?php if($tipo == "Empresa"){ ?>
	                $.extend( true, {}, buttonCommon, {
	                    extend: 'print',
	                    text: "Imprimir",
	                    className: "btn btn-primary",
	                    customize: function ( win ) {
	                    	$(win.document.body).prepend("<div class='row'></div>");
	                    	$(win.document.body).find(".row").prepend("<div class='infos col-12'></div>");
                            $(win.document.body).find(".infos")
                                .css( 'font-size', '1rem' )
                                .prepend("<p>CNPJ: <b><?= $empresa->cnpj ?></b></p></p>")
                                .prepend("<p>Empresa: <b><?= $empresa->nome_fantasia ?></b></p></p>")
                                .prepend("<h5>Informações</h5>")
                                ;
                            
                            $(win.document.body).find( 'p' ).css("margin", ".1rem");
                            $(win.document.body).find( '.row' ).append("<hr />");
                            
                            $(win.document.body).find( 'h1' )
                                .css( 'font-size', '1.3rem' )
                                .css( 'margin-top', '.2rem' )
                                .html("<hr />Relatório")
                                ;
                        }
	                } )
	                <?php }else{ ?>
		                $.extend( true, {}, buttonCommon, {
		                    extend: 'print',
		                    text: "Imprimir",
		                    className: "btn btn-primary",
		                    customize: function ( win ) {
		                    	$(win.document.body).prepend("<div class='row'></div>");
		                    	$(win.document.body).find(".row").prepend("<div class='infos col-12'></div>");
	                            $(win.document.body).find(".infos")
	                                .css( 'font-size', '1rem' )
	                                .prepend("<p>CNPJ: <b><?= $departamento->cnpj ?></b></p></p>")
	                                .prepend("<p>Empresa: <b><?= $departamento->nome_empresa ?></b></p></p>")
	                                .prepend("<p>Departamento: <b><?= $departamento->nome ?></b></p></p>")
	                                .prepend("<h5>Informações</h5>")
	                                ;
	                            
	                            $(win.document.body).find( 'p' ).css("margin", ".1rem");
	                            $(win.document.body).find( '.row' ).append("<hr />");
	                            
	                            $(win.document.body).find( 'h1' )
	                                .css( 'font-size', '1.3rem' )
	                                .css( 'margin-top', '.2rem' )
	                                .html("<hr />Relatório")
	                                ;
	                        }
		                } )
                	<?php } ?>
	            ],
			    language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    }
				}
			});
		}

		function destroyDataTables(){
			table.destroy();
		}

		$(document).ready(function(){

			iniciarDataTables();
		});
	</script>
</body>
</html>