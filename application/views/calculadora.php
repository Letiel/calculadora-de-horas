<!DOCTYPE html>
<html>
<head>
	<title>Cálculo de horas</title>
	<?php include "inc/headBasico.php"; ?>
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet"> -->
	<style type="text/css">
		body{
			background-color: #fdfdfd;
			font-family: 'Raleway', sans-serif;
		}

		.btn_calcular, .esconder, .calcular{
			display: none !important;
		}

		.input-hora{
			width: 9%;
			margin: 0 1% 15px 0;
			float: left;
		}

		@media(max-width: 1178px){
			.input-hora{
				width: 19%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}

		@media(max-width: 988px){
			.input-hora{
				width: 49%;
				margin: 0 1% 15px 0;
				float: left;
			}			
		}
		.table tr td{
			padding: 0 !important;
		}
		.falta, .extra, .carga_horaria, .total_dentro, .coluna, .somas-periodos span{
			padding: 12px 0 0 0 !important;
			display: block;
			text-align: center;
			vertical-align: middle;
		}
		.somas-periodos span{
			font-weight: bold;
		}
		.table tr td input{
			width: 80%;
			border: none;
			background: transparent;
			padding: 8px;
			outline: none;
			border: none;
			border-radius: 0px;
			box-shadow: none;
		}
		.table tr td input:focus{
			border: none !important;
			box-shadow: none !important;
		}
		.table tr td input.hora.saida-1,
		.table tr td input.hora.saida-2,
		.table tr td input.hora.saida-3,
		.table tr td input.hora.saida-4,
		.table tr td input.hora.saida-5{
			width: calc(100% - 20px);
		}
		.table tr td input.checkDia{
			width: 20px;
			float: right;
			margin: 16px 0;
			display: none;
		}
		.table tr td input:focus{
			outline: none;
			background: #fff;
		}

		.calcular:focus,
		.calcular:hover,
		.calcular:active{
			outline: none;
			text-decoration: none;
		}

		.calcular_tudo{
			position: fixed;
			right: 5px;
			bottom: 5px;
			transition: 0.3s all;
		}

		.horarios tr td{
			font-weight: 500;
		}

		.folga {
		    padding: 0px 8px 0 8px !important;
		}
	</style>
	
	<script defer type="text/javascript" src="/fontawesome/js/fontawesome-all.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<div class="col-5 pt-2 mt-3 pl-5 pb-3 float-left infos" style="border-right: 1px solid rgba(0,0,0,.1)">
					<h2>Informações:</h2>
					<h3><b><?= $funcionario->nome ?></b></h3>
					<h5>Empresa: <b><?= $funcionario->nome_empresa ?></b></h5>
					<h5>Departamento: <b><?= $funcionario->nome_departamento ?></b></h5>
					<h5>Setor: <b><?= $funcionario->nome_setor ?></b></h5>
				</div>
				<div class="col-3 pt-2 mt-3 pl-5 pb-3 float-left" style="border-right: 1px solid rgba(0,0,0,.1)">
					<h3>Horários</h3>
					<?php 
						$domingo = "<tr><td>Dom:</td>";
						$segunda = "<tr><td>Seg:</td>";
						$terca = "<tr><td>Ter:</td>";
						$quarta = "<tr><td>Qua:</td>";
						$quinta = "<tr><td>Qui:</td>";
						$sexta = "<tr><td>Sex:</td>";
						$sabado = "<tr><td>Sáb:</td>";
						if(!empty($dia['domingo']->padrao_entrada_1) && $dia['domingo']->padrao_saida_1){
							$domingo .= "<td>".$dia['domingo']->padrao_entrada_1." - ".$dia['domingo']->padrao_saida_1."</td>";
						}
						if(!empty($dia['domingo']->padrao_entrada_2) && $dia['domingo']->padrao_saida_2){
							$domingo .= "<td>".$dia['domingo']->padrao_entrada_2." - ".$dia['domingo']->padrao_saida_2."</td>";
						}
						if(!empty($dia['domingo']->padrao_entrada_3) && $dia['domingo']->padrao_saida_3){
							$domingo .= "<td>".$dia['domingo']->padrao_entrada_3." - ".$dia['domingo']->padrao_saida_3."</td>";
						}
						if(!empty($dia['domingo']->padrao_entrada_4) && $dia['domingo']->padrao_saida_4){
							$domingo .= "<td>".$dia['domingo']->padrao_entrada_4." - ".$dia['domingo']->padrao_saida_4."</td>";
						}
						if(!empty($dia['domingo']->padrao_entrada_5) && $dia['domingo']->padrao_saida_5){
							$domingo .= "<td>".$dia['domingo']->padrao_entrada_5." - ".$dia['domingo']->padrao_saida_5."</td>";
						}
						
						if(!empty($dia['segunda']->padrao_entrada_1) && $dia['segunda']->padrao_saida_1){
							$segunda .= "<td>".$dia['segunda']->padrao_entrada_1." - ".$dia['segunda']->padrao_saida_1."</td>";
						}
						if(!empty($dia['segunda']->padrao_entrada_2) && $dia['segunda']->padrao_saida_2){
							$segunda .= "<td>".$dia['segunda']->padrao_entrada_2." - ".$dia['segunda']->padrao_saida_2."</td>";
						}
						if(!empty($dia['segunda']->padrao_entrada_3) && $dia['segunda']->padrao_saida_3){
							$segunda .= "<td>".$dia['segunda']->padrao_entrada_3." - ".$dia['segunda']->padrao_saida_3."</td>";
						}
						if(!empty($dia['segunda']->padrao_entrada_4) && $dia['segunda']->padrao_saida_4){
							$segunda .= "<td>".$dia['segunda']->padrao_entrada_4." - ".$dia['segunda']->padrao_saida_4."</td>";
						}
						if(!empty($dia['segunda']->padrao_entrada_5) && $dia['segunda']->padrao_saida_5){
							$segunda .= "<td>".$dia['segunda']->padrao_entrada_5." - ".$dia['segunda']->padrao_saida_5."</td>";
						}
						
						if(!empty($dia['terca']->padrao_entrada_1) && $dia['terca']->padrao_saida_1){
							$terca .= "<td>".$dia['terca']->padrao_entrada_1." - ".$dia['terca']->padrao_saida_1."</td>";
						}
						if(!empty($dia['terca']->padrao_entrada_2) && $dia['terca']->padrao_saida_2){
							$terca .= "<td>".$dia['terca']->padrao_entrada_2." - ".$dia['terca']->padrao_saida_2."</td>";
						}
						if(!empty($dia['terca']->padrao_entrada_3) && $dia['terca']->padrao_saida_3){
							$terca .= "<td>".$dia['terca']->padrao_entrada_3." - ".$dia['terca']->padrao_saida_3."</td>";
						}
						if(!empty($dia['terca']->padrao_entrada_4) && $dia['terca']->padrao_saida_4){
							$terca .= "<td>".$dia['terca']->padrao_entrada_4." - ".$dia['terca']->padrao_saida_4."</td>";
						}
						if(!empty($dia['terca']->padrao_entrada_5) && $dia['terca']->padrao_saida_5){
							$terca .= "<td>".$dia['terca']->padrao_entrada_5." - ".$dia['terca']->padrao_saida_5."</td>";
						}
						
						if(!empty($dia['quarta']->padrao_entrada_1) && $dia['quarta']->padrao_saida_1){
							$quarta .= "<td>".$dia['quarta']->padrao_entrada_1." - ".$dia['quarta']->padrao_saida_1."</td>";
						}
						if(!empty($dia['quarta']->padrao_entrada_2) && $dia['quarta']->padrao_saida_2){
							$quarta .= "<td>".$dia['quarta']->padrao_entrada_2." - ".$dia['quarta']->padrao_saida_2."</td>";
						}
						if(!empty($dia['quarta']->padrao_entrada_3) && $dia['quarta']->padrao_saida_3){
							$quarta .= "<td>".$dia['quarta']->padrao_entrada_3." - ".$dia['quarta']->padrao_saida_3."</td>";
						}
						if(!empty($dia['quarta']->padrao_entrada_4) && $dia['quarta']->padrao_saida_4){
							$quarta .= "<td>".$dia['quarta']->padrao_entrada_4." - ".$dia['quarta']->padrao_saida_4."</td>";
						}
						if(!empty($dia['quarta']->padrao_entrada_5) && $dia['quarta']->padrao_saida_5){
							$quarta .= "<td>".$dia['quarta']->padrao_entrada_5." - ".$dia['quarta']->padrao_saida_5."</td>";
						}
						
						if(!empty($dia['quinta']->padrao_entrada_1) && $dia['quinta']->padrao_saida_1){
							$quinta .= "<td>".$dia['quinta']->padrao_entrada_1." - ".$dia['quinta']->padrao_saida_1."</td>";
						}
						if(!empty($dia['quinta']->padrao_entrada_2) && $dia['quinta']->padrao_saida_2){
							$quinta .= "<td>".$dia['quinta']->padrao_entrada_2." - ".$dia['quinta']->padrao_saida_2."</td>";
						}
						if(!empty($dia['quinta']->padrao_entrada_3) && $dia['quinta']->padrao_saida_3){
							$quinta .= "<td>".$dia['quinta']->padrao_entrada_3." - ".$dia['quinta']->padrao_saida_3."</td>";
						}
						if(!empty($dia['quinta']->padrao_entrada_4) && $dia['quinta']->padrao_saida_4){
							$quinta .= "<td>".$dia['quinta']->padrao_entrada_4." - ".$dia['quinta']->padrao_saida_4."</td>";
						}
						if(!empty($dia['quinta']->padrao_entrada_5) && $dia['quinta']->padrao_saida_5){
							$quinta .= "<td>".$dia['quinta']->padrao_entrada_5." - ".$dia['quinta']->padrao_saida_5."</td>";
						}
						
						if(!empty($dia['sexta']->padrao_entrada_1) && $dia['sexta']->padrao_saida_1){
							$sexta .= "<td>".$dia['sexta']->padrao_entrada_1." - ".$dia['sexta']->padrao_saida_1."</td>";
						}
						if(!empty($dia['sexta']->padrao_entrada_2) && $dia['sexta']->padrao_saida_2){
							$sexta .= "<td>".$dia['sexta']->padrao_entrada_2." - ".$dia['sexta']->padrao_saida_2."</td>";
						}
						if(!empty($dia['sexta']->padrao_entrada_3) && $dia['sexta']->padrao_saida_3){
							$sexta .= "<td>".$dia['sexta']->padrao_entrada_3." - ".$dia['sexta']->padrao_saida_3."</td>";
						}
						if(!empty($dia['sexta']->padrao_entrada_4) && $dia['sexta']->padrao_saida_4){
							$sexta .= "<td>".$dia['sexta']->padrao_entrada_4." - ".$dia['sexta']->padrao_saida_4."</td>";
						}
						if(!empty($dia['sexta']->padrao_entrada_5) && $dia['sexta']->padrao_saida_5){
							$sexta .= "<td>".$dia['sexta']->padrao_entrada_5." - ".$dia['sexta']->padrao_saida_5."</td>";
						}

						if(!empty($dia['sabado']->padrao_entrada_1) && $dia['sabado']->padrao_saida_1){
							$sabado .= "<td>".$dia['sabado']->padrao_entrada_1." - ".$dia['sabado']->padrao_saida_1."</td>";
						}
						if(!empty($dia['sabado']->padrao_entrada_2) && $dia['sabado']->padrao_saida_2){
							$sabado .= "<td>".$dia['sabado']->padrao_entrada_2." - ".$dia['sabado']->padrao_saida_2."</td>";
						}
						if(!empty($dia['sabado']->padrao_entrada_3) && $dia['sabado']->padrao_saida_3){
							$sabado .= "<td>".$dia['sabado']->padrao_entrada_3." - ".$dia['sabado']->padrao_saida_3."</td>";
						}
						if(!empty($dia['sabado']->padrao_entrada_4) && $dia['sabado']->padrao_saida_4){
							$sabado .= "<td>".$dia['sabado']->padrao_entrada_4." - ".$dia['sabado']->padrao_saida_4."</td>";
						}
						if(!empty($dia['sabado']->padrao_entrada_5) && $dia['sabado']->padrao_saida_5){
							$sabado .= "<td>".$dia['sabado']->padrao_entrada_5." - ".$dia['sabado']->padrao_saida_5."</td>";
						}

						$domingo .= "</tr>";
						$segunda .= "</tr>";
						$terca .= "</tr>";
						$quarta .= "</tr>";
						$quinta .= "</tr>";
						$sexta .= "</tr>";
						$sabado .= "</tr>";
					?>
					<table class="horarios table">
						<?php echo $domingo ?>
						<?php echo $segunda ?>
						<?php echo $terca ?>
						<?php echo $quarta ?>
						<?php echo $quinta ?>
						<?php echo $sexta ?>
						<?php echo $sabado ?>
					</table>
				</div>
				<div class="periodo-horario col-3 pt-2 mt-3 float-right periodos">
					<h3>Período</h3>
					<div class="col-12 float-left">
						<p>De:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="batch-icon batch-icon-calendar"></i></span>
							<input type="text" name="periodo_inicio" id="periodo_inicio" class="form-control datepicker" value="<?= date('d/m/Y', strtotime('-1 month')) ?>">
						</div>
					</div>
					<div class="col-12 float-left">
						<p>Até:</p>
						<div class="input-group">
							<span class="input-group-addon"><i class="batch-icon batch-icon-calendar"></i></span>
							<input type="text" name="periodo_fim" id="periodo_fim" class="form-control datepicker" value="<?= date('d/m/Y') ?>">
						</div>
					</div>
				</div>
				<div class="col-12 float-left">
					<hr />
				</div>
			</div>
			<div class="col-12">
				<div class="calculos" style="display: none; margin-bottom: 150px;">
					<h3 class='mt-3 pl-4 h3_calcular'>Calcular Horários</h3>
					<button class="btn btn-sm btn-success calcular_tudo">Calcular Tudo</button>
					<p class="text-info">* Esta calculadora ainda está em fase de testes. Por favor, revise os resultados dos cálculos.</p>
					<div class="table-responsive">
						<table class="table table-datatable table-bordered">
							<thead>
								<tr>
									<th>Data</th>
									<th>Ent 1</th>
									<th>Saí 1</th>
									<th>Ent 2</th>
									<th>Saí 2</th>
									<th>Ent 3</th>
									<th>Saí 3</th>
									<th>Ent 4</th>
									<th>Saí 4</th>
									<th>Ent 5</th>
									<th>Saí 5</th>
									<th>Cg Hor.</th>
									<th>H.Norm.</th>
									<th>Falta</th>
									<th>Extra</th>
									<?php foreach ($extras as $extra): ?>
										<th><?= $extra->coluna ?></th>
									<?php endforeach ?>
									
								</tr>
							</thead>
							<tbody id="calcularTable">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal_carregando" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	        <h1 class="text-center"><i class="fas fa-spinner fa-spin"></i></h1>
	        <h3 class='text-center'>Calculando</h3>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
			<div class="modal-body">
				<h1 class="text-center text-danger"><i class="fas fa-exclamation"></i></h1>
				<!-- <h3 class='text-center'></h3> -->
				<h4 class="text-center text-danger mensagem"></h4>
				<div class="text-center">
					<button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
				</div>
			</div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php include "inc/js.php" ?>

	<!-- DATATABLES ADICIONAIS -->
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<!-- DATATABLES ADICIONAIS -->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<!-- <script src="/assets/js/datepicker-pt-br.js"></script> -->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>


	<!-- Moment -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment-with-locales.min.js"></script>

	<script type="text/javascript">
		function calculaDias(date1, date2){
			//formato do brasil 'pt-br'
			moment.locale('pt-br');
			//setando data1
			var data1 = moment(date1,'DD/MM/YYYY');
			//setando data2
			var data2 = moment(date2,'DD/MM/YYYY');
			
			//tirando a diferenca da data2 - data1 em dias
			var diff  = data2.diff(data1, 'days');

			return diff;
		}

		function toDate(texto) {
			let partes = texto.split('/');
			return new Date(partes[2], partes[1]-1, partes[0]);
		}

		function toString(date) {
			return ('0' + date.getDate()).slice(-2) + '/' +
			('0' + (date.getMonth() + 1)).slice(-2) + '/' +
			date.getFullYear();
		}

	</script>
	<!-- Moment -->

	<script type="text/javascript">
		function carregando(){
			$("#modal_carregando").modal("show");
		}

		function carregou(){
			$("#modal_carregando").modal("hide");			
		}

		function mensagem(mensagem){
			$(".mensagem").html(mensagem);
			$("#modal_mensagem").modal("show");
		}

		function iniciar(){
			if ($("#periodo_inicio").val() != "" && $("#periodo_fim").val() != "") {
				var data1 = moment($("#periodo_inicio").val(),'DD/MM/YYYY');
				//setando data2
				var data2 = moment($("#periodo_fim").val(),'DD/MM/YYYY');
				if(data2.diff(data1, 'months') > 2){
					alert("O período não pode ser maior do que 3 meses.");
				}

				totalDias = calculaDias($("#periodo_inicio").val(), $("#periodo_fim").val());


				let d1 = toDate($("#periodo_inicio").val()),
				d2 = toDate($("#periodo_fim").val()),
				datas = [];

				datas.push( toString(d1) );

				while ( d1 < d2 ) {
					d1.setDate( d1.getDate() + 1 );
					datas.push( toString(d1) );
				}

				if (totalDias >= 0) {
					result = "";
					$(".calculos").show();
					for (var i = 0; i < datas.length; i++) {
						data = datas[i].split("/");
						data = new Date(data[2]+"/"+data[1]+"/"+data[0]);
						var dias = ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"];
						dia_semana = dias[data.getDay()];
						
						result += '<tr class="conjunto conjunto-'+i+' '+datas[i].replace(/\//g, "-")+'"> <th style="min-width: 100px;" scope="row"><p><small>'+dia_semana+" "+datas[i]+'</small></p></th> <td> <span style="display: none;"></span><p></p><input type="text" class="hora entrada-1" placeholder="__:__"></td> <td> <span style="display: none;"></span><p></p><input type="text" class="hora saida-1" placeholder="__:__"></td> <td> <span style="display: none;"></span><p></p><input type="text" class="hora entrada-2" placeholder="__:__"></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora saida-2" placeholder="__:__"></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora entrada-3" placeholder="__:__"></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora saida-3" placeholder="__:__" ></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora entrada-4" placeholder="__:__"></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora saida-4" placeholder="__:__" ></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora entrada-5" placeholder="__:__"></td> <td><span style="display: none;"></span><p></p> <input type="text" class="hora saida-5" placeholder="__:__" ></td><td><span class="carga_horaria"></td><td><span class="total_dentro"></span></span></td> <td><span class="falta"></span></td> <td><span class="extra"></span><button style="display: none;" class="btn btn-link calcular" value="'+datas[i]+'"></button></td>';
						<?php foreach ($extras as $extra): 
							echo "result += '<td><span class=\"coluna col_$extra->id_coluna\"></span></td>';";
						endforeach ?>
						result += '</tr></td></tr>';
						//campos com total do período
					}
					result += '<tr class="somas-periodos"><th><b>SOMA</b></th><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><b class="resultado-carga_horaria"></b></td><td><b class="resultado-total_dentro"></b></td><td><b class="resultado-falta"></b></td><td><b class="resultado-extra"></b</td>';
					<?php foreach ($extras as $extra): 
						echo "result += '<td><b class=\"resultado-col_$extra->id_coluna\"></b></td>';";
					endforeach ?>
					result += '<td style="display: none;"></td></tr>';
					$("#calcularTable").html(result);
					$('.hora').mask('00:00');
					$("[data-toggle=popover]").popover();
					$(	".table").show();
					marcarFeriados();
				}

			}
		}

		function marcarPreenchidos(){
			$.post("/calculadora/getPreenchidos", {
				data_inicio: $("#periodo_inicio").val(),
				data_fim: $("#periodo_fim").val(),
				id_funcionario: <?= $funcionario->id ?>
			}, function(result){
				result = JSON.parse(result);
				if(result != null)
				for (var i = 0; i <= result.length - 1; i++) {
					r = result[i];

					if(r.h_entrada_1 != "" && r.h_entrada_1 != null){
						r.h_entrada_1 = r.h_entrada_1.split(":");
						r.h_entrada_1 = r.h_entrada_1[0]+":"+r.h_entrada_1[1];
						$("."+r.data+" .entrada-1").val(r.h_entrada_1);
					}
					if(r.h_entrada_2 != "" && r.h_entrada_2 != null){
						r.h_entrada_2 = r.h_entrada_2.split(":");
						r.h_entrada_2 = r.h_entrada_2[0]+":"+r.h_entrada_2[1];
						$("."+r.data+" .entrada-2").val(r.h_entrada_2);
					}
					if(r.h_entrada_3 != "" && r.h_entrada_3 != null){
						r.h_entrada_3 = r.h_entrada_3.split(":");
						r.h_entrada_3 = r.h_entrada_3[0]+":"+r.h_entrada_3[1];
						$("."+r.data+" .entrada-3").val(r.h_entrada_3);
					}
					if(r.h_entrada_4 != "" && r.h_entrada_4 != null){
						r.h_entrada_4 = r.h_entrada_4.split(":");
						r.h_entrada_4 = r.h_entrada_4[0]+":"+r.h_entrada_4[1];
						$("."+r.data+" .entrada-4").val(r.h_entrada_4);
					}
					if(r.h_entrada_5 != "" && r.h_entrada_5 != null){
						r.h_entrada_5 = r.h_entrada_5.split(":");
						r.h_entrada_5 = r.h_entrada_5[0]+":"+r.h_entrada_5[1];
						$("."+r.data+" .entrada-5").val(r.h_entrada_5);
					}

					if(r.h_saida_1 != "" && r.h_saida_1 != null){
						r.h_saida_1 = r.h_saida_1.split(":");
						r.h_saida_1 = r.h_saida_1[0]+":"+r.h_saida_1[1];
						$("."+r.data+" .saida-1").val(r.h_saida_1);
					}
					if(r.h_saida_2 != "" && r.h_saida_2 != null){
						r.h_saida_2 = r.h_saida_2.split(":");
						r.h_saida_2 = r.h_saida_2[0]+":"+r.h_saida_2[1];
						$("."+r.data+" .saida-2").val(r.h_saida_2);
					}
					if(r.h_saida_3 != "" && r.h_saida_3 != null){
						r.h_saida_3 = r.h_saida_3.split(":");
						r.h_saida_3 = r.h_saida_3[0]+":"+r.h_saida_3[1];
						$("."+r.data+" .saida-3").val(r.h_saida_3);
					}
					if(r.h_saida_4 != "" && r.h_saida_4 != null){
						r.h_saida_4 = r.h_saida_4.split(":");
						r.h_saida_4 = r.h_saida_4[0]+":"+r.h_saida_4[1];
						$("."+r.data+" .saida-4").val(r.h_saida_4);
					}
					if(r.h_saida_5 != "" && r.h_saida_5 != null){
						r.h_saida_5 = r.h_saida_5.split(":");
						r.h_saida_5 = r.h_saida_5[0]+":"+r.h_saida_5[1];
						$("."+r.data+" .saida-5").val(r.h_saida_5);
					}

					if(r.carga_horaria != "" && r.carga_horaria != null){
						r.carga_horaria = r.carga_horaria.split(":");
						r.carga_horaria = r.carga_horaria[0]+":"+r.carga_horaria[1];
						$("."+r.data+" .carga_horaria").html(r.carga_horaria);
					}
					if(r.total_dentro != "" && r.total_dentro != null){
						r.total_dentro = r.total_dentro.split(":");
						r.total_dentro = r.total_dentro[0]+":"+r.total_dentro[1];
						$("."+r.data+" .total_dentro").html(r.total_dentro);
					}
					if(r.extra != "" && r.extra != null){
						r.extra = r.extra.split(":");
						r.extra = r.extra[0]+":"+r.extra[1];
						$("."+r.data+" .extra").html(r.extra);
					}
					if(r.falta != "" && r.falta != null){
						r.falta = r.falta.split(":");
						r.falta = r.falta[0]+":"+r.falta[1];
						$("."+r.data+" .falta").html(r.falta);
					}

					<?php foreach ($extras as $extra): ?>
						if(r.<?= "col_".$extra->id_coluna ?> != "" && r.<?= "col_".$extra->id_coluna ?> != null){
							r.<?= "col_".$extra->id_coluna ?> = r.<?= "col_".$extra->id_coluna ?>.split(":");
							r.<?= "col_".$extra->id_coluna ?> = r.<?= "col_".$extra->id_coluna ?>[0]+":"+r.<?= "col_".$extra->id_coluna ?>[1];
							$("."+r.data+" .col_<?= $extra->id_coluna ?>").html(r.<?= "col_".$extra->id_coluna ?>);
						}
					<?php endforeach ?>
				}
				marcarAfastamentos();
			});
		}

		function marcarAfastamentos(){
			$.post("/calculadora/getAfastamentos", {
				data_inicio: $("#periodo_inicio").val(),
				data_fim: $("#periodo_fim").val(),
				id_funcionario: <?= $funcionario->id ?>
			}, function(result){
				result = JSON.parse(result);
				if(result != null)
				for (var i = 0; i <= result.length - 1; i++) {
					r = result[i];
					
					if(r.horario_entrada_1 != null && r.horario_entrada_1!= ""){
						if($("."+r.dia+" .entrada-1").val() != null && $("."+r.dia+" .entrada-1").val() != ""){
							$("."+r.dia+" .entrada-1").addClass("folga");
							parcial = r.horario_entrada_1.indexOf("*.");
							if(parcial != "-1"){
								r.horario_entrada_1 = r.horario_entrada_1.replace("*.", "");
								$("."+r.dia+" .entrada-1").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .entrada-1").parent().find("p").html(r.horario_entrada_1);
							}
							$("."+r.dia+" .entrada-1").parent().prop("title", r.horario_entrada_1);
						}else{
							$("."+r.dia+" .entrada-1").val(r.horario_entrada_1);
						}
					}

					if(r.horario_entrada_2 != null && r.horario_entrada_2!= ""){
						if($("."+r.dia+" .entrada-2").val() != null && $("."+r.dia+" .entrada-2").val() != ""){
							$("."+r.dia+" .entrada-2").addClass("folga");
							parcial = r.horario_entrada_2.indexOf("*.");
							if(parcial != "-1"){
								r.horario_entrada_2 = r.horario_entrada_2.replace("*.", "");
								$("."+r.dia+" .entrada-2").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .entrada-2").parent().find("p").html(r.horario_entrada_2);
							}
							$("."+r.dia+" .entrada-2").parent().prop("title", r.horario_entrada_2);
						}else{
							$("."+r.dia+" .entrada-2").val(r.horario_entrada_1);
						}
					}

					if(r.horario_entrada_3 != null && r.horario_entrada_3!= ""){
						if($("."+r.dia+" .entrada-3").val() != null && $("."+r.dia+" .entrada-3").val() != ""){
							$("."+r.dia+" .entrada-3").addClass("folga");
							parcial = r.horario_entrada_3.indexOf("*.");
							if(parcial != "-1"){
								r.horario_entrada_3 = r.horario_entrada_3.replace("*.", "");
								$("."+r.dia+" .entrada-3").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .entrada-3").parent().find("p").html(r.horario_entrada_3);
							}

							$("."+r.dia+" .entrada-3").parent().prop("title", r.horario_entrada_3);
						}else{
							$("."+r.dia+" .entrada-3").val(r.horario_entrada_1);
						}
					}

					if(r.horario_entrada_4 != null && r.horario_entrada_4!= ""){
						if($("."+r.dia+" .entrada-4").val() != null && $("."+r.dia+" .entrada-4").val() != ""){
							$("."+r.dia+" .entrada-4").addClass("folga");
							parcial = r.horario_entrada_4.indexOf("*.");
							if(parcial != "-1"){
								r.horario_entrada_4 = r.horario_entrada_4.replace("*.", "");
								$("."+r.dia+" .entrada-4").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .entrada-4").parent().find("p").html(r.horario_entrada_4);
							}
							$("."+r.dia+" .entrada-4").parent().prop("title", r.horario_entrada_4);
						}else{
							$("."+r.dia+" .entrada-4").val(r.horario_entrada_1);
						}
					}

					if(r.horario_entrada_5 != null && r.horario_entrada_5!= ""){
						if($("."+r.dia+" .entrada-5").val() != null && $("."+r.dia+" .entrada-5").val() != ""){
							$("."+r.dia+" .entrada-5").addClass("folga");
							parcial = r.horario_entrada_5.indexOf("*.");
							if(parcial != "-1"){
								r.horario_entrada_5 = r.horario_entrada_5.replace("*.", "");
								$("."+r.dia+" .entrada-5").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .entrada-5").parent().find("p").html(r.horario_entrada_5);
							}
							$("."+r.dia+" .entrada-5").parent().prop("title", r.horario_entrada_5);
						}else{
							$("."+r.dia+" .entrada-5").val(r.horario_entrada_1);
						}
					}

					// 
					if(r.horario_saida_1 != null && r.horario_saida_1!= ""){
						if($("."+r.dia+" .saida-1").val() != null && $("."+r.dia+" .saida-1").val() != ""){
							$("."+r.dia+" .saida-1").addClass("folga");
							parcial = r.horario_saida_1.indexOf("*.");
							if(parcial != "-1"){
								r.horario_saida_1 = r.horario_saida_1.replace("*.", "");
								$("."+r.dia+" .saida-1").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .saida-1").parent().find("p").html(r.horario_saida_1);
							}
							$("."+r.dia+" .saida-1").parent().prop("title", r.horario_saida_1);
						}else{
							$("."+r.dia+" .saida-1").val(r.horario_saida_1);
						}
					}

					if(r.horario_saida_2 != null && r.horario_saida_2!= ""){
						if($("."+r.dia+" .saida-2").val() != null && $("."+r.dia+" .saida-2").val() != ""){
							$("."+r.dia+" .saida-2").addClass("folga");
							parcial = r.horario_saida_2.indexOf("*.");
							if(parcial != "-1"){
								r.horario_saida_2 = r.horario_saida_2.replace("*.", "");
								$("."+r.dia+" .saida-2").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .saida-2").parent().find("p").html(r.horario_saida_2);
							}
							$("."+r.dia+" .saida-2").parent().prop("title", r.horario_saida_2);
						}else{
							$("."+r.dia+" .saida-2").val(r.horario_saida_1);
						}
					}

					if(r.horario_saida_3 != null && r.horario_saida_3!= ""){
						if($("."+r.dia+" .saida-3").val() != null && $("."+r.dia+" .saida-3").val() != ""){
							$("."+r.dia+" .saida-3").addClass("folga");
							parcial = r.horario_saida_3.indexOf("*.");
							if(parcial != "-1"){
								r.horario_saida_3 = r.horario_saida_3.replace("*.", "");
								$("."+r.dia+" .saida-3").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .saida-3").parent().find("p").html(r.horario_saida_3);
							}
							$("."+r.dia+" .saida-3").parent().prop("title", r.horario_saida_3);
						}else{
							$("."+r.dia+" .saida-3").val(r.horario_saida_1);
						}
					}

					if(r.horario_saida_4 != null && r.horario_saida_4!= ""){
						if($("."+r.dia+" .saida-4").val() != null && $("."+r.dia+" .saida-4").val() != ""){
							$("."+r.dia+" .saida-4").addClass("folga");
							parcial = r.horario_saida_4.indexOf("*.");
							if(parcial != "-1"){
								r.horario_saida_4 = r.horario_saida_4.replace("*.", "");
								$("."+r.dia+" .saida-4").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .saida-4").parent().find("p").html(r.horario_saida_4);
							}
							$("."+r.dia+" .saida-4").parent().prop("title", r.horario_saida_4);
						}else{
							$("."+r.dia+" .saida-4").val(r.horario_saida_1);
						}
					}

					if(r.horario_saida_5 != null && r.horario_saida_5!= ""){
						if($("."+r.dia+" .saida-5").val() != null && $("."+r.dia+" .saida-5").val() != ""){
							$("."+r.dia+" .saida-5").addClass("folga");
							parcial = r.horario_saida_5.indexOf("*.");
							if(parcial != "-1"){
								r.horario_saida_5 = r.horario_saida_5.replace("*.", "");
								$("."+r.dia+" .saida-5").parent().find("p").html("*");
							}else{
								$("."+r.dia+" .saida-5").parent().find("p").html(r.horario_saida_5);
							}
							$("."+r.dia+" .saida-5").parent().prop("title", r.horario_saida_5);
						}else{
							$("."+r.dia+" .saida-5").val(r.horario_entrada_1);
						}
					}

					
					// $("."+r.dia+" .entrada-2").val(r.horario_entrada_2);
					// $("."+r.dia+" .entrada-3").val(r.horario_entrada_3);
					// $("."+r.dia+" .entrada-4").val(r.horario_entrada_4);
					// $("."+r.dia+" .entrada-5").val(r.horario_entrada_5);

					// $("."+r.dia+" .saida-1").val(r.horario_saida_1);
					// $("."+r.dia+" .saida-2").val(r.horario_saida_2);
					// $("."+r.dia+" .saida-3").val(r.horario_saida_3);
					// $("."+r.dia+" .saida-4").val(r.horario_saida_4);
					// $("."+r.dia+" .saida-5").val(r.horario_saida_5);
					// $("."+r.dia+" input").addClass("folga");
				}
				iniciarDataTables();
			});
		}

		function marcarFeriados(){
			<?php foreach ($feriados as $feriado): ?>
				$(".<?= $feriado->data ?>").addClass("feriado");
				$(".<?= $feriado->data ?>").prop("title", "<?= $feriado->nome ?>");
			<?php endforeach ?>
			$(".feriado input").val("Feriado");
			$(".feriado th p").css("color", "#C00");
			$(".feriado tr").css("color", "#C00");
			$(".feriado td").css("color", "#C00");
			$(".feriado td input").css("color", "#C00");
			marcarPreenchidos();
		}

		
		// function renderColumn(value, column, row, iDataIndex) {
		//     if (column != sortColumn && $(value).find('input').length > 0) {
		//         return $(value).find('input').val();
		//     }
		//     console.log(value);
		//     return value;
		// }

		var buttonCommon = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                	if($(node).find("input").val() != undefined && $(node).find("input").val() != "undefined"){
	                		return "<center>"+"<p>"+$(node).find("p").html()+"</p>"+$(node).find("input").val()+"</center>";
	                	}else if($(node).find("small").html() != undefined && $(node).find("small").html() != "undefined"){
	                		return "<center>"+$(node).find("small").html()+"</center>";
	                    }else if($(node).find("span").html() != undefined && $(node).find("span").html() != "undefined"){
	                		return "<center>"+$(node).find("span").html()+"</center>";
	                    }else if($(node).find("b").html() != undefined && $(node).find("b").html() != "undefined"){
	                		return "<center><b>"+$(node).find("b").html()+"</b></center>";
	                    }else if($(node).find("td").html() != undefined && $(node).find("td").html() != "undefined"){
	                		return "<center>"+$(node).find("td").html()+"</center>";
	                    }else{
	                    	return "";
	                    }
	                }
	            }
	        }
	    };

	    var button2 = {
	        exportOptions: {
	            format: {
	                body: function ( data, row, column, node ) {
	                	if($(node).find("input").val() != undefined && $(node).find("input").val() != "undefined"){
	                		if($(node).find("p").html() != "" && ""+$(node).find("p").html() != null)
	                			return ""+$(node).find("p").html()+" - "+$(node).find("input").val();
	                		else
	                			return $(node).find("input").val();
	                	}else if($(node).find("small").html() != undefined && $(node).find("small").html() != "undefined"){
	                		return $(node).find("small").html();
	                    }else if($(node).find("span").html() != undefined && $(node).find("span").html() != "undefined"){
	                		return $(node).find("span").html();
	                    }else if($(node).find("b").html() != undefined && $(node).find("b").html() != "undefined"){
	                		return $(node).find("b").html();
	                    }else if($(node).find("td").html() != undefined && $(node).find("td").html() != "undefined"){
	                		return $(node).find("td").html();
	                    }else{
	                    	return "";
	                    }
	                }
	            }
	        }
	    };
		var table;
		function iniciarDataTables(){
			table = $(".table-datatable").DataTable({
				paging: false,
				searching: false,
			    ordering:  false,
			    responsive: true,
			    info: false,
			    dom: 'Bfrtip',
			    buttons: [
	                $.extend( true, {}, button2, {
	                    extend: 'copyHtml5',
	                    orientation: 'landscape',
	                    text: 'Copiar',
	                    className: "btn btn-info",
	                } ),
	                $.extend( true, {}, button2, {
	                    extend: 'excelHtml5',
	                    text: "Excel",
	                    className: "btn btn-success",
	                } ),
	                $.extend( true, {}, buttonCommon, {
	                    extend: 'print',
	                    text: "Imprimir",
	                    className: "btn btn-primary",
	                    customize: function ( win ) {
	                    	$(win.document.body).prepend("<div class='row'></div>");
	                    	$(win.document.body).find(".row").prepend("<div class='horarios col-6'></div>");
	                    	$(win.document.body).find(".row").prepend("<div class='infos col-6' style='border-right: 1px solid #ccc;'></div>");
                            $(win.document.body).find(".infos")
                                .css( 'font-size', '1rem' )
                                .prepend("<p>Setor: <b><?= $funcionario->nome_setor ?></b></p>")
                                .prepend("<p>Departamento: <b><?= $funcionario->nome_departamento ?></b></p>")
                                .prepend("<p>Empresa: <b><?= $funcionario->nome_empresa ?></b></p>")
                                .prepend("<p>CNPJ: <b><?= $funcionario->cnpj ?></b></p>")
                                .prepend("<p>Nº Folha: <b><?= $funcionario->numero_folha ?></b></p>")
                                .prepend("<p>PIS / PASEP: <b><?= $funcionario->pis ?></b></p>")
                                .prepend("<p>Admissão: <b><?= date('d/m/Y', strtotime($funcionario->admissao)) ?></b></p>")
                                .prepend("<p>CTPS: <b><?= $funcionario->ctps ?></b></p>")
                                .prepend("<p>Nome: <b><?= $funcionario->nome ?></b></p></p>")
                                .prepend("<h5>Informações</h5>")
                                ;
                            $(win.document.body).find(".horarios")
                            <?php 
                            	$domingo = "";
                            	$segunda = "";
                            	$terca = "";
                            	$quarta = "";
                            	$quinta = "";
                            	$sexta = "";
                            	$sabado = "";
                            	if(!empty($dia['domingo']->padrao_entrada_1) && $dia['domingo']->padrao_saida_1){
                            		$domingo .= $dia['domingo']->padrao_entrada_1." - ".$dia['domingo']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['domingo']->padrao_entrada_2) && $dia['domingo']->padrao_saida_2){
                            		$domingo .= $dia['domingo']->padrao_entrada_2." - ".$dia['domingo']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['domingo']->padrao_entrada_3) && $dia['domingo']->padrao_saida_3){
                            		$domingo .= $dia['domingo']->padrao_entrada_3." - ".$dia['domingo']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['domingo']->padrao_entrada_4) && $dia['domingo']->padrao_saida_4){
                            		$domingo .= $dia['domingo']->padrao_entrada_4." - ".$dia['domingo']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['domingo']->padrao_entrada_5) && $dia['domingo']->padrao_saida_5){
                            		$domingo .= $dia['domingo']->padrao_entrada_5." - ".$dia['domingo']->padrao_saida_5." ";
                            	}
                            	
                            	if(!empty($dia['segunda']->padrao_entrada_1) && $dia['segunda']->padrao_saida_1){
                            		$segunda .= $dia['segunda']->padrao_entrada_1." - ".$dia['segunda']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['segunda']->padrao_entrada_2) && $dia['segunda']->padrao_saida_2){
                            		$segunda .= $dia['segunda']->padrao_entrada_2." - ".$dia['segunda']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['segunda']->padrao_entrada_3) && $dia['segunda']->padrao_saida_3){
                            		$segunda .= $dia['segunda']->padrao_entrada_3." - ".$dia['segunda']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['segunda']->padrao_entrada_4) && $dia['segunda']->padrao_saida_4){
                            		$segunda .= $dia['segunda']->padrao_entrada_4." - ".$dia['segunda']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['segunda']->padrao_entrada_5) && $dia['segunda']->padrao_saida_5){
                            		$segunda .= $dia['segunda']->padrao_entrada_5." - ".$dia['segunda']->padrao_saida_5." ";
                            	}
                            	
                            	if(!empty($dia['terca']->padrao_entrada_1) && $dia['terca']->padrao_saida_1){
                            		$terca .= $dia['terca']->padrao_entrada_1." - ".$dia['terca']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['terca']->padrao_entrada_2) && $dia['terca']->padrao_saida_2){
                            		$terca .= $dia['terca']->padrao_entrada_2." - ".$dia['terca']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['terca']->padrao_entrada_3) && $dia['terca']->padrao_saida_3){
                            		$terca .= $dia['terca']->padrao_entrada_3." - ".$dia['terca']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['terca']->padrao_entrada_4) && $dia['terca']->padrao_saida_4){
                            		$terca .= $dia['terca']->padrao_entrada_4." - ".$dia['terca']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['terca']->padrao_entrada_5) && $dia['terca']->padrao_saida_5){
                            		$terca .= $dia['terca']->padrao_entrada_5." - ".$dia['terca']->padrao_saida_5." ";
                            	}
                            	
                            	if(!empty($dia['quarta']->padrao_entrada_1) && $dia['quarta']->padrao_saida_1){
                            		$quarta .= $dia['quarta']->padrao_entrada_1." - ".$dia['quarta']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['quarta']->padrao_entrada_2) && $dia['quarta']->padrao_saida_2){
                            		$quarta .= $dia['quarta']->padrao_entrada_2." - ".$dia['quarta']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['quarta']->padrao_entrada_3) && $dia['quarta']->padrao_saida_3){
                            		$quarta .= $dia['quarta']->padrao_entrada_3." - ".$dia['quarta']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['quarta']->padrao_entrada_4) && $dia['quarta']->padrao_saida_4){
                            		$quarta .= $dia['quarta']->padrao_entrada_4." - ".$dia['quarta']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['quarta']->padrao_entrada_5) && $dia['quarta']->padrao_saida_5){
                            		$quarta .= $dia['quarta']->padrao_entrada_5." - ".$dia['quarta']->padrao_saida_5." ";
                            	}
                            	
                            	if(!empty($dia['quinta']->padrao_entrada_1) && $dia['quinta']->padrao_saida_1){
                            		$quinta .= $dia['quinta']->padrao_entrada_1." - ".$dia['quinta']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['quinta']->padrao_entrada_2) && $dia['quinta']->padrao_saida_2){
                            		$quinta .= $dia['quinta']->padrao_entrada_2." - ".$dia['quinta']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['quinta']->padrao_entrada_3) && $dia['quinta']->padrao_saida_3){
                            		$quinta .= $dia['quinta']->padrao_entrada_3." - ".$dia['quinta']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['quinta']->padrao_entrada_4) && $dia['quinta']->padrao_saida_4){
                            		$quinta .= $dia['quinta']->padrao_entrada_4." - ".$dia['quinta']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['quinta']->padrao_entrada_5) && $dia['quinta']->padrao_saida_5){
                            		$quinta .= $dia['quinta']->padrao_entrada_5." - ".$dia['quinta']->padrao_saida_5." ";
                            	}
                            	
                            	if(!empty($dia['sexta']->padrao_entrada_1) && $dia['sexta']->padrao_saida_1){
                            		$sexta .= $dia['sexta']->padrao_entrada_1." - ".$dia['sexta']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['sexta']->padrao_entrada_2) && $dia['sexta']->padrao_saida_2){
                            		$sexta .= $dia['sexta']->padrao_entrada_2." - ".$dia['sexta']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['sexta']->padrao_entrada_3) && $dia['sexta']->padrao_saida_3){
                            		$sexta .= $dia['sexta']->padrao_entrada_3." - ".$dia['sexta']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['sexta']->padrao_entrada_4) && $dia['sexta']->padrao_saida_4){
                            		$sexta .= $dia['sexta']->padrao_entrada_4." - ".$dia['sexta']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['sexta']->padrao_entrada_5) && $dia['sexta']->padrao_saida_5){
                            		$sexta .= $dia['sexta']->padrao_entrada_5." - ".$dia['sexta']->padrao_saida_5." ";
                            	}

                            	if(!empty($dia['sabado']->padrao_entrada_1) && $dia['sabado']->padrao_saida_1){
                            		$sabado .= $dia['sabado']->padrao_entrada_1." - ".$dia['sabado']->padrao_saida_1." ";
                            	}
                            	if(!empty($dia['sabado']->padrao_entrada_2) && $dia['sabado']->padrao_saida_2){
                            		$sabado .= $dia['sabado']->padrao_entrada_2." - ".$dia['sabado']->padrao_saida_2." ";
                            	}
                            	if(!empty($dia['sabado']->padrao_entrada_3) && $dia['sabado']->padrao_saida_3){
                            		$sabado .= $dia['sabado']->padrao_entrada_3." - ".$dia['sabado']->padrao_saida_3." ";
                            	}
                            	if(!empty($dia['sabado']->padrao_entrada_4) && $dia['sabado']->padrao_saida_4){
                            		$sabado .= $dia['sabado']->padrao_entrada_4." - ".$dia['sabado']->padrao_saida_4." ";
                            	}
                            	if(!empty($dia['sabado']->padrao_entrada_5) && $dia['sabado']->padrao_saida_5){
                            		$sabado .= $dia['sabado']->padrao_entrada_5." - ".$dia['sabado']->padrao_saida_5." ";
                            	}
                            ?>
                            	.append("<h5>Horários</h5>")
                            	.append("<p>Domingo: <?= $domingo ?></p>")
                            	.append("<p>Segunda: <?= $segunda ?></p>")
                            	.append("<p>Terça: <?= $terca ?></p>")
                            	.append("<p>Quarta: <?= $quarta ?></p>")
                            	.append("<p>Quinta: <?= $quinta ?></p>")
                            	.append("<p>Sexta: <?= $sexta ?></p>")
                            	.append("<p>Sábado: <?= $sabado ?></p>")
                            	.append("<h5 style='padding-top: 5px;'>Período selecionado</h5>")
                            	.append("<p style='color: #D00;'>De "+$("#periodo_inicio").val()+" Até "+$("#periodo_fim").val()+"</p>")
                            ;
                            $(win.document.body).find( 'p' ).css("margin", ".1rem");
                            $(win.document.body).find( '.row' ).append("<hr />");
                            
                            $(win.document.body).find( 'h1' )
                                .css( 'font-size', '1.3rem' )
                                .css( 'margin-top', '.2rem' )
                                .html("<hr />Cartão Ponto")
                                ;
                        }
	                } )
	            ],
			    language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    }
				}
			});
		}

		function destroyDataTables(){
			table.destroy();
		}

		function calcular_tudo(){
			total_elementos = $(".conjunto").length;
			for(var i = 0; i < total_elementos; i++) {
				$(".conjunto-"+i).find(".calcular").val();
				$.ajax({
				  type: 'POST',
				  url: "/calculadora/efetuar_calculo",
				  data: {
					dia_calculo: $(".conjunto-"+i).find(".calcular").val(),
			
					id_funcionario: <?= $funcionario->id ?>,

					entrada1: $(".conjunto-"+i).find(".entrada-1").val(),
					saida1: $(".conjunto-"+i).find(".saida-1").val(),

					entrada2: $(".conjunto-"+i).find(".entrada-2").val(),
					saida2: $(".conjunto-"+i).find(".saida-2").val(),

					entrada3: $(".conjunto-"+i).find(".entrada-3").val(),
					saida3: $(".conjunto-"+i).find(".saida-3").val(),

					entrada4: $(".conjunto-"+i).find(".entrada-4").val(),
					saida4: $(".conjunto-"+i).find(".saida-4").val(),

					entrada5: $(".conjunto-"+i).find(".entrada-5").val(),
					saida5: $(".conjunto-"+i).find(".saida-5").val(),

				  },
				  success: function(result){
				  	resultado = jQuery.parseJSON(result);
				  	
				  	if(resultado.erro == "0"){
				  		if(resultado.falta != null){
				  			resultado.falta = resultado.falta.split(":");
				  			resultado.falta = resultado.falta[0]+":"+resultado.falta[1];
				  			$(".conjunto-"+i).find($(".falta")).html(resultado.falta);
				  		}
				  		if(resultado.extra != null){
				  			resultado.extra = resultado.extra.split(":");
				  			resultado.extra = resultado.extra[0]+":"+resultado.extra[1];
				  			$(".conjunto-"+i).find($(".extra")).html(resultado.extra);
				  		}
				  		if(resultado.carga_horaria != null){
				  			resultado.carga_horaria = resultado.carga_horaria.split(":");
				  			resultado.carga_horaria = resultado.carga_horaria[0]+":"+resultado.carga_horaria[1];
				  			$(".conjunto-"+i).find($(".carga_horaria")).html(resultado.carga_horaria);
				  		}
				  		if(resultado.total_dentro != null){
				  			resultado.total_dentro = resultado.total_dentro.split(":");
				  			resultado.total_dentro = resultado.total_dentro[0]+":"+resultado.total_dentro[1];
				  			$(".conjunto-"+i).find($(".total_dentro")).html(resultado.total_dentro);
				  		}
				  		<?php foreach ($extras as $extra): ?>
				  			if(resultado.<?= "col_".$extra->id_coluna ?> != null){
				  				resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>.split(":");
				  				resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>[0]+":"+resultado.<?= "col_".$extra->id_coluna ?>[1];
				  				$(".conjunto-"+i).find($(".<?= 'col_'.$extra->id_coluna ?>")).html(resultado.<?= "col_".$extra->id_coluna ?>);
				  			}
				  		<?php endforeach ?>
				  	}else{
				  		mensagem(resultado.mensagem);
				  	}
				  },
				  dataType: "text",
				  async:false
				});
			}

			$.ajax({
			  type: 'POST',
			  url: "/calculadora/calcular_periodo",
			  data: {
				data_inicio: $("#periodo_inicio").val(),
				data_fim: $("#periodo_fim").val(),
				id_funcionario: <?= $funcionario->id ?>,
			  },
			  success: function(result){
			  	resultado = jQuery.parseJSON(result);
			  	if(resultado.falta != null){
			  		resultado.falta = resultado.falta.split(":");
			  		resultado.falta = resultado.falta[0]+":"+resultado.falta[1];
		  			$(".resultado-falta").html(resultado.falta);
			  	}
			  	if(resultado.extra != null){
			  		resultado.extra = resultado.extra.split(":");
			  		resultado.extra = resultado.extra[0]+":"+resultado.extra[1];
		  			$(".resultado-extra").html(resultado.extra);
			  	}
			  	if(resultado.carga_horaria != null){
			  		resultado.carga_horaria = resultado.carga_horaria.split(":");
			  		resultado.carga_horaria = resultado.carga_horaria[0]+":"+resultado.carga_horaria[1];
		  			$(".resultado-carga_horaria").html(resultado.carga_horaria);
			  	}
			  	if(resultado.total_dentro != null){
			  		resultado.total_dentro = resultado.total_dentro.split(":");
			  		resultado.total_dentro = resultado.total_dentro[0]+":"+resultado.total_dentro[1];
		  			$(".resultado-total_dentro").html(resultado.total_dentro);
			  	}
		  		<?php foreach ($extras as $extra): ?>
			  		if(resultado.<?= "col_".$extra->id_coluna ?> != null){
			  			resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>.split(":");
			  			resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>[0]+":"+resultado.<?= "col_".$extra->id_coluna ?>[1];
		  				$(".<?= 'resultado-col_'.$extra->id_coluna ?>").html(resultado.<?= "col_".$extra->id_coluna ?>);
			  		}
		  		<?php endforeach ?>
			  },
			  dataType: "text",
			  async:false
			});


			carregou();
		}

		$(function(){
			iniciar();

			$( "body" ).on( "click", ".buttons-copy", function() {
				toastr.options.closeDuration = 300;
				toastr.options.timeOut = 3000;
				toastr.options.preventDuplicates = false;
			  	toastr.info('Copiado para área de transferência.');
			});

			$(".table-datatable input").change(function(){
				$(this).parent().find("span").html("<p>"+$(this).parent().find("p").html()+"</p>" + $(this).val());
			});

			$(".calcular_tudo").hover(function(){
				$(".calcular_tudo").removeClass("btn-sm");
				$(".calcular_tudo").addClass("btn-lg");
			});

			$(".calcular_tudo").mouseleave(function(){
				$(".calcular_tudo").removeClass("btn-lg");
				$(".calcular_tudo").addClass("btn-sm");
			});

			$(".calcular_tudo").click(function(){
				carregando();
				setTimeout(calcular_tudo, 500);
			});

			$("#modal_mensagem").on("hidden.bs.modal", function () {
				carregou();
			});

			// $.fn.datepicker.dates['pt-BR'] = {
			//     days: ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"],
			//     daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
			//     daysMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sá"],
			//     months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
			//     monthsShort: ["Jan", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
			//     today: "Hoje",
			//     clear: "Limpar",
			    
			//     weekStart: 0
			// };

			$('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
				// language: 'pt-BR'
			});

			$(".datepicker").change(function(){
				var data1 = moment($("#periodo_inicio").val(),'DD/MM/YYYY');
				//setando data2
				var data2 = moment($("#periodo_fim").val(),'DD/MM/YYYY');
				if(data2.diff(data1, 'months') > 2){
					alert("O período não pode ser maior do que 3 meses.");
					$("#calcularTable").html("");
					return false;
				}else{
					table.destroy();
					iniciar();
				}
			});

			$(document).on('click', ".calcular", function(event){
				carregando();
				$.post("/calculadora/efetuar_calculo", {
					dia_calculo: $(this).val(),
					id_funcionario: <?= $funcionario->id ?>,
					
					entrada1: $(event.target).parent().parent().find(".entrada-1").val(),
					saida1: $(event.target).parent().parent().find(".saida-1").val(),
					
					entrada2: $(event.target).parent().parent().find(".entrada-2").val(),
					saida2: $(event.target).parent().parent().find(".saida-2").val(),
					
					entrada3: $(event.target).parent().parent().find(".entrada-3").val(),
					saida3: $(event.target).parent().parent().find(".saida-3").val(),
					
					entrada4: $(event.target).parent().parent().find(".entrada-4").val(),
					saida4: $(event.target).parent().parent().find(".saida-4").val(),
					
					entrada5: $(event.target).parent().parent().find(".entrada-5").val(),
					saida5: $(event.target).parent().parent().find(".saida-5").val(),
					
				}, function(result){
					
					resultado = jQuery.parseJSON(result);
					
					if(resultado.erro == "0"){
						if(resultado.falta != null){
							resultado.falta = resultado.falta.split(":");
							resultado.falta = resultado.falta[0]+":"+resultado.falta[1];
							$(event.target).parent().parent().find($(".falta")).html(resultado.falta);
						}
						if(resultado.extra != null){
							resultado.extra = resultado.extra.split(":");
							resultado.extra = resultado.extra[0]+":"+resultado.extra[1];
							$(event.target).parent().parent().find($(".extra")).html(resultado.extra);
						}
						if(resultado.carga_horaria != null){
							resultado.carga_horaria = resultado.carga_horaria.split(":");
							resultado.carga_horaria = resultado.carga_horaria[0]+":"+resultado.carga_horaria[1];
							$(event.target).parent().parent().find($(".carga_horaria")).html(resultado.carga_horaria);
						}
						if(resultado.total_dentro != null){
							resultado.total_dentro = resultado.total_dentro.split(":");
							resultado.total_dentro = resultado.total_dentro[0]+":"+resultado.total_dentro[1];
							$(event.target).parent().parent().find($(".total_dentro")).html(resultado.total_dentro);
						}
						<?php foreach ($extras as $extra): ?>
							if(resultado.<?= "col_".$extra->id_coluna ?> != null){
								resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>.split(":");
								resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>[0]+":"+resultado.<?= "col_".$extra->id_coluna ?>[1];
								$(event.target).parent().parent().find($(".<?= 'col_'.$extra->id_coluna ?>")).html(resultado.<?= "col_".$extra->id_coluna ?>);
							}
						<?php endforeach ?>
						carregou();
					}else{
						mensagem(resultado.mensagem);
					}
				});
			});

			$(document).on('keydown', 'input', function(event){
				tecla = event.keyCode || event.which;
				if(tecla == 9){
					var elemento = $(this);
					var value = elemento.parent().parent().find(".calcular").val();
					var classe = elemento.attr("class");
					for(var i = 1; i <= 5; i++) {
						if(classe == "hora saida-"+i || classe == "hora saida-"+i+" folga"){
							$.post("/calculadora/efetuar_calculo", {
								dia_calculo: value,
								id_funcionario: <?= $funcionario->id ?>,
								
								entrada1: elemento.parent().parent().find(".entrada-1").val(),
								saida1: elemento.parent().parent().find(".saida-1").val(),
								
								entrada2: elemento.parent().parent().find(".entrada-2").val(),
								saida2: elemento.parent().parent().find(".saida-2").val(),
								
								entrada3: elemento.parent().parent().find(".entrada-3").val(),
								saida3: elemento.parent().parent().find(".saida-3").val(),
								
								entrada4: elemento.parent().parent().find(".entrada-4").val(),
								saida4: elemento.parent().parent().find(".saida-4").val(),
								
								entrada5: elemento.parent().parent().find(".entrada-5").val(),
								saida5: elemento.parent().parent().find(".saida-5").val(),
								
							}, function(result){
								
								resultado = jQuery.parseJSON(result);
								
								if(resultado.erro == "0"){

									if(resultado.falta != null){
										resultado.falta = resultado.falta.split(":");
										resultado.falta = resultado.falta[0]+":"+resultado.falta[1];
										$(event.target).parent().parent().find($(".falta")).html(resultado.falta);
									}
									if(resultado.extra != null){
										resultado.extra = resultado.extra.split(":");
										resultado.extra = resultado.extra[0]+":"+resultado.extra[1];
										$(event.target).parent().parent().find($(".extra")).html(resultado.extra);
									}
									if(resultado.carga_horaria != null){
										resultado.carga_horaria = resultado.carga_horaria.split(":");
										resultado.carga_horaria = resultado.carga_horaria[0]+":"+resultado.carga_horaria[1];
										$(event.target).parent().parent().find($(".carga_horaria")).html(resultado.carga_horaria);
									}
									if(resultado.total_dentro != null){
										resultado.total_dentro = resultado.total_dentro.split(":");
										resultado.total_dentro = resultado.total_dentro[0]+":"+resultado.total_dentro[1];
										$(event.target).parent().parent().find($(".total_dentro")).html(resultado.total_dentro);
									}

									elemento.parent().parent().find($(".falta")).html(resultado.falta);
									elemento.parent().parent().find($(".extra")).html(resultado.extra);
									elemento.parent().parent().find($(".carga_horaria")).html(resultado.carga_horaria);
									elemento.parent().parent().find($(".total_dentro")).html(resultado.total_dentro);
									<?php foreach ($extras as $extra): ?>
										elemento.parent().parent().find($(".<?= 'col_'.$extra->id_coluna ?>")).html("");
										if(resultado.<?= "col_".$extra->id_coluna ?> != null){
											resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>.split(":");
											resultado.<?= "col_".$extra->id_coluna ?> = resultado.<?= "col_".$extra->id_coluna ?>[0]+":"+resultado.<?= "col_".$extra->id_coluna ?>[1];
											elemento.parent().parent().find($(".<?= 'col_'.$extra->id_coluna ?>")).html(resultado.<?= "col_".$extra->id_coluna ?>);
										}
									<?php endforeach ?>
								}else{
									mensagem(resultado.mensagem);
								}
							});
						}
					}
				}
			});
		})
	</script>
</body>
</html>