<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Cadastrar Empresa</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Cadastrar Empresa
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Razão Social</label>
													<input class="form-control" type="text" name="razao_social" placeholder="Razão Social" value="<?= set_value('razao_social') ?>" />
													<p class="text-danger"><?= form_error("razao_social") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome Fantasia</label>
													<input class="form-control" type="text" name="nome_fantasia" placeholder="Nome Fantasia"value="<?= set_value('nome_fantasia') ?>" />
													<p class="text-danger"><?= form_error("nome_fantasia") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>CNPJ</label>
													<input class="form-control cnpj" type="text" name="cnpj" placeholder="CNPJ" value="<?= set_value('cnpj') ?>"/>
													<p class="text-danger"><?= form_error("cnpj") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Cidade</label>
													<select class="form-control" name="id_cidade" id="cidades">
														<option value="">Selecione</option>
													</select>
													<p class="text-danger"><?= form_error("id_cidade") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Endereço</label>
													<input class="form-control" type="text" name="endereco" placeholder="Endereço" value="<?= set_value('endereco') ?>"/>
													<p class="text-danger"><?= form_error("endereco") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome do Responsável</label>
													<input class="form-control" type="text" name="nome_responsavel" placeholder="Responsável" value="<?= set_value('nome_responsavel') ?>"/>
													<p class="text-danger"><?= form_error("nome_responsavel") ?></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#cidades").select2({
				theme: "bootstrap",
				ajax: {
				    url: '/empresas/select2_cidades',
				    dataType: 'json',
				    delay: 500,
				    data: function (params) {
				      return {
				        q: params.term,
				      };
				    },
				    processResults: function (data, params) {
				      return {
				        results: data.items,
				      };
				    },
				    cache: true
				  },
				  placeholder: 'Cidade',
				  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				  minimumInputLength: 1,
				  'locale': 'pt-br'
				});
				$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
			});
	</script>
</body>
</html>
