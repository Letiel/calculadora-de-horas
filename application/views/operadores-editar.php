<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Alterar Operador</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Alterar Operador
									<button  class="gerar_senha btn btn-warning float-right" value="<?= $operador->id ?>"><i class="batch-icon batch-icon-asterisk mr-3"></i>Gerar nova senha</button>
								</div>
								<div class="card-body">
									<form method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Nome</label>
													<input class="form-control" type="text" name="nome" placeholder="Nome" value="<?= $operador->nome ?>" required/>
													<p class="text-danger"><?= form_error("nome") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Login</label>
													<input class="form-control" type="text" name="login" placeholder="Login" value="<?= $operador->login ?>" required/>
													<p class="text-danger"><?= form_error("login") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Empresas</label>
													<select name="empresas[]" id="empresas" class="form-control" multiple="multiple">
														<?php foreach ($empresas as $empresa): ?>
															<option <?= in_array($empresa->id, $usuario_empresa) ? "selected" : "" ?> value="<?= $empresa->id ?>"><?= $empresa->nome_fantasia ?></option>
														<?php endforeach ?>
													</select>
													<p class="text-danger"><?= form_error("empresas[]") ?></p>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Tipo de usuário</label>
													<select name="adm" class="form-control">
														<option value="0" <?= !$operador->adm ? "selected" : "" ?>>Operador Normal</option>
														<option value="1" <?= $operador->adm ? "selected" : "" ?>>Operador Administrador</option>
													</select>
													<small>* Administradores tem acesso irrestrito ao sistema.</small>
												</div>
											</div>
											<div class="col-12">
												<p>* Usuários com <b>acesso administrativo</b> podem <b>alterar</b> dados de <b>outros operadores</b>, <b>inclusive os seus</b>.</p>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<button type="submit" class="btn btn-primary float-right">Salvar</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#empresas").select2({
				theme: "bootstrap",
				placeholder: 'Empresas',
				escapeMarkup: function (markup) { return markup; },
			});

			$(".gerar_senha").click(function(){
				if(confirm("Tem certeza? Uma nova senha será gerada.")){
					$.post("/operadores/gerar-nova-senha", {
						id: <?= $operador->id ?>
					}, function(result){
						result = JSON.parse(result);
						if(result.sucesso == "1"){
							location.href = result.conteudo;
						}else{
							alert(result.conteudo);
						}
					});
				}
			});
		});
	</script>
</body>
</html>
