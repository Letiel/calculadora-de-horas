<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Funcionários</title>
	<?php include "inc/headBasico.php" ?>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-xl-12 mb-5">
							<div class="card card-md">
								<div class="card-header">
									Funcionários
									<a href="/funcionarios/cadastrar" class="btn btn-primary float-right"><span class="batch-icon batch-icon-user-alt-2 mr-3"></span>Novo</a>
								</div>
								<div class="card-body">
									<table class="table table-datatable table-striped table-hover table-responsive">
										<thead>
											<tr>
												<th>Nome</th>
												<th>Empresa</th>
												<th>Horário</th>
												<th>Departamento</th>
												<th>Setor</th>
												<?php if ($this->session->userdata("adm")): ?>
													<th>Alterar</th>
												<?php endif ?>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($funcionarios as $funcionario): ?>
												<tr>
													<td><?= $funcionario->nome ?></td>
													<td><?= $funcionario->nome_fantasia ?></td>
													<td><?= $funcionario->nome_horario ?></td>
													<td><?= $funcionario->nome_departamento ?></td>
													<td><?= $funcionario->nome_setor ?></td>
													<?php if ($this->session->userdata("adm")): ?>
														<td>
															<a title="Editar" href="/funcionarios/editar/<?= $funcionario->id ?>" class="btn btn-info btn-md"><span class="batch-icon batch-icon-pencil"></span></a>
															<a title="Afastamentos" href="/funcionarios/afastamentos/<?= $funcionario->id ?>" class="btn btn-secondary btn-md"><span class="batch-icon batch-icon-power"></span></a>
															<button class="btn btn-danger btn-md btn-excluir" value="<?= $funcionario->id ?>" title="Excluir"><span class="batch-icon batch-icon-bin-alt-2"></span></button>
														</td>
													<?php endif ?>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
									<?= $paginacao ?>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".btn-excluir").click(function(){
				if(confirm("Tem certeza? Esta ação não pode ser desfeita.")){
					$.post("/funcionarios/excluir", {
						id: $(this).val()
					}, function(result){
						location.reload();
					});
				}
			});
		});
	</script>
</body>
</html>
