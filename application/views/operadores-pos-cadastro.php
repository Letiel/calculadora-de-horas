<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Operador</title>
	<?php include "inc/headBasico.php" ?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-bootstrap.min.css">
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<?php include "inc/topo1.php" ?>
			<div class="right-column">
				<?php include "inc/topo2.php" ?>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-12 mb-5">
							<div class="card card-md" style="height: auto;">
								<div class="card-header">
									Operador
								</div>
								<div class="card-body">
									<div class="col-12">
										<h2 class="text-warning text-center">A senha gerada para este usuário é</h2>
										<p class="text-warning text-center" style="font-size: 30px;"><b><?= $senha ?></b></p>
									</div>
									<div class="col-12">
										<a class="btn btn-primary" href="/operadores">Operadores</a>
										<a class="btn btn-primary" href="/operadores/cadastrar">Cadastrar Outro</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php include 'inc/footer.php' ?>
				</main>
			</div>
		</div>
	</div>
	<?php include 'inc/js.php' ?>
	<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery-mask/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#empresas").select2({
				theme: "bootstrap",
				  placeholder: 'Departamento',
				  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				});
			});
	</script>
</body>
</html>
