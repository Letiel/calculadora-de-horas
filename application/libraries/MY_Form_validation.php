<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {
    protected $CI;

    public function __construct() {
        parent::__construct();
        $CI =& get_instance();
    }

    function date($date){
        $CI =& get_instance();
        $CI->form_validation->set_message('date', 'O campo %s não contém uma data válida.');
        $a = explode("-", $date);
        if(count($a) == 1)
            $a = explode("/", $date);
        
        if(count($a) != 1){
            // Verificando formato da data
            if(strlen($a[0]) == 4){
                $year = $a[0];
                if($a[1] > 12){
                    $day = $a[1];
                    $month = $a[2];
                }else{
                    $day = $a[2];
                    $month = $a[1];
                }
            }elseif(strlen($a[1]) == 4){
                $year = $a[1];
                if($a[0] > 12){
                    $day = $a[0];
                    $month = $a[2];
                }else{
                    $day = $a[2];
                    $month = $a[0];
                }
            }elseif(strlen($a[2]) == 4){
                $year = $a[2];
                if($a[0] > 12){
                    $day = $a[0];
                    $month = $a[1];
                }else{
                    $day = $a[1];
                    $month = $a[0];
                }
            }
            return checkdate($month, $day, $year);
        }else{
            return false;
        }
    }

    function time($time){
        $CI =& get_instance();
        $CI->form_validation->set_message('time', 'O campo %s não contém um horário válido.');

        if(list($hour, $minute) = explode(":", $time)){
            if($hour < 0 || $hour > 23 || $minute < 0 || $minute > 59)
                return false;
            else
                return true;
        }else{
            return false;
        }

    }

    function datetime($datetime){

        $CI =& get_instance();
        $CI->form_validation->set_message('datetime', 'O campo %s não contém uma data válida.');
        if(list($date, $time) = explode(" ", $datetime)){
            $a = explode("-", $date);
            if(count($a) == 1)
                $a = explode("/", $date);

            if(count($a) == 3){
                if(strlen($a[0]) == 4){
                    $year = $a[0];
                    if($a[1] > 12){
                        $day = $a[1];
                        $month = $a[2];
                    }else{
                        $day = $a[2];
                        $month = $a[1];
                    }
                }elseif(strlen($a[1]) == 4){
                    $year = $a[1];
                    if($a[0] > 12){
                        $day = $a[0];
                        $month = $a[2];
                    }else{
                        $day = $a[2];
                        $month = $a[0];
                    }
                }elseif(strlen($a[2]) == 4){
                    $year = $a[2];
                    if($a[0] > 12){
                        $day = $a[0];
                        $month = $a[1];
                    }else{
                        $day = $a[1];
                        $month = $a[0];
                    }
                }

                if(checkdate($month, $day, $year)){
                    if(list($hour, $minute) = explode(":", $time)){
                        if($hour < 0 || $hour > 23 || $minute < 0 || $minute > 59)
                            return false;
                        else
                            return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

}