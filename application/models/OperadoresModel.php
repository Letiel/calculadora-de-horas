<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class OperadoresModel extends CI_Model{
		function __construct(){
			parent::__construct();
			$this->load->model("UsuariosModel", "usuarios");//adicionando só para verificar e atualizar os dados da sessão de usuário
			$this->usuarios->atualizar_sessao();
		}

		function getOperadores($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("usuarios.id_conta", $this->session->userdata("id_conta"));
			return $this->db->get("usuarios");
		}

		function cadastrar(){
			$post = $this->input->post();
			$senha = substr(uniqid(""), 0, 6);
			$post["senha"] = sha1($senha);
			$post["id_conta"] = $this->session->userdata("id_conta");
			$empresas = $post["empresas"];
			unset($post["empresas"]);
			if($this->db->insert("usuarios", $post)){
				$id = $this->db->insert_id();
				foreach($empresas as $empresa){
					$array = array("id_usuario"=>$id, "id_empresa"=>$empresa);
					$this->db->insert("usuario_empresa", $array);
				}
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Operador Cadastrado com Sucesso');");
				$this->session->set_flashdata("senha", $senha);
				redirect("/operadores/pos-cadastro");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function getOperador($id = NULL){
			if($id != NULL){
				$this->db->where("usuarios.id", $id);
				$this->db->where("usuarios.id_conta", $this->session->userdata("id_conta"));
				return $this->db->get("usuarios");
			}
		}

		function getEmpresasOperador($id = NULL){
			if($id != NULL){
				$this->db->where("usuario_empresa.id_usuario", $id);
				$this->db->select("usuario_empresa.id_empresa");
				$empresas = $this->db->get("usuario_empresa")->result();
				$itens = array();
				foreach($empresas as $empresa){//monto um array para testar e deixar selecionado na view
					$itens[] = $empresa->id_empresa;
				}
				return $itens;
			}
		}

		function editar($id = null){
			if($id != null){
				$post = $this->input->post();
				$post["id_conta"] = $this->session->userdata("id_conta");
				$empresas = $post["empresas"];
				unset($post["empresas"]);
				unset($post["senha_antiga"]);
				unset($post["nova_senha"]);
				$this->db->where("usuarios.id", $id);
				if($id == $this->session->userdata("id")){
					$this->session->set_userdata("adm", $post["adm"]);
				}
				if($this->db->update("usuarios", $post)){
					$this->db->start_cache();
					$this->db->where("usuario_empresa.id_usuario", $id);
					$this->db->delete("usuario_empresa");
					$this->db->stop_cache();
					$this->db->flush_cache();
					foreach($empresas as $empresa){
						$array = array("id_usuario"=>$id, "id_empresa"=>$empresa);
						$this->db->insert("usuario_empresa", $array);
					}
					$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Operador Alterado com Sucesso');");
					$this->session->set_flashdata("senha", $senha);
					redirect("/operadores");
				}else{
					$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar o cadastro.');");
				}
			}
		}

		function gerar_nova_senha(){
			$post = $this->input->post();
			$senha = substr(uniqid(""), 0, 6);
			$senhaSha1 = sha1($senha);
			if($this->db->query("UPDATE usuarios SET senha = '$senhaSha1' WHERE usuarios.id = $post[id]")){
				// redirect("/operadores/editar/$post[id]");
				$this->session->set_flashdata("senha", $senha);
				echo json_encode(array("sucesso"=>"1", "conteudo"=>"/operadores/pos-cadastro"));
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->start_cache();
			$this->db->where("usuario_empresa.id_usuario", $post["id"]);
			$this->db->delete("usuario_empresa");
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("usuarios.id", $post["id"]);
			$this->db->delete("usuarios");
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Removido com sucesso');");
			if($this->session->userdata("id") == $post["id"]){
				$this->session->set_userdata("logado", false);
			}
		}
	}