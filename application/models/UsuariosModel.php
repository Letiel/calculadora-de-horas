<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class UsuariosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function login(){
			$post = $this->input->post();
			$this->db->join("contas", "contas.id = usuarios.id_conta");
			$this->db->like("usuarios.login", $post["login"]);
			$this->db->where("usuarios.senha", sha1($post["senha"]));
			$this->db->select("usuarios.id, usuarios.adm, usuarios.nome, usuarios.login, contas.data_validade, contas.id as id_conta, contas.data_validade");
			$usuario = $this->db->get("usuarios");
			if($usuario->num_rows() > 0){
				$usuario = $usuario->first_row();
				if(strtotime($usuario->data_validade) < strtotime(date("Y-m-d H:i:s"))){
					$this->session->set_flashdata("retorno_login", "Conta com validade vencida!");
					return false;
				}

				if (strtotime($usuario->data_validade) <= strtotime(date("Y-m-d H:i:s", strtotime("+7 days")))){
					$this->session->set_flashdata("retorno", "toastr.warning('Aviso!', 'Sua conta está próxima do vencimento.');");
				}
				$this->session->set_userdata(array(
					"id"=>$usuario->id,
					"id_conta"=>$usuario->id_conta,
					"nome"=>$usuario->nome,
					"login"=>$usuario->login,
					"data_validade"=>$usuario->data_validade,
					"adm"=>$usuario->adm,
					"logado"=>true
				));
				return true;
			}
			return false;
		}

		function atualizar_sessao(){
			if(empty($this->session->userdata("id"))){
				redirect("/");
				exit();
			}
			$this->db->join("contas", "contas.id = usuarios.id_conta");
			$this->db->like("usuarios.id", $this->session->userdata("id"));
			$this->db->select("usuarios.id, usuarios.adm, usuarios.nome, usuarios.login, contas.data_validade, contas.id as id_conta, contas.data_validade");
			$usuario = $this->db->get("usuarios");
			if($usuario->num_rows() > 0){
				$usuario = $usuario->first_row();
				if(strtotime($usuario->data_validade) < strtotime(date("Y-m-d H:i:s"))){
					$this->session->set_flashdata("retorno_login", "Conta com validade vencida!");
					return false;
				}
				$this->session->set_userdata(array(
					"id"=>$usuario->id,
					"id_conta"=>$usuario->id_conta,
					"nome"=>$usuario->nome,
					"login"=>$usuario->login,
					"data_validade"=>$usuario->data_validade,
					"adm"=>$usuario->adm,
					"logado"=>true
				));
			}
		}

		function atualizar_conta(){
			$post = $this->input->post();

			$array = array(
				"nome"=>$post["nome"],
				"login"=>$post["login"]
			);

			$this->db->start_cache();
			$this->db->where("usuarios.id", $this->session->userdata("id"));
			$usuario = $this->db->get("usuarios")->first_row();
			$this->db->stop_cache();
			$this->db->flush_cache();

			if(!empty($post["senha_antiga"]) && !empty($post["nova_senha"])){
				$senha_antiga = sha1($post["senha_antiga"]);
				if($usuario->senha == $senha_antiga)
					$array["senha"] = sha1($post['nova_senha']);
				else{
					$this->session->set_flashdata("retorno", "toastr.warning('Ops!', 'Senha antiga não confere!');");
					redirect("/minha-conta");
				}
			}
			$this->db->where("usuarios.id", $this->session->userdata("id"));
			if($this->db->update("usuarios", $array)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Atualizado com Sucesso.');");
				$this->session->set_userdata(array(
					"nome"=>$post["nome"],
					"login"=>$post["login"],
					"logado"=>true
				));
				redirect("/minha-conta");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Problema ao atualizar.');");
			}
		}

		function consultar_totais(){
			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("usuario_empresa", "usuario_empresa.id_empresa = empresas.id");
			$this->db->join("usuarios", "usuario_empresa.id_usuario = usuarios.id");
			$empresas = $this->db->get("empresas")->num_rows();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));

			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$funcionarios = $this->db->get("funcionarios")->num_rows();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));

			$this->db->join("departamentos", "departamentos.id = setores.id_departamento");
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$setores = $this->db->get("setores")->num_rows();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));

			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$departamentos = $this->db->get("departamentos")->num_rows();
			$this->db->stop_cache();
			$this->db->flush_cache();


			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");

			$horarios = $this->db->get("grupos_horarios")->num_rows();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$retorno = "";
			if($empresas < 1){
				$retorno .= "toastr.error('Nenhuma empresa cadastrada!<br /> <a class=\"font-weight-bold\" href=\"/empresas\">Cadastrar</a>');";
			}
			if($funcionarios < 1){
				$retorno .= "toastr.error('Nenhum funcionário cadastrado!<br /> <a class=\"font-weight-bold\" href=\"/funcionarios\">Cadastrar</a>');";
			}
			// if($departamentos < 1){
			// 	$retorno .= "toastr.error('Nenhum departamento cadastrado!');";
			// }
			// if($setores < 1){
			// 	$retorno .= "toastr.error('Nenhum setor cadastrado!');";
			// }
			if($horarios < 1){
				$retorno .= "toastr.error('Nenhum horário cadastrado!<br /> <a class=\"font-weight-bold\" href=\"/horarios\">Cadastrar</a>');";
			}
			$this->session->set_flashdata("retorno", "$retorno");
		}
	}