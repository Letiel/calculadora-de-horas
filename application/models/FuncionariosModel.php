<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class FuncionariosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getFuncionarios($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa", 'left');
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->join("cidades", "cidades.id = funcionarios.id_cidade", 'left');
			$this->db->join("estados", "estados.uf = cidades.uf", 'left');
			$this->db->join("departamentos", "departamentos.id = funcionarios.id_departamento", 'left');
			$this->db->join("setores", "setores.id = funcionarios.id_setor", 'left');
			$this->db->join("grupos_horarios", "grupos_horarios.id = funcionarios.id_horario", 'left');
			$this->db->select("funcionarios.*, empresas.nome_fantasia, cidades.nome as nome_cidade, cidades.uf, departamentos.nome as nome_departamento, setores.nome as nome_setor, grupos_horarios.nome as nome_horario");
			return $this->db->get("funcionarios");
		}

		function getFuncionario($id){
			$this->db->where("funcionarios.id", $id);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa", 'left');
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->join("cidades", "cidades.id = funcionarios.id_cidade", 'left');
			$this->db->join("estados", "estados.uf = cidades.uf", 'left');
			$this->db->join("departamentos", "departamentos.id = funcionarios.id_departamento", 'left');
			$this->db->join("setores", "setores.id = funcionarios.id_setor", 'left');
			$this->db->join("grupos_horarios", "grupos_horarios.id = funcionarios.id_horario", 'left');
			$this->db->select("funcionarios.*, empresas.nome_fantasia, cidades.nome as nome_cidade, cidades.uf, departamentos.nome as nome_departamento, setores.nome as nome_setor, grupos_horarios.nome as nome_horario");
			return $this->db->get("funcionarios");
		}

		function select2_empresas(){
			$get = $this->input->get();
			$this->db->like("empresas.razao_social", $get["q"]);
			$this->db->or_like("empresas.nome_fantasia", $get["q"]);
			$this->db->select("empresas.id, empresas.nome_fantasia as text");
			$empresas = $this->db->get("empresas")->result();
			echo json_encode(array("items"=>$empresas));
		}

		function select2_horarios(){
			$get = $this->input->get();
			$this->db->like("grupos_horarios.nome", $get["q"]);
			$this->db->or_like("empresas.nome_fantasia", $get["q"]);
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->select("grupos_horarios.id, CONCAT(grupos_horarios.nome, ' - ', empresas.nome_fantasia) as text");
			$grupos = $this->db->get("grupos_horarios")->result();
			echo json_encode(array("items"=>$grupos));
		}

		function select2_departamentos(){
			$get = $this->input->get();
			$this->db->like("departamentos.nome", $get["q"]);
			$this->db->or_like("empresas.nome_fantasia", $get["q"]);
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->select("departamentos.id, CONCAT(departamentos.nome, ' - ', empresas.nome_fantasia) as text");
			$departamentos = $this->db->get("departamentos")->result();
			echo json_encode(array("items"=>$departamentos));
		}


		function select2_setores(){
			$get = $this->input->get();
			$this->db->like("setores.nome", $get["q"]);
			$this->db->or_like("departamentos.nome", $get["q"]);
			$this->db->or_like("empresas.nome_fantasia", $get["q"]);
			$this->db->join("departamentos", "departamentos.id = setores.id_departamento");
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->select("setores.id, CONCAT(setores.nome, ' - ', empresas.nome_fantasia) as text");
			$setores = $this->db->get("setores")->result();
			echo json_encode(array("items"=>$setores));
		}

		function cadastrar(){
			$post = $this->input->post();
			$post["admissao"] = date("Y-m-d", strtotime($post["admissao"]));
			$post["demissao"] = date("Y-m-d", strtotime($post["demissao"]));
			if($this->db->insert("funcionarios", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Funcionário Cadastrado com Sucesso');");
				redirect("/funcionarios");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function editar(){
			$post = $this->input->post();
			$post["admissao"] = $post["admissao"] != '' ? date("Y-m-d", strtotime($post["admissao"])) : '0000-00-00';
			$post["demissao"] = $post["demissao"] != '' ? date("Y-m-d", strtotime($post["demissao"])) : '0000-00-00';
			$this->db->where("funcionarios.id", (int)$this->uri->segment(3));
			if($this->db->update("funcionarios", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Funcionário Alterado com Sucesso');");
				redirect("/funcionarios");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("funcionarios.id", $post["id"]);

			if($this->db->delete("funcionarios")){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Funcionário Removido com Sucesso');");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível remover.');");
			}
		}
	}