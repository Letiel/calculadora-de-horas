<?php
	defined("BASEPATH") OR exit("No direct script access allowed");

	class FeriadosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getFeriados($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("feriados.id_empresa IS NULL");
			$this->db->or_where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("empresas", "empresas.id = feriados.id_empresa", "left");
			$this->db->select("feriados.id, feriados.nome, feriados.data, empresas.nome_fantasia, feriados.id_empresa");
			$this->db->order_by("feriados.data", "ASC");
			return $this->db->get("feriados");
		}

		function getFeriado($id){
			$this->db->where("feriados.id", $id);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("empresas", "empresas.id = feriados.id_empresa");
			$this->db->select("feriados.id, feriados.nome, feriados.data, empresas.nome_fantasia, feriados.id_empresa");
			return $this->db->get("feriados");
		}

		function cadastrar(){
			$post = $this->input->post();
			$post["data"] = explode("/", $post["data"]);
			$post["data"] = $post["data"][2]."-".$post["data"][1]."-".$post["data"][0];
			if($this->db->insert("feriados", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Feriado Cadastrado com Sucesso');");
				redirect("/feriados");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function editar($id){
			$post = $this->input->post();
			$post["data"] = explode("/", $post["data"]);
			$post["data"] = $post["data"][2]."-".$post["data"][1]."-".$post["data"][0];
			$this->db->where("feriados.id", $id);
			if($this->db->update("feriados", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Feriado Alterado com Sucesso');");
				redirect("/feriados");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("feriados.id", $post["id"]);
			$this->db->where("feriados.id_empresa IS NOT NULL");
			if($this->db->delete("feriados")){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Feriado Removido com Sucesso');");
				// redirect("/feriados");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não Foi Possível Realizar a Remoção.');");
				// redirect("/feriados");
			}
		}

		function inserirFeriados($ano = null){
			if($ano === null)
				$ano = intval(date('Y'));

			$pascoa     = easter_date($ano); // Limite de 1970 ou após 2037 da easter_date PHP consulta http://www.php.net/manual/pt_BR/function.easter-date.php
			$dia_pascoa = date('j', $pascoa);
			$mes_pascoa = date('n', $pascoa);
			$ano_pascoa = date('Y', $pascoa);

			$feriados = array(
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 1,  1,   $ano)),
					"nome"=>"Confraternização Universal"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 4,  21,  $ano)),
					"nome"=>"Tiradentes"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 5,  1,   $ano)),
					"nome"=>"Dia do Trabalhador"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 9,  7,   $ano)),
					"nome"=>"Dia da Independência"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 10,  12, $ano)),
					"nome"=>"N. S. Aparecida"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 11,  2,  $ano)),
					"nome"=>"Todos os Santos"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 11, 15,  $ano)),
					"nome"=>"Proclamação da República"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, 12, 25,  $ano)),
					"nome"=>"Natal"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 47,  $ano_pascoa)),
					"nome"=>"3ª feira Carnaval"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 2 ,  $ano_pascoa)),
					"nome"=>"6º feira Santa"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, $mes_pascoa, $dia_pascoa     ,  $ano_pascoa)),
					"nome"=>"Páscoa"
				),
				array(
					"data"=>date("Y-m-d", mktime(0, 0, 0, $mes_pascoa, $dia_pascoa + 60,  $ano_pascoa)),
					"nome"=>"Corpus Christi"
				),
			);
			$this->db->insert_batch("feriados", $feriados);
		}
	}