<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class HorariosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function cadastrar_grupo(){
			$post = $this->input->post();
			if($this->db->insert("grupos_horarios", $post)){
				$id = $this->db->insert_id();
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Grupo Cadastrado com Sucesso');");
				redirect("/horarios/editar/".$id);
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function getGrupos($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("empresas.nome_fantasia as nome_empresa, grupos_horarios.nome, grupos_horarios.id");
			return $this->db->get("grupos_horarios");
		}

		function getGrupo($id){
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("grupos_horarios.id", $id);
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("empresas.nome_fantasia as nome_empresa, grupos_horarios.nome, grupos_horarios.id, empresas.id as id_empresa");
			return $this->db->get("grupos_horarios");
		}

		function getHorariosGrupo($grupo, $dia){
			$this->db->start_cache();
			$this->db->where("horarios.dia", $dia);
			$this->db->where("horarios.id_grupo", $grupo);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("grupos_horarios", "horarios.id_grupo = grupos_horarios.id");
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("horarios.*");
			$horarios = $this->db->get("horarios");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($horarios->num_rows() == 0){
				$data = array(
					"dia"=>$dia,
					"id_grupo"=>$grupo,
					"padrao_entrada_1"=>"00:00",
					"padrao_saida_1"=>"00:00",
				);
				$this->db->start_cache();
				$this->db->insert("horarios", $data);
				$inserido = $this->db->insert_id();
				$this->db->stop_cache();
				$this->db->flush_cache();

				$this->db->start_cache();
				$this->db->where("horarios.id", $inserido);
				$horarios = $this->db->get("horarios");
				$this->db->stop_cache();
				$this->db->flush_cache();
				
			}
			return $horarios;
		}

		function editable(){
			$post = $this->input->post();
			if($this->db->query("UPDATE horarios SET $post[name]='$post[value]' WHERE id = $post[pk]")){
				$this->load->model("CalculadoraModel", "calculadora");
				$carga = $this->calculadora->getCargaHoraria($post["pk"]);
				return array('success'=>true, 'newValue'=>$post['value'], "carga_horaria"=>$carga);
			}else{
				return array('success'=>false, 'msg'=>"Ocorreu um erro interno.");
			}
		}

		function copiar_horario(){
			$post = $this->input->post();
			$this->db->start_cache();
			$this->db->where("horarios.id_grupo", $post["grupo"]);
			$this->db->where("horarios.dia", $post["de"]);
			$this->db->select("padrao_entrada_1, padrao_saida_1, padrao_entrada_2, padrao_saida_2, padrao_entrada_3, padrao_saida_3, padrao_entrada_4, padrao_saida_4, padrao_entrada_5, padrao_saida_5, carga_horaria, regras_tolerancias, utiliza_compensacao, tolerancia_diaria, tol_entrada_1_antes, tol_entrada_1_depois, tol_entrada_2_antes, tol_entrada_2_depois, tol_entrada_3_antes, tol_entrada_3_depois, tol_entrada_4_antes, tol_entrada_4_depois, tol_entrada_5_antes, tol_entrada_5_depois, tol_saida_1_antes, tol_saida_1_depois, tol_saida_2_antes, tol_saida_2_depois, tol_saida_3_antes, tol_saida_3_depois, tol_saida_4_antes, tol_saida_4_depois, tol_saida_5_antes, tol_saida_5_depois, comp_tol_falta, comp_tol_extra, fechamento");
			$horario = $this->db->get("horarios");
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($horario->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Horário não encontrado', 'Ops');");
				// redirect("/horarios/editar/$post[grupo]");
			}else{
				$array = array("segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo");
				$horario = $horario->first_row();
				foreach ($array as $dia) {
					if($post[$dia] == "true"){
						$this->db->start_cache();
						$this->db->where("horarios.id_grupo", $post["grupo"]);
						$this->db->where("horarios.dia", $dia);
						$this->db->update("horarios", $horario);
						$this->db->stop_cache();
						$this->db->flush_cache();
					}
				}
				$this->session->set_flashdata("retorno", "toastr.success('Copiado com sucesso', 'Sucesso!');");
			}
		}

		function avancado(){
			$post = $this->input->post();
			$array = array("segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo", "feriados");
			$this->db->start_cache();
			$this->db->where("dias_colunas.id_horario", $post["id_horario"]);
			$this->db->where("dias_colunas.id_coluna", $post["id_coluna"]);
			$this->db->delete("dias_colunas");
			$this->db->stop_cache();
			$this->db->flush_cache();

			foreach ($array as $dia) {
				if($post[$dia] == "true"){
					$this->db->start_cache();
					$this->db->insert("dias_colunas", array("id_coluna"=>$post["id_coluna"], "id_horario"=>$post["id_horario"], "dia"=>$dia));
					$this->db->stop_cache();
					$this->db->flush_cache();
				}
			}
			$this->session->set_flashdata("retorno", "toastr.success('Salvo com sucesso', 'Sucesso!');");
		}

		function preencher_avancado(){
			$post = $this->input->post();
			$array = array("segunda"=>false, "terca"=>false, "quarta"=>false, "quinta"=>false, "sexta"=>false, "sabado"=>false, "domingo"=>false, "feriados"=>false);
			$this->db->start_cache();
			$this->db->where("dias_colunas.id_coluna", $post["id_coluna"]);
			$colunas = $this->db->get("dias_colunas")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			foreach($colunas  as $coluna){
				$array[$coluna->dia] = true;
			}

			echo json_encode($array);
		}
	}