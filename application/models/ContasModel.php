<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ContasModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getValidadeConta(){
			$this->db->where("contas.id", $this->session->userdata("id_conta"));
			return $this->db->get("contas");
		}
	}