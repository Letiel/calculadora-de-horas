<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class AfastamentosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getTiposAfastamentos($inicio = NULL, $maximo = NULL){
			$this->db->limit($maximo, $inicio);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = tipos_afastamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->order_by("tipos_afastamentos.descricao");
			$this->db->select("tipos_afastamentos.id, tipos_afastamentos.descricao");
			return $this->db->get("tipos_afastamentos");
		}

		function getTipoAfastamento($id){
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("tipos_afastamentos.id", $id);
			$this->db->join("empresas", "empresas.id = tipos_afastamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("tipos_afastamentos.id, tipos_afastamentos.descricao, empresas.nome_fantasia as nome_empresa");
			return $this->db->get("tipos_afastamentos");
		}

		function cadastrar_tipo(){
			$post = $this->input->post();
			if($this->db->insert("tipos_afastamentos", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Tipo de Afastamento Cadastrado com Sucesso');");
				redirect("/afastamentos");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function atribuir($redirecionamento = "/afastamentos", $funcionario = null){
			$post = $this->input->post();
			if($redirecionamento == "/afastamentos"){
				if(!empty($post["hora_inicio"])){
					$post["data_inicio"] .= "T".$post["hora_inicio"];
				}else{
					$post["data_inicio"] .= "T00:00:00";
				}
				if(!empty($post["hora_fim"])){
					$post["data_fim"] .= "T".$post["hora_fim"];
				}else{
					$post["data_fim"] .= "T23:59:59";
				}
			}

			unset($post["hora_inicio"]);
			unset($post["hora_fim"]);
			if($this->db->insert("afastamentos", $post)){
				$inicio = new DateTime($post["data_inicio"]);
				$fim = new DateTime($post["data_fim"]);
				$this->load->model("CalculadoraModel", "calculadora");
				for ($i = clone $inicio; $i <= $fim; $i->add(new DateInterval("P1D"))) { //atualizando os horários já cadastrados
					$dia_calculo = $i->format("Y-m-d");
					$calculado = $this->calculadora->getCalculosDia($funcionario, $dia_calculo);
					if($calculado->num_rows() > 0){
						$calculado = $calculado->first_row();
						$this->calculadora->calcularHorasFuncionario((array) $calculado);//atualizando os horários conforme novo horário de afastamento
					}
				}

				$this->session->set_flashdata("retorno", "toastr.info('', 'Os dias referentes ao afastamento foram recalculados.');toastr.success('Sucesso!', 'Afastamento Atribuído com Sucesso');");
				redirect($redirecionamento);
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->start_cache();
			$this->db->where("id_tipo", $post["id"]);
			$this->db->delete("afastamentos");
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("id", $post["id"]);
			if($this->db->delete("tipos_afastamentos")){
				$this->session->set_flashdata("retorno", "toastr.success('Afastamento removido com Sucesso', 'Sucesso!');");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Não foi possível remover.', 'Erro!');");
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}

		function editable(){
			$post = $this->input->post();
			$this->db->where("tipos_afastamentos.id", $post["pk"]);
			if($this->db->update("tipos_afastamentos", array("descricao"=>$post["value"]))){
				return array('success'=>true, 'newValue'=>$post['value']);
			}else{
				return array('success'=>false, 'msg'=>"Ocorreu um erro interno.");
			}
		}

		function getAfastamentosFuncionario($inicio = null, $maximo = null){
			$this->db->start_cache();
			$this->db->limit($maximo, $inicio);
			$this->db->where("afastamentos.id_funcionario", (int)$this->uri->segment(3));
			$this->db->order_by("id", "DESC");

			$this->db->join("tipos_afastamentos", "tipos_afastamentos.id = afastamentos.id_tipo");
			$this->db->select("afastamentos.*, tipos_afastamentos.descricao");

			$afastamentos = $this->db->get("afastamentos");
			$this->db->stop_cache();
			$this->db->flush_cache();
			return $afastamentos;
		}

		function editar($afastamento){
			$post = $this->input->post();
			if(!empty($post["hora_inicio"])){
				$post["data_inicio"] .= "T".$post["hora_inicio"];
			}else{
				$post["data_inicio"] .= "T00:00:00";
			}
			if(!empty($post["hora_fim"])){
				$post["data_fim"] .= "T".$post["hora_fim"];
			}else{
				$post["data_fim"] .= "T23:59:59";
			}

			$funcionario = $post["id_funcionario"];

			unset($post["hora_inicio"]);
			unset($post["hora_fim"]);

			$this->db->where("afastamentos.id", $afastamento);
			if($this->db->update("afastamentos", $post)){
				$inicio = new DateTime($post["data_inicio"]);
				$fim = new DateTime($post["data_fim"]);
				$this->load->model("CalculadoraModel", "calculadora");
				for ($i = clone $inicio; $i <= $fim; $i->add(new DateInterval("P1D"))) { //atualizando os horários já cadastrados
					$dia_calculo = $i->format("Y-m-d");
					$calculado = $this->calculadora->getCalculosDia($funcionario, $dia_calculo);
					if($calculado->num_rows() > 0){
						$calculado = $calculado->first_row();
						$this->calculadora->calcularHorasFuncionario((array) $calculado);//atualizando os horários conforme novo horário de afastamento
					}
				}
				$this->session->set_flashdata("retorno", "toastr.info('', 'Os dias referentes ao afastamento foram recalculados.');toastr.success('Sucesso!', 'Afastamento Alterado com Sucesso.');");
				// redirect("/funcionarios");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar alteração.');");
			}
		}

		function getAfastamento($afastamento){
			$this->db->start_cache();
			$this->db->where("afastamentos.id", $afastamento);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("funcionarios", "funcionarios.id = afastamentos.id_funcionario");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->select("afastamentos.*");
			$afastamento = $this->db->get("afastamentos");
			$this->db->stop_cache();
			$this->db->flush_cache();
			return $afastamento;
		}

		function excluir_afastamento_funcionario(){
			$post = $this->input->post();


			$this->db->start_cache();
			$this->db->where("id", $post["id"]);
			$afastamento = $this->db->get("afastamentos")->first_row();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("id", $post["id"]);
			if($this->db->delete("afastamentos")){

				$inicio = new DateTime(date("Y-m-d H:i:s", strtotime($afastamento->data_inicio)));
				$fim = new DateTime(date("Y-m-d H:i:s", strtotime($afastamento->data_fim)));
				$this->load->model("CalculadoraModel", "calculadora");
				// echo "Afastamento removido...";
				for ($i = clone $inicio; $i <= $fim; $i->add(new DateInterval("P1D"))) { //atualizando os horários já cadastrados
					$dia_calculo = $i->format("Y-m-d");
					$calculado = $this->calculadora->getCalculosDia($afastamento->id_funcionario, $dia_calculo);
					// echo "Vou procurar... no dia $dia_calculo do funcionario $afastamento->id_funcionario...";
					if($calculado->num_rows() > 0){
						// echo "Achei...";
						$calculado = $calculado->first_row();
						$this->calculadora->calcularHorasFuncionario((array) $calculado);//atualizando os horários conforme novo horário de afastamento
						// echo "Recalculei...";
					}else{
						// echo "Nao achei...";
					}
				}
				$this->session->set_flashdata("retorno", "toastr.info('', 'Os dias referentes ao afastamento foram recalculados.');toastr.success('Afastamento removido com Sucesso', 'Sucesso!');");

			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Não foi possível remover.', 'Erro!');");
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}
	}