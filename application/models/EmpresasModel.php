<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class EmpresasModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function cadastrar(){
			$post = $this->input->post();
			$post["id_conta"] = $this->session->userdata("id_conta");
			if($this->db->insert("empresas", $post)){
				$id = $this->db->insert_id();
				$this->db->start_cache();
				$this->db->insert("usuario_empresa", array("id_usuario"=>$this->session->userdata("id"), "id_empresa"=>$id));
				$this->db->stop_cache();
				$this->db->flush_cache();
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Empresa Cadastrada com Sucesso');");
				redirect("/empresas");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function getEmpresas($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("cidades", "cidades.id = empresas.id_cidade");
			$this->db->select("cidades.nome as nome_cidade, empresas.*");
			return $this->db->get("empresas");
		}

		function getEmpresasConta($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("cidades", "cidades.id = empresas.id_cidade");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("cidades.nome as nome_cidade, empresas.*");
			return $this->db->get("empresas");
		}

		function getEmpresa($id){
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("empresas.id", $id);
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("cidades", "cidades.id = empresas.id_cidade");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("cidades.nome as nome_cidade, empresas.*");
			return $this->db->get("empresas");
		}

		function editar(){
			$post = $this->input->post();
			$this->db->where("empresas.id", $this->uri->segment(3));
			if($this->db->update("empresas", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Empresa Alterada com Sucesso');");
				redirect("/empresas");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("empresas.id", $post["id"]);
			if($this->db->delete("empresas")){
				$this->db->start_cache();
				$this->db->where("usuario_empresa.id_empresa", $post["id"]);
				$this->db->delete("usuario_empresa");
				$this->db->stop_cache();
				$this->db->flush_cache();
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Empresa Removida com Sucesso');");
				// redirect("/empresas");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não Foi Possível Realizar a Remoção.');");
				// redirect("/empresas");
			}
		}

		function select2_cidades(){
			$get = $this->input->get();
			$this->db->like("cidades.nome", $get["q"]);
			// $this->db->or_like("estados.nome", $get["q"]);
			$this->db->or_like("estados.uf", $get["q"]);
			$this->db->join("estados", "estados.uf = cidades.uf");
			$this->db->select("cidades.id, CONCAT(cidades.nome, ' - ', estados.uf) as text");
			$cidades = $this->db->get("cidades")->result();
			echo json_encode(array("items"=>$cidades));
		}
	}