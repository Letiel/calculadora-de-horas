<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class CalculadoraModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getCargaHoraria($pk){

			$this->db->start_cache();

			$this->db->where("horarios.id", $pk);
			$this->db->select("*");
			$horario = $this->db->get("horarios")->first_row();

			$this->db->stop_cache();
			$this->db->flush_cache();

			$post["dia_calculo"] = date("Y-m-d");

			if($horario->padrao_entrada_1 != "" && $horario->padrao_entrada_1 != NULL && $horario->padrao_entrada_1 != $horario->padrao_saida_1)
				$data["padrao"]["entrada"]["_1"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_1);
			else
				$data["padrao"]["entrada"]["_1"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_2 != "" && $horario->padrao_entrada_2 != NULL && $horario->padrao_entrada_2 != $horario->padrao_saida_2)
				$data["padrao"]["entrada"]["_2"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_2);
			else
				$data["padrao"]["entrada"]["_2"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_3 != "" && $horario->padrao_entrada_3 != NULL && $horario->padrao_entrada_3 != $horario->padrao_saida_3)
				$data["padrao"]["entrada"]["_3"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_3);
			else
				$data["padrao"]["entrada"]["_3"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_4 != "" && $horario->padrao_entrada_4 != NULL && $horario->padrao_entrada_4 != $horario->padrao_saida_4)
				$data["padrao"]["entrada"]["_4"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_4);
			else
				$data["padrao"]["entrada"]["_4"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_5 != "" && $horario->padrao_entrada_5 != NULL && $horario->padrao_entrada_5 != $horario->padrao_saida_5)
				$data["padrao"]["entrada"]["_5"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_5);
			else
				$data["padrao"]["entrada"]["_5"] = new DateTime("0000-00-00 00:00:00");

			//SAÍDA
			if($horario->padrao_saida_1 != "" && $horario->padrao_saida_1 != NULL && $horario->padrao_entrada_1 != $horario->padrao_saida_1)
				$data["padrao"]["saida"]["_1"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_1);
			else
				$data["padrao"]["saida"]["_1"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_2 != "" && $horario->padrao_saida_2 != NULL && $horario->padrao_entrada_2 != $horario->padrao_saida_2)
				$data["padrao"]["saida"]["_2"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_2);
			else
				$data["padrao"]["saida"]["_2"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_3 != "" && $horario->padrao_saida_3 != NULL && $horario->padrao_entrada_3 != $horario->padrao_saida_3)
				$data["padrao"]["saida"]["_3"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_3);
			else
				$data["padrao"]["saida"]["_3"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_4 != "" && $horario->padrao_saida_4 != NULL && $horario->padrao_entrada_4 != $horario->padrao_saida_4)
				$data["padrao"]["saida"]["_4"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_4);
			else
				$data["padrao"]["saida"]["_4"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_5 != "" && $horario->padrao_saida_5 != NULL && $horario->padrao_entrada_5 != $horario->padrao_saida_5)
				$data["padrao"]["saida"]["_5"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_5);
			else
				$data["padrao"]["saida"]["_5"] = new DateTime("0000-00-00 00:00:00");


			if($data["padrao"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
				$data["padrao"]["saida"]["_1"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_2"] < $data["padrao"]["saida"]["_1"]){
				$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_2"] < $data["padrao"]["entrada"]["_2"]){
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_3"] < $data["padrao"]["saida"]["_2"]){
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_3"] < $data["padrao"]["entrada"]["_3"]){
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_4"] < $data["padrao"]["saida"]["_3"]){
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_4"] < $data["padrao"]["entrada"]["_4"]){
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_5"] < $data["padrao"]["saida"]["_4"]){
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_5"] < $data["padrao"]["entrada"]["_5"]){
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
				//
			}

			$data["carga_horaria"] = "00:00:00";
			$turno1 = $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S");
			$turno2 = $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S");
			$turno3 = $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S");
			$turno4 = $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S");
			$turno5 = $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S");
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno1);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno2);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno3);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno4);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno5);
			$this->db->start_cache();
			$this->db->where("horarios.id", $pk);
			$this->db->update("horarios", array("carga_horaria"=>$data["carga_horaria"]));
			$this->db->stop_cache();
			$this->db->flush_cache();
			return $data["carga_horaria"];
		}

		function getFuncionario($id){
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("funcionarios.id", $id);
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("grupos_horarios", "grupos_horarios.id = funcionarios.id_horario", "left");
			// $this->db->join("horarios", "grupos_horarios.id = horarios.id_grupo", "left");
			$this->db->join("setores", "setores.id = funcionarios.id_setor", "left");
			$this->db->join("departamentos", "departamentos.id = funcionarios.id_departamento", "left");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa", "left");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->select("funcionarios.id, funcionarios.numero_folha, funcionarios.admissao, funcionarios.id_empresa, funcionarios.nome, funcionarios.numero_identificador, funcionarios.pis, funcionarios.ctps, funcionarios.observacoes, empresas.nome_fantasia as nome_empresa, departamentos.nome as nome_departamento, setores.nome as nome_setor, empresas.cnpj");
			return $this->db->get("funcionarios");
		}

		function getHorariosDia($funcionario, $dia){
			$this->db->where("horarios.dia", $dia);
			$this->db->where("funcionarios.id", $funcionario);
			$this->db->join("grupos_horarios", "grupos_horarios.id = horarios.id_grupo");
			$this->db->join("funcionarios", "funcionarios.id_horario = grupos_horarios.id");
			$this->db->select("padrao_entrada_1, padrao_entrada_2, padrao_entrada_3, padrao_entrada_4, padrao_entrada_5, padrao_saida_1, padrao_saida_2, padrao_saida_3, padrao_saida_4, padrao_saida_5");
			return $this->db->get("horarios");
		}

		function getCalculosDia($funcionario, $dia){
			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("calculos.data", $dia);
			$this->db->where("calculos.id_funcionario", $funcionario);
			$this->db->select("calculos.h_entrada_1 as entrada1, calculos.h_entrada_2 as entrada2, calculos.h_entrada_3 as entrada3, calculos.h_entrada_4 as entrada4, calculos.h_entrada_5 as entrada5, calculos.h_saida_1 as saida1, calculos.h_saida_2 as saida2, calculos.h_saida_3 as saida3, calculos.h_saida_4 as saida4, calculos.h_saida_5 as saida5, DATE_FORMAT(calculos.data, '%d/%m/%Y') as dia_calculo, calculos.id_funcionario");
			return $this->db->get("calculos");
		}

		function getFeriados($empresa){
			$this->db->where("feriados.id_empresa IS NULL");
			$this->db->or_where("feriados.id_empresa = $empresa");
			$this->db->select("DATE_FORMAT(feriados.data, '%d-%m-%Y') as data, feriados.nome");
			return $this->db->get("feriados");
		}

		function getAfastamentos($post = null, $return = false){
			$data = null;
			if($post == null){
				$post = $this->input->post();
				$data_inicio = explode("/", $post["data_inicio"]);
				$data_fim = explode("/", $post["data_fim"]);
				$data_inicio = $data_inicio[2]."-".$data_inicio[1]."-".$data_inicio[0];
				$data_fim = $data_fim[2]."-".$data_fim[1]."-".$data_fim[0];
			}
			$this->db->start_cache();
			// $this->db->where("afastamentos.dia >= '$data_inicio'");
			// $this->db->where("afastamentos.dia <= '$data_fim 23:59:59'");

			if(!empty($post["id_tipo"]))
				$this->db->where("afastamentos.id_tipo", $post["id_tipo"]);

			$this->db->where("afastamentos.id_funcionario", $post["id_funcionario"]);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("funcionarios", "funcionarios.id = afastamentos.id_funcionario");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->join("tipos_afastamentos", "tipos_afastamentos.id = afastamentos.id_tipo");
			$this->db->select("afastamentos.*, tipos_afastamentos.descricao as tipo, funcionarios.id_horario as id_horario");
			$afastamentos = $this->db->get("afastamentos")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();
			$cont = 0;
			foreach($afastamentos as $afastamento){
				$inicio = new DateTime($afastamento->data_inicio);
				$fim = new DateTime($afastamento->data_fim);
				$total_dias = $inicio->diff($fim)->format("%d"); //em dias
				$cont2 = 1; //segundo contador para calcular dias de cada período
				for ($i = clone $inicio; $i <= $fim; $i->add(new DateInterval("P1D"))) {
					if($total_dias == 0){ //caso seja somente um dia
						@$data[$cont]->hora_inicio = $inicio->format("H:i:s");
						@$data[$cont]->hora_fim = $fim->format("H:i:s");
					}else{
						/*if($cont2 == 1){ //se for o primeiro
							@$data[$cont]->hora_inicio = $inicio->format("H:i:s");
							@$hora_fim = new DateTime($i->format("Y-m-d")." 23:59:59");
							@$data[$cont]->hora_fim = $hora_fim->format("H:i:s");
						}else if($cont2 == $total_dias){ //se for o último
							@$hora_inicio = new DateTime($i->format("Y-m-d")." 00:00:00");
							@$data[$cont]->hora_inicio = $hora_inicio->format("H:i:s");
							@$data[$cont]->hora_fim = $fim->format("H:i:s");
						}else{ //demais dias, dentro do intervalo
							@$hora_inicio = new DateTime($i->format("Y-m-d")." 00:00:00");
							@$data[$cont]->hora_inicio = $hora_inicio->format("H:i:s");
							@$hora_fim = new DateTime($i->format("Y-m-d")." 23:59:59");
							@$data[$cont]->hora_fim = $hora_fim->format("H:i:s");
						}*/

						if($i->format("Y-m-d") == $inicio->format("Y-m-d")){ //se for o primeiro
							@$data[$cont]->hora_inicio = $inicio->format("H:i:s");
							@$hora_fim = new DateTime($i->format("Y-m-d")." 23:59:59");
							@$data[$cont]->hora_fim = $hora_fim->format("H:i:s");
						}else if($i->format("Y-m-d") == $fim->format("Y-m-d")){ //se for o último
							@$hora_inicio = new DateTime($i->format("Y-m-d")." 00:00:00");
							@$data[$cont]->hora_inicio = $hora_inicio->format("H:i:s");
							@$data[$cont]->hora_fim = $fim->format("H:i:s");
						}else{ //demais dias, dentro do intervalo
							@$hora_inicio = new DateTime($i->format("Y-m-d")." 00:00:00");
							@$data[$cont]->hora_inicio = $hora_inicio->format("H:i:s");
							@$hora_fim = new DateTime($i->format("Y-m-d")." 23:59:59");
							@$data[$cont]->hora_fim = $hora_fim->format("H:i:s");
						}
					}
					@$data[$cont]->dia = $i->format("d-m-Y");
					@$data[$cont]->id_horario = $afastamento->id_horario;
					@$data[$cont]->tipo = $afastamento->tipo;
					$cont++;
					$cont2++;
				}
			}
			$afastamentos_tratados = $data;
			if (is_array($afastamentos_tratados) || is_object($afastamentos_tratados))
			foreach($afastamentos_tratados as $afastamento){
				$dia = date("N", strtotime($afastamento->dia));//dia da semana em formato numérico [1=segunda, 7=domingo]
				$dias = array("", "segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo");
				$this->db->start_cache();
				$this->db->where("horarios.id_grupo", $afastamento->id_horario);
				$this->db->where("horarios.dia", $dias[$dia]);
				$horarios = $this->db->get("horarios")->first_row();
				$this->db->stop_cache();
				$this->db->flush_cache();

				$data_inicio = new DateTime($afastamento->dia." ".$afastamento->hora_inicio);
				$data_fim = new DateTime($afastamento->dia." ".$afastamento->hora_fim);

				// echo "<h1>".$data_inicio->format("His")."</h1>";
				// if($data_inicio->format("His") == "000000" && $data_inicio > $data_fim){
				// 	$data_inicio->sub(new DateInterval("P1D"));
				// }

				@$horario->padrao_entrada_1 = new DateTime($afastamento->dia." ".$horarios->padrao_entrada_1);
				@$horario->padrao_entrada_2 = new DateTime($afastamento->dia." ".$horarios->padrao_entrada_2);
				@$horario->padrao_entrada_3 = new DateTime($afastamento->dia." ".$horarios->padrao_entrada_3);
				@$horario->padrao_entrada_4 = new DateTime($afastamento->dia." ".$horarios->padrao_entrada_4);
				@$horario->padrao_entrada_5 = new DateTime($afastamento->dia." ".$horarios->padrao_entrada_5);

				@$horario->padrao_saida_1 = new DateTime($afastamento->dia." ".$horarios->padrao_saida_1);
				@$horario->padrao_saida_2 = new DateTime($afastamento->dia." ".$horarios->padrao_saida_2);
				@$horario->padrao_saida_3 = new DateTime($afastamento->dia." ".$horarios->padrao_saida_3);
				@$horario->padrao_saida_4 = new DateTime($afastamento->dia." ".$horarios->padrao_saida_4);
				@$horario->padrao_saida_5 = new DateTime($afastamento->dia." ".$horarios->padrao_saida_5);

				$afastamento->horario_entrada_1 = "";
				$afastamento->horario_saida_1 = "";
				$afastamento->horario_entrada_2 = "";
				$afastamento->horario_saida_2 = "";
				$afastamento->horario_entrada_3 = "";
				$afastamento->horario_saida_3 = "";
				$afastamento->horario_entrada_4 = "";
				$afastamento->horario_saida_4 = "";
				$afastamento->horario_entrada_5 = "";
				$afastamento->horario_saida_5 = "";
				$afastamento->p_inicio = "";
				$afastamento->p_fim = "";


				$p_inicio = "-";
				$p_fim = "-";
				if($horarios->padrao_entrada_5 != "" && $horarios->padrao_entrada_5 != NULL){
					// echo " |".$data_inicio->format("d/m/Y H:i:s")." - ".$horario->padrao_entrada_1->format("d/m/Y H:i:s")."| ";
					if($data_inicio <= $horario->padrao_entrada_1){
						$p_inicio = "-1";
					}else if($data_inicio >= $horario->padrao_entrada_1 && $data_inicio <= $horario->padrao_saida_1){
						$p_inicio = "1";
					}else if($data_inicio >= $horario->padrao_saida_1 && $data_inicio <= $horario->padrao_entrada_2){
						$p_inicio = "1-2";
					}else if($data_inicio >= $horario->padrao_entrada_2 && $data_inicio <= $horario->padrao_saida_2){
						$p_inicio = "2";
					}else if($data_inicio >= $horario->padrao_saida_2 && $data_inicio <= $horario->padrao_entrada_3){
						$p_inicio = "2-3";
					}else if($data_inicio >= $horario->padrao_entrada_3 && $data_inicio <= $horario->padrao_saida_3){
						$p_inicio = "3";
					}else if($data_inicio >= $horario->padrao_saida_3 && $data_inicio <= $horario->padrao_entrada_4){
						$p_inicio = "3-4";
					}else if($data_inicio >= $horario->padrao_entrada_4 && $data_inicio <= $horario->padrao_saida_4){
						$p_inicio = "4";
					}else if($data_inicio >= $horario->padrao_saida_4 && $data_inicio <= $horario->padrao_entrada_5){
						$p_inicio = "4-5";
					}else if($data_inicio >= $horario->padrao_entrada_5 && $data_inicio <= $horario->padrao_saida_5){
						$p_inicio = "5";
					}else if($data_inicio >= $horario->padrao_saida_5){
						$p_inicio = "5+";
					}

					if($data_fim <= $horario->padrao_entrada_1){
						$p_fim = "-1";
					}else if($data_fim > $horario->padrao_entrada_1 && $data_fim < $horario->padrao_saida_1){
						$p_fim = "1";
					}else if($data_fim >= $horario->padrao_saida_1 && $data_fim <= $horario->padrao_entrada_2){
						$p_fim = "1-2";
					}else if($data_fim > $horario->padrao_entrada_2 && $data_fim < $horario->padrao_saida_2){
						$p_fim = "2";
					}else if($data_fim >= $horario->padrao_saida_2 && $data_fim <= $horario->padrao_entrada_3){
						$p_fim = "2-3";
					}else if($data_fim > $horario->padrao_entrada_3 && $data_fim < $horario->padrao_saida_3){
						$p_fim = "3";
					}else if($data_fim >= $horario->padrao_saida_3 && $data_fim <= $horario->padrao_entrada_4){
						$p_fim = "3-4";
					}else if($data_fim > $horario->padrao_entrada_4 && $data_fim < $horario->padrao_saida_4){
						$p_fim = "4";
					}else if($data_fim >= $horario->padrao_saida_4 && $data_fim <= $horario->padrao_entrada_5){
						$p_fim = "4-5";
					}else if($data_fim > $horario->padrao_entrada_5 && $data_fim < $horario->padrao_saida_5){
						$p_fim = "5";
					}else if($data_fim >= $horario->padrao_saida_5){
						$p_fim = "5+";
					}
				}else if($horarios->padrao_entrada_4 != "" && $horarios->padrao_entrada_4 != NULL){
					// echo " |".$data_inicio->format("d/m/Y H:i:s")." - ".$horario->padrao_entrada_1->format("d/m/Y H:i:s")."| ";
					if($data_inicio <= $horario->padrao_entrada_1){
						$p_inicio = "-1";
					}else if($data_inicio >= $horario->padrao_entrada_1 && $data_inicio <= $horario->padrao_saida_1){
						$p_inicio = "1";
					}else if($data_inicio >= $horario->padrao_saida_1 && $data_inicio <= $horario->padrao_entrada_2){
						$p_inicio = "1-2";
					}else if($data_inicio >= $horario->padrao_entrada_2 && $data_inicio <= $horario->padrao_saida_2){
						$p_inicio = "2";
					}else if($data_inicio >= $horario->padrao_saida_2 && $data_inicio <= $horario->padrao_entrada_3){
						$p_inicio = "2-3";
					}else if($data_inicio >= $horario->padrao_entrada_3 && $data_inicio <= $horario->padrao_saida_3){
						$p_inicio = "3";
					}else if($data_inicio >= $horario->padrao_saida_3 && $data_inicio <= $horario->padrao_entrada_4){
						$p_inicio = "3-4";
					}else if($data_inicio >= $horario->padrao_entrada_4 && $data_inicio <= $horario->padrao_saida_4){
						$p_inicio = "4";
					}else if($data_inicio >= $horario->padrao_saida_4){
						$p_inicio = "4-5";
					}

					if($data_fim <= $horario->padrao_entrada_1){
						$p_fim = "-1";
					}else if($data_fim > $horario->padrao_entrada_1 && $data_fim < $horario->padrao_saida_1){
						$p_fim = "1";
					}else if($data_fim >= $horario->padrao_saida_1 && $data_fim <= $horario->padrao_entrada_2){
						$p_fim = "1-2";
					}else if($data_fim > $horario->padrao_entrada_2 && $data_fim < $horario->padrao_saida_2){
						$p_fim = "2";
					}else if($data_fim >= $horario->padrao_saida_2 && $data_fim <= $horario->padrao_entrada_3){
						$p_fim = "2-3";
					}else if($data_fim > $horario->padrao_entrada_3 && $data_fim < $horario->padrao_saida_3){
						$p_fim = "3";
					}else if($data_fim >= $horario->padrao_saida_3 && $data_fim <= $horario->padrao_entrada_4){
						$p_fim = "3-4";
					}else if($data_fim > $horario->padrao_entrada_4 && $data_fim < $horario->padrao_saida_4){
						$p_fim = "4";
					}else if($data_fim >= $horario->padrao_saida_4){
						$p_fim = "4-5";
					}
				}else if($horarios->padrao_entrada_3 != "" && $horarios->padrao_entrada_3 != NULL){
					// echo " |".$data_inicio->format("d/m/Y H:i:s")." - ".$horario->padrao_entrada_1->format("d/m/Y H:i:s")."| ";
					if($data_inicio <= $horario->padrao_entrada_1){
						$p_inicio = "-1";
					}else if($data_inicio >= $horario->padrao_entrada_1 && $data_inicio <= $horario->padrao_saida_1){
						$p_inicio = "1";
					}else if($data_inicio >= $horario->padrao_saida_1 && $data_inicio <= $horario->padrao_entrada_2){
						$p_inicio = "1-2";
					}else if($data_inicio >= $horario->padrao_entrada_2 && $data_inicio <= $horario->padrao_saida_2){
						$p_inicio = "2";
					}else if($data_inicio >= $horario->padrao_saida_2 && $data_inicio <= $horario->padrao_entrada_3){
						$p_inicio = "2-3";
					}else if($data_inicio >= $horario->padrao_entrada_3 && $data_inicio <= $horario->padrao_saida_3){
						$p_inicio = "3";
					}else if($data_inicio >= $horario->padrao_saida_3){
						$p_inicio = "3-4";
					}

					if($data_fim <= $horario->padrao_entrada_1){
						$p_fim = "-1";
					}else if($data_fim > $horario->padrao_entrada_1 && $data_fim < $horario->padrao_saida_1){
						$p_fim = "1";
					}else if($data_fim >= $horario->padrao_saida_1 && $data_fim <= $horario->padrao_entrada_2){
						$p_fim = "1-2";
					}else if($data_fim > $horario->padrao_entrada_2 && $data_fim < $horario->padrao_saida_2){
						$p_fim = "2";
					}else if($data_fim >= $horario->padrao_saida_2 && $data_fim <= $horario->padrao_entrada_3){
						$p_fim = "2-3";
					}else if($data_fim > $horario->padrao_entrada_3 && $data_fim < $horario->padrao_saida_3){
						$p_fim = "3";
					}else if($data_fim >= $horario->padrao_saida_3){
						$p_fim = "3-4";
					}
				}else if($horarios->padrao_entrada_2 != "" && $horarios->padrao_entrada_2 != NULL){
					// echo " |".$data_inicio->format("d/m/Y H:i:s")." - ".$horario->padrao_entrada_1->format("d/m/Y H:i:s")."| ";
					if($data_inicio <= $horario->padrao_entrada_1){
						$p_inicio = "-1";
					}else if($data_inicio >= $horario->padrao_entrada_1 && $data_inicio <= $horario->padrao_saida_1){
						$p_inicio = "1";
					}else if($data_inicio >= $horario->padrao_saida_1 && $data_inicio <= $horario->padrao_entrada_2){
						$p_inicio = "1-2";
					}else if($data_inicio >= $horario->padrao_entrada_2 && $data_inicio <= $horario->padrao_saida_2){
						$p_inicio = "2";
					}else if($data_inicio >= $horario->padrao_saida_2){
						$p_inicio = "2-3";
					}

					if($data_fim <= $horario->padrao_entrada_1){
						$p_fim = "-1";
					}else if($data_fim > $horario->padrao_entrada_1 && $data_fim < $horario->padrao_saida_1){
						$p_fim = "1";
					}else if($data_fim >= $horario->padrao_saida_1 && $data_fim <= $horario->padrao_entrada_2){
						$p_fim = "1-2";
					}else if($data_fim > $horario->padrao_entrada_2 && $data_fim < $horario->padrao_saida_2){
						$p_fim = "2";
					}else if($data_fim >= $horario->padrao_saida_2){
						$p_fim = "2-3";
					}
				}else if($horarios->padrao_entrada_1 != "" && $horarios->padrao_entrada_1 != NULL){
					// echo " |".$data_inicio->format("d/m/Y H:i:s")." - ".$horario->padrao_entrada_1->format("d/m/Y H:i:s")."| ";
					if($data_inicio <= $horario->padrao_entrada_1){
						$p_inicio = "-1";
					}else if($data_inicio >= $horario->padrao_entrada_1 && $data_inicio <= $horario->padrao_saida_1){
						$p_inicio = "1";
					}else if($data_inicio >= $horario->padrao_saida_1){
						$p_inicio = "1-2";
					}

					if($data_fim <= $horario->padrao_entrada_1){
						$p_fim = "-1";
					}else if($data_fim > $horario->padrao_entrada_1 && $data_fim < $horario->padrao_saida_1){
						$p_fim = "1";
					}else if($data_fim >= $horario->padrao_saida_1){
						$p_fim = "1-2";
					}
				}

				switch ($p_inicio) {
					case '-1':
						switch ($p_fim) {
							case '1':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								break;
							case '1-2':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								break;
							case '2':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								break;
							case '2-3':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								break;
							case '3':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_1 = $afastamento->tipo;
								$afastamento->horario_saida_1 = $afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '1':
						switch ($p_fim) {
							case '1':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								break;
							case '1-2':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								break;
							case '2':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								break;
							case '2-3':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								break;
							case '3':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_1 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_1 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '1-2':
						switch ($p_fim) {
							case '2':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								break;
							case '2-3':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								break;
							case '3':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_2 = $afastamento->tipo;
								$afastamento->horario_saida_2 = $afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '2':
						switch ($p_fim) {
							case '2':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								break;
							case '2-3':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								break;
							case '3':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_2 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_2 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '2-3':
						switch ($p_fim) {
							case '3':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_3 = $afastamento->tipo;
								$afastamento->horario_saida_3 = $afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '3':
						switch ($p_fim) {
							case '3':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '3-4':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								break;
							case '4':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_3 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_3 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '3-4':
						switch ($p_fim) {
							case '4':
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_4 = $afastamento->tipo;
								$afastamento->horario_saida_4 = $afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '4':
						switch ($p_fim) {
							case '4':
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '4-5':
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								break;
							case '5':
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_4 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_4 = "*.".$afastamento->tipo;
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '4-5':
						switch ($p_fim) {
							case '5':
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_5 = $afastamento->tipo;
								$afastamento->horario_saida_5 = $afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					case '5':
						switch ($p_fim) {
							case '5':
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							case '5+':
								$afastamento->horario_entrada_5 = "*.".$afastamento->tipo;
								$afastamento->horario_saida_5 = "*.".$afastamento->tipo;
								break;
							
							default:
								//nada
								break;
						}
						break;
					default:
						# code...
						break;
				}

				$afastamento->dia = date("d-m-Y", strtotime($afastamento->dia));

				$afastamento->p_inicio = $p_inicio;
				$afastamento->p_fim = $p_fim;
			}
			if(!$return){
				echo empty($afastamentos_tratados) ? json_encode(array("")) : json_encode($afastamentos_tratados);
			}else{
				return $afastamentos_tratados;
			}
		}

		function getPreenchidos(){
			$post = $this->input->post();

			$post["data_inicio"] = explode("/", $post["data_inicio"]);
			$post["data_fim"] = explode("/", $post["data_fim"]);
			$post["data_inicio"] = $post["data_inicio"][2]."-".$post["data_inicio"][1]."-".$post["data_inicio"][0];
			$post["data_fim"] = $post["data_fim"][2]."-".$post["data_fim"][1]."-".$post["data_fim"][0];

			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("calculos.id_funcionario", $post["id_funcionario"]);
			$this->db->where("calculos.data BETWEEN '$post[data_inicio]' AND '$post[data_fim]'");

			$this->db->join("calculos_faixas_extras", "calculos.id = calculos_faixas_extras.id_calculo", "left");
			$this->db->join("funcionarios", "funcionarios.id = calculos.id_funcionario");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->join("faixas_extras", "faixas_extras.id = calculos_faixas_extras.id_faixa", "left");

			$this->db->select("calculos.id, SUBSTR(calculos.h_entrada_1, 1, 5) as h_entrada_1, SUBSTR(calculos.h_entrada_2, 1, 5) as h_entrada_2, SUBSTR(calculos.h_entrada_3, 1, 5) as h_entrada_3, SUBSTR(calculos.h_entrada_4, 1, 5) as h_entrada_4, SUBSTR(calculos.h_entrada_5, 1, 5) as h_entrada_5, SUBSTR(calculos.h_saida_1, 1, 5) as h_saida_1, SUBSTR(calculos.h_saida_2, 1, 5) as h_saida_2, SUBSTR(calculos.h_saida_3, 1, 5) as h_saida_3, SUBSTR(calculos.h_saida_4, 1, 5) as h_saida_4, h_saida_5, carga_horaria, horas_trabalhadas, falta, extra, DATE_FORMAT(calculos.data, '%d-%m-%Y') as data, SUBSTR(calculos.horas_validas, 1, 5) as total_dentro");
			$calculos = $this->db->get("calculos")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			foreach($calculos as $calculo){
				$this->db->start_cache();
				$this->db->where("calculos_faixas_extras.id_calculo", $calculo->id);
				$this->db->join("faixas_extras", "faixas_extras.id = calculos_faixas_extras.id_faixa");
				$this->db->select("resultado, faixas_extras.coluna, faixas_extras.id as id_coluna");
				$extras = $this->db->get("calculos_faixas_extras");
				if($extras->num_rows() > 0){
					$extras = $extras->result();
					foreach ($extras as $extra) {
						$coluna = "col_".$extra->id_coluna;
						$calculo->$coluna = $extra->resultado;
					}
				}
				$this->db->stop_cache();
				$this->db->flush_cache();
			}

			echo json_encode($calculos);
		}

		function getFaixasExtras($funcionario){
			$this->db->where("funcionarios.id", $funcionario);
			$this->db->join("grupos_horarios", "grupos_horarios.id = faixas_extras.id_horario");
			$this->db->join("funcionarios", "grupos_horarios.id = funcionarios.id_horario");
			$this->db->select("faixas_extras.de, faixas_extras.ate, faixas_extras.coluna, faixas_extras.id as id_coluna");
			return $this->db->get("faixas_extras");
		}

		function calcularHorasFuncionario($post = null){ //calculadora em si
			if($post == null)
				$post = $this->input->post();
			$data = array();
			$data["erro"] = "0";
			$extra = array();
			$falta = array();
			$post["dia_calculo"] = explode("/", $post["dia_calculo"]);
			$post["dia_calculo"] = $post["dia_calculo"][2]."-".$post["dia_calculo"][1]."-".$post["dia_calculo"][0];


			// PEGANDO VALORES DO BANCO PARA CÁLCULO --------------------------------------------------------

			$dia_semana = date("N", strtotime($post["dia_calculo"]));
			$dias = array("segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo");//nessa ordem
			$dia_semana = $dias[($dia_semana - 1)];//domingo, segunda, etc
			$this->db->start_cache();

			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("funcionarios.id", $post["id_funcionario"]);
			$this->db->where("horarios.dia", $dia_semana);
			$this->db->join("grupos_horarios", "grupos_horarios.id = horarios.id_grupo");
			$this->db->join("funcionarios", "funcionarios.id_horario = grupos_horarios.id");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->select("horarios.*, empresas.id as id_empresa");
			$horario = $this->db->get("horarios")->first_row();

			$this->db->stop_cache();
			$this->db->flush_cache();


			// FIM PEGANDO VALORES DO BANCO PARA CÁLCULO ----------------------------------------------------

			$utilizar_clt       = $horario->regras_tolerancias != "CLT" ? false : true;
			$post['tol_diaria'] = $utilizar_clt ? "00:10:00" : $horario->tolerancia_diaria.":00";
			$compensacao_diaria = $horario->utiliza_compensacao == 0 ? false : true;


			for ($i=1; $i <= 5 ; $i++) {
				$post["entrada$i"] = is_numeric(str_replace(array(":", "/", " "), "", $post["entrada$i"])) ? $post["entrada$i"] : "";
				$post["saida$i"] = is_numeric(str_replace(array(":", "/", " "), "", $post["saida$i"])) ? $post["saida$i"] : "";

				//horários informados
				$data["horario"]["entrada"]["_$i"] = $post["entrada$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["entrada$i"]) : new DateTime("0000-00-00 00:00:00");
				$data["horario"]["saida"]["_$i"] = $post["saida$i"] != "" ? new DateTime($post["dia_calculo"]." ".$post["saida$i"]) : new DateTime("0000-00-00 00:00:00");
			}


			$tolerancia["entrada"]["antes"]["_1"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_1_antes.":00";
			$tolerancia["entrada"]["depois"]["_1"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_1_depois.":00";
			$tolerancia["saida"]["antes"]["_1"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_1_antes.":00";
			$tolerancia["saida"]["depois"]["_1"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_1_depois.":00";

			$tolerancia["entrada"]["antes"]["_2"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_2_antes.":00";
			$tolerancia["entrada"]["depois"]["_2"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_2_depois.":00";
			$tolerancia["saida"]["antes"]["_2"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_2_antes.":00";
			$tolerancia["saida"]["depois"]["_2"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_2_depois.":00";

			$tolerancia["entrada"]["antes"]["_3"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_3_antes.":00";
			$tolerancia["entrada"]["depois"]["_3"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_3_depois.":00";
			$tolerancia["saida"]["antes"]["_3"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_3_antes.":00";
			$tolerancia["saida"]["depois"]["_3"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_3_depois.":00";

			$tolerancia["entrada"]["antes"]["_4"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_4_antes.":00";
			$tolerancia["entrada"]["depois"]["_4"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_4_depois.":00";
			$tolerancia["saida"]["antes"]["_4"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_4_antes.":00";
			$tolerancia["saida"]["depois"]["_4"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_4_depois.":00";

			$tolerancia["entrada"]["antes"]["_5"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_5_antes.":00";
			$tolerancia["entrada"]["depois"]["_5"] = $utilizar_clt ? "00:05:00" : $horario->tol_entrada_5_depois.":00";
			$tolerancia["saida"]["antes"]["_5"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_5_antes.":00";
			$tolerancia["saida"]["depois"]["_5"] = $utilizar_clt ? "00:05:00" : $horario->tol_saida_5_depois.":00";

			$tolerancia_diaria = $horario->tolerancia_diaria.":00";
			$tolerancia_comp_falta = $horario->comp_tol_falta.":00";
			$tolerancia_comp_extra = $horario->comp_tol_extra.":00";

			if($horario->padrao_entrada_1 != "" && $horario->padrao_entrada_1 != NULL && $horario->padrao_entrada_1 != $horario->padrao_saida_1)
				$data["padrao"]["entrada"]["_1"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_1);
			else
				$data["padrao"]["entrada"]["_1"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_2 != "" && $horario->padrao_entrada_2 != NULL && $horario->padrao_entrada_2 != $horario->padrao_saida_2)
				$data["padrao"]["entrada"]["_2"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_2);
			else
				$data["padrao"]["entrada"]["_2"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_3 != "" && $horario->padrao_entrada_3 != NULL && $horario->padrao_entrada_3 != $horario->padrao_saida_3)
				$data["padrao"]["entrada"]["_3"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_3);
			else
				$data["padrao"]["entrada"]["_3"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_4 != "" && $horario->padrao_entrada_4 != NULL && $horario->padrao_entrada_4 != $horario->padrao_saida_4)
				$data["padrao"]["entrada"]["_4"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_4);
			else
				$data["padrao"]["entrada"]["_4"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_entrada_5 != "" && $horario->padrao_entrada_5 != NULL && $horario->padrao_entrada_5 != $horario->padrao_saida_5)
				$data["padrao"]["entrada"]["_5"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_entrada_5);
			else
				$data["padrao"]["entrada"]["_5"] = new DateTime("0000-00-00 00:00:00");

			//SAÍDA
			if($horario->padrao_saida_1 != "" && $horario->padrao_saida_1 != NULL && $horario->padrao_entrada_1 != $horario->padrao_saida_1)
				$data["padrao"]["saida"]["_1"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_1);
			else
				$data["padrao"]["saida"]["_1"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_2 != "" && $horario->padrao_saida_2 != NULL && $horario->padrao_entrada_2 != $horario->padrao_saida_2)
				$data["padrao"]["saida"]["_2"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_2);
			else
				$data["padrao"]["saida"]["_2"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_3 != "" && $horario->padrao_saida_3 != NULL && $horario->padrao_entrada_3 != $horario->padrao_saida_3)
				$data["padrao"]["saida"]["_3"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_3);
			else
				$data["padrao"]["saida"]["_3"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_4 != "" && $horario->padrao_saida_4 != NULL && $horario->padrao_entrada_4 != $horario->padrao_saida_4)
				$data["padrao"]["saida"]["_4"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_4);
			else
				$data["padrao"]["saida"]["_4"] = new DateTime("0000-00-00 00:00:00");

			if($horario->padrao_saida_5 != "" && $horario->padrao_saida_5 != NULL && $horario->padrao_entrada_5 != $horario->padrao_saida_5)
				$data["padrao"]["saida"]["_5"] = new DateTime($post["dia_calculo"]." ".$horario->padrao_saida_5);
			else
				$data["padrao"]["saida"]["_5"] = new DateTime("0000-00-00 00:00:00");


			if($data["padrao"]["saida"]["_1"] < $data["padrao"]["entrada"]["_1"]){
				$data["padrao"]["saida"]["_1"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_2"] < $data["padrao"]["saida"]["_1"]){
				$data["padrao"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_2"] < $data["padrao"]["entrada"]["_2"]){
				$data["padrao"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_3"] < $data["padrao"]["saida"]["_2"]){
				$data["padrao"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_3"] < $data["padrao"]["entrada"]["_3"]){
				$data["padrao"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_4"] < $data["padrao"]["saida"]["_3"]){
				$data["padrao"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_4"] < $data["padrao"]["entrada"]["_4"]){
				$data["padrao"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["entrada"]["_5"] < $data["padrao"]["saida"]["_4"]){
				$data["padrao"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["padrao"]["saida"]["_5"] < $data["padrao"]["entrada"]["_5"]){
				$data["padrao"]["saida"]["_5"]->add(new DateInterval('P1D'));
				//
			}

			if($data["horario"]["saida"]["_1"] < $data["horario"]["entrada"]["_1"]){
				$data["horario"]["saida"]["_1"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["entrada"]["_2"] < $data["horario"]["saida"]["_1"]){
				$data["horario"]["entrada"]["_2"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["saida"]["_2"] < $data["horario"]["entrada"]["_2"]){
				$data["horario"]["saida"]["_2"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["entrada"]["_3"] < $data["horario"]["saida"]["_2"]){
				$data["horario"]["entrada"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["saida"]["_3"] < $data["horario"]["entrada"]["_3"]){
				$data["horario"]["saida"]["_3"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["entrada"]["_4"] < $data["horario"]["saida"]["_3"]){
				$data["horario"]["entrada"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["saida"]["_4"] < $data["horario"]["entrada"]["_4"]){
				$data["horario"]["saida"]["_4"]->add(new DateInterval('P1D'));
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["entrada"]["_5"] < $data["horario"]["saida"]["_4"]){
				$data["horario"]["entrada"]["_5"]->add(new DateInterval('P1D'));
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			if($data["horario"]["saida"]["_5"] < $data["horario"]["entrada"]["_5"]){
				$data["horario"]["saida"]["_5"]->add(new DateInterval('P1D'));
			}

			$fechamento = new DateTime($post["dia_calculo"]." ".$horario->fechamento);
			$data["ultimo_horario"] = $fechamento;
			// $meia_noite->add(new DateInterval("P1D"));
			// if($data["padrao"]["saida"]["_5"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_5"] > $meia_noite){
			// 	$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_5"];
			// }else if($data["padrao"]["saida"]["_4"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_4"] > $meia_noite){
			// 	$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_4"];
			// }else if($data["padrao"]["saida"]["_3"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_3"] > $meia_noite){
			// 	$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_3"];
			// }else if($data["padrao"]["saida"]["_2"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_2"] > $meia_noite){
			// 	$data["ultimo_horario"] = clone $data["padrao"]["saida"]["_2"];
			// }else if($data["padrao"]["saida"]["_1"]->format("Y") != "-0001" && $data["padrao"]["saida"]["_1"] > $meia_noite){
			// 	$data["ultimo_horario"] = clone $data["padrao"]["horario"]["_1"];
			// }else{
			// 	$data["ultimo_horario"] = $meia_noite;
			// }

			// --------------

			for ($i=1; $i <= 5 ; $i++) {
				// Comparações para horários informados
				$anterior["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$anterior["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
				$posterior["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$posterior["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

				$anterior2["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$anterior2["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
				$posterior2["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$posterior2["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

				$anterior3["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$anterior3["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
				$posterior3["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$posterior3["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

				$anterior4["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$anterior4["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
				$posterior4["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$posterior4["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

				$anterior5["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$anterior5["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];
				$posterior5["entrada"]["_$i"] = clone $data["padrao"]["entrada"]["_$i"];
				$posterior5["saida"]["_$i"] = clone $data["padrao"]["saida"]["_$i"];

				// Adicionando e subtraindo dias
				$anterior["entrada"]["_$i"]->sub(new DateInterval("P1D"));
				$anterior["saida"]["_$i"]->sub(new DateInterval("P1D"));
				$posterior["entrada"]["_$i"]->add(new DateInterval("P1D"));
				$posterior["saida"]["_$i"]->add(new DateInterval("P1D"));

				$anterior2["entrada"]["_$i"]->sub(new DateInterval("P2D"));
				$anterior2["saida"]["_$i"]->sub(new DateInterval("P2D"));
				$posterior2["entrada"]["_$i"]->add(new DateInterval("P2D"));
				$posterior2["saida"]["_$i"]->add(new DateInterval("P2D"));

				$anterior3["entrada"]["_$i"]->sub(new DateInterval("P3D"));
				$anterior3["saida"]["_$i"]->sub(new DateInterval("P3D"));
				$posterior3["entrada"]["_$i"]->add(new DateInterval("P3D"));
				$posterior3["saida"]["_$i"]->add(new DateInterval("P3D"));

				$anterior4["entrada"]["_$i"]->sub(new DateInterval("P4D"));
				$anterior4["saida"]["_$i"]->sub(new DateInterval("P4D"));
				$posterior4["entrada"]["_$i"]->add(new DateInterval("P4D"));
				$posterior4["saida"]["_$i"]->add(new DateInterval("P4D"));

				$anterior5["entrada"]["_$i"]->sub(new DateInterval("P5D"));
				$anterior5["saida"]["_$i"]->sub(new DateInterval("P5D"));
				$posterior5["entrada"]["_$i"]->add(new DateInterval("P5D"));
				$posterior5["saida"]["_$i"]->add(new DateInterval("P5D"));
			}

			// -----------------------------------------------------------
			for ($i=1; $i <= 5 ; $i++){
				//---------------DEFININDO LOCALIZAÇÕES DOS HORÁRIOS----------------------
				$localizacao["entrada"]["$i"] = "5-1";
				$localizacao["saida"]["$i"] = "5-1";

				if( ($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_1"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_1"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_1"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_1"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_1"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_1"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_1"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_1"])){
						$localizacao["entrada"]["$i"] = "1";
				}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_2"]) ||
					($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_1"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_2"])){
						$localizacao["entrada"]["$i"] = "1-2";
				}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_2"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_2"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_2"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_2"])) && $data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
						$localizacao["entrada"]["$i"] = "2";
				}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_3"]) ||
					($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_2"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_3"])){
						$localizacao["entrada"]["$i"] = "2-3";
				}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_3"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_3"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_3"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_3"])) && $data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
						$localizacao["entrada"]["$i"] = "3";
				}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_4"]) ||
					($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_3"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_4"])){
						$localizacao["entrada"]["$i"] = "3-4";
				}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_4"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_4"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_4"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_4"])) && $data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
						$localizacao["entrada"]["$i"] = "4";
				}else if(($data["horario"]["entrada"]["_$i"] > $data["padrao"]["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $data["padrao"]["entrada"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] > $anterior["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $anterior['entrada']["_5"]) ||
					($data["horario"]["entrada"]["_$i"] > $posterior["saida"]["_4"] && $data["horario"]["entrada"]["_$i"] < $posterior['entrada']["_5"])){
						$localizacao["entrada"]["$i"] = "4-5";
				}else if((($data["horario"]["entrada"]["_$i"] >= $data["padrao"]["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $data["padrao"]["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior["saida"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior2["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior2["saida"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior2["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior2["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior3["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior3["saida"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior3["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior3["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior4["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior4["saida"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior4["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior4["saida"]["_5"]) || ($data["horario"]["entrada"]["_$i"] >= $anterior5["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $anterior5["saida"]["_5"]) ||
					($data["horario"]["entrada"]["_$i"] >= $posterior5["entrada"]["_5"] && $data["horario"]["entrada"]["_$i"] <= $posterior5["saida"]["_5"])) && $data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
						$localizacao["entrada"]["$i"] = "5";
				}

				// ------------------------------

				if( ($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_1"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_1"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_1"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_1"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_1"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_1"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_1"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_1"])){
						$localizacao["saida"]["$i"] = "1";
				}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_2"]) ||
					($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_1"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_2"])){
						$localizacao["saida"]["$i"] = "1-2";
				}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_2"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_2"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_2"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_2"])) && $data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
						$localizacao["saida"]["$i"] = "2";
				}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_3"]) ||
					($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_2"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_3"])){
						$localizacao["saida"]["$i"] = "2-3";
				}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_3"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_3"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_3"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_3"])) && $data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
						$localizacao["saida"]["$i"] = "3";
				}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_4"]) ||
					($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_3"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_4"])){
						$localizacao["saida"]["$i"] = "3-4";
				}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_4"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_4"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_4"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_4"])) && $data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
						$localizacao["saida"]["$i"] = "4";
				}else if(($data["horario"]["saida"]["_$i"] > $data["padrao"]["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] > $anterior["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $anterior['entrada']["_5"]) ||
					($data["horario"]["saida"]["_$i"] > $posterior["saida"]["_4"] && $data["horario"]["saida"]["_$i"] < $posterior['entrada']["_5"])){
						$localizacao["saida"]["$i"] = "4-5";
				}else if((($data["horario"]["saida"]["_$i"] >= $data["padrao"]["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $data["padrao"]["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior["saida"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior2["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior2["saida"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior2["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior2["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior3["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior3["saida"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior3["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior3["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior4["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior4["saida"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior4["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior4["saida"]["_5"]) || ($data["horario"]["saida"]["_$i"] >= $anterior5["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $anterior5["saida"]["_5"]) ||
					($data["horario"]["saida"]["_$i"] >= $posterior5["entrada"]["_5"] && $data["horario"]["saida"]["_$i"] <= $posterior5["saida"]["_5"])) && $data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
						$localizacao["saida"]["$i"] = "5";
				}
			}

			//---------------CALCULANDO CARGA, HORAS EXTRAS E FALTAS-----------------
			$data["carga_horaria"] = "00:00:00";
			$turno1 = $data["padrao"]["entrada"]["_1"]->diff($data["padrao"]["saida"]["_1"])->format("%H:%I:%S");
			$turno2 = $data["padrao"]["entrada"]["_2"]->diff($data["padrao"]["saida"]["_2"])->format("%H:%I:%S");
			$turno3 = $data["padrao"]["entrada"]["_3"]->diff($data["padrao"]["saida"]["_3"])->format("%H:%I:%S");
			$turno4 = $data["padrao"]["entrada"]["_4"]->diff($data["padrao"]["saida"]["_4"])->format("%H:%I:%S");
			$turno5 = $data["padrao"]["entrada"]["_5"]->diff($data["padrao"]["saida"]["_5"])->format("%H:%I:%S");
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno1);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno2);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno3);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno4);
			$data["carga_horaria"] = $this->somarHoras($data["carga_horaria"], $turno5);

			$data["total_trabalhado"] = "00:00:00";
			$per1 = $data["horario"]["entrada"]["_1"]->diff($data["horario"]["saida"]["_1"])->format("%H:%I:%S");
			$per2 = $data["horario"]["entrada"]["_2"]->diff($data["horario"]["saida"]["_2"])->format("%H:%I:%S");
			$per3 = $data["horario"]["entrada"]["_3"]->diff($data["horario"]["saida"]["_3"])->format("%H:%I:%S");
			$per4 = $data["horario"]["entrada"]["_4"]->diff($data["horario"]["saida"]["_4"])->format("%H:%I:%S");
			$per5 = $data["horario"]["entrada"]["_5"]->diff($data["horario"]["saida"]["_5"])->format("%H:%I:%S");
			$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per1);
			$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per2);
			$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per3);
			$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per4);
			$data["total_trabalhado"] = $this->somarHoras($data["total_trabalhado"], $per5);
			for ($i=1; $i <= 5 ; $i++){
				// echo " <br />----------------| $i |----------------- <br />";
				switch ($localizacao["entrada"]["$i"]) {//horas extras
					case "5-1"://entrada está anterior ao padrão de entrada 1
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$calc = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								break;
							case "1-2":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "2":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								break;
							case "2-3":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "3":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								break;
							case "3-4":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
								if(!$calc->invert)
									$extra[$i][] = $calc->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									$calc = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_1"]);
									if(!$calc->invert)
										$extra[$i][] = $calc->format("%H:%I:%S");
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
											$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
										}else{
											$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
										}
									}
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
						}//fim switch
						break;
					case '1'://entrada está dentro do padrão 1
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($temp)->format("%H:%I:%S");
									}
								}
								break;
							case "1-2":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "2":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								break;
							case "2-3":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "3":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								break;
							case "3-4":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "1-2":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($posterior["entrada"]["_1"])->format("%H:%I:%S");
									}
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								break;
							case "2-3":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "3":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								break;
							case "3-4":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								// $extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "2":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}
								}
								break;
							case "2-3":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "3":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								break;
							case "3-4":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "2-3":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									}
								}
								break;
							case "2-3":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								break;
							case "3-4":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "3":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
								break;
							case "2-3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									}
								}
								break;
							case "3-4":
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "4":
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "3-4":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
								break;
							case "2-3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}
								break;
							case "3-4":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "4":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								break;
							case "4-5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "4":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
								break;
							case "2-3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}
								break;
							case "3-4":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "4":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									}
								}
								break;
							case "4-5":
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
							case "5":
								$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "4-5":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
								break;
							case "2-3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}
								break;
							case "3-4":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "4":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
								}
								break;
							case "4-5":
								if($data["horario"]["saida"]["_$i"]->format("His") < $data["horario"]["entrada"]["_$i"]->format("His")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}else{
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "5":
								$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
								break;
							case "5-1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001"){
									$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}else{
									if($data["horario"]["saida"]["_$i"]->format("dmY") == $data["horario"]["entrada"]["_$i"]){
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}else{
										$extra[$i][] = $data["horario"]["entrada"]["_$i"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
									}
								}
								break;
						}//fim switch
						break;
					case "5":
						switch ($localizacao["saida"]["$i"]) {//testando localizacao da saída
							case "1":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
								}
								break;
							case "1-2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "2":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
								}
								break;
							case "2-3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "3":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
								}
								break;
							case "3-4":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");

								}
								break;
							case "4":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");

								}
								break;
							case "4-5":
								if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
									$temp = clone $data["padrao"]["entrada"]["_1"];
									$temp->add(new DateInterval("P1D"));
									$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
									$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								}
								break;
							case "5":
								if($data["horario"]["saida"]["_$i"]->format("YmdHis") < $data["horario"]["entrada"]["_$i"]->format("YmdHis")){
									if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001"){
										$temp = clone $data["padrao"]["entrada"]["_1"];
										$temp->add(new DateInterval("P1D"));
										$extra[$i][] = $data["padrao"]["saida"]["_5"]->diff($temp)->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S");
										$extra[$i][] = $data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S");
									}
								}
								break;
							case "5-1":
								$temp = clone $data["padrao"]["saida"]["_5"];
								if($data["horario"]["saida"]["_$i"] < $data["padrao"]["entrada"]["_1"])
									$temp->sub(new DateInterval("P1D"));
								$extra[$i][] = $temp->diff($data["horario"]["saida"]["_$i"])->format("%H:%I:%S");
								break;
						}//fim switch
						break;
				}
				if($data["horario"]["entrada"]["_$i"]->format("Y") == "-0001")
					continue;
			} // Fim do For



			$data["extra"] = "00:00:00";
			$data["falta_tol"]["total"]["soma_real"] = "00:00:00";
			$data["falta_tol"]["total"]["soma"] = "00:00:00";
			$data["extra_tol"]["total"]["soma_real"] = "00:00:00";
			$data["extra_tol"]["total"]["soma"] = "00:00:00";
			for ($i=1; $i <= 5; $i++) {
				if(isset($extra[$i])){
					for ($j=0; $j < sizeof($extra[$i]); $j++) {
						$data["extra"] = $this->somarHoras($data["extra"], $extra[$i][$j]);
					}
				}

				if($data["horario"]["entrada"]["_$i"]->format("Y") != "-0001"){
					$data["tolerancia"]["entrada"]["_$i"] = $this->calcularTolerancia($data["horario"]["entrada"]["_$i"]->format("H:i:s"), $data["padrao"]["entrada"]["_$i"]->format("H:i:s"), $tolerancia["entrada"]["antes"]["_$i"], $tolerancia["entrada"]["depois"]["_$i"], "entrada");
					$data["tolerancia"]["saida"]["_$i"] = $this->calcularTolerancia($data["horario"]["saida"]["_$i"]->format("H:i:s"), $data["padrao"]["saida"]["_$i"]->format("H:i:s"), $tolerancia["saida"]["antes"]["_$i"], $tolerancia["saida"]["depois"]["_$i"], "saida");

					//valores reais, sem tolerancias
					$data["falta_tol"]["_$i"]["soma_real"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["falta_real"], $data["tolerancia"]["saida"]["_$i"]["falta_real"]);
					$data["falta_tol"]["total"]["soma_real"] = $this->somarHoras($data["falta_tol"]["_$i"]["soma_real"], $data["falta_tol"]["total"]["soma_real"]);

					$data["extra_tol"]["_$i"]["soma_real"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["extra_real"], $data["tolerancia"]["saida"]["_$i"]["extra_real"]);
					$data["extra_tol"]["total"]["soma_real"] = $this->somarHoras($data["extra_tol"]["_$i"]["soma_real"], $data["extra_tol"]["total"]["soma_real"]);

					//subtraídos das tolerancias
					$data["falta_tol"]["_$i"]["soma"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["falta"], $data["tolerancia"]["saida"]["_$i"]["falta"]);
					$data["falta_tol"]["total"]["soma"] = $this->somarHoras($data["falta_tol"]["_$i"]["soma"], $data["falta_tol"]["total"]["soma"]);

					$data["extra_tol"]["_$i"]["soma"] = $this->somarHoras($data["tolerancia"]["entrada"]["_$i"]["extra"], $data["tolerancia"]["saida"]["_$i"]["extra"]);
					$data["extra_tol"]["total"]["soma"] = $this->somarHoras($data["extra_tol"]["_$i"]["soma"], $data["extra_tol"]["total"]["soma"]);

					// echo $data["extra_tol"]["total"]["soma_real"]." :::";
				}
			}

			$data["falta"] = "00:00:00";

			$data["total_dentro"] = $this->somarHoras($data["extra"], $data["total_trabalhado"], true);
			$data["falta"] = $this->somarHoras($data["total_dentro"], $data["carga_horaria"], true);
			if(strtotime($data["extra_tol"]["total"]["soma_real"]) > strtotime($tolerancia_diaria)){
				// $data['passou_tol_diaria']['extra'] = "1";
			}else{
				// $data['passou_tol_diaria']['extra'] = "0";
				if($data["carga_horaria"] != "00:00" && $data["carga_horaria"] != "00:00:00"){
					$data["extra"] = $this->somarHoras($data["extra"], $data["extra_tol"]["total"]["soma_real"], true);
					$data["extra"] = $this->somarHoras($data["extra"], $data["extra_tol"]["total"]["soma"]);
				}
				// descomentar as duas linhas acima
			}

			if(strtotime($data["falta_tol"]["total"]["soma_real"]) > strtotime($tolerancia_diaria)){
				// $data['passou_tol_diaria']['atraso'] = "1";
			}else{
				// $data['passou_tol_diaria']['atraso'] = "0";
				$data["falta"] = $this->somarHoras($data["falta"], $data["falta_tol"]["total"]["soma_real"], true);
				$data["falta"] = $this->somarHoras($data["falta"], $data["falta_tol"]["total"]["soma"]);
			}
			if($compensacao_diaria){
				if(strtotime($data["total_trabalhado"]) >= strtotime($data["carga_horaria"])){//ta fazendo extra
					$data["compensacao"] = date("H:i:s", strtotime($data["extra"]))." | ".date("H:i:s", strtotime($tolerancia_comp_extra));
					if(strtotime($data["extra"]) <= strtotime($tolerancia_comp_extra)){
						$data["extra"] = "00:00:00";
					}else{
						// $data["extra"] = $this->somarHoras($data["extra"], $data["falta"], true);
						$data["extra"] = $this->somarHoras($data["carga_horaria"], $data["total_trabalhado"], true);
					}
					$data["falta"] = "00:00:00";
				}

				if(strtotime($data["total_trabalhado"]) <= strtotime($data["carga_horaria"])){//falta
					if(strtotime($data["falta"]) <= strtotime($tolerancia_comp_falta)){
						$data["falta"] = "00:00:00";
					}else{
						// $data["falta"] = $this->somarHoras($data["falta"], $data["extra"], true);
						$data["falta"] = $this->somarHoras($data["total_trabalhado"], $data["carga_horaria"], true);
					}
					$data["extra"] = "00:00:00";
				}

				
				if(strtotime($data["total_trabalhado"]) > strtotime($data["carga_horaria"])){
					$data["total_dentro"] = $data["carga_horaria"];
				}else{
					$data["total_dentro"] = $data["total_trabalhado"];
				}
			}
			// $data["localizacao"] = $localizacao;

			// CASO EXISTA UM AFASTAMENTO

			$afastamentos = $this->getAfastamentos(
				array(
					"data_inicio"=>"$post[dia_calculo] 00:00:00",
					"data_fim"=>"$post[dia_calculo] 23:59:59",
					"id_funcionario"=>$post["id_funcionario"]
					),
				true
				);

			/*$afastamento = $this->db->query("
				SELECT *
				FROM `afastamentos`
				WHERE `afastamentos`.`id_funcionario` = '1'
				AND (`afastamentos`.`data_inicio` <= '$post[dia_calculo] 00:00:00' AND `afastamentos`.`data_fim` >= '$post[dia_calculo] 23:59:59')
				OR (`afastamentos`.`data_inicio` >= '$post[dia_calculo] 00:00:00' AND `afastamentos`.`data_fim` <= '$post[dia_calculo] 23:59:59')
				OR (`afastamentos`.`data_inicio` >= '$post[dia_calculo] 00:00:00' AND `afastamentos`.`data_fim` >= '$post[dia_calculo] 23:59:59') LIMIT 1");*/

			$dia_calculo_ = explode("-", $post["dia_calculo"]);
			$dia_calculo = $dia_calculo_[2]."-".$dia_calculo_[1]."-".$dia_calculo_[0];
			$afastamento = null;
			if (is_array($afastamentos) || is_object($afastamentos))
			foreach ($afastamentos as $a) {
				if($dia_calculo == $a->dia){
					$afastamento = $a;
				}
			}

			$carga_horaria_real = $data["carga_horaria"];
			if($afastamento != null){
				$afastamento->data_inicio = new DateTime($afastamento->hora_inicio);
				$afastamento->data_fim = new DateTime($afastamento->hora_fim);
				$afastamento->hora_inicio = clone $afastamento->data_inicio;
				$afastamento->hora_fim = clone $afastamento->data_fim;
				$afastamento->hora_inicio->setDate($dia_calculo_[0], $dia_calculo_[1], $dia_calculo_[2]);
				$afastamento->hora_fim->setDate($dia_calculo_[0], $dia_calculo_[1], $dia_calculo_[2]);
				$primeira = clone $data["padrao"]["entrada"]["_1"];
				$ultima = clone $data["padrao"]["saida"]["_1"];
				if($post["entrada2"] != "" && $post["entrada2"] != null)
					$ultima = clone $data["padrao"]["saida"]["_2"];
				if($post["entrada3"] != "" && $post["entrada3"] != null)
					$ultima = clone $data["padrao"]["saida"]["_3"];
				if($post["entrada4"] != "" && $post["entrada4"] != null)
					$ultima = clone $data["padrao"]["saida"]["_4"];
				if($post["entrada5"] != "" && $post["entrada5"] != null)
					$ultima = clone $data["padrao"]["saida"]["_5"];

				// calculando quantas horas de afastamento estão dentro dos turnos
				$afastamento->total_dentro = "00:00:00";//horas fora do afastamento que não se pode trabalhar
				// for ($i=1; $i <= 5 ; $i++) {
				// 	if($data["padrao"]["entrada"]["_$i"]->format("Y") == "-0001")
				// 		continue;
				// 	if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_$i"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_$i"]){
				// 		$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_$i"]->diff($data["padrao"]["entrada"]["_$i"])->format("%H:%I:%S"), $afastamento->total_dentro);
				// 		// echo "|$i - ".$data["padrao"]["saida"]["_$i"]->diff($data["padrao"]["entrada"]["_$i"])->format("%H:%I:%S")."- 1|";
				// 	}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_$i"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_$i"]){
				// 		$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_$i"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
				// 		// echo "|$i - ".$data["padrao"]["saida"]["_$i"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
				// 	}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_$i"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_$i"]){
				// 		$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_$i"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
				// 		// echo "|$i - ".$data["padrao"]["entrada"]["_$i"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
				// 	}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_$i"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_$i"]){
				// 		$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
				// 		// echo "|$i - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
				// 	}
				// 	// echo $afastamento->total_dentro."\n\n";
				// 	// echo "\n\n";
				// }

				// ---------------------------- CONTANDO AFASTAMENTO DENTRO DOS TURNOS -----------------
				if($data["padrao"]["entrada"]["_1"]->format("Y") != "-0001")
					if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_1"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_1"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_1"])->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|1 - ".$data["padrao"]["saida"]["_1"]->diff($data["padrao"]["entrada"]["_1"])->format("%H:%I:%S")."- 1|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_1"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_1"] && $afastamento->hora_fim < $data["padrao"]["entrada"]["_2"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_1"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|1 - ".$data["padrao"]["saida"]["_1"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
					}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_1"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_1"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_1"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|1 - ".$data["padrao"]["entrada"]["_1"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_1"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_1"]){
						$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|1 - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
					}

				if($data["padrao"]["entrada"]["_2"]->format("Y") != "-0001")
					if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_2"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_2"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|2 - ".$data["padrao"]["saida"]["_2"]->diff($data["padrao"]["entrada"]["_2"])->format("%H:%I:%S")."- 1|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_2"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_2"] && $afastamento->hora_fim < $data["padrao"]["entrada"]["_3"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_2"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|2 - ".$data["padrao"]["saida"]["_2"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
					}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_2"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_2"] && $afastamento->hora_inicio > $data["padrao"]["saida"]["_1"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_2"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|2 - ".$data["padrao"]["entrada"]["_2"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_2"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_2"]){
						$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|2 - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
					}

				if($data["padrao"]["entrada"]["_3"]->format("Y") != "-0001")
					if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_3"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_3"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|3 - ".$data["padrao"]["saida"]["_3"]->diff($data["padrao"]["entrada"]["_3"])->format("%H:%I:%S")."- 1|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_3"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_3"] && $afastamento->hora_fim < $data["padrao"]["entrada"]["_4"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_3"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|3 - ".$data["padrao"]["saida"]["_3"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
					}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_3"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_3"] && $afastamento->hora_inicio > $data["padrao"]["saida"]["_2"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_3"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|3 - ".$data["padrao"]["entrada"]["_3"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_3"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_3"]){
						$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|3 - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
					}

				if($data["padrao"]["entrada"]["_4"]->format("Y") != "-0001")
					if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_4"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_4"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|4 - ".$data["padrao"]["saida"]["_4"]->diff($data["padrao"]["entrada"]["_4"])->format("%H:%I:%S")."- 1|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_4"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_4"] && $afastamento->hora_fim < $data["padrao"]["entrada"]["_5"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_4"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|4 - ".$data["padrao"]["saida"]["_4"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
					}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_4"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_4"] && $afastamento->hora_inicio > $data["padrao"]["saida"]["_3"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_4"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|4 - ".$data["padrao"]["entrada"]["_4"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_4"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_4"]){
						$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|4 - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
					}

				if($data["padrao"]["entrada"]["_5"]->format("Y") != "-0001")
					if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_5"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_5"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_5"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|5 - ".$data["padrao"]["saida"]["_5"]->diff($data["padrao"]["entrada"]["_5"])->format("%H:%I:%S")."- 1|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_5"] && $afastamento->hora_fim >= $data["padrao"]["saida"]["_5"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["saida"]["_5"]->diff($afastamento->hora_inicio)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|5 - ".$data["padrao"]["saida"]["_5"]->diff($afastamento->hora_inicio)->format("%H:%I:%S")."- 2|";
					}else if($afastamento->hora_inicio <= $data["padrao"]["entrada"]["_5"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_5"] && $afastamento->hora_inicio > $data["padrao"]["saida"]["_4"]){
						$afastamento->total_dentro = $this->somarHoras($data["padrao"]["entrada"]["_5"]->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|5 - ".$data["padrao"]["entrada"]["_5"]->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 3|";
					}else if($afastamento->hora_inicio >= $data["padrao"]["entrada"]["_5"] && $afastamento->hora_fim <= $data["padrao"]["saida"]["_5"]){
						$afastamento->total_dentro = $this->somarHoras($afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S"), $afastamento->total_dentro);
						// echo "|5 - ".$afastamento->hora_inicio->diff($afastamento->hora_fim)->format("%H:%I:%S")."- 4|";
					}

				// -------------------------- FIM CONTANDO AFASTAMENTO DENTRO DOS TURNOS ---------------

				$data["carga_horaria"] = $this->somarHoras($afastamento->total_dentro, $data["carga_horaria"], true);
				if(strtotime($afastamento->total_dentro) >= strtotime($tolerancia_diaria)){
					if(strtotime($afastamento->total_dentro) >= strtotime($data["falta"])){
						$data["falta"] = "00:00:00";
					}else{
						$data["falta"] = $this->somarHoras($afastamento->total_dentro, $data["falta"], true);
					}
				}else{
					$data["falta"] = "00:00:00";
				}
			}
			// FIM CASO EXISTA UM AFASTAMENTO


			// CASO SEJA UM FERIADO
			$this->db->start_cache();
			$this->db->where("feriados.data", $post["dia_calculo"]);
			$this->db->where("(feriados.id_empresa IS NULL OR feriados.id_empresa = ".$horario->id_empresa.")");
			$this->db->join("empresas", "empresas.id = feriados.id_empresa", "left");
			$feriado = $this->db->get("feriados");
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($feriado->num_rows() == 1){ //é feriado
				$data["extra"] = $data["total_trabalhado"];
				$data["falta"] = "00:00:00";
				$data["carga_horaria"] = "00:00:00";
				$feriado = true;
			}else{
				$feriado = false;
			}

			// FIM CASO SEJA UM FERIADO

			// UNSET PARA ECONOMIZAR TRÁFEGO
			// unset($data["falta_tol"]);
			// unset($data["extra_tol"]);
			// unset($data["horario"]);
			// unset($data["padrao"]);
			unset($data["tolerancia"]);
			unset($data["ultimo_horario"]);

			// INSERINDO CÁLCULOS NO BANCO
			$this->db->start_cache();
			$this->db->where("data", $post["dia_calculo"]);
			$this->db->where("id_funcionario", $post["id_funcionario"]);
			$this->db->select("calculos.id");
			$calculo = $this->db->get("calculos");
			$this->db->stop_cache();
			$this->db->flush_cache();

			$data["carga_horaria"] = $carga_horaria_real;

			$this->db->start_cache();
			$insert = array(
				"id_funcionario"=>$post["id_funcionario"],
				"data"=>$post["dia_calculo"],
				"carga_horaria"=>$data["carga_horaria"],
				"horas_trabalhadas"=>$data["total_trabalhado"],
				"horas_validas"=>$data["total_dentro"],
				"falta"=>$data["falta"],
				"extra"=>$data["extra"],
				"padrao_entrada_1"=>$data["padrao"]["entrada"]["_1"]->format("H:i:s"),
				"padrao_saida_1"=>$data["padrao"]["saida"]["_1"]->format("H:i:s"),
				"padrao_entrada_2"=>$data["padrao"]["entrada"]["_2"]->format("H:i:s"),
				"padrao_saida_2"=>$data["padrao"]["saida"]["_2"]->format("H:i:s"),
				"padrao_entrada_3"=>$data["padrao"]["entrada"]["_3"]->format("H:i:s"),
				"padrao_saida_3"=>$data["padrao"]["saida"]["_3"]->format("H:i:s"),
				"padrao_entrada_4"=>$data["padrao"]["entrada"]["_4"]->format("H:i:s"),
				"padrao_saida_4"=>$data["padrao"]["saida"]["_4"]->format("H:i:s"),
				"padrao_entrada_5"=>$data["padrao"]["entrada"]["_5"]->format("H:i:s"),
				"padrao_saida_5"=>$data["padrao"]["saida"]["_5"]->format("H:i:s"),
			);

			if($data["horario"]["entrada"]["_1"] != $data["horario"]["saida"]["_1"]){
				$insert["h_entrada_1"] = $data["horario"]["entrada"]["_1"]->format("H:i:s");
				$insert["h_saida_1"] = $data["horario"]["saida"]["_1"]->format("H:i:s");
			}
			if($data["horario"]["entrada"]["_2"] != $data["horario"]["saida"]["_2"]){
				$insert["h_entrada_2"] = $data["horario"]["entrada"]["_2"]->format("H:i:s");
				$insert["h_saida_2"] = $data["horario"]["saida"]["_2"]->format("H:i:s");
			}
			if($data["horario"]["entrada"]["_3"] != $data["horario"]["saida"]["_3"]){
				$insert["h_entrada_3"] = $data["horario"]["entrada"]["_3"]->format("H:i:s");
				$insert["h_saida_3"] = $data["horario"]["saida"]["_3"]->format("H:i:s");
			}
			if($data["horario"]["entrada"]["_4"] != $data["horario"]["saida"]["_4"]){
				$insert["h_entrada_4"] = $data["horario"]["entrada"]["_4"]->format("H:i:s");
				$insert["h_saida_4"] = $data["horario"]["saida"]["_4"]->format("H:i:s");
			}
			if($data["horario"]["entrada"]["_5"] != $data["horario"]["saida"]["_5"]){
				$insert["h_entrada_5"] = $data["horario"]["entrada"]["_5"]->format("H:i:s");
				$insert["h_saida_5"] = $data["horario"]["saida"]["_5"]->format("H:i:s");
			}

			if($data["horario"]["entrada"]["_1"]->format("Y") == "-0001"){
				$insert["h_entrada_1"] = "";
			}else{
				$insert["h_entrada_1"] = $data["horario"]["entrada"]["_1"]->format("H:i:s");
			}

			if($data["horario"]["saida"]["_1"]->format("Y") == "-0001"){
				$insert["h_saida_1"] = "";
			}else{
				$insert["h_saida_1"] = $data["horario"]["saida"]["_1"]->format("H:i:s");
			}

			if($data["horario"]["entrada"]["_2"]->format("Y") == "-0001"){
				$insert["h_entrada_2"] = "";
			}else{
				$insert["h_entrada_2"] = $data["horario"]["entrada"]["_2"]->format("H:i:s");
			}

			if($data["horario"]["saida"]["_2"]->format("Y") == "-0001"){
				$insert["h_saida_2"] = "";
			}else{
				$insert["h_saida_2"] = $data["horario"]["saida"]["_2"]->format("H:i:s");
			}

			if($data["horario"]["entrada"]["_3"]->format("Y") == "-0001"){
				$insert["h_entrada_3"] = "";
			}else{
				$insert["h_entrada_3"] = $data["horario"]["entrada"]["_3"]->format("H:i:s");
			}

			if($data["horario"]["saida"]["_3"]->format("Y") == "-0001"){
				$insert["h_saida_3"] = "";
			}else{
				$insert["h_saida_3"] = $data["horario"]["saida"]["_3"]->format("H:i:s");
			}

			if($data["horario"]["entrada"]["_4"]->format("Y") == "-0001"){
				$insert["h_entrada_4"] = "";
			}else{
				$insert["h_entrada_4"] = $data["horario"]["entrada"]["_4"]->format("H:i:s");
			}

			if($data["horario"]["saida"]["_4"]->format("Y") == "-0001"){
				$insert["h_saida_4"] = "";
			}else{
				$insert["h_saida_4"] = $data["horario"]["saida"]["_4"]->format("H:i:s");
			}

			if($data["horario"]["entrada"]["_5"]->format("Y") == "-0001"){
				$insert["h_entrada_5"] = "";
			}else{
				$insert["h_entrada_5"] = $data["horario"]["entrada"]["_5"]->format("H:i:s");
			}

			if($data["horario"]["saida"]["_5"]->format("Y") == "-0001"){
				$insert["h_saida_5"] = "";
			}else{
				$insert["h_saida_5"] = $data["horario"]["saida"]["_5"]->format("H:i:s");
			}


			if($calculo->num_rows() == 0){
				$this->db->insert("calculos", $insert);
				$calculo = null;
				$calculo = new stdClass();
				$calculo->id = $this->db->insert_id();
			}else{
				$calculo = $calculo->first_row();
				$this->db->where("calculos.id", $calculo->id);
				$this->db->update("calculos", $insert);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
			// FIM INSERINDO CÁLCULOS NO BANCO

			// CASO TENHA FAIXAS

			// INSERINDO FAIXAS EXTRAS
			$this->db->start_cache();
			$this->db->where("grupos_horarios.id", $horario->id_grupo);
			$this->db->join("grupos_horarios", "grupos_horarios.id = faixas_extras.id_horario");
			$this->db->select("faixas_extras.*, faixas_extras.id as id_coluna");
			$extras = $this->db->get("faixas_extras")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();
			// print_r($extras);

			foreach ($extras as $extra) {
				$ext = $data["extra"];
				$de = $extra->de.":00";
				$ate = "00:00:00";
				$data["col_".$extra->id_coluna] = "00:00:00";
				if(empty($extra->ate)){
					$ate = $ext;
				}else{
					$ate = $extra->ate.":00";
				}

				$this->db->start_cache();//testando se dia está setado para colocar horas extras em apenas uma coluna específica
				$this->db->where("dias_colunas.id_horario", $horario->id_grupo);
				$this->db->where("dias_colunas.dia", $dia_semana);
				$this->db->join("faixas_extras", "faixas_extras.id = dias_colunas.id_coluna");
				$this->db->select("dias_colunas.id_coluna, faixas_extras.coluna, faixas_extras.id as id_coluna");
				$dias_colunas = $this->db->get("dias_colunas");
				$this->db->stop_cache();
				$this->db->flush_cache();

				if($feriado){
					$this->db->start_cache();
					$this->db->where("dias_colunas.id_horario", $horario->id_grupo);
					$this->db->where("dias_colunas.dia", "feriados");
					$this->db->join("faixas_extras", "faixas_extras.id = dias_colunas.id_coluna");
					$this->db->select("dias_colunas.id_coluna, faixas_extras.coluna, faixas_extras.id as id_coluna");
					$feriados_colunas = $this->db->get("dias_colunas");
					$this->db->stop_cache();
					$this->db->flush_cache();

					if($feriados_colunas->num_rows() > 0){
						$feriados_colunas = $feriados_colunas->first_row();
						$data["col_".$feriados_colunas->id_coluna] = $ext;
					}else{
						if(strtotime($de) <= strtotime($ext) && strtotime($ate) <= strtotime($ext)){
							$data["col_".$extra->id_coluna] = $this->somarHoras($de, $ate, true);
						}else if(strtotime($de) <= strtotime($ext) && strtotime($ate) >= strtotime($ext)){
							$data["col_".$extra->id_coluna] = $this->somarHoras($de, $ext, true);
						}else{
							$data["col_".$extra->id_coluna] = "00:00:00";
						}
					}
				}else{

					// caso queria que feriados sejam direcionados à uma coluna

					if($dias_colunas->num_rows() > 0){

						$dias_colunas = $dias_colunas->first_row();
						$data["col_".$dias_colunas->id_coluna] = $ext;

					}else{
						if(strtotime($de) <= strtotime($ext) && strtotime($ate) <= strtotime($ext)){
							$data["col_".$extra->id_coluna] = $this->somarHoras($de, $ate, true);
						}else if(strtotime($de) <= strtotime($ext) && strtotime($ate) >= strtotime($ext)){
							$data["col_".$extra->id_coluna] = $this->somarHoras($de, $ext, true);
						}else{
							$data["col_".$extra->id_coluna] = "00:00:00";
						}
					}
				}

				// $ext = new DateTime($post["dia_calculo"]." ".$data["extra"]);
				// $de = new DateTime($post["dia_calculo"]." ".$extra->de);
				// if(empty($extra->ate)){
				// 	$ate = clone $ext;
				// }else{
				// 	$ate = new DateTime($post["dia_calculo"]." ".$extra->ate);
				// }

				// if($de <= $ext && $ate <= $ext){
				// 	$data["col_".$extra->id_coluna] = $de->diff($ate)->format("%H:%I:%S");
				// }else if($de <= $ext && $ate >= $ext){
				// 	$data["col_".$extra->id_coluna] = $de->diff($ext)->format("%H:%I:%S");
				// }else{
				// 	$data["col_".$extra->id_coluna] = "00:00:00";
				// }

				$this->db->start_cache();
				$this->db->where("id_calculo", $calculo->id);
				$this->db->where("id_faixa", $extra->id);
				$this->db->select("id");
				$calculo_extra = $this->db->get("calculos_faixas_extras");
				$this->db->stop_cache();
				$this->db->flush_cache();

				$this->db->start_cache();
				$insert = array(
					"id_calculo"=>$calculo->id,
					"id_faixa"=>$extra->id,
					"resultado"=>$data["col_".$extra->id_coluna]
				);

				if($calculo_extra->num_rows() > 0){
					$calculo_extra = $calculo_extra->first_row();
					$this->db->where("id", $calculo_extra->id);
					$this->db->update("calculos_faixas_extras", $insert);
				}else{
					$this->db->insert("calculos_faixas_extras", $insert);
				}
				$this->db->stop_cache();
				$this->db->flush_cache();
			}
			// FIM INSERINDO FAIXAS EXTRAS

			// FIM CASO TENHA FAIXAS

			unset($data["horario"]);
			unset($data["padrao"]);

			echo json_encode($data);
		}

		function calcular_periodo($return = false, $post = null){
			$post = $post == null ? $this->input->post() : $post;

			$post["data_inicio"] = explode("/", $post["data_inicio"]);
			$post["data_fim"] = explode("/", $post["data_fim"]);
			$post["data_inicio"] = $post["data_inicio"][2]."-".$post["data_inicio"][1]."-".$post["data_inicio"][0];
			$post["data_fim"] = $post["data_fim"][2]."-".$post["data_fim"][1]."-".$post["data_fim"][0];

			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("calculos.id_funcionario", $post["id_funcionario"]);
			$this->db->where("calculos.data BETWEEN '$post[data_inicio]' AND '$post[data_fim]'");
			$this->db->join("funcionarios", "funcionarios.id = calculos.id_funcionario");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->select("calculos.id, calculos.extra, calculos.falta, calculos.carga_horaria, calculos.horas_trabalhadas, calculos.horas_validas");
			$calculos = $this->db->get("calculos")->result();

			$soma["falta"] = "00:00:00";
			$soma["extra"] = "00:00:00";
			$soma["carga_horaria"] = "00:00:00";
			$soma["horas_trabalhadas"] = "00:00:00";
			$soma["horas_validas"] = "00:00:00";

			foreach($calculos as $calculo){
				$soma["falta"] = $this->somarHoras($soma["falta"], $calculo->falta);
				$soma["extra"] = $this->somarHoras($soma["extra"], $calculo->extra);
				$soma["carga_horaria"] = $this->somarHoras($soma["carga_horaria"], $calculo->carga_horaria);
				$soma["horas_trabalhadas"] = $this->somarHoras($soma["horas_trabalhadas"], $calculo->horas_trabalhadas);
				$soma["horas_validas"] = $this->somarHoras($soma["horas_validas"], $calculo->horas_validas);
				$this->db->start_cache();
				$this->db->where("id_calculo", $calculo->id);
				$this->db->join("faixas_extras", "faixas_extras.id = calculos_faixas_extras.id_faixa");
				$this->db->select("faixas_extras.coluna, faixas_extras.id as id_coluna, calculos_faixas_extras.*");
				$extras = $this->db->get("calculos_faixas_extras")->result();
					foreach($extras as $extra){
						if(!isset($soma["col_".$extra->id_coluna])){
							$soma["col_".$extra->id_coluna] = "00:00:00";
						}
						$soma["col_".$extra->id_coluna] = $this->somarHoras($soma["col_".$extra->id_coluna].":00", $extra->resultado, false, true);
					}
				$this->db->stop_cache();
				$this->db->flush_cache();
			}
			$soma["falta"] = explode(":", $soma["falta"]);
			$soma["falta"] = $soma["falta"][0].":".$soma["falta"][1];

			$soma["extra"] = explode(":", $soma["extra"]);
			$soma["extra"] = $soma["extra"][0].":".$soma["extra"][1];

			$soma["carga_horaria"] = explode(":", $soma["carga_horaria"]);
			$soma["carga_horaria"] = $soma["carga_horaria"][0].":".$soma["carga_horaria"][1];

			$soma["horas_trabalhadas"] = explode(":", $soma["horas_trabalhadas"]);
			$soma["horas_trabalhadas"] = $soma["horas_trabalhadas"][0].":".$soma["horas_trabalhadas"][1];

			$soma["horas_validas"] = explode(":", $soma["horas_validas"]);
			$soma["horas_validas"] = $soma["horas_validas"][0].":".$soma["horas_validas"][1];

			$soma["total_dentro"] = $soma["horas_validas"];
			if($return){
				return $soma;
			}else{
				echo json_encode($soma);
			}
		}

		function relatorioEmpresa($empresa, $data_inicio, $data_fim){
			$data_inicio = explode("-", $data_inicio);
			$data_inicio = $data_inicio[2]."/".$data_inicio[1]."/".$data_inicio[0];
			$data_fim = explode("-", $data_fim);
			$data_fim = $data_fim[2]."/".$data_fim[1]."/".$data_fim[0];

			$this->db->start_cache();
			$this->db->where("funcionarios.id_empresa", $empresa);
			$this->db->select("funcionarios.id, funcionarios.id_horario");
			$funcionarios = $this->db->get("funcionarios");
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($funcionarios->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum funcionário encontrado.', 'Ops');");
				redirect("/relatorios");
			}

			$funcionarios = $funcionarios->result();

			$total['carga_horaria'] = "00:00:00";
			$total['horas_trabalhadas'] = "00:00:00";
			$total['horas_validas'] = "00:00:00";
			$total['falta'] = "00:00:00";
			$total['extra'] = "00:00:00";
			foreach($funcionarios as $funcionario){
				$result = $this->calcular_periodo(true, array("data_inicio"=>$data_inicio, "data_fim"=>$data_fim, "id_funcionario"=>$funcionario->id));
				$total['carga_horaria'] = $this->somarHoras($total["carga_horaria"], $result["carga_horaria"].":00");
				$total['horas_trabalhadas'] = $this->somarHoras($total["horas_trabalhadas"], $result["horas_trabalhadas"].":00");
				$total['horas_validas'] = $this->somarHoras($total["horas_validas"], $result["horas_validas"].":00");
				$total['falta'] = $this->somarHoras($total["falta"], $result["falta"].":00");
				$total['extra'] = $this->somarHoras($total["extra"], $result["extra"].":00");

				// faixas do horário do funcionário
				$this->db->start_cache();
				$this->db->where("faixas_extras.id_horario", $funcionario->id_horario);
				$this->db->select("faixas_extras.coluna, faixas_extras.id as id_coluna");
				$faixas = $this->db->get("faixas_extras")->result();
				$this->db->stop_cache();
				$this->db->flush_cache();

				foreach($faixas as $faixa){
					$total["col_".$faixa->id_coluna] = isset($total["col_".$faixa->id_coluna]) ? $total["col_".$faixa->id_coluna] : "00:00:00";
					$result["col_".$faixa->id_coluna] = isset($result["col_".$faixa->id_coluna]) ? $result["col_".$faixa->id_coluna] : "00:00:00";
					$total["col_".$faixa->id_coluna] = $this->somarHoras($total["col_".$faixa->id_coluna], $result["col_".$faixa->id_coluna].":00");
					$total["campos"][] = array("id"=>$faixa->id_coluna, "coluna"=>$faixa->coluna);
				}
				// fim faixas do horário do funcionário
				$total["de"] = $data_inicio;
				$total["ate"] = $data_fim;
				return $total;
			}

		}

		function relatorioDepartamento($departamento, $data_inicio, $data_fim){
			$data_inicio = explode("-", $data_inicio);
			$data_inicio = $data_inicio[2]."/".$data_inicio[1]."/".$data_inicio[0];
			$data_fim = explode("-", $data_fim);
			$data_fim = $data_fim[2]."/".$data_fim[1]."/".$data_fim[0];

			$this->db->start_cache();
			$this->db->where("funcionarios.id_departamento", $departamento);
			$this->db->select("funcionarios.id, funcionarios.id_horario");
			$funcionarios = $this->db->get("funcionarios");
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($funcionarios->num_rows() == 0){
				$this->session->set_flashdata("retorno", "toastr.error('Nenhum funcionário encontrado.', 'Ops');");
				redirect("/relatorios");
			}

			$funcionarios = $funcionarios->result();

			$total['carga_horaria'] = "00:00:00";
			$total['horas_trabalhadas'] = "00:00:00";
			$total['horas_validas'] = "00:00:00";
			$total['falta'] = "00:00:00";
			$total['extra'] = "00:00:00";
			$total["campos"] = null;
			foreach($funcionarios as $funcionario){
				$result = $this->calcular_periodo(true, array("data_inicio"=>$data_inicio, "data_fim"=>$data_fim, "id_funcionario"=>$funcionario->id));
				$total['carga_horaria'] = $this->somarHoras($total["carga_horaria"], $result["carga_horaria"].":00");
				$total['horas_trabalhadas'] = $this->somarHoras($total["horas_trabalhadas"], $result["horas_trabalhadas"].":00");
				$total['horas_validas'] = $this->somarHoras($total["horas_validas"], $result["horas_validas"].":00");
				$total['falta'] = $this->somarHoras($total["falta"], $result["falta"].":00");
				$total['extra'] = $this->somarHoras($total["extra"], $result["extra"].":00");

				// faixas do horário do funcionário
				$this->db->start_cache();
				$this->db->where("faixas_extras.id_horario", $funcionario->id_horario);
				$this->db->select("faixas_extras.coluna, faixas_extras.id as id_coluna");
				$faixas = $this->db->get("faixas_extras")->result();
				$this->db->stop_cache();
				$this->db->flush_cache();

				foreach($faixas as $faixa){
					$total["col_".$faixa->id_coluna] = isset($total["col_".$faixa->id_coluna]) ? $total["col_".$faixa->id_coluna] : "00:00:00";
					$result["col_".$faixa->id_coluna] = isset($result["col_".$faixa->id_coluna]) ? $result["col_".$faixa->id_coluna] : "00:00:00";
					$total["col_".$faixa->id_coluna] = $this->somarHoras($total["col_".$faixa->id_coluna], $result["col_".$faixa->id_coluna].":00");
					$total["campos"][] = array("id"=>$faixa->id_coluna, "coluna"=>$faixa->coluna);
				}
				// fim faixas do horário do funcionário
				$total["de"] = $data_inicio;
				$total["ate"] = $data_fim;
				return $total;
			}

		}

		function relatorioAfastamentosFuncionario($array = null){
			if(empty($array))
				$array = $this->input->post();
			$array["data_inicio"] = $array["de"];
			$array["data_fim"] = $array["ate"];

			$data_inicio = new DateTime($array["data_inicio"]);
			$data_fim = new DateTime($array["data_fim"]);

			$afastamentos = (object)$this->getAfastamentos($array, true);
			
			$this->db->stop_cache();
			$this->db->flush_cache();
			$total = "00:00:00";
			$total_dentro = "00:00:00";

			$data = array();

			for ($i = clone $data_inicio; $i <= $data_fim; $i->add(new DateInterval('P1D'))) { 
				@$data["dias"][$i->format("Y-m-d")]["data"] = $i->format("Y-m-d");
				@$data["dias"][$i->format("Y-m-d")]["dentro"] = "00:00:00";
				@$data["dias"][$i->format("Y-m-d")]["total"] = "00:00:00";
			}


			foreach($afastamentos as $afastamento){
				$afastamento->dia = new DateTime($afastamento->dia);
				if($data_inicio <= $afastamento->dia && $data_fim >= $afastamento->dia){
					// echo $afastamento->dia->format("d/m/Y")."<br />";
					// print_r($afastamento);
					$dia = date("N", strtotime($afastamento->dia->format("Y-m-d")));//N = 1 Segunda, N = 7 Domingo
					$dias = array("", "segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo");
					// echo "<p><b>";
					// echo $afastamento->dia->format("d/m/Y")."($dias[$dia])";
					// echo "</b></p>";

					// echo "<pre>";
					$horario = $this->getHorariosDia($array["id_funcionario"], $dias[$dia])->first_row();

					$dia = $afastamento->dia->format("Y-m-d");

					if(!empty($horario->padrao_entrada_1)){
						$horario->padrao_entrada_1 = new DateTime($dia." ".$horario->padrao_entrada_1.":00");
					}else{
						$horario->padrao_entrada_1 = new DateTime("");
					}
					if(!empty($horario->padrao_entrada_2)){
						$horario->padrao_entrada_2 = new DateTime($dia." ".$horario->padrao_entrada_2.":00");
					}else{
						$horario->padrao_entrada_2 = new DateTime("");
					}
					if(!empty($horario->padrao_entrada_3)){
						$horario->padrao_entrada_3 = new DateTime($dia." ".$horario->padrao_entrada_3.":00");
					}else{
						$horario->padrao_entrada_3 = new DateTime("");
					}
					if(!empty($horario->padrao_entrada_4)){
						$horario->padrao_entrada_4 = new DateTime($dia." ".$horario->padrao_entrada_4.":00");
					}else{
						$horario->padrao_entrada_4 = new DateTime("");
					}
					if(!empty($horario->padrao_entrada_5)){
						$horario->padrao_entrada_5 = new DateTime($dia." ".$horario->padrao_entrada_5.":00");
					}else{
						$horario->padrao_entrada_5 = new DateTime("");
					}

					if(!empty($horario->padrao_saida_1)){
						$horario->padrao_saida_1 = new DateTime($dia." ".$horario->padrao_saida_1.":00");
					}else{
						$horario->padrao_saida_1 = new DateTime("");
					}
					if(!empty($horario->padrao_saida_2)){
						$horario->padrao_saida_2 = new DateTime($dia." ".$horario->padrao_saida_2.":00");
					}else{
						$horario->padrao_saida_2 = new DateTime("");
					}
					if(!empty($horario->padrao_saida_3)){
						$horario->padrao_saida_3 = new DateTime($dia." ".$horario->padrao_saida_3.":00");
					}else{
						$horario->padrao_saida_3 = new DateTime("");
					}
					if(!empty($horario->padrao_saida_4)){
						$horario->padrao_saida_4 = new DateTime($dia." ".$horario->padrao_saida_4.":00");
					}else{
						$horario->padrao_saida_4 = new DateTime("");
					}
					if(!empty($horario->padrao_saida_5)){
						$horario->padrao_saida_5 = new DateTime($dia." ".$horario->padrao_saida_5.":00");
					}else{
						$horario->padrao_saida_5 = new DateTime("");
					}

					$afastamento->inicio = new DateTime($dia." ".$afastamento->hora_inicio);
					$afastamento->fim = new DateTime($dia." ".$afastamento->hora_fim);

					$dentro = "00:00:00";
					switch($afastamento->p_inicio){
						case "-1":
						switch ($afastamento->p_fim) {
							case '1':
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "1-2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							break;
							case "2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "2-3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							break;
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_1->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "1":
						switch ($afastamento->p_fim) {
							case '1':
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "1-2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							break;
							case "2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "2-3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							break;
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_1->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "1-2":
						switch ($afastamento->p_fim) {
							case "2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "2-3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							break;
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_2->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "2":
						switch ($afastamento->p_fim) {
							case "2":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "2-3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							break;
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_2->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "2-3":
						switch ($afastamento->p_fim) {
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_3->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "3":
						switch ($afastamento->p_fim) {
							case "3":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "3-4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							break;
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_3->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "3-4":
						switch ($afastamento->p_fim) {
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_4->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "4":
						switch ($afastamento->p_fim) {
							case "4":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "4-5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							break;
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_4->format("H:i:s"), true));
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "4-5":
						switch ($afastamento->p_fim) {
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($horario->padrao_entrada_5->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						case "5":
						switch ($afastamento->p_fim) {
							case "5":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $afastamento->fim->format("H:i:s"), true));
							break;
							case "5+":
							$dentro = $this->somarHoras($dentro, $this->somarHoras($afastamento->inicio->format("H:i:s"), $horario->padrao_saida_5->format("H:i:s"), true));
							break;
							default:
														//
							break;
						}
						break;
						default:
												//
						break;
					}

					$horas = $afastamento->fim->diff($afastamento->inicio)->format("%H:%I:%S");

					$total_dentro = $this->somarHoras($total_dentro, $dentro);
					$total = $this->somarHoras($total, $horas);

					$data["dias"][$afastamento->dia->format("Y-m-d")]["dentro"] = $dentro;
					$data["dias"][$afastamento->dia->format("Y-m-d")]["total"] = $horas;
				}
			}
				
			return array("total_dentro"=>$total_dentro, "total"=>$total, "data"=>$data);
		}

		function relatorioAfastamentosEmpresa(){
			$post = $this->input->post();
			$post["id_funcionario"] = null;
			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("id_empresa", $post["id_empresa"]);
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->select("funcionarios.nome, funcionarios.id, empresas.id as id_empresa");
			$funcionarios = $this->db->get("funcionarios")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$total = "00:00:00";
			$total_dentro = "00:00:00";

			$data = array();

			foreach($funcionarios as $funcionario){
				$post["id_funcionario"] = $funcionario->id;
				$resultado = (object)$this->relatorioAfastamentosFuncionario($post);

				$total = $this->somarHoras($total, $resultado->total);
				$total_dentro = $this->somarHoras($total_dentro, $resultado->total_dentro);

				foreach ($resultado->data["dias"] as $res) {
					if(!isset($data["dias"][$res["data"]]["dentro"])){
						$data["dias"][$res["data"]]["dentro"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["total"])){
						$data["dias"][$res["data"]]["total"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["data"])){
						$data["dias"][$res["data"]]["data"] = $res["data"];
					}

					$data["dias"][$res["data"]]["dentro"] = $this->somarHoras($data["dias"][$res["data"]]["dentro"], $res["dentro"]);
					$data["dias"][$res["data"]]["total"] = $this->somarHoras($data["dias"][$res["data"]]["total"], $res["total"]);

				}
			}

			$totais = (object)array("total"=>$total, "total_dentro"=>$total_dentro, "data"=>$data);
			return $totais;
		}

		function relatorioAfastamentosDepartamento(){
			$post = $this->input->post();
			$post["id_funcionario"] = null;
			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("id_departamento", $post["id_departamento"]);
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->join("departamentos", "departamentos.id = funcionarios.id_departamento");
			$this->db->select("funcionarios.nome, funcionarios.id, empresas.id as id_empresa");
			$funcionarios = $this->db->get("funcionarios")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$total = "00:00:00";
			$total_dentro = "00:00:00";

			foreach($funcionarios as $funcionario){
				$post["id_funcionario"] = $funcionario->id;
				$resultado = (object)$this->relatorioAfastamentosFuncionario($post);

				$total = $this->somarHoras($total, $resultado->total);
				$total_dentro = $this->somarHoras($total_dentro, $resultado->total_dentro);


				foreach ($resultado->data["dias"] as $res) {
					if(!isset($data["dias"][$res["data"]]["dentro"])){
						$data["dias"][$res["data"]]["dentro"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["total"])){
						$data["dias"][$res["data"]]["total"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["data"])){
						$data["dias"][$res["data"]]["data"] = $res["data"];
					}

					$data["dias"][$res["data"]]["dentro"] = $this->somarHoras($data["dias"][$res["data"]]["dentro"], $res["dentro"]);
					$data["dias"][$res["data"]]["total"] = $this->somarHoras($data["dias"][$res["data"]]["total"], $res["total"]);

				}

			}

			$totais = (object)array("total"=>$total, "total_dentro"=>$total_dentro, "data"=>$data);
			return $totais;
		}

		function relatorioAfastamentosTipo(){
			$post = $this->input->post();
			$post["id_funcionario"] = null;
			$this->db->flush_cache();
			$this->db->start_cache();
			
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("afastamentos.id_tipo", $post["id_tipo"]);
			$this->db->join("afastamentos", "afastamentos.id_funcionario = funcionarios.id");
			$this->db->join("empresas", "empresas.id = funcionarios.id_empresa");
			$this->db->select("funcionarios.nome, funcionarios.id, empresas.id as id_empresa");
			$this->db->group_by("funcionarios.id");
			$funcionarios = $this->db->get("funcionarios")->result();

			$this->db->stop_cache();
			$this->db->flush_cache();

			// echo "<pre>";
			// print_r($funcionarios);
			// echo "</pre>";

			$total = "00:00:00";
			$total_dentro = "00:00:00";

			foreach($funcionarios as $funcionario){
 				$post["id_funcionario"] = $funcionario->id;
				$resultado = (object)$this->relatorioAfastamentosFuncionario($post);

				// echo "<pre>";
				// print_r($resultado);
				// echo "<pre>";
				$total = $this->somarHoras($total, $resultado->total);
				$total_dentro = $this->somarHoras($total_dentro, $resultado->total_dentro);

				foreach ($resultado->data["dias"] as $res) {
					if(!isset($data["dias"][$res["data"]]["dentro"])){
						$data["dias"][$res["data"]]["dentro"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["total"])){
						$data["dias"][$res["data"]]["total"] = "00:00:00";
					}
					if(!isset($data["dias"][$res["data"]]["data"])){
						$data["dias"][$res["data"]]["data"] = $res["data"];
					}

					$data["dias"][$res["data"]]["dentro"] = $this->somarHoras($data["dias"][$res["data"]]["dentro"], $res["dentro"]);
					$data["dias"][$res["data"]]["total"] = $this->somarHoras($data["dias"][$res["data"]]["total"], $res["total"]);
					// echo "(".$data["dias"][$res["data"]]["data"].") ".$data["dias"][$res["data"]]["total"]." + ".$res["total"]." = ".$data["dias"][$res["data"]]["total"]."<br />";
				}

				// foreach ($data as $dat) {
				// 	foreach ($dat["dias"] as $da) {
				// 		if(!isset($data["dias"][$da["data"]]["dentro"])){
				// 			$data["dias"][$da["data"]]["dentro"] = "00:00:00";
				// 		}
				// 		// if(!isset($data[$funcionario->id]["dias"][$res["data"]]["total"])){
				// 		// 	$data[$funcionario->id]["dias"][$res["data"]]["total"] = "00:00:00";
				// 		// }
				// 		// if(!isset($data[$funcionario->id]["dias"][$res["data"]]["data"])){
				// 		// 	$data[$funcionario->id]["dias"][$res["data"]]["data"] = $res["data"];
				// 		// }


				// 		$data["dias"][$da["data"]]["dentro"] = $this->somarHoras($data["dias"][$da["data"]]["dentro"], $da["dentro"]);
				// 	}
				// }

				// echo "<pre>";
				// print_r($data);
				// echo "</pre>";

			}

			$totais = (object)array("total"=>$total, "total_dentro"=>$total_dentro, "data"=>$data);
			return $totais;
		}

		function somarHoras($entrada, $saida, $subtrair = false, $limitar_caracteres = false){
			$hora1 = explode(":",$entrada);
			$hora2 = explode(":",$saida);
			$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
			$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];

			if(!$subtrair)
				$resultado = $acumulador2 + $acumulador1;
			else
				$resultado = $acumulador2 - $acumulador1;

			if($resultado <= 0){
				$hora_ponto = ceil($resultado / 3600);
			}
			else{
				$hora_ponto = floor($resultado / 3600);
			}
			$resultado = $resultado - ($hora_ponto * 3600);
			$min_ponto = abs(floor($resultado / 60));
			$resultado = abs($resultado - ($min_ponto * 60));
			$secs_ponto = $resultado;
			if(!$limitar_caracteres)
				$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
			else
				$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT);

			return str_replace("-", "", $tempo);
		}

		function calcularTolerancia($entrada, $horario_padrao, $tolerancia_abaixo, $tolerancia_acima, $tipo){
			$nome = "";
			$d_entrada = new DateTime($entrada);
			$d_horario_padrao = new DateTime($horario_padrao);
			$d_tolerancia_abaixo = new DateTime($tolerancia_abaixo);
			$entrada = strtotime($entrada);

			$diff = $d_entrada->diff($d_horario_padrao);
			$horario_tolerado['abaixo'] = $d_horario_padrao->diff($d_tolerancia_abaixo); //horario mínimo

			//calculando horário tolerado acima do horário nominal
			$segundos = 0;
			list($h, $m, $s) = explode(":", $tolerancia_acima);
			$segundos += $h * 3600;
			$segundos += $m * 60;
			$segundos += $s;
			$add = strtotime($horario_padrao) + $segundos;

			$horario_tolerado['acima'] = $add;//strtotime do horário máximo acima do nominal
			$horario_tolerado['abaixo'] = strtotime($horario_tolerado['abaixo']->h.":".$horario_tolerado['abaixo']->i.":".$horario_tolerado['abaixo']->s);

			// echo date("d/m/Y H:i:s", $horario_tolerado["acima"])." | ".date("d/m/Y H:i:s", $horario_tolerado["abaixo"]).":::";

			$array = array(
				"status"=>"",
				"extra_real"=>"00:00:00",
				"falta_real"=>"00:00:00",
				"falta"=>"00:00:00",
				"extra"=>"00:00:00",
			);
			if($entrada > strtotime($horario_padrao)){
				if($tipo == "entrada")
					$nome = "falta";
				else
					$nome = "extra";
				$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", strtotime($horario_padrao)), date("H:i:s", $entrada));
			}

			if($entrada < strtotime($horario_padrao)){
				if($tipo == "entrada")
					$nome = "extra";
				else
					$nome = "falta";
				$array[$nome.'_real'] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", strtotime($horario_padrao)));
			}
			if($entrada > $horario_tolerado['acima']){
				$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $horario_tolerado['acima']), date("H:i:s", $entrada));
			}

			if($entrada < $horario_tolerado['abaixo']){
				$array[$nome] = $this->calcularHorasToleradas(date("H:i:s", $entrada), date("H:i:s", $horario_tolerado['abaixo']));
			}
			$array['status'] = $nome;
			return $array;
		}

		function calcularHorasToleradas($entrada, $saida, $outro_dia = false, $somar = false){
			$hora1 = explode(":",$entrada);
			$hora2 = explode(":",$saida);
			$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
			$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
			if($somar){
				$resultado = $acumulador2 + $acumulador1;
			}else{
				$resultado = $acumulador2 - $acumulador1;
			}
			$hora_ponto = floor($resultado / 3600);
			$resultado = $resultado - ($hora_ponto * 3600);
			$min_ponto = floor($resultado / 60);
			$resultado = $resultado - ($min_ponto * 60);
			$secs_ponto = $resultado;
			$tempo = str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
			return $tempo;
		}

		function calcularPorcentagemHoras($entrada, $porcentagem){
			// $entrada = "01:30:00";
		    $hora1 = explode(":",$entrada);
		    $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
		    $acumulador1 = $acumulador1 * ($porcentagem/100);
		    $resultado = $acumulador1;
		    $hora_ponto = floor($resultado / 3600);
		    $resultado = $resultado - ($hora_ponto * 3600);
			$min_ponto = abs(floor($resultado / 60));
			$resultado = abs($resultado - ($min_ponto * 60));
			$secs_ponto = $resultado;
			echo str_pad($hora_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($min_ponto, 2, '0', STR_PAD_LEFT).":".str_pad($secs_ponto, 2, '0', STR_PAD_LEFT);
		}
	}
