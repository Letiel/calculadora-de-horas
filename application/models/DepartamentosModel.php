<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class DepartamentosModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function cadastrar(){
			$post = $this->input->post();
			if($this->db->insert("departamentos", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Departamento Cadastrado com Sucesso');");
				redirect("/departamentos");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function getDepartamentos($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("contas.id", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->join("contas", "empresas.id_conta = contas.id");
			$this->db->select("empresas.cnpj, empresas.nome_fantasia as nome_empresa, departamentos.*");
			return $this->db->get("departamentos");
		}

		function getDepartamento($id){
			$this->db->where("departamentos.id", $id);
			$this->db->where("contas.id", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->join("contas", "empresas.id_conta = contas.id");
			$this->db->select("empresas.cnpj, empresas.nome_fantasia as nome_empresa, departamentos.*");
			return $this->db->get("departamentos");
		}

		function editar($id){
			$post = $this->input->post();
			$this->db->where("departamentos.id", $id);
			if($this->db->update("departamentos", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Departamento Alterado com Sucesso');");
				redirect("/departamentos");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("departamentos.id", $post["id"]);
			if($this->db->delete("departamentos")){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Departamento Removido com Sucesso');");
				// redirect("/departamentos");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não Foi Possível Realizar a Remoção.');");
				// redirect("/departamentos");
			}
		}
	}