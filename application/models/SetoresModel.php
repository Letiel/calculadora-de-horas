<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class SetoresModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function cadastrar(){
			$post = $this->input->post();
			if($this->db->insert("setores", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Setor Cadastrado com Sucesso');");
				redirect("/setores");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function getSetores($inicio = null, $maximo = null){
			$this->db->limit($maximo, $inicio);
			$this->db->where("contas.id", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("departamentos", "departamentos.id = setores.id_departamento");
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->join("contas", "empresas.id_conta = contas.id");
			$this->db->select("empresas.nome_fantasia as nome_empresa, departamentos.nome as nome_departamento, setores.*");
			return $this->db->get("setores");
		}

		function getSetor($id){
			$this->db->where("setores.id", $id);
			$this->db->where("contas.id", $this->session->userdata("id_conta"));
			$this->db->join("departamentos", "departamentos.id = setores.id_departamento");
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->join("contas", "empresas.id_conta = contas.id");
			$this->db->select("empresas.nome_fantasia as nome_empresa, departamentos.nome as nome_departamento, setores.*");
			return $this->db->get("setores");
		}

		function editar($id){
			$post = $this->input->post();
			$this->db->where("setores.id", $id);
			if($this->db->update("setores", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Setor Alterado com Sucesso');");
				redirect("/setores");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("setores.id", $post["id"]);
			if($this->db->delete("setores")){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Setor Removido com Sucesso');");
				// redirect("/departamentos");
			}else{
				$this->session->set_flashdata("retorno", "toastr.error('Erro!', 'Não Foi Possível Realizar a Remoção.');");
				// redirect("/departamentos");
			}
		}

		function select2_departamentos(){
			$get = $this->input->get();
			$this->db->like("departamentos.nome", $get["q"]);
			$this->db->or_like("empresas.nome_fantasia", $get["q"]);
			$this->db->or_like("empresas.razao_social", $get["q"]);
			$this->db->join("empresas", "empresas.id = departamentos.id_empresa");
			$this->db->select("departamentos.id, CONCAT(departamentos.nome, ' - ', empresas.nome_fantasia) as text");
			$departamentos = $this->db->get("departamentos")->result();
			echo json_encode(array("items"=>$departamentos));
		}
	}