<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class HorasExtrasModel extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function getHorasExtras($id){
			$this->db->where("grupos_horarios.id", $id);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->where("usuario_empresa.id_usuario", $this->session->userdata("id"));
			$this->db->join("grupos_horarios", "faixas_extras.id_horario = grupos_horarios.id");
			// $this->db->join("horarios", "horarios.id_grupo = grupos_horarios.id");
			$this->db->join("empresas", "empresas.id = grupos_horarios.id_empresa");
			$this->db->join("usuario_empresa", "empresas.id = usuario_empresa.id_empresa");
			$this->db->order_by("faixas_extras.id_horario", "ASC");
			$this->db->select("faixas_extras.*, empresas.id as id_empresa, empresas.nome_fantasia, grupos_horarios.nome as nome_horario");
			return $this->db->get("faixas_extras");
		}

		function cadastrarFaixas(){
			$post = $this->input->post();

			for ($i=0; $i < sizeof($post["de"]); $i++) { 
				if(empty($post["de"][$i]) || empty($post["coluna"][$i]))
					continue;
				$ob[$i] = new stdClass();

				$ob[$i]->id_horario = $post["id_horario"];

				$ob[$i]->de = $post["de"][$i];

				$ob[$i]->ate = $post["ate"][$i];

				$ob[$i]->coluna = $post["coluna"][$i];
			}

			if($this->db->insert_batch("faixas_extras", $ob)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Cadastrado com Sucesso');");
				redirect("/horarios/editar/".$post["id_horario"]);
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar o cadastro.');");
			}
		}

		function edit(){
			$post = $this->input->post();
			$this->db->where("faixas_extras.id", $post["id"]);
			$this->db->where("empresas.id_conta", $this->session->userdata("id_conta"));
			$this->db->join("grupos_horarios", "grupos_horarios.id = faixas_extras.id_horario");
			$this->db->join("empresas", "grupos_horarios.id_empresa = empresas.id");
			$this->db->select("faixas_extras.*, empresas.id as id_empresa, empresas.nome_fantasia, grupos_horarios.nome as nome_horario");
			echo json_encode($this->db->get("faixas_extras")->first_row());
		}

		function editar(){
			$post = $this->input->post();
			$this->db->where("id", $post["id"]);
			unset($post["id"]);
			if($this->db->update("faixas_extras", $post)){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Alterado com Sucesso');");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível realizar a alteração.');");
			}
		}

		function excluir(){
			$post = $this->input->post();
			$this->db->where("faixas_extras.id", $post["id"]);
			$this->db->where("faixas_extras.id_horario IN (SELECT grupos_horarios.id FROM grupos_horarios JOIN empresas ON (empresas.id = grupos_horarios.id_empresa) WHERE empresas.id_conta = ".$this->session->userdata('id_conta').")");
			if($this->db->delete("faixas_extras")){
				$this->session->set_flashdata("retorno", "toastr.success('Sucesso!', 'Removido com Sucesso');");
			}else{
				$this->session->set_flashdata("retorno", "toastr.danger('Erro!', 'Não foi possível remover.');");
			}
		}

		function existe($id_horario){ //teste se horário existe para realizar cadastro posterior
			$this->db->where("grupos_horarios.id", $id_horario);
			$horario = $this->db->get("grupos_horarios");
			if($horario->num_rows() == 1)
				return true;
			return false;
		}
	}