<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Letiel">

<link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

<!-- CSS - REQUIRED - START -->
<!-- Batch Icons -->
<link rel="stylesheet" href="/assets/fonts/batch-icons/css/batch-icons.css">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="/assets/css/bootstrap/mdb.min.css">
<!-- Custom Scrollbar -->
<link rel="stylesheet" href="/assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css">
<!-- Hamburger Menu -->
<link rel="stylesheet" href="/assets/css/hamburgers/hamburgers.css">

<!-- CSS - REQUIRED - END -->

<!-- CSS - OPTIONAL - START -->
<!-- Font Awesome -->
<link rel="stylesheet" href="/assets/fonts/font-awesome/css/font-awesome.min.css">
<!-- JVMaps -->
<link rel="stylesheet" href="/assets/plugins/jvmaps/jqvmap.min.css">
<!-- CSS - OPTIONAL - END -->

<!-- QuillPro Styles -->
<link rel="stylesheet" href="/assets/css/quillpro/quillpro.css">

<link rel="stylesheet" href="/assets/plugins/toastr/toastr.min.css">

<link rel="stylesheet" href="/assets/plugins/datatables/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="/assets/plugins/datatables/css/responsive.bootstrap4.min.css">
<style type="text/css">
	.card-md{
		height: auto !important;
	}
</style>