<nav class="sidebar-horizontal navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="navbar-collapse" id="navbar-header-menu-outer">
		<ul class="navbar-nav navbar-header-menu mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="/">
					<i class="batch-icon batch-icon-browser-alt"></i>
					Início
				</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" id="navbar-dropdown-dashboard-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
					<i class="batch-icon batch-icon-crate"></i>
					Relatórios
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-dashboard-link">
					<li><a class="dropdown-item" href="/relatorios/funcionarios">Funcionários</a></li>
					<li><a class="dropdown-item" href="/relatorios/empresas">Empresas</a></li>
					<!-- <li><a class="dropdown-item" href="/relatorios">Horários</a></li> -->
					<li><a class="dropdown-item" href="/relatorios/departamentos">Departamentos</a></li>
					<li><a class="dropdown-item" href="/relatorios/afastamentos">Afastamentos</a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" id="navbar-dropdown-dashboard-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
					<i class="batch-icon batch-icon-settings-alt"></i>
					Configurações
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-dashboard-link">
					<li><a class="dropdown-item" href="/empresas">Empresas</a></li>
					<li><a class="dropdown-item" href="/funcionarios">Funcionários</a></li>
					<li><a class="dropdown-item" href="/afastamentos">Afastamentos</a></li>
					<li><a class="dropdown-item" href="/horarios">Horários</a></li>
					<li><a class="dropdown-item" href="/departamentos">Departamentos</a></li>
					<li><a class="dropdown-item" href="/setores">Setores</a></li>
					<?php if ($this->session->userdata("adm")): ?>
						<li><a class="dropdown-item" href="/operadores">Operadores</a></li>
					<?php endif ?>
					<li><a class="dropdown-item" href="/minha-conta">Minha conta</a></li>
				</ul>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/logout">
					<i class="batch-icon batch-icon-out"></i>
					Sair
				</a>
			</li>
		</ul>
	</div>
</nav>