<!-- SCRIPTS - REQUIRED START -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Bootstrap core JavaScript -->
<!-- JQuery -->
<script type="text/javascript" src="/assets/js/jquery/jquery-3.1.1.min.js"></script>
<!-- Popper.js - Bootstrap tooltips -->
<script type="text/javascript" src="/assets/js/bootstrap/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/assets/js/bootstrap/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/assets/js/bootstrap/mdb.min.js"></script>
<!-- Velocity -->
<script type="text/javascript" src="/assets/plugins/velocity/velocity.min.js"></script>
<script type="text/javascript" src="/assets/plugins/velocity/velocity.ui.min.js"></script>
<!-- Custom Scrollbar -->
<script type="text/javascript" src="/assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- jQuery Visible -->
<script type="text/javascript" src="/assets/plugins/jquery_visible/jquery.visible.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="/assets/js/misc/ie10-viewport-bug-workaround.js"></script>

<!-- SCRIPTS - REQUIRED END -->

<!-- QuillPro Scripts -->
<!-- <script type="text/javascript" src="/assets/js/scripts.js"></script> -->

<script type="text/javascript" src="/assets/plugins/toastr/toastr.min.js"></script>

<script type="text/javascript" src="/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="/assets/plugins/datatables/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="/assets/js/scripts.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	$.fn.datepicker.dates['pt-BR'] = {
	    days: ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"],
	    daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
	    daysMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sá"],
	    months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	    monthsShort: ["Jan", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
	    today: "Hoje",
	    clear: "Limpar",
	    
	    weekStart: 0
	};
	$.fn.datepicker.defaults.language = "pt-BR";
});

	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": true,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "120000",
	  "timeOut": "0",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	<?= $this->session->flashdata("retorno") ?>
</script>